class RankIngresTeacherScreensController < ApplicationController
before_filter :login_required
  filter_access_to :all
  def index
     begin
       @batch =[]
      config_value = Configuration.find_by_config_key('TermwiseExamManagement').config_value
      if config_value == "1"
        if @current_user.admin
           @batch= Batch.active
        elsif @current_user.employee
           unless @current_user.employee_entry.batches.blank?
           @batch = @current_user.employee_entry.batches + @current_user.employee_record.subjects.all(:group => 'batch_id').map{|x|x.batch}
            @batch = @batch.uniq
           else
            @batch_list= @current_user.employee_record.subjects.all(:group => 'batch_id').map{|x|x.batch}
            @batch = @batch_list.uniq
           end
  
        end
      else
        redirect_to :controller => "exam", :action => "create_exam"
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, create_exam action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to:controller => "exam", :action => "create_exam"
    end
  end

  def update_batch
    begin     
      @batch = Batch.find_all_by_course_id(params[:course_name], :conditions => { :is_deleted => false, :is_active => true })
      render(:update) do |page|
        page.replace_html 'update_batch', :partial=>'update_batch'
      end
    rescue Exception => e
      Rails.logger.info "Exception in teacher screen controller, update_batch"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :index
    end
  end
  
  def batch_term
    begin
      @batch = Batch.find(params[:batch_id])
      @batch_terms = @batch.terms
      render(:update) do |page|
        page.replace_html "batch_terms", :partial=>"batch_term"
      end
    rescue Exception => e
      Rails.logger.info "Exception in teacher screen controller, batch_term"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :index
    end
  end

  def term_subjects
    begin
        @batch = Batch.find(params[:batch_id])
        @term_id = params[:term_id]
        @normal_subjects = Subject.find_all_by_batch_id(@batch.id,:conditions=>"no_exams = false AND elective_group_id IS NULL AND is_deleted = false")
        @elective_subjects = []
        elective_subjects = Subject.find_all_by_batch_id(@batch.id,:conditions=>"no_exams = false AND elective_group_id IS NOT NULL AND is_deleted = false")
        elective_subjects.each do |e|
          is_assigned = StudentsSubject.find_all_by_subject_id(e.id)
          unless is_assigned.empty?
            @elective_subjects.push e
          end
        end
        @all_subjects = @normal_subjects+@elective_subjects
    rescue Exception => e
      Rails.logger.info "Exception in teacher screen controller, term_subjects"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :index
    end
  end

  def add_scores_to_teacher_screen
    @batch = Batch.find params[:batch_id]
    @subject_id = params[:subject_id]
    @subject_name = Subject.find(params[:subject_id]).name
    @exam_groups = []
    @term_exam_groups = TermExamGroup.find_all_by_term_id(params[:term_id])
    Rails.logger.info @term_exam_groups.count
    employee = Employee.find_by_user_id(@current_user.id)
    @deadline = RankIngresDeadline.find_by_batch_id_and_subject_id_and_employee_id_and_term_id(params[:batch_id],params[:subject_id],employee.id,params[:term_id])
    dead_line = @deadline.deadline_date unless @deadline.nil?
    if @deadline.nil? or dead_line.to_date >= Date.today
      @term_exam_groups.each do |group|
        unless group.exam_group.nil?
          Rails.logger.info group.exam_group
        @exam_groups << group.exam_group
        end
      end
      Rails.logger.info @exam_groups
      unless @exam_groups.blank?
        @employee_subjects=[]
        @employee_subjects= @current_user.employee_record.subjects.map { |n| n.id} if @current_user.employee?
        #@exam = Exam.find params[:id], :include => :exam_group
        #
        #      ******************hemanth changes *********************************
        #      unless @employee_subjects.include?(params[:subject_id]) or @current_user.admin? or @current_user.privileges.map{|p| p.name}.include?('ExaminationControl') or @current_user.privileges.map{|p| p.name}.include?('EnterResults')
        #        flash[:notice] = "#{t('flash_msg6')}"
        #        redirect_to :controller=>"user", :action=>"dashboard"
        #      end
        #******************hemanth changes *********************************
        exam_subject = Subject.find(params[:subject_id])
        is_elective = exam_subject.elective_group_id
        if is_elective == nil
          rank_teachers = RankIngresTeacherStudent.find_by_batch_id_and_subject_id_and_employee_id(params[:batch_id],params[:subject_id],employee.id)
          unless rank_teachers.nil?
            @students = []
            unless rank_teachers.student_ids.nil?
            rank_teachers.student_ids.each do|ids|
              @students << Student.find_by_id(ids)
            end
            end
            @students = @batch.students.by_first_name
          else
            @students = @batch.students.by_first_name
          end
        else
          assigned_stu = StudentsSubject.find_all_by_subject_id(exam_subject.id)
          assigned_students = []
          assigned_stu.each do |ss|
            elective_disable =  RankStudentSubjectElective.find_by_students_subject_id(ss.id)
            exam = Exam.find_by_exam_group_id_and_subject_id(@exam_groups.last.id,ss.subject_id)
            exam_score = exam.score_for(ss.student_id) unless exam.nil?
             unless exam_score.nil?
            if exam_score.marks.nil?
              if elective_disable.nil?
                assigned_students << ss
              end
            else
              assigned_students << ss
            end
            if elective_disable.nil?
                assigned_students << ss
              end
             end
          end
          @students = []
          assigned_students.each do |s|
            student = @batch.students.find_by_id(s.student_id)
            @students.push [student.first_name, student.id, student] unless student.nil?
          end
          @ordered_students = @students.uniq.sort
          @students=[]
          @ordered_students.each do|s|
            @students.push s[2]
          end
        end
        @config = Configuration.get_config_value('ExamResultType') || 'Marks'

        @grades = @batch.grading_level_list
      else
        flash[:notice] = "#{t('create_exam_groups_for_term')}"
      end
    else
      flash[:notice] = "#{t('employee_exceeds_deadline')}"
    end
  end

  def save_scores
    @error= false
    params[:exam].each_pair do |exam_student, details|
      sse = exam_student.split('-')
      @exam= Exam.find(sse.first)
      student_id = sse.last
      absent_students = params[:absent_students].present? ? params[:absent_students] : []
      @exam_score = ExamScore.find(:first, :conditions => {:exam_id => @exam.id, :student_id => student_id} )
      if @exam_score.nil?
        if absent_students.include?(student_id.to_s)
          ExamScore.create(:exam_id=>@exam.id,:student_id=>student_id,:remarks=>details[:remarks])
        else
          if details[:marks].present? or details[:grading_level_id].present?
            if details[:marks].to_f <= @exam.maximum_marks.to_f
              ExamScore.create do |score|
                score.exam_id          = @exam.id
                score.student_id       = student_id
                score.marks            = details[:marks]
                score.remarks          = details[:remarks]
              end
            else
              @error = true
            end
          end
        end
      else
        save_flag=0
        if absent_students.include?(student_id.to_s)
          details[:marks] = nil
          details[:grading_level_id] = nil
          save_flag = 1
        else
          save_flag = 1 if (details[:marks].present? or details[:grading_level_id].present?)
        end
        if details[:marks].to_f <= @exam.maximum_marks.to_f
          if save_flag == 1
            if @exam_score.update_attributes(details)
            else
              flash[:warn_notice] = "#{t('flash4')}"
              @error = nil
            end
          else
            @exam_score.destroy
          end
        else
          @error = true
        end
      end
    end
    flash[:warn_notice] = "#{t('error_while_adding_scores')}" if @error == true
    flash[:notice] = "#{t('added_scores')}" if @error == false
    redirect_to :action => :term_subjects , :term_id => params[:term_id],:batch_id => params[:batch_id]
  end

  def tutor_reports
    employee = Employee.find_by_user_id(@current_user.id)
    sql =   "SELECT * FROM batch_tutors WHERE employee_id=#{employee.id}"
    batch_ids =  ActiveRecord::Base.connection.select_all(sql)
    @tutors_batchs = []
    batch_ids.each do |val|
      @tutors_batchs << Batch.find(val['batch_id'])
    end
  end

  def terms_update
    batch = Batch.find(params[:batch_id])
    @terms =  batch.terms
    @batch_students = batch.students
    render(:update) do |page|
        page.replace_html "batch_terms", :partial=>"term_update"
        page.replace_html "batch_students", :partial=>"batch_students"
      end
  end
end




