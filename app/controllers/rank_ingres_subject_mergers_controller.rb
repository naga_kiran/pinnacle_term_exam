class RankIngresSubjectMergersController < ApplicationController
before_filter :login_required
  filter_access_to :all
  def index
    @subject_mergers = RankIngresSubjectMerger.all
  end

  def new
    @batches = Batch.all
    if request.post?
      unless params[:subject_id].nil? or params[:subject_id].count <= 1
      subject_merger = RankIngresSubjectMerger.new
      subject_merger.name = params[:subject_merger][:name]
      subject_merger.batch_id = params[:subject_merger][:batch_id]
      subject_merger.subject_id = params[:subject_id]
      subject_merger.save
        flash[:notice] = "#{t('created_sucessfully')}"
      redirect_to :action => "index"
      else
       flash[:notice] = "#{t('select_more_than_two_subjects')}"
      end
    end
  end

  def edit
   @subject_merger = RankIngresSubjectMerger.find params[:id]
   @batches = Batch.all
    subjects = @subject_merger.batch.subjects.active
    elective_subjects = subjects.select {|s| s.elective_group_id != nil && s.is_deleted == false}
    @normal_subjects = subjects - elective_subjects
    remaining_subjects =  RankIngresSubjectMerger.find_all_by_batch_id(@subject_merger.batch_id,:conditions => ["id != ?", @subject_merger.id ])
    unless remaining_subjects.blank?
      remaining_subjects.each do |rs|
        merged_subjects = Subject.find_all_by_id(rs.subject_id)
        @normal_subjects = @normal_subjects - merged_subjects
      end
    end
   if request.post?
     unless  params[:subject_id].nil? or params[:subject_id].count <= 1
     @subject_merger.update_attributes(:name => params[:subject_merger][:name] ,:batch_id => params[:subject_merger][:batch_id] ,:subject_id => params[:subject_id] )
       flash[:notice] = "#{t('updated_sucessfully')}"
        redirect_to :action => "index"
     else
       flash[:notice] = "#{t('select_more_than_two_subjects')}"

     end
    end
    
  end

  def destroy
    subject_merger = RankIngresSubjectMerger.find params[:id]
    subject_merger.destroy
    flash[:notice] = "#{t('deleted_successfully')}"
    redirect_to :action => "index"
  end

  def batch_subjects
    @batch  = Batch.find params[:batch_id]
    subjects = @batch.subjects.active
    elective_subjects = subjects.select {|s| s.elective_group_id != nil && s.is_deleted == false}
    @normal_subjects = subjects - elective_subjects
    unless @batch.rank_ingres_subject_mergers.blank?
     @batch.rank_ingres_subject_mergers.each do |sm|
       merged_subjects = Subject.find_all_by_id(sm.subject_id)
        @normal_subjects = @normal_subjects - merged_subjects
     end
    end
   @normal_subjects = [] if @normal_subjects.count == 1
    render(:update) do |page|
      page.replace_html 'batch_subjects', :partial=>'batch_subjects'
    end
  end
end
