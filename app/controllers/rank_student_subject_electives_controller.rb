class RankStudentSubjectElectivesController < ApplicationController
 before_filter :login_required
  filter_access_to :all
  
  def search_ajax
    if params[:option] == "active" or params[:option]=="sibling"
      if params[:query].length>= 3
        @students = Student.find(:all,
          :conditions => ["ltrim(first_name) LIKE ? OR ltrim(middle_name) LIKE ? OR ltrim(last_name) LIKE ?
                            OR admission_no = ? OR (concat(ltrim(rtrim(first_name)), \" \",ltrim(rtrim(last_name))) LIKE ? )
                              OR (concat(ltrim(rtrim(first_name)), \" \", ltrim(rtrim(middle_name)), \" \",ltrim(rtrim(last_name))) LIKE ? ) ",
            "#{params[:query]}%","#{params[:query]}%","#{params[:query]}%",
            "#{params[:query]}", "#{params[:query]}%", "#{params[:query]}%" ],
          :order => "batch_id asc,first_name asc",:include =>  [{:batch=>:course}]) unless params[:query] == ''
      else
        @students = Student.find(:all,
          :conditions => ["admission_no = ? " , params[:query]],
          :order => "batch_id asc,first_name asc",:include =>  [{:batch=>:course}]) unless params[:query] == ''
      end
      @students.reject!{|r| r.immediate_contact_id.nil?} if @students.present? and params[:option]=="sibling"
      render :layout => false
    else
      if params[:query].length>= 3
        @archived_students = ArchivedStudent.find(:all,
          :conditions => ["ltrim(first_name) LIKE ? OR ltrim(middle_name) LIKE ? OR ltrim(last_name) LIKE ?
                            OR admission_no = ? OR (concat(ltrim(rtrim(first_name)), \" \",ltrim(rtrim(last_name))) LIKE ? )
                              OR (concat(ltrim(rtrim(first_name)), \" \", ltrim(rtrim(middle_name)), \" \",ltrim(rtrim(last_name))) LIKE ? ) ",
            "#{params[:query]}%","#{params[:query]}%","#{params[:query]}%",
            "#{params[:query]}", "#{params[:query]}", "#{params[:query]}" ],
          :order => "batch_id asc,first_name asc",:include =>  [{:batch=>:course}]) unless params[:query] == ''
      else
        @archived_students = ArchivedStudent.find(:all,
          :conditions => ["admission_no = ? " , params[:query]],
          :order => "batch_id asc,first_name asc",:include =>  [{:batch=>:course}]) unless params[:query] == ''
      end
      render :partial => "search_ajax"
    end
  end
 
    def rank_elective_subjects
     @student = Student.find(params[:id])
     @student_subjects = StudentsSubject.find_all_by_student_id(params[:id],:include => [:subject],:conditions => ["subjects.elective_group_id IS NOT NULL"])
    end
    
    def elective_disable
        if params[:mode] == "Disable"
        @student_elective = RankStudentSubjectElective.new(:students_subject_id => params[:students_subject_id],:is_deleted => true ,:term_id => params[:term_id])
        if @student_elective.save
       flash[:notice] = "#{t('elective_disable_sucessfully')}"
        else
       flash[:notice] = "#{t('elective_disable_unsucessfully')}"
       end
       else
         @student_elective = RankStudentSubjectElective.find_by_students_subject_id_and_term_id(params[:students_subject_id], params[:term_id])
        if @student_elective.destroy
        flash[:notice] = "#{t('elective_enable_sucessfully')}"
        else
       flash[:notice] = "#{t('elective_enable_unsucessfully')}"
        end
        end
     redirect_to :action => "rank_elective_subjects", :id =>params[:id]
    end
end
