class TermsController < ApplicationController
  FILE_EXTENSIONS = [".jpg",".jpeg",".png"]#,".gif",".png"]
  FILE_MAXIMUM_SIZE_FOR_FILE=1048576
  filter_access_to :all
  before_filter :initial_queries, :only => [:term_exam_group, :create_exam_group, :new_exam_group]
  before_filter :protect_other_batch_exams, :only => [:show, :term_exam_groups]
  before_filter :authenticate_termwise, :except => [:settings, :upload_signature, :tutor_batch_students, :tutor_signature, :principal_signature, :tutor_comments, :save_comments, :director_signature, :upload_director_signature,:rating_comments,:rating_batch_students,:subject_elective_destroy,:subject_destroy]

  def home
    begin
        if @current_user.admin?
         elsif  @current_user.is_a_batch_tutor?
          flash[:notice] = "#{t('deny_permission')}"
          redirect_to :controller => :report_builders, :action => :report_and_term
         else
          flash[:notice] = "#{t('deny_permission')}"
          redirect_to :controller => :report_builders, :action => :report_and_term
        end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, home action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def update_exam_form
    # data added from exam controller, update_exam_form action
    begin
      @batch = Batch.find(params[:batch])
      @name = params[:exam_option][:name]
      @type = params[:exam_option][:exam_type]
      @cce_exam_category_id = params[:exam_option][:cce_exam_category_id]
      @cce_exam_categories = CceExamCategory.all if @batch.cce_enabled?
      unless @name == ''
        @exam_group = ExamGroup.new
        @normal_subjects = Subject.find_all_by_batch_id(@batch.id,:conditions=>"no_exams = false AND elective_group_id IS NULL AND is_deleted = false")
        @elective_subjects = []
        elective_subjects = Subject.find_all_by_batch_id(@batch.id,:conditions=>"no_exams = false AND elective_group_id IS NOT NULL AND is_deleted = false")
        elective_subjects.each do |e|
          is_assigned = StudentsSubject.find_all_by_subject_id(e.id)
          unless is_assigned.empty?
            @elective_subjects.push e
          end
        end
        @all_subjects = @normal_subjects+@elective_subjects
        @all_subjects.each { |subject| @exam_group.exams.build(:subject_id => subject.id) }
        if @type == 'Marks' or @type == 'MarksAndGrades'
          render(:update) do |page|
            page.replace_html 'exam-form', :partial=>'exam_marks_form'
            page.replace_html 'flash', :text=>''
          end
        else
          render(:update) do |page|
            page.replace_html 'exam-form', :partial=>'exam_grade_form'
            page.replace_html 'flash', :text=>''
          end
        end

      else
        render(:update) do |page|
          page.replace_html 'flash', :text=>"<div class='errorExplanation'><p>#{t('flash_msg9')}</p></div>"
        end
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, update_exam_form action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def create_exam_group
    # data added from exam_groups controller, create action
    begin
      config_value = Configuration.find_by_config_key('TermwiseExamManagement').config_value
      if config_value == "1"
        @exam_group = ExamGroup.new(params[:exam_group])
        @exam_group.batch_id = @batch.id
        @type = @exam_group.exam_type
        @error=false
        unless @type=="Grades"
          params[:exam_group][:exams_attributes].each do |exam|
            if exam[1][:_delete].to_s=="0" and @error==false
              unless exam[1][:maximum_marks].present?
                @exam_group.errors.add_to_base("#{t('maxmarks_cant_be_blank')}")
                @error=true
              end
              unless exam[1][:minimum_marks].present?
                @exam_group.errors.add_to_base("#{t('minmarks_cant_be_blank')}")
                @error=true
              end
            end
          end
        end
        if @error==false and @exam_group.save
          flash[:notice] =  "#{t('flash1')}"
          redirect_to :controller => "terms", :action => "term_exams", :batch_id => @batch.id, :term_id => params[:term_id]
        else
          @cce_exam_categories = CceExamCategory.all if @batch.cce_enabled?
          render 'new'
        end
      else
        redirect_to :controller => "exam_groups", :action => "create"
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, create_exam_group action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def index
    begin
      if @current_user.privileges.include?(Privilege.find_by_name("TermManager")) or @current_user.admin?
        @courses = Batch.active
      else
        flash[:notice] = "#{t('deny_permission')}"
        redirect_to :controller => :exam, :action => :index
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, index action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  # def settings
  #   # data added from configurations controller, settings action
  #   begin
  #     @config = Configuration.get_multiple_configs_as_hash ['InstitutionName', 'InstitutionAddress', 'InstitutionPhoneNo', \
  #         'StudentAttendanceType', 'CurrencyType', 'ExamResultType', 'AdmissionNumberAutoIncrement','EmployeeNumberAutoIncrement', \
  #         'NetworkState','TermwiseExamManagement','Locale','FinancialYearStartDate','FinancialYearEndDate','EnableNewsCommentModeration','DefaultCountry','TimeZone','FirstTimeLoginEnable']
  #     @grading_types = Course::GRADINGTYPES
  #     @enabled_grading_types = Configuration.get_grading_types
  #     @time_zones = TimeZone.all
  #     @school_detail = SchoolDetail.first || SchoolDetail.new
  #     @countries=Country.all
  #     if request.post?
  #       Configuration.set_config_values(params[:configuration])
  #       session[:language] = nil unless session[:language].nil?
  #       @school_detail.logo = params[:school_detail][:school_logo] if params[:school_detail].present?
  #       unless @school_detail.save
  #         @config = Configuration.get_multiple_configs_as_hash ['InstitutionName', 'InstitutionAddress', 'InstitutionPhoneNo', \
  #             'StudentAttendanceType', 'CurrencyType', 'ExamResultType', 'AdmissionNumberAutoIncrement','EmployeeNumberAutoIncrement', \
  #             'NetworkState','TermwiseExamManagement','Locale','FinancialYearStartDate','FinancialYearEndDate','EnableNewsCommentModeration','DefaultCountry','TimeZone','FirstTimeLoginEnable']
  #         return
  #       end
  #       @current_user.clear_menu_cache
  #       Configuration.clear_school_cache(@current_user)
  #       News.new.reload_news_bar
  #       load "#{Rails.root}/vendor/plugins/pinnacle_term_exam/init.rb"
  #       flash[:notice] = "#{t('flash_msg8')}"
  #       redirect_to :action => "settings"  and return

  #     end
  #   rescue Exception => e
  #     Rails.logger.info "Exception in terms controller, settings action"
  #     Rails.logger.info e
  #     flash[:notice] = "Sorry, something went wrong. Please inform administration"
  #     redirect_to :action => :home
  #   end
  # end

  def create_exam
    # data added from exam controller, create_exam action
    begin
      @batch = []
      if (@current_user.admin? or @current_user.employee? or @current_user.privileges.collect(&:name).include?('ExaminationControl'))
        config_value = Configuration.find_by_config_key('TermwiseExamManagement').config_value
        if config_value == "1"
       if @current_user.admin
           @batch= Batch.active
        elsif @current_user.employee
           unless @current_user.employee_entry.batches.blank?
            @batch = @current_user.employee_entry.batches +  @current_user.employee_record.subjects.all(:group => 'batch_id').map{|x|x.batch}
            @batch = @batch.uniq
            else
            @batch_list= @current_user.employee_record.subjects.all(:group => 'batch_id').map{|x|x.batch}
            @batch = @batch_list.uniq
           end

        end
        else
          redirect_to :controller => "exam", :action => "create_exam"
        end
      else
        flash[:notice] = "Not Permitted to Access the Page "
        redirect_to :action => :home
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, create_exam action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
 
  end

  def update_batch
    begin
      @batch = Batch.find_all_by_course_id(params[:course_name], :conditions => { :is_deleted => false, :is_active => true })
      if params[:evaluation_group].present?
        render(:update) do |page|
          page.replace_html 'update_batch', :partial=>'update_evaluation_batch'
        end
      else
        render(:update) do |page|
          page.replace_html 'update_batch', :partial=>'update_batch'
        end
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, update_batch action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def term_exam_group
    begin
      @sms_setting = SmsSetting.new
      @term = Term.find(params[:term_id])
      @exam_groups = @term.exam_groups
      @subject_exams = SubjectExamGroup.find_all_by_term_id(@term.id)
      if @current_user.employee?
        @user_privileges = @current_user.privileges
        @employee_subjects= @current_user.employee_record.subjects.map { |n| n.id}
        if @employee_subjects.empty? and !@user_privileges.map{|p| p.name}.include?('ExaminationControl') and !@user_privileges.map{|p| p.name}.include?('EnterResults')
          flash[:notice] = "#{t('flash_msg4')}"
          redirect_to :controller => 'user', :action => 'dashboard'
        end
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, term_exam_group action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def show
    # data added from exam_groups controller, show action
    begin
      @exam_group = ExamGroup.find(params[:id], :include => :exams)
      if @current_user.employee?
        @user_privileges = @current_user.privileges
        @employee_subjects= @current_user.employee_record.subjects.map { |n| n.id}
        if @employee_subjects.empty? and !@current_user.privileges.map{|p| p.name}.include?("ExaminationControl") and !@current_user.privileges.map{|p| p.name}.include?("EnterResults")
          flash[:notice] = "#{t('flash_msg4')}"
          redirect_to :controller => 'user', :action => 'dashboard'
        end
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, show action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def new_term
    # term validations is checked for start_date and end_date b/w batch date ranges ,validations for overlappings of terms and uniqness of term name and code
    begin
      if @current_user.privileges.include?(Privilege.find_by_name("TermManager")) or @current_user.admin?
        @courses = Batch.active
        if request.post?
          @term = Term.new( :name=> params[:term][:name], :code => params[:term][:code], :start_date => params[:start_date], :end_date => params[:end_date], :batch_id => params[:batch_id], :assumption_date => params[:assumption_date], :ca_term_id => params[:ca_term_id])
          if params[:batch_id] == ""
            @term.errors.add_to_base('Please select the batch and continue')
          end
          unless params[:batch_id]== ""
            @batch = Batch.find(params[:batch_id])
          end
          batch_terms = @batch.terms
          batch_terms.each do |term|
            if term.name == @term.name
              @term.errors.add_to_base('Term name already exist')
            end
            if term.code == @term.code
              @term.errors.add_to_base('Term code already exist')
            end
            if @term.start_date.between?(term.start_date,term.end_date) or @term.end_date.between?(term.start_date,term.end_date)
              @batch_term_exist = true
            end
          end
          unless @batch_term_exist.nil?
            @term.errors.add_to_base('Term range is overlapping with another term in batch')
          end
          unless params[:batch_id]== ""
            if !(@term.start_date.between?(@batch.start_date,@batch.end_date)) or !(@term.end_date.between?(@batch.start_date,@batch.end_date))
              @term.errors.add_to_base("Term range is not in between the batch you selected (#{@batch.start_date.to_date} to #{@batch.end_date.to_date})")
            end
          end
          if @term.start_date > @term.end_date
            @term.errors.add_to_base('Start date is greater than end date')
          end
          if @term.start_date > @batch.end_date
            @term.errors.add_to_base('Start date is greater than batch end date')
          end
          if @term.errors.empty?
            @term.save
            redirect_to :controller => "terms", :action =>"new_term"
            flash[:notice]=t('term_created')
          else
            render :action=>'new_term'
          end
        end
      else
        flash[:notice] = "#{t('deny_permission')}"
        redirect_to :controller => :exam, :action => :index
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, new_term action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def edit
    begin
      if @current_user.privileges.include?(Privilege.find_by_name("TermManager")) or @current_user.admin?
        @term = Term.find(params[:id])
        @courses = Batch.active
        @batch_terms = @term.batch.terms - @term.to_a
      else
        flash[:notice] = "#{t('deny_permission')}"
        redirect_to :controller => :exam, :action => :index
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, edit action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def update
    begin
      @term = Term.find(params[:id])
      @batch = Batch.find(params[:batch_id])
      @batch_terms = @batch.terms
      batch_terms = @batch_terms - @term.to_a
      unless batch_terms.empty?
        batch_terms.each do |term|
          if term.name == params[:term][:name]
            @term.errors.add_to_base('Term name already exist')
          end
          if term.code == params[:term][:code]
            @term.errors.add_to_base('Term code already exist')
          end
          if params[:start_date].to_date.between?(term.start_date.to_date,term.end_date.to_date) or params[:end_date].to_date.between?(term.start_date.to_date,term.end_date.to_date)
            @batch_term_exist = true
          end
        end
      end
      unless @batch_term_exist.nil?
        @term.errors.add_to_base('Term range is overlapping with another term in batch')
      end
      unless params[:batch_id]== ""
        if !(params[:start_date].to_date.between?(@batch.start_date.to_date,@batch.end_date.to_date)) or !(params[:end_date].to_date.between?(@batch.start_date.to_date,@batch.end_date.to_date))
          @term.errors.add_to_base("Term range is not in between the batch you selected (#{@batch.start_date.to_date} to #{@batch.end_date.to_date})")
        end
      end
      if params[:start_date].to_date > params[:end_date].to_date
        @term.errors.add_to_base('Start date is greater than end date')
      end
      if params[:start_date].to_date > @batch.end_date.to_date
        @term.errors.add_to_base('Start date is greater than batch end date')
      end
      if @term.errors.empty?
        @term.update_attributes(:name=> params[:term][:name], :code => params[:term][:code], :batch_id => @batch.id, :start_date => params[:start_date], :end_date => params[:end_date],:assumption_date => params[:assumption_date], :ca_term_id => params[:ca_term_id])
        redirect_to :controller => "terms", :action =>"list_terms"
        flash[:notice]="Term Updated Sucessfully"
      else
        render :action=>'edit'
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, update action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def destroy
    begin
      if @current_user.privileges.include?(Privilege.find_by_name("TermManager")) or @current_user.admin?
        report = Term.find(params[:id])
        report.destroy
        flash[:notice] = "#{t('term_deleted')}"
        redirect_to :action => :list_terms
      else
        flash[:notice] = "#{t('deny_permission')}"
        redirect_to :controller => :exam, :action => :index
      end
    rescue Exception => e
      Rails.logger.info "Exception in report builders controller, destroy action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :index
    end
  end

  def update_term
    begin
      @batch = Batch.find(params[:batch_id])
      @batch_terms = @batch.terms
      @departments = EmployeeDepartment.ordered
      render(:update) do |page|
        page.replace_html "update_terms", :partial=>"update_term"
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, update_term action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def update_employees
    @employees = Employee.find_all_by_employee_department_id(params[:department_id]).sort_by{|e| e.full_name.downcase}
    render :update do |page|
      page.replace_html 'employee-list', :partial => 'employee_list'
    end
  end

  def update_batch_subjects
    begin
      @batch = Batch.find(params[:batch_id])
      @batch_terms = @batch.terms
      @batch_subjects = @batch.subjects
      render(:update) do |page|
        page.replace_html "update_terms", :partial=>"update_subjects"
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, update_term action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def batch_term
    begin
      @batch = Batch.find(params[:batch_id])
      @batch_terms = @batch.terms
      if params[:evaluation_group].present?
        render(:update) do |page|
          page.replace_html "batch_terms", :partial=>"evaluation_batch_term"
        end
      else
        render(:update) do |page|
          page.replace_html "batch_terms", :partial=>"batch_term"
        end
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, batch_term action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def term_evaluation
    begin
      @term = Term.find(params[:term_id])
      if @current_user.employee?
        @user_privileges = @current_user.privileges
        @employee_subjects= @current_user.employee_record.subjects.map { |n| n.id}
        if @employee_subjects.empty? and !@user_privileges.map{|p| p.name}.include?('ExaminationControl') and !@user_privileges.map{|p| p.name}.include?('EnterResults')
          flash[:notice] = "#{t('flash_msg4')}"
          redirect_to :controller => 'user', :action => 'dashboard'
        end
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, term_evaluation_group action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def existing_terms
    begin
      @batch = Batch.find(params[:batch_id])
      @batch_terms = @batch.terms
      render(:update) do |page|
        page.replace_html "batch_terms", :partial=>"existing_terms"
        page.replace_html "existing_terms", :partial=>"ca_terms"
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, batch_term action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def term_exams
    # calculating the available term exams to assign, by differencing all term exams by already assigned term exams
    begin
      @assigned_exam_groups = []
      @batch = Batch.find(params[:batch_id])
      @batch_exam_groups = @batch.exam_groups
      @batch_exam_groups.reject!{|e| e.exam_type=="Grades"}
      @term = Term.find(params[:term_id])
      @batch_exam_groups.each do |exam|
        term_exam_groups = TermExamGroup.find_by_exam_group_id(exam.id)
        term_exam_group = term_exam_groups.to_a - TermExamGroup.find_all_by_term_id(@term.id).to_a
        term_exam_group.each do |term_exam|
          @assigned_exam_groups << ExamGroup.find(term_exam.exam_group_id)
        end
      end
      @term_exam_groups = @batch_exam_groups - @assigned_exam_groups
      if request.post?
        unless params[:exam_grouping].nil?
          unless params[:exam_grouping][:exam_group_ids].nil?
            weightages = params[:weightage]
            total = 0
            weightages.map{|w| total+=w.to_f}
            unless total=="100".to_f
              flash[:notice]="#{t('flash9')}"
              return
            else
              TermExamGroup.delete_all(:term_id=>params[:term_id])
              exam_group_ids = params[:exam_grouping][:exam_group_ids]
              exam_group_ids.each_with_index do |e,i|
                TermExamGroup.create(:exam_group_id=>e,:term_id=>params[:term_id],:weightage=>weightages[i])
              end
            end
          end
        else
          TermExamGroup.delete_all(:term_id=>params[:term_id])
        end
        flash[:notice]="#{t('term_flash1')}"
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, term_exams action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def new_exam_group
    # data added from exam_groups controller, new action
    begin
      config_value = Configuration.find_by_config_key('TermwiseExamManagement').config_value
      if config_value == "1"
        @user_privileges = @current_user.privileges
        @cce_exam_categories = CceExamCategory.all if @batch.cce_enabled?
        if !@current_user.admin? and !@user_privileges.map{|p| p.name}.include?('ExaminationControl') and !@user_privileges.map{|p| p.name}.include?('EnterResults')
          flash[:notice] = "#{t('flash_msg4')}"
          redirect_to :controller => 'user', :action => 'dashboard'
        end
      else
        redirect_to :controller => "exam_group", :action => "new"
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, new_exam_group action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def generated_report4
    # data from exam controller, generated_report4 action
    begin
      if params[:exam_report].nil? or params[:exam_report][:batch_id].empty?
        flash[:notice] = "#{t('select_a_batch_to_continue')}"
        redirect_to :action=>'grouped_exam_report' and return
      end
      if params[:term_id].empty?
        flash[:notice] = "#{t('select_a_term_to_continue')}"
        redirect_to :action=>'grouped_exam_report' and return
      end
      @previous_batch = 0
      #grouped-exam-report-for-batch
      @type = params[:type]
      @batch = Batch.find(params[:exam_report][:batch_id])
      @students=@batch.students.by_first_name
      @student = @students.first  unless @students.empty?
      if @student.blank?
        flash[:notice] = "#{t('flash5')}"
        redirect_to :action=>'grouped_exam_report' and return
      end
      @exam_groups = Term.find(params[:term_id]).exam_groups
      @exam_groups.reject!{|e| e.result_published==false}
      general_subjects = Subject.find_all_by_batch_id(@batch.id, :conditions=>"elective_group_id IS NULL AND is_deleted=false")
      student_electives = StudentsSubject.find_all_by_student_id(@student.id,:conditions=>"batch_id = #{@batch.id}")
      elective_subjects = []
      student_electives.each do |elect|
        elective_subjects.push Subject.find(elect.subject_id)
      end
      @subjects = general_subjects + elective_subjects
      @subjects.reject!{|s| (s.no_exams==true or s.exam_not_created(@exam_groups.collect(&:id)))}
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, generated_report4 action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def combined_grouped_exam_report_pdf
    # data from exam controller, combined_grouped_exam_report_pdf action
    begin
      @batch = Batch.find(params[:batch])
      @students = @batch.students.by_first_name
      @exam_groups = Term.find(params[:term_id]).exam_groups
      @exam_groups.reject!{|e| e.result_published==false}
      PDFKit.configure do |config|
        config.wkhtmltopdf = '/opt/wkhtmltopdf'
        config.default_options = {
          :page_size     => 'Letter',
          :margin_top    => '0.5in',
          :margin_right  => '0.5in',
          :margin_bottom => '0.1in',
          :margin_left   => '0.5in',
          #        :header_center => render_to_string(:template => "#{Rails.root}/vendor/plugins/rt_report_builder/app/views/report_builders/header.html.erb", :layout => false),
          #        :footer_center => render_to_string(:template =>"#{Rails.root}/vendor/plugins/rt_report_builder/app/views/report_builders/footer.html.erb",:layout => false)
        }
      end
      render :layout => false
      # render :pdf => 'combined_grouped_exam_report_pdf'
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, combined_grouped_exam_report_pdf action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end


  def final_report_type
    # data from exam controller, final_report_type action
    begin
      @grouped_exams = TermExamGroup.find_all_by_term_id(params[:term_id])
      render(:update) do |page|
        page.replace_html 'report_type',:partial=>'report_type'
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, final_report_type action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def upload_signature
    begin
      if @current_user.privileges.include?(Privilege.find_by_name("TermManager")) or @current_user.admin?
        if request.post?
          if params[:commit] == "Save"
            if params[:batch_id] == ""
              flash[:notice] = "#{t('select_a_batch_to_continue')}"
              redirect_to :action=> 'tutor_signature' and return
            elsif params[:signature][:signature_attachment]
              @tutor_signature = TermSignature.find(:all,:conditions =>{:batch_id => params[:batch_id], :is_principal => false, :is_director => false,:school_id =>MultiSchool.current_school.id})
              if @tutor_signature.present?
                @tutor_signature.first.update_attributes(:signature_attachment=>params[:signature][:signature_attachment],:school_id =>MultiSchool.current_school.id)
              else
                @term = TermSignature.new(:name => params[:signature][:name],:batch_id => params[:batch_id],:signature_attachment =>params[:signature][:signature_attachment], :is_principal => false, :is_director => false,:school_id =>MultiSchool.current_school.id)
                @term.save
              end
              flash[:notice] = "Tutor signature is saved"
              redirect_to :action=> 'upload_signature' and return
            else
              flash[:notice] = "Select an image to upload"
              redirect_to :action=> 'tutor_signature' and return
            end
          else
            if params[:signature]
              @principal_signature = TermSignature.find(:all,:conditions => {:is_principal => true,:school_id =>MultiSchool.current_school.id})
              if @principal_signature.present?
                @principal_signature.first.update_attributes(:name => params[:name], :signature_attachment => params[:signature][:signature_attachment],:school_id =>MultiSchool.current_school.id)
              else
                @term =  TermSignature.new(:name => params[:name], :signature_attachment => params[:signature][:signature_attachment], :is_principal => true,:school_id =>MultiSchool.current_school.id)
                @term.save
              end
              flash[:notice] = "Principal signature is saved"
              redirect_to :action=> 'upload_signature' and return
            else
              flash[:notice] = "Select an image to upload"
              redirect_to :action=> 'principal_signature' and return
            end
          end
        end
      else
        flash[:notice] = "#{t('deny_permission')}"
        redirect_to :controller => :exam, :action => :index
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, upload_signature action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def tutor_signature
    begin
      if @current_user.privileges.include?(Privilege.find_by_name("TermManager")) or @current_user.admin?
        @term = TermSignature.new
      else
        flash[:notice] = "#{t('deny_permission')}"
        redirect_to :controller => :exam, :action => :index
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, tutor_signature action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def principal_signature
    begin
      if @current_user.privileges.include?(Privilege.find_by_name("TermManager")) or @current_user.admin?
        @term = TermSignature.new
      else
        flash[:notice] = "#{t('deny_permission')}"
        redirect_to :controller => :exam, :action => :index
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, principal_signature action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def director_signature
    begin
      if @current_user.privileges.include?(Privilege.find_by_name("TermManager")) or @current_user.admin?
        @term = TermSignature.new
      else
        flash[:notice] = "#{t('deny_permission')}"
        redirect_to :controller => :exam, :action => :index
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, director_signature action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def upload_director_signature
    begin
      if @current_user.privileges.include?(Privilege.find_by_name("TermManager")) or @current_user.admin?
        if request.post?
          if params[:signature]
            @director_signature = TermSignature.find(:all,:conditions => {:is_director => true,:school_id=>MultiSchool.current_school.id})
            if @director_signature.present?
              @director_signature.first.update_attributes(:signature_attachment => params[:signature][:signature_attachment],:school_id=>MultiSchool.current_school.id)
            else
              @term =  TermSignature.new(:name => params[:name], :signature_attachment => params[:signature][:signature_attachment], :is_director => true,:school_id=>MultiSchool.current_school.id)
              @term.save
            end
            flash[:notice] = "Director signature is saved"
            redirect_to :action=> 'upload_signature' and return
          else
            flash[:notice] = "Select an image to upload"
            redirect_to :action=> 'director_signature' and return
          end
        end
      else
        flash[:notice] = "#{t('deny_permission')}"
        redirect_to :controller => :exam, :action => :index
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, upload_director_signature action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def tutor_comments
    begin
      if @current_user.privileges.include?(Privilege.find_by_name("TermManager")) or @current_user.employee? 
        @employee = @current_user.employee_entry
       @employee_postion = @employee.employee_position
      if (@employee_postion.name.downcase =~ /principal(.*)/ )
        @batches = Batch.active.all
        @principal = "true"
      else
        @principal = " false"
        @batches = @employee.batches 
        @batches = @batches.uniq
          if @batches.blank?
          flash[:notice] = "#{t('no_batches')}"
        end
      end
      else
        flash[:notice] = "#{t('deny_permission')}"
        redirect_to :controller => :report_builders, :action => :index
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, tutor_comments action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def save_comments
    begin
      if params[:term_id].present?
      params[:exam].each_pair do |student,details|
      @tutor_comment = TutorComment.find_by_batch_id_and_term_id_and_student_id_and_user_id_and_is_principal(params[:batch_id],params[:term_id],student,@current_user.id,params[:is_principal] == "true" ? true : false)
     unless @tutor_comment.nil?
        @tutor_comment.update_attributes(:description => details[:description])
        flash[:notice] = "Updated Sucessfully"
      else
      @tutor_comment = TutorComment.new(:description => details[:description], :batch_id => params[:batch_id],:user_id => @current_user.id, :term_id => params[:term_id], :student_id => student,:is_principal =>params[:is_principal] == "true" ? true : false)
      @tutor_comment.save
       flash[:notice] = "Saved Sucessfully"
      end
      end
      redirect_to :action =>:tutor_comments
      else
        flash[:notice] = "Select Term To Proceed"
      redirect_to :action =>:tutor_comments
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, save_comments action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :tutor_comments
    end
  end

  def batch_terms_comments
    begin
      @batch = Batch.find(params[:batch_id])
      @terms = @batch.terms
      render(:update) do |page|
        page.replace_html 'batch_terms',:partial=>'batch_terms_comments'
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, batch_terms_comments action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def tutor_batch_students
    begin
    @term = Term.find(params[:term_id])
    @batch_students = @term.batch.students
    @employee = @current_user.employee_entry
       @employee_postion = @employee.employee_position
     if (@employee_postion.name.downcase =~ /principal(.*)/ )
       @principal = "true"
      else
        @principal = "false"
     end
     render(:update) do |page|
        page.replace_html 'batch_students',:partial=>'tutor_batch_students'
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, tutor_batch_students action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def list_terms
    begin
       if request.post?
         @batch = Batch.find(params[:batch_id])
             @terms = @batch.terms
             render(:update) do |page|
                page.replace_html   'batch_terms', :partial=>"term_options"
             end
       else
      if @current_user.privileges.include?(Privilege.find_by_name("TermManager")) or @current_user.admin?
        @batches= Batch.all(:joins => :terms).uniq

      else
        flash[:notice] = "#{t('deny_permission')}"
        redirect_to :controller => :exam, :action => :index
      end
  end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, list_terms action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def add_exam_to_term
    begin
      if @current_user.privileges.include?(Privilege.find_by_name("TermManager")) or @current_user.admin?
        @batch = Batch.active.all
        @terms = Term.all
        if request.post?
          if params[:term_id].empty?
            flash[:notice] = "Select the term and continue"
          elsif params[:name].empty?
            flash[:notice] = "Enter the name and continue"
          elsif params[:exam_group][:start_time].empty?
            flash[:notice] = "Select the start time and continue"
          elsif params[:exam_group][:end_time].empty?
            flash[:notice] = "Select the end time and continue"
          end
          SubjectExamGroup.create(:name => params[:name],:term_id => params[:term_id], :start_time => params[:exam_group][:start_time], :end_time => params[:exam_group][:end_time] )
          flash[:notice] = "exam is assigned to term sucessfully"
          redirect_to :action => "add_exam_to_term"
        end
      else
        flash[:notice] = "#{t('deny_permission')}"
        redirect_to :controller => :exam, :action => :index
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, add_exam_to_term action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def assign_exam_to_subject
    begin
      if @current_user.privileges.include?(Privilege.find_by_name("TermManager")) or @current_user.admin?
        @batch = Batch.active.all
        if request.post?
          @subject_ids = []
          params[:subject].each do |key,value|
            @subject_ids << key if value.to_i == 1
          end
          if params[:exam_group_id].empty?
            flash[:notice] = "Select the exam_group and continue"
          elsif @subject_ids.empty?
            flash[:notice] = "Select a subject and continue"
          end
          @subject_ids.each do |sub_id|
            AssignedExamGroup.create(:subject_exam_group_id => params[:exam_group_id] ,:subject_id => sub_id )
          end
          flash[:notice] = "Exam is assigned to subject sucessfully"
          redirect_to :action => :home
        end
      else
        flash[:notice] = "#{t('deny_permission')}"
        redirect_to :controller => :exam, :action => :index
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, assign_exam_to_subject action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end
  def update_term_subjects
    @term_exams = SubjectExamGroup.find_all_by_term_id(params[:term_id])
    render(:update) do |page|
      page.replace_html   'update_term_subjects', :partial=>"update_term_subjects"
    end
  end

  def term_subjects
    @exam_group = SubjectExamGroup.find(params[:exam_group_id])
    @term = Term.find(@exam_group.term_id)
    @term_subjects = @term.batch.subjects
    render(:update) do |page|
      page.replace_html   'term_subjects', :partial=>"term_subjects"
    end
  end

  def exam_subject
    @subjects = []
    @subject_group = SubjectExamGroup.find(params[:id])
    @batch = Term.find(@subject_group.term_id).batch
    @assigned_exam_group = AssignedExamGroup.find_all_by_subject_exam_group_id(@subject_group.id)
  end

  def subject_students
    @batch = Batch.find(params[:batch_id])
    @students = @batch.students
    @assigned_exam_group = AssignedExamGroup.find(params[:assigned_exam_group_id])
    @subject_group = SubjectExamGroup.find(@assigned_exam_group.subject_exam_group_id)
  end

  def save_subject_score
    begin
      if @current_user.privileges.include?(Privilege.find_by_name("TermManager")) or @current_user.admin?
        if request.post?
          params[:exam].each_pair do |k,v|
            @score = SubjectExamScore.find_by_student_id_and_assigned_exam_group_id(k,params[:assigned_exam_group_id])
            if @score.nil?
              SubjectExamScore.create(:student_id => k, :assigned_exam_group_id => params[:assigned_exam_group_id], :value => v[:value] )
            else
              @score.update_attributes(:student_id => k, :assigned_exam_group_id => params[:assigned_exam_group_id], :value => v[:value])
            end
          end
          flash[:notice] = "Exam value is added sucussfully"
          redirect_to :action => "index", :controller => "exam"
        end
      else
        flash[:notice] = "#{t('deny_permission')}"
        redirect_to :controller => :exam, :action => :index
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, save_subject_score action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def delete_subject_exam_groups
    begin
      if @current_user.privileges.include?(Privilege.find_by_name("TermManager")) or @current_user.admin?
        @assigned_exam_group = AssignedExamGroup.find(params[:assigned_exam_group_id])
        unless @assigned_exam_group.nil?
          @score = SubjectExamScore.find_all_by_assigned_exam_group_id(@assigned_exam_group.id)
          unless @score.empty?
            SubjectExamScore.delete_all "assigned_exam_group_id = #{@assigned_exam_group.id}"
          end
          @assigned_exam_group.destroy
        end
        flash[:notice] = " Assigned subject is deleted sucussfully"
        redirect_to :action => :home
      else
        flash[:notice] = "#{t('deny_permission')}"
        redirect_to :controller => :exam, :action => :index
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, delete_subject_exam_groups action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end


  def add_evaluation_group
    begin
      @courses = Batch.active
      if request.post?
        @evaluation_group = EvaluationGroup.new(:name=> params[:evaluation_group][:name], :code => params[:evaluation_group][:code], :area => params[:area], :batch_id => params[:batch_id], :term_id => params[:term_id],:subject_id => params[:subject_id],:score_format => params[:evaluation_group][:score_format],:employee_id => params[:employee_id])
        if @evaluation_group.save
          redirect_to :action => 'infant_data'
          flash[:notice] = "Evaluation Group created Sucussfully"
        else
          render :action => 'add_evaluation_group'
        end
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, add_evaluation_group action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def add_evaluation_type
    begin
      @courses = Batch.find_all_by_id(EvaluationGroup.all.map {|p| p.batch_id}.uniq)
      if request.post?
        @evaluation_type = EvaluationType.new(:name=> params[:evaluation_type][:name], :code => params[:evaluation_type][:code], :batch_id => params[:batch_id], :term_id => params[:term_id],:evaluation_group_id => params[:evaluation_group_id])
        if @evaluation_type.save
          redirect_to :action => 'infant_data'
          flash[:notice] = "Evaluation type created Sucussfully"
        else
          render :action => 'add_evaluation_type'
        end
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, add_evaluation_type action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def add_evaluation_area
    begin
      @courses =  Batch.find_all_by_id(EvaluationGroup.all.map {|p| p.batch_id}.uniq)
      if request.post?
        @evaluation_area = EvaluationArea.new(:name=> params[:evaluation_area][:name], :code => params[:evaluation_area][:code], :batch_id => params[:batch_id], :term_id => params[:term_id],:evaluation_group_id => params[:evaluation_group_id],:evaluation_type_id => params[:evaluation_type_id])
        if @evaluation_area.save
          redirect_to :action => 'infant_data'
          flash[:notice] = "Evaluation area created Sucussfully"
        else
          render :action => 'add_evaluation_area'
        end
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, add_evaluation_area action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def term_evaluation_group
    begin
      @term = Term.find(params[:term_id])
      @term_evaluation_groups = @term.evaluation_groups
      render(:update) do |page|
        page.replace_html   'evaluation_groups', :partial=>"evaluation_group"
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, term_evaluation_group action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def term_evaluation_type
    begin
      @evaluation_group = EvaluationGroup.find(params[:evaluation_group_id])
      @term_evaluation_types = @evaluation_group.evaluation_types
      render(:update) do |page|
        page.replace_html   'evaluation_types', :partial=>"evaluation_type"
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, term_evaluation_type action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def get_evaluation_type
    @evaluation_group = EvaluationGroup.find(params[:id])
    if @evaluation_group.score_format == "Alphabets"
      @score_values = ["A","b","C","d","E"]
    elsif @evaluation_group.score_format == "Emerging"
      @score_values = ["Emerging","Expected","Exceeding"]
    elsif @evaluation_group.score_format == "Numbers"
      @score_values = (0..5)
    elsif @evaluation_group.score_format == "ExamGroups"
      @exam_groups = @evaluation_group.term.exam_groups
    elsif @evaluation_group.score_format == "Extracurricular"
      @score_values = ["Key skill learnt","Conduct","Punctuality","Participation"]
      @sub_values =  ["a","b","c","d","e"]
    elsif @evaluation_group.score_format == "Extracurricular2"
      @score_values = ["Grading Rubric","Excellent","Very Good","Improving","Unsatisfactory"]
      @sub_values =  ["a","b","c","d","e"]
    elsif @evaluation_group.score_format == "Effort"
      @score_values = ["Needs improvement ","Satisfactory ","Excellent"]
    elsif @evaluation_group.score_format == "Extracurricular3"
      @score_values = ["Excellent","Very Good","Good","Needs Improvement"]
    end
    @evaluation_types = @evaluation_group.evaluation_types
    @students = @evaluation_group.term.batch.students
  end

  def get_evaluation_area
    @score_values = params[:score_values]
    evaluation_type = EvaluationType.find(params[:id])
    @evaluation_areas = evaluation_type.evaluation_areas
    @students = evaluation_type.evaluation_group.term.batch.students
    render(:update) do |page|
      page.replace_html   'evaluation_areas', :partial=>"evaluation_area"
    end
  end

  def evaluation_score
    @score_values = params[:score_values]
    @area = EvaluationArea.find(params[:id])
    @students = @area.evaluation_type.evaluation_group.term.batch.students
    render(:update) do |page|
      page.replace_html   'evaluation_students', :partial=>"evaluation_students"
    end
  end

  def save_score
    if params[:evaluation_area_id].present?
      params[:score].each do |score|
        @score = EvaluationScore.find_by_student_id_and_evaluation_area_id(score.first.to_i,params[:evaluation_area_id])
        unless @score.nil?
          @score.update_attribute('performance', score.last['value'])
          flash[:notice]= "Score is Updated Sucessfully"
        else
          EvaluationScore.create(:student_id => score.first.to_i, :performance => score.last['value'],  :evaluation_type_id => params[:evaluation_type_id], :evaluation_area_id => params[:evaluation_area_id])
          flash[:notice]= "Score is saved Sucessfully"
        end
      end
    elsif params[:evaluation_type_id].present?
      params[:score].each do |score|
        @score = EvaluationScore.find_by_student_id_and_evaluation_type_id_and_evaluation_area_id(score.first.to_i,params[:evaluation_type_id],nil)
        unless @score.nil?
          @score.update_attribute('performance', score.last['value'])
          flash[:notice]= "Score is Updated Sucessfully"
        else
          EvaluationScore.create(:student_id => score.first.to_i, :performance => score.last['value'],  :evaluation_type_id => params[:evaluation_type_id])
          flash[:notice]= "Score is saved Sucessfully"
        end
      end

    elsif  params[:types].present?
      params[:types].each_pair do |evaluation_student,details|
        evaluation_split_student = evaluation_student.split('-')
        @score = EvaluationScore.find_by_evaluation_type_id_and_student_id(evaluation_split_student.first,evaluation_split_student.last)
        unless @score.nil?
          @score.update_attributes(:performance => details)
          flash[:notice]= "Score is Updated Sucessfully"
        else
          EvaluationScore.create(:student_id => evaluation_split_student.last ,:evaluation_type_id => evaluation_split_student.first,:performance => details)
          flash[:notice]= "Score is saved Sucessfully"
        end
      end
    elsif params[:areas].present?
      params[:areas].each_pair do |evaluation_student,details|
        evaluation_split_student = evaluation_student.split('-')
        @score = EvaluationScore.find_by_evaluation_area_id_and_student_id(evaluation_split_student.first,evaluation_split_student.last)
        unless @score.nil?
          @score.update_attributes(:performance => details)
          flash[:notice]= "Score is Updated Sucessfully"
        else
          EvaluationScore.create(:student_id => evaluation_split_student.last ,:evaluation_area_id => evaluation_split_student.first,:performance => details)
          flash[:notice]= "Score is saved Sucessfully"
        end
      end
    elsif params[:examgroups].present?
      params[:exam].each_pair do |evaluation_student, details|
        evaluation_split_student = evaluation_student.split('-')
        @score = EvaluationScore.find_by_evaluation_type_id_and_student_id(evaluation_split_student.first,evaluation_split_student.last)
        unless @score.nil?
          @score.update_attributes(:exam_group_scores => details)
          flash[:notice]= "Score is Updated Sucessfully"
        else
          EvaluationScore.create(:student_id => evaluation_split_student.last ,:evaluation_type_id => evaluation_split_student.first,:exam_group_scores => details)
          flash[:notice]= "Score is saved Sucessfully"
        end
      end    
    end
    redirect_to :action => :evaluation_performance
  end
  
  def assign_template
    begin
      @term_ids =[]
      @templates = ReportBuilder.all
      @batches = Batch.active
      if request.post?
        params[:term_id].each do |key,value|
          @term_ids << key if value.to_i == 1
        end
        @exist = AssignTemplate.find_by_batch_id_and_report_builder_id(params[:batch_id],params[:template_id])
        unless @exist.nil?
          @exist.update_attributes(:batch_id => params[:batch_id], :report_builder_id => params[:template_id], :term_id => @term_ids)
          flash[:notice]= "Template Assigned Updated Sucussfully"
        else
          AssignTemplate.create(:batch_id => params[:batch_id], :report_builder_id => params[:template_id], :term_id => @term_ids)
          flash[:notice]= "Template Assigned Sucussfully"
        end
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, assign_template action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def get_terms
    begin
      @batch = Batch.find(params[:batch_id])
      @batch_terms = @batch.terms
      render(:update) do |page|
        page.replace_html "batch_terms", :partial=>"get_terms"
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, get_terms action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def reports
    begin
      config_value = Configuration.find_by_config_key('TermwiseExamManagement').config_value
      if config_value == "1"
        @student = Student.find params[:id]
        @batch = @student.batch
        @grouped_exams = GroupedExam.find_all_by_batch_id(@batch.id)
        @normal_subjects = Subject.find_all_by_batch_id(@batch.id,:conditions=>"no_exams = false AND elective_group_id IS NULL AND is_deleted = false")
        @student_electives =StudentsSubject.all(:conditions=>{:student_id=>@student.id,:batch_id=>@batch.id,:subjects=>{:is_deleted=>false}},:joins=>[:subject])
        @elective_subjects = []
        @student_electives.each do |e|
          @elective_subjects.push Subject.find(e.subject_id)
        end
        @subjects = @normal_subjects+@elective_subjects
        @exam_groups = @batch.exam_groups
        @exam_groups.reject!{|e| e.result_published==false}
        @old_batches = @student.graduated_batches
      else
        redirect_to :controller => "students", :action => "reports"
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, reports action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def assigned_batches
    begin
      @batches = Batch.all
      @assigned_batches = AssignTemplate.find_all_by_report_builder_id(params[:template_id])
      render(:update) do |page|
        page.replace_html "assigned_batches", :partial=>"assigned_batches"
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, assigned_batches action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def delete_assigned_batch
    begin
      assign_template = AssignTemplate.find(params[:id])
      assign_template.destroy
      flash[:notice] = "Assign template deleted succussfully for seleted batch"
      redirect_to :action => :assign_template
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, delete_assigned_batch action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def evaluation_comment
    begin
      @courses = Batch.find_all_by_id(EvaluationGroup.all.map {|p| p.batch_id}.uniq)
      if request.post?
        @evaluation = EvaluationComment.find_by_evaluation_group_id_and_term_id(params[:evaluation_group_id],params[:term_id])
        unless @evaluation.nil?
          @evaluation.update_attributes(:comment=> params[:evaluation_comment][:comment], :user_id => @current_user.id)
          redirect_to :action => 'infant_data'
          flash[:notice] = "Evaluation Comment Updated Sucussfully"
        else
          @evaluation_comment = EvaluationComment.new(:comment=> params[:evaluation_comment][:comment], :user_id => @current_user.id, :batch_id => params[:batch_id], :term_id => params[:term_id],:evaluation_group_id => params[:evaluation_group_id])
          if @evaluation_comment.save
            redirect_to :action => 'infant_data'
            flash[:notice] = "Evaluation Comment created Sucussfully"
          else
            render :action => 'evaluation_comment'
          end
        end
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, evaluation_comment action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  #  def manage_evaluation
  #
  #  end

  def manage_evaluation_groups
      @courses =Batch.find_all_by_id(EvaluationGroup.all.map {|p| p.batch_id}.uniq)
  end

  def manage_evaluation_type
    @courses =Batch.find_all_by_id(EvaluationGroup.all.map {|p| p.batch_id}.uniq)
  end

  def manage_evaluation_area
    @courses = Batch.find_all_by_id(EvaluationGroup.all.map {|p| p.batch_id}.uniq)
  end

  def update_evaluation_term
    begin
      @evaluation_terms = EvaluationGroup.find_all_by_batch_id(params[:batch_id]).map { |g| Term.find(g.term_id) }.uniq
      render(:update) do |page|
        page.replace_html "evaluation_term", :partial=>"update_evaluation_term"
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, update_evaluation_term action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def update_evaluation_group
    begin
      @evaluation_groups = EvaluationGroup.find_all_by_term_id(params[:term_id])
      render(:update) do |page|
        page.replace_html "evaluation_group", :partial=>"update_evaluation_group"
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, update_evaluation_group action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def edit_groups
    @alpha,@numeric,@emerging,@exam_groups,@extra,@effort= false
    @batches = Batch.active.all
    @evaluation_group = EvaluationGroup.find(params[:id])
    @batch = Batch.find(@evaluation_group.batch_id) 
    @terms = @batch.terms
    @departments = EmployeeDepartment.ordered
    @employee = Employee.find_by_id(@evaluation_group.employee_id) unless @evaluation_group.employee_id.nil?
    @department = @employee.employee_department unless @employee.nil?
    @employees = @department.employees unless @department.nil?
    @subjects = @batch.subjects
    if @evaluation_group.score_format == "Alphabets"
      @alpha = true
    elsif @evaluation_group.score_format == "Numbers"
      @numeric = true
    elsif @evaluation_group.score_format == "Emerging"
      @emerging = true
    elsif @evaluation_group.score_format == "ExamGroups"
      @exam_groups = true
    elsif @evaluation_group.score_format == "Extracurricular"
      @extra = true
      elsif @evaluation_group.score_format == "Effort"
      @effort = true
    end
    @evaluation_group.subject_id.nil? or @evaluation_group.subject_id.blank? ? @sub = false : @sub = true
    @evaluation_group.area.nil? or @evaluation_group.area.blank? ? @area = false : @area = true
     @sub== true ? @value = "subject" :  @area == true  ? @value ="area" :  @value = ""
  end

  def update_groups
    begin
      @evaluation_group = EvaluationGroup.find(params[:id])
      term = @evaluation_group.term
      if request.xhr?
        render :update do |page|
          if @evaluation_group.update_attributes( :name=> params[:edit_groups][:name], :code => params[:edit_groups][:code], :area => params[:area], :batch_id => params[:batch_id].empty? ? @evaluation_group.batch_id : params[:batch_id], :term_id => params[:term_id].empty? ? params[:term_id] : @evaluation_group.term_id,:subject_id => params[:subject_id],:score_format => params[:edit_groups][:score_format],:employee_id => params[:employee_id])
            @evaluation_groups = term.evaluation_groups
            if params[:term_id] != term.id
              unless @evaluation_group.evaluation_types.empty?
                @evaluation_group.evaluation_types.each do |type|
                  unless type.evaluation_areas.empty?
                    type.evaluation_areas.each do |area|
                      area.update_attributes(:batch_id => params[:batch_id].empty? ? @evaluation_group.batch_id : params[:batch_id], :term_id => params[:term_id])
                    end
                  end
                  type.update_attributes(:batch_id => params[:batch_id].empty? ? @evaluation_group.batch_id : params[:batch_id], :term_id => params[:term_id])
                end
              end
            end
            #            flash[:notice] = "Evaluation Group Updated Succussfully"
            page << 'Modalbox.hide()'
            page.replace_html 'evaluation_group',:partial => 'update_evaluation_group'
          else
            page.replace_html 'form-errors', :partial => 'errors_subjects'
          end
        end
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, update_groups action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def group_destroy
    begin
      @group = EvaluationGroup.find(params[:id])
      if @group.destroy
        unless @group.evaluation_types.empty?
          @group.evaluation_types.each do |type|
            unless type.evaluation_areas.empty?
              type.evaluation_areas.each do |area|
                area.destroy
              end
              type.destroy
            end
          end
        end
        flash[:notice] = "Evaluation Group Deleted Succussfully"
        redirect_to :action => "manage_evaluation_groups"
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, group_destroy action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def list_eval_types
    @evaluation_types = EvaluationGroup.find(params[:group_id]).evaluation_types
    render(:update) do |page|
      page.replace_html "evaluation_type", :partial=>"list_eval_types"
    end
  end
  
  def list_eval_areas
    @evaluation_areas = EvaluationType.find(params[:group_id]).evaluation_areas
    render(:update) do |page|
      page.replace_html "evaluation_area", :partial=>"list_eval_areas"
    end
  end
  
  def edit_areas
    @courses = Batch.active
    @evaluation_area = EvaluationArea.find(params[:id])
  end

  def update_areas
    begin
      @evaluation_area = EvaluationArea.find(params[:id])
      @evaluation_type = @evaluation_area.evaluation_type
      @evaluation_group = @evaluation_type.evaluation_group
      term = @evaluation_group.term
      if request.xhr?
        render :update do |page|
          if @evaluation_area.update_attributes(:name=> params[:edit_areas][:name], :code => params[:edit_areas][:code], :batch_id => params[:batch_id].empty? ? @evaluation_group.batch_id : params[:batch_id] , :term_id => params[:term_id]?  params[:term_id] : @evaluation_group.term_id , :evaluation_group_id => params[:evaluation_group_id]?  @evaluation_type.evaluation_group_id : params[:evaluation_group_id],:evaluation_type_id => params[:evaluation_type_id]? @evaluation_type.id : params[:evaluation_type_id]  )
            @evaluation_groups = term.evaluation_groups
            if params[:term_id] != term.id
              unless @evaluation_group.evaluation_types.empty?
                @evaluation_group.evaluation_types.each do |type|
                  unless type.evaluation_areas.empty?
                    type.evaluation_areas.each do |area|
                      area.update_attributes(:batch_id => params[:batch_id].empty? ? @evaluation_group.batch_id : params[:batch_id], :term_id => params[:term_id])
                    end
                  end
                  type.update_attributes(:batch_id => params[:batch_id].empty? ? @evaluation_group.batch_id : params[:batch_id], :term_id => params[:term_id])
                end
              end
            end
            #            flash[:notice] = "Evaluation Group Updated Succussfully"
            page << 'Modalbox.hide()'
            page.replace_html 'evaluation_group',:partial => 'update_evaluation_group'
          else
            page.replace_html 'form-errors', :partial => 'errors_subjects'
          end
        end
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, update_groups action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def area_destroy
    begin
      @evaluation_area = EvaluationArea.find(params[:id])
      @evaluation_area.destroy
      flash[:notice] = "Evaluation Group Deleted Succussfully"
      redirect_to :action => "manage_evaluation_groups"
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, area_destroy action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end
  def edit_types
    @courses = Batch.active
    @evaluation_type = EvaluationType.find(params[:id])
  end
  
  def update_type
    begin
      @evaluation_type = EvaluationType.find(params[:id])
      term = @evaluation_type.evaluation_group.term
      @evaluation_group = @evaluation_type.evaluation_group
      if request.xhr?
        render :update do |page|
          if @evaluation_type.update_attributes(:name=> params[:edit_type][:name], :code => params[:edit_type][:code], :batch_id => params[:batch_id].empty? ? @evaluation_type.batch_id : params[:batch_id] , :term_id => params[:term_id]?  params[:term_id].empty? ? term.id : params[:term_id] : term.id, :evaluation_group_id => params[:evaluation_group_id]? params[:evaluation_group_id].empty? ? @evaluation_type.evaluation_group_id : params[:evaluation_group_id]  : @evaluation_type.evaluation_group_id )
            @evaluation_types = @evaluation_group.evaluation_types
            if params[:term_id] != term.id
              unless @evaluation_type.evaluation_types.empty?
                @evaluation_group.evaluation_types.each do |type|
                  unless type.evaluation_areas.empty?
                    type.evaluation_areas.each do |area|
                      area.update_attributes(:batch_id => params[:batch_id].empty? ? @evaluation_group.batch_id : params[:batch_id], :term_id => params[:term_id])
                    end
                  end
                  type.update_attributes(:batch_id => params[:batch_id].empty? ? @evaluation_group.batch_id : params[:batch_id], :term_id => params[:term_id])
                end
              end
            end
            #            flash[:notice] = "Evaluation Group Updated Succussfully"
            page << 'Modalbox.hide()'
            page.replace_html 'evaluation_type',:partial => 'list_eval_types'
          else
            page.replace_html 'form-errors', :partial => 'errors_subjects'
          end
        end
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, update_types action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def type_destroy
    begin
      @evaluation_type = EvaluationType.find(params[:id])
      if @evaluation_type.destroy

        unless @evaluation_type.evaluation_areas.empty?
          @evaluation_type.evaluation_areas.each do |area|
            area.destroy
          end
          @evaluation_type.destroy
        end

        flash[:notice] = "Evaluation Group Deleted Succussfully"
        redirect_to :action => "manage_evaluation_groups"
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, group_destroy action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  #  def add_grading_scale
  #    if request.post?
  #      @grading = TermGradingScale.new(:name => params[:name], :grading => {"#{params[:grade_name]}".to_sym  => {:min => params[:min_limit], :max => params[:max_limit]}})
  #      if @grading.save
  #        flash[:notice] = "Grading Scale is added Succussfully"
  #        redirect_to :action => :home
  #      else
  #        render :add_grading_scale
  #      end
  #    end
  #
  #  end

  def authenticate_termwise
    # find and create TermwiseExamManagement config key in configurations table
    begin
      @config_key = Configuration.find_by_config_key('TermwiseExamManagement')
      if @config_key.nil?
        Configuration.create(:config_key => 'TermwiseExamManagement',:config_value => false)
      end
      @config_key = Configuration.find_by_config_key('TermwiseExamManagement')
      if @config_key.config_value == "0"
        redirect_to :controller => "exam", :action => "index"
        flash[:notice] = "#{t('enable_term_wise_management')}"
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, authenticate_termwise action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def junior_batch_ratings
    begin
      if (@current_user.privileges.include?(Privilege.find_by_name("TermManager")) and @current_user.employee? and @detail != "true") or @current_user.admin?
        @batches = Batch.active
      else
        flash[:notice] = "#{t('deny_permission')}"
        redirect_to :controller => :exam, :action => :index
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, junior_batch_ratings action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def term_exam_groups
     begin
      @exam_group = []
      @exam_name = []
      @term_group = TermExamGroup.find_all_by_term_id(params[:term_id])
      @term_group.each do |term_group|
        @exam_group << term_group.exam_group_id
      end
      @exam_group.each do |exam_group|
        @name = ExamGroup.find(exam_group)
        @exam_name << @name
      end
      render(:update) do |page|
        page.replace_html 'exam_groups',:partial=>'term_exam_groups'
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, term_exam_groups action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def rating_batch_students
    begin
      @exam = ExamGroup.find(params[:exam_id])
      @batch_students = @exam.batch.students.by_first_name
      render(:update) do |page|
        page.replace_html 'rating_students',:partial=>'rating_batch_students'
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, tutor_batch_students action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def save_ratings_comments
    @error= false
    params[:exam].each_pair do |student_id, details|
      @exam_score = RatingComment.find(:first, :conditions => {:exam_id => params[:exam_id], :student_id => student_id} )
      if @exam_score.nil?
        if details[:effort].to_f <= 5
          RatingComment.create do |score|
	          score.user_id          = params[:user_id]
            score.exam_id          = params[:exam_id]
            score.student_id       = student_id
            score.effort           = details[:effort]
            score.home_work        = details[:home_work]
            score.class_work       = details[:class_work]
            score.attitude         = details[:attitude]
            score.comment_desc     = details[:comment_desc]
          end
        else
          @error = true
        end
      else
        if details[:effort].to_f <= 5
          if @exam_score.update_attributes(details)
          else
            flash[:warn_notice] = "#{t('flash4')}"
            @error = nil
          end
        else
          @error = true
        end
      end
    end
    flash[:warn_notice] = "#{t('rating_flash2')}" if @error == true
    flash[:notice] = "#{t('rating_flash3')}" if @error == false
    redirect_to :action => :junior_batch_ratings
  end
  
  def infant_students
    begin
      @term_id = EvaluationGroup.find(params[:evaluation_group_id]).term_id
      @exam_group = []
      @exam_name = []
      @term_group = TermExamGroup.find_all_by_term_id(@term_id)
      @term_group.each do |term_group|
        @exam_group << term_group.exam_group_id
      end
      @exam_group.each do |exam_group|
        @name = ExamGroup.find(exam_group)
        @exam_name << @name
      end
      @batch_id = Term.find(@term_id).batch_id
      @batch_students = Batch.find(@batch_id).students
      render(:update) do |page|
        page.replace_html 'infant_students',:partial=>'infant_students'
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, tutor_batch_students action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end
  def subject_destroy
  end

  def save_infant_comments
    @error= false
    params[:exam].each_pair do |student_id, details|
      @exam_score = EvaluationComment.find(:first, :conditions => {:evaluation_group_id => params[:evaluation_group_id], :student_id => student_id ,:term_id => params[:term_id] } )
      if @exam_score.nil?
        if !details[:comment].nil?
          EvaluationComment.create do |score|
	          score.user_id          = params[:user_id]
            score.evaluation_group_id  = params[:evaluation_group_id]
            score.student_id       = student_id
            score.term_id          = params[:term_id]
            score.comment          = details[:comment]
            score.batch_id = params[:batch_id]
          end
        else
          @error = true
        end
      else
        if !details[:comment].nil?
          if @exam_score.update_attributes(details)
          else
            flash[:warn_notice] = "#{t('flash4')}"
            @error = nil
          end
        else
          @error = true
        end
      end
    end
    flash[:warn_notice] = "#{t('rating_flash2')}" if @error == true
    flash[:notice] = "#{t('rating_flash3')}" if @error == false
    redirect_to :action => :evaluation_comment
  end

  def subject_elective_destroy
    begin
      if @current_user.privileges.include?(Privilege.find_by_name("TermManager")) or @current_user.admin?
        Rails.logger.info params[:admin_no]
        Rails.logger.info params[:subject_id]
        @elective_subject = StudentsSubject.find_by_student_id_and_subject_id(params[:admin_no],params[:subject_id])
        @elective_subject.destroy
        flash[:notice] = "Deleleted Sucessfully"
        redirect_to :action => :subject_destroy
      else
        flash[:notice] = "#{t('deny_permission')}"
        redirect_to :controller => :exam, :action => :index
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, subject_elective_destroy"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def evaluation_performance
    begin
      if (@current_user.admin? or @current_user.employee? or @current_user.privileges.collect(&:name).include?('ExaminationControl'))
        config_value = Configuration.find_by_config_key('TermwiseExamManagement').config_value
        if config_value == "1"
          privilege = current_user.privileges.map{|p| p.name}
          if current_user.admin or privilege.include?("ExaminationControl") or privilege.include?("EnterResults")
            @course= Course.find(:all,:conditions => { :is_deleted => false }, :order => 'code asc')
          elsif current_user.employee
            @course= current_user.employee_record.subjects.all(:group => 'batch_id').map{|x|x.batch.course}
          end
        else
          redirect_to :controller => "exam", :action => "create_exam"
        end
      else
        flash[:notice] = "Not Permitted to Access the Page "
        redirect_to :action => :home
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, evaluation_performance action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end
  def view
    @courses = Batch.active
    if params[:evaluation_group] == "subject" or params[:evaluation_group].blank?
      render(:update) do |page|
        page.replace_html "render_partial", :partial => "views_subjects"
      end
    elsif params[:evaluation_group] == "area"
      render(:update) do |page|
        page.replace_html "render_partial", :partial => "views_area"
      end
    end
  end

  private
  def initial_queries
    # data added from exam_groups controller, initial_queries action
    Rails.logger.info params[:batch_id].inspect

    @batch = Batch.find params[:batch_id], :include => :course unless params[:batch_id].nil?
    @course = @batch.course unless @batch.nil?
    Rails.logger.info @batch.id.inspect
  end

  def protect_other_batch_exams
    # data added from exam_groups controller, protect_other_batch_exams action
    @user_privileges = @current_user.privileges
    if !@current_user.admin? and !@user_privileges.map{|p| p.name}.include?('ExaminationControl') and !@user_privileges.map{|p| p.name}.include?('EnterResults')
      @user_subjects = @current_user.employee_record.subjects.all(:group => 'batch_id')
      @user_batches = @user_subjects.map{|x|x.batch_id} unless @current_user.employee_record.blank? or @user_subjects.nil?

      unless @user_batches.include?(params[:batch_id].to_i)
        flash[:notice] = "#{t('flash_msg4')}"
        redirect_to :controller => 'user', :action => 'dashboard'
      end
    end
  end

end
