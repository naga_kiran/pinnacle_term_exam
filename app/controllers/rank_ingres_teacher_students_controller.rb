class RankIngresTeacherStudentsController < ApplicationController
  before_filter :login_required
  filter_access_to :all
  def assign_students_teacher
    @batches = Batch.active
    @normal_subjects = []
    @employees = []
    if request.post?
      teacher_student = RankIngresTeacherStudent.find_by_batch_id_and_subject_id_and_employee_id(params[:students][:batch_id],params[:students][:subject_id],params[:students][:employee_id])
      unless  teacher_student.nil?
        unless params[:students][:student_ids].blank?
        teacher_student.update_attributes(params[:students])
        else
        teacher_student.destroy
        end
      else
        RankIngresTeacherStudent.create(params[:students])
      end
    end
  end
  def  update_subjects
    @batch = Batch.find(params[:batch_id])
    subjects = @batch.subjects.active
    elective_subjects = subjects.select {|s| s.elective_group_id != nil && s.is_deleted == false}
    @normal_subjects = subjects - elective_subjects
    #@students = @batch.students
    render :update do |page|
      page.replace_html 'subjects', :partial => 'subjects'
      # page.replace_html 'students', :partial => 'students'
    end
  end
  def update_subject_employees
    @batch = params[:batch]
    @subject = params[:subject_id]
    employe_ids = EmployeesSubject.find_all_by_subject_id(params[:subject_id]).map {|p| p.employee_id}
    @employees = Employee.find_all_by_id(employe_ids)
    render :update do |page|
      page.replace_html 'employee', :partial => 'employee'
    end
  end
  def student_list
    other_students = []
    @teacher_student = []
    batch =Batch.find params[:batch].to_i
    teacher_students = RankIngresTeacherStudent.find_all_by_subject_id(params[:subject_id])
    unless teacher_students.blank? or teacher_students.empty?
      teacher_students.each do |s|
        if s.subject_id == params[:subject_id].to_i and s.employee_id == params[:employee_id].to_i
        s.student_ids.each do |sid|
           @teacher_student <<  Student.find_by_id(sid)
        end
        else
          s.student_ids.each do |sid|
           other_students <<  Student.find_by_id(sid)
        end
        end
      end
      unless other_students.empty? or other_students.blank?
        @students = batch.students - other_students
      else
        @students = batch.students
      end
    else
      @students = batch.students
    end
    render :update do |page|
      page.replace_html 'students', :partial => 'students'
    end
  end
end
