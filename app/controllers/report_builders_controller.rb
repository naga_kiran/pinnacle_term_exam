class ReportBuildersController < ApplicationController
  unloadable
  before_filter :login_required
  filter_access_to :all
  before_filter :authenticate_report_builder
  before_filter :check_privileges
  require 'pdfkit'

  def index
    begin
      config = Configuration.find_by_config_key("TermwiseExamManagement").config_value
       if config == "1"
         @detail = "false"
      if @current_user.employee?
        @employee_additional_detail = Employee.find_by_user_id(@current_user.id).employee_additional_details
        unless @employee_additional_detail.empty?
          @employee_additional_detail.each do |detail|
            if detail.additional_field.name == "House Parent"
              if detail.additional_info == "yes"
                @detail = "true"
              end
            end
          end
        end
      end
      if @current_user.privileges.include?(Privilege.find_by_name("TermManager")) or @current_user.admin?
        else
        flash[:notice] = "#{t('deny_permission')}"
        redirect_to :controller => :exam, :action => :index
      end
      else
        flash[:notice] = "#{t('enable_settings')}"
        redirect_to :controller => :exam, :action => :index
      end
    rescue Exception => e
      Rails.logger.info "Exception in report builders controller, index action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :controller => :exam, :action => :index
    end
  end

  def report_and_term
  end

  def load_sub_menu_values
    # student basic data and examination data is added for tinymce editor
    begin
      @student_basic_data = ReportBuilder.student_basic_data
      @student_previous_data = ReportBuilder.student_previous_data
      @student_additional_data = StudentAdditionalField.all
      @parent_data = ReportBuilder.parent_data
      @tutor_data = ReportBuilder.tutor_data
      @date_time = ReportBuilder.date_time
      @student_attendance_data = ReportBuilder.student_attendance_data
      @student_examination_data = ReportBuilder.student_examination_data
      @principal_data = ReportBuilder.examination_data
      @page_oreintation = ReportBuilder.page_orientation
      #      @exam_data = ReportBuilder.exam_data
    rescue Exception => e
      Rails.logger.info "Exception in report builders controller, load_sub_menu_values action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :index
    end
  end


  def generate_template
    # new form for generating different template formats
    begin
      if @current_user.privileges.include?(Privilege.find_by_name("TermManager")) or @current_user.admin?
        load_sub_menu_values
      else
        flash[:notice] = "#{t('deny_permission')}"
        redirect_to :controller => :exam, :action => :index
      end
    rescue Exception => e
      Rails.logger.info "Exception in report builders controller, generate_template action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :index
    end
  end

  def create_template
    begin
      @report =  ReportBuilder.new(:name => params[:report_builder][:name], :description => params[:report_builder][:description], :page_orientation => session[:orientation].present? ? session[:orientation] : 'Portrait')
      @report.save
      session[:orientation] = nil
      flash[:notice] = "#{t('template_created')}"
      redirect_to :action => :view_template
    rescue Exception => e
      Rails.logger.info "Exception in report builders controller, create_template action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :index
    end
  end

  def edit
    # edit form for generating different template formats
    begin
      if @current_user.privileges.include?(Privilege.find_by_name("TermManager")) or @current_user.admin?
        load_sub_menu_values
      else
        flash[:notice] = "#{t('deny_permission')}"
        redirect_to :controller => :exam, :action => :index
      end
    rescue Exception => e
      Rails.logger.info "Exception in report builders controller, edit action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :index
    end
  end

  def update
    begin
      @template = ReportBuilder.find(params[:report_builder][:id])
      @template.update_attributes(:name => params[:report_builder][:name], :description => params[:report_builder][:description], :page_orientation => session[:orientation].present? ? session[:orientation] : @template.page_orientation)
      session[:orientation] = nil
      flash[:notice] = "#{t('template_updated')}"
      redirect_to :action => :view_template
    rescue Exception => e
      Rails.logger.info "Exception in report builders controller, update action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :index
    end
  end

  def destroy
    begin
      report = ReportBuilder.find(params[:id])
      report.destroy
      assign_template = AssignTemplate.find_all_by_report_builder_id(report.id)
      unless assign_template.empty?
        assign_template.each do |assign|
          assign.destroy
        end
      end
      flash[:notice] = "#{t('template_deleted')}"
      redirect_to :action => :view_template
    rescue Exception => e
      Rails.logger.info "Exception in report builders controller, destroy action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :index
    end
  end

  def generate_report
    begin
      if @current_user.privileges.include?(Privilege.find_by_name("ViewStudentReport")) or @current_user.admin?
      else
        flash[:notice] = "#{t('deny_permission')}"
        redirect_to :controller => :exam, :action => :index
      end
    rescue Exception => e
      Rails.logger.info "Exception in report builders controller, generate_report action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :index
    end
  end

  def batch_students
    begin
      @batch_terms=[]
      @batch = Batch.find(params[:batch_id])
      #      @batch_terms = @batch.terms
      if params[:profile]
         @batch_terms = @batch.terms
#        assign_template = AssignTemplate.find_all_by_batch_id(params[:batch_id])
#        unless assign_template.empty?
#        assign_template.each do |template|
#          @batch_terms << Term.find(template.term_id.first)
#        end
#      end
      else
        assign_template = AssignTemplate.find_by_batch_id_and_report_builder_id(params[:batch_id],params[:template_id]).to_a
      unless assign_template.empty?
        assign_template.each do |template|
          @batch_terms = Term.find_all_by_id(template.term_id)
        end
      end
      end
      @batch_students = @batch.students.all(:order => "first_name")
      render(:update) do |page|
        page.replace_html 'batch_students',:partial=>'batch_students'
      end
    rescue Exception => e
      Rails.logger.info "Exception in report builders controller, batch_students action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :index
    end
  end

  def student_wise_report
    begin
      #system("rsync -ruv /srv/fedena_app/rank_fedena/uploads/ /srv/fedena_app/rank_fedena/public/images/uploads/")
      if @current_user.privileges.include?(Privilege.find_by_name("TermManager")) or @current_user.admin? or @current_user.student? or @current_user.parent?
        @templates =  ReportBuilder.find_all_by_id(AssignTemplate.find_all_by_batch_id(Batch.all.map{|b| b.id}).map {|a| a.report_builder_id}.uniq)
        @batches = []
        if params[:student_id]
          @student_batches =[]
          student = Student.find(params[:student_id])
          bs = student.batch_students
          unless bs.empty?
            bs.each do |s|
              @student_batches << Batch.find(s.batch_id)
            end
          end
          @student_batches << student.batch
        end
      else
        flash[:notice] = "#{t('deny_permission')}"
        redirect_to :controller => :exam, :action => :index
      end
    rescue Exception => e
      Rails.logger.info "Exception in report builders controller, student_wise_report action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :index
    end
  end

  def batch_wise_report
    begin
      if @current_user.privileges.include?(Privilege.find_by_name("TermManager")) or @current_user.admin?
        @templates = ReportBuilder.find_all_by_id(AssignTemplate.find_all_by_batch_id(Batch.all.map{|b| b.id}).map {|a| a.report_builder_id}.uniq)
        @batches = []
        @batches=Batch.active.uniq

      else
        flash[:notice] = "#{t('deny_permission')}"
        redirect_to :controller => :exam, :action => :index
      end
    rescue Exception => e
      Rails.logger.info "Exception in report builders controller, batch_wise_report action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :index
    end
  end

  def student_wise_pdf_report
    begin
      # pdf report is generated for selected student params
      @student_ids = []
      @term_exam_groups =[]
      @weighted_exam_groups = []
      @term_ids =[]
      @weighted_total = 0
      query = []
      @sorted_description =[]
      if params[:student]
        params[:student].each do |key,value|
          @student_ids << key if value.to_i == 1
        end
      elsif params[:student_id]
        @student_ids = params[:student_id].to_a
        @current_student = Student.find(params[:student_id])
        @current_batch = @current_student.batch.id
        if !params[:batch_id].empty?
          if @current_student.batch.id.to_i != params[:batch_id].to_i
            bs = @current_student.batch_students
            batch_student = BatchStudent.find_by_student_id_and_batch_id(@current_student.id,params[:batch_id]) unless bs.empty?
            unless batch_student.nil?
              @current_student.update_attribute :batch_id, params[:batch_id].to_i
            end
          end
        end
      end
      if params[:term]
        params[:term].each do |key,value|
          @term_ids << key if value.to_i == 1
        end
        unless @term_ids.empty?
          @term_ids.each do |term_id|
            @term_exam_groups << Term.find(term_id).exam_groups
          end
          @term_exam_groups = @term_exam_groups.compact.flatten
        end
        unless @term_exam_groups.empty?
          @term_exam_groups.each do |term_exam_group|
            term_group = TermExamGroup.find_by_exam_group_id(term_exam_group.id)
            if term_group.weightage > 5
              if term_group.weightage == 60
                @comment_exam_group = ExamGroup.find(term_group.exam_group_id)
              end
              @weighted_exam_groups <<  ExamGroup.find(term_group.exam_group_id)
              @maximum_marks = Exam.find_by_exam_group_id(term_group.exam_group_id).maximum_marks.to_f
              if @maximum_marks > 5
                Rails.logger.info @maximum_marks
              @weighted_total = @weighted_total + @maximum_marks
              end
            end
          end
        end
      end
      if  params[:batch_id].empty?
        flash[:notice] = "#{t('select_a_batch_to_continue')}"
        redirect_to :action=> 'student_wise_report' and return
      elsif params[:template_id].nil?
      elsif params[:template_id].empty?
        flash[:notice] = "#{t('select_a_template_to_continue')}"
        redirect_to :action=> 'student_wise_report' and return
      elsif @student_ids.empty?
        flash[:notice] = "#{t('select_a_student_to_continue')}"
        redirect_to :action=> 'student_wise_report' and return
      elsif @term_ids.empty?
        flash[:notice] = "Select a term to continue"
        redirect_to :action=> 'student_wise_report' and return
      end
      if params[:template_id].nil?
        assign_template =  AssignTemplate.find_all_by_batch_id(params[:batch_id])
        assign_template.each do |template|
          if  template.term_id.include? @term_ids.first
            assign_template = AssignTemplate.find(template.id)
            @report = ReportBuilder.find(assign_template.report_builder_id)
          end
        end
      else
        @report = ReportBuilder.find(params[:template_id])
      end
      @student_ids = ReportBuilder.sort_by_first_name(@student_ids)
      @student_ids.each do |student_id|
        # the content of tinymce description part whole thing is splitted then it is made loop one by one as word
        description = @report.description.split unless @report.description.nil?
        description.each_with_index do |word, i|
          @value = ReportBuilder.match_string(query, word, student_id, i, params[:template_id],@term_ids).to_s
        end
        @sorted_description << @value
        query = []
      end
     PDFKit.configure do |config|
        config.wkhtmltopdf = '/opt/wkhtmltopdf'
        config.default_options = {
          :encoding => "UTF-8",
          :page_size     => 'Letter',
          :margin_top    => '0.3in',
          :margin_right  => @report.page_orientation == 'Landscape' ? '0.3in' : '0.5in',
          :margin_bottom => '0.1in',
          :margin_left   => @report.page_orientation == 'Landscape' ? '0.3in' : '0.5in',
          :print_media_type => true,
          #:header_right =>  "Page [page]",
          :orientation => @report.page_orientation == 'Landscape' ?  'Landscape' : 'Portrait'
        }
      end
        render :layout => false
      rescue Exception => e
      Rails.logger.info "Exception in report builders controller, student_wise_pdf_report action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :index
    end
  end

  def batch_wise_pdf_report
    begin
      # pdf report is generated for selected batch students
      if  params[:batch_id].empty?
        flash[:notice] = "#{t('select_a_batch_to_continue')}"
        redirect_to :action=> 'batch_wise_report' and return
      elsif params[:template_id].empty?
        flash[:notice] = "#{t('select_a_template_to_continue')}"
        redirect_to :action=> 'batch_wise_report' and return
      elsif params[:term].empty?
        flash[:notice] = "#{t('select_a_term_to_continue')}"
        redirect_to :action=> 'batch_wise_report' and return
      end
      @term_exam_groups =[]
      @term_ids =[]
      if params[:term]
        params[:term].each do |key,value|
          @term_ids << key if value.to_i == 1
        end
        unless @term_ids.empty?
          @term_ids.each do |term_id|
            @term_exam_groups << Term.find(term_id).exam_groups
          end
          @term_exam_groups = @term_exam_groups.compact.flatten
        end
      else
        @term_ids=Batch.find(params[:batch_id]).terms.map{|term| term.id}
        unless @term_ids.empty?
          @term_ids.each do |term_id|
            @term_exam_groups << Term.find(term_id).exam_groups
          end
          @term_exam_groups = @term_exam_groups.compact.flatten
        end
      end
      @student_ids = []
      query = []
      @sorted_description =[]
      report = ReportBuilder.find(params[:template_id])
      @batch = Batch.find(params[:batch_id])
      @student_ids = @batch.students.map{|b| b.id}
      @student_ids = ReportBuilder.sort_by_first_name(@student_ids)
      @student_ids.each do |student_id|
        # the content of tinymce description part whole thing is splitted then it is made loop one by one as word
        description = report.description.split
        description.each_with_index do |word, i|
          @value = ReportBuilder.match_string(query, word, student_id, i, params[:template_id],@term_ids).to_s
        end
        @sorted_description << @value
        query = []
      end
      # PDFKit.configure do |config|
      #   config.wkhtmltopdf = '/opt/wkhtmltopdf'
      #   config.default_options = {
      #     :page_size     => 'Letter',
      #     :margin_top    => '0.5in',
      #     :margin_right  => '0.5in',
      #     :margin_bottom => '0.2in',
      #     :margin_left   => '0.5in',
      #     :print_media_type => true,
      #     :orientation => report.page_orientation == 'Landscape' ?  'Landscape' : 'Portrait'
      #     #        :header_center => render_to_string(:template => "#{Rails.root}/vendor/plugins/rt_report_builder/app/views/report_builders/header.html.erb", :layout => false),
      #     #        :footer_center => render_to_string(:template =>"#{Rails.root}/vendor/plugins/rt_report_builder/app/views/report_builders/footer.html.erb",:layout => false)
      #   }
      # end
      PDFKit.configure do |config|
        config.wkhtmltopdf = '/opt/wkhtmltopdf'
        config.default_options = {
          :encoding => "UTF-8",
          :page_size     => 'Letter',
          :margin_top    => '0.3in',
          :margin_right  => report.page_orientation == 'Landscape' ? '0.3in' : '0.5in',
          :margin_bottom => '0.1in',
          :margin_left   => report.page_orientation == 'Landscape' ? '0.3in' : '0.5in',
          :print_media_type => true,
          #:header_right =>  "Page [page]",
          :orientation => report.page_orientation == 'Landscape' ?  'Landscape' : 'Portrait'
        }
      end
      render :layout => false
    rescue Exception => e
      Rails.logger.info "Exception in report builders controller, batch_wise_pdf_report action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :index
    end
  end

  def view_template
    begin
      if @current_user.privileges.include?(Privilege.find_by_name("TermManager")) or @current_user.admin?
        @templates = ReportBuilder.all
      else
        flash[:notice] = "#{t('deny_permission')}"
        redirect_to :controller => :exam, :action => :index
      end
    rescue Exception => e
      Rails.logger.info "Exception in report builders controller, view_template action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :index
    end
  end

  def generated_report4
    begin
      if params[:student].nil?
        if params[:exam_report].nil? or params[:exam_report][:batch_id].empty?
          flash[:notice] = "#{t('select_a_batch_to_continue')}"
          redirect_to :action=>'grouped_exam_report' and return
        end
      else
        if params[:type].nil?
          flash[:notice] = "#{t('invalid_parameters')}"
          redirect_to :action=>'grouped_exam_report' and return
        end
      end
      @previous_batch = 0
      #grouped-exam-report-for-batch
      if params[:student].nil?
        @type = params[:type]
        @batch = Batch.find(params[:exam_report][:batch_id])
        @students=@batch.students.by_first_name
        @student = @students.first  unless @students.empty?
        if @student.blank?
          flash[:notice] = "#{t('flash5')}"
          redirect_to :action=>'grouped_exam_report' and return
        end
        if @type == 'grouped'
          @grouped_exams = GroupedExam.find_all_by_batch_id(@batch.id)
          @exam_groups = []
          @grouped_exams.each do |x|
            @exam_groups.push ExamGroup.find(x.exam_group_id)
          end
        else
          @exam_groups = ExamGroup.find_all_by_batch_id(@batch.id)
          @exam_groups.reject!{|e| e.result_published==false}
        end
        general_subjects = Subject.find_all_by_batch_id(@batch.id, :conditions=>"elective_group_id IS NULL AND is_deleted=false")
        student_electives = StudentsSubject.find_all_by_student_id(@student.id,:conditions=>"batch_id = #{@batch.id}")
        elective_subjects = []
        student_electives.each do |elect|
          elective_subjects.push Subject.find(elect.subject_id)
        end
        @subjects = general_subjects + elective_subjects
        @subjects.reject!{|s| (s.no_exams==true or s.exam_not_created(@exam_groups.collect(&:id)))}
      else
        @student = Student.find(params[:student])
        if params[:batch].present?
          @batch = Batch.find(params[:batch])
          @previous_batch = 1
        else
          @batch = @student.batch
        end
        @type  = params[:type]
        if params[:type] == 'grouped'
          @grouped_exams = GroupedExam.find_all_by_batch_id(@batch.id)
          @exam_groups = []
          @grouped_exams.each do |x|
            @exam_groups.push ExamGroup.find(x.exam_group_id)
          end
        else
          @exam_groups = ExamGroup.find_all_by_batch_id(@batch.id)
          @exam_groups.reject!{|e| e.result_published==false}
        end
        general_subjects = Subject.find_all_by_batch_id(@batch.id, :conditions=>"elective_group_id IS NULL AND is_deleted=false")
        student_electives = StudentsSubject.find_all_by_student_id(@student.id,:conditions=>"batch_id = #{@batch.id}")
        elective_subjects = []
        student_electives.each do |elect|
          elective_subjects.push Subject.find(elect.subject_id)
        end
        @subjects = general_subjects + elective_subjects
        @subjects.reject!{|s| (s.no_exams==true or s.exam_not_created(@exam_groups.collect(&:id)))}
        if request.xhr?
          render(:update) do |page|
            page.replace_html   'grouped_exam_report', :partial=>"grouped_exam_report"
          end
        else
          @students = Student.find_all_by_id(params[:student])
        end
      end
    rescue Exception => e
      Rails.logger.info "Exception in report builders controller, generated_report4 action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :index
    end
  end

  def combined_grouped_exam_report_pdf
    begin
      @data_hash = GroupedExamReport.fetch_grouped_exam_data(:batch => params[:batch], :report_format_type => "pdf", :type => "grouped")
      render :pdf => 'combined_grouped_exam_report_pdf'
    rescue Exception => e
      Rails.logger.info "Exception in report builders controller, destroy action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :index
    end
  end

  def page_orientation
    if params[:orientation].present?
      session[:orientation] = params[:orientation]
      render :template => false
    end
  end

  def home_work_ratings
    begin
      if @current_user.privileges.include?(Privilege.find_by_name("TermManager")) or @current_user.admin?
        @batches = Batch.active
        if request.post?
          @student_ids = []
          params[:student].each do |key,value|
            @student_ids << key if value.to_i == 1
          end
          @student_ids.each do |student_id|
            @student_rating = HomeWorkRating.find(:all,:conditions => {:batch_id => params[:batch_id], :student_id => student_id })
            if @student_rating.present?
              @student_rating.first.update_attributes(:rating => params[:rating], :user_id => params[:user_id])
            else
              @rating = HomeWorkRating.new(:rating => params[:rating], :user_id => params[:user_id], :batch_id => params[:batch_id], :student_id => student_id)
              @rating.save
            end
          end
          flash[:notice] = "Rating is added Sucussfully"
          redirect_to :action =>:index
        end
      else
        flash[:notice] = "#{t('deny_permission')}"
        redirect_to :controller => :exam, :action => :index
      end
    rescue Exception => e
      Rails.logger.info "Exception in report_builders controller, home_work_ratings action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :index
    end
  end

  def elective_group_comment
    begin
      if @current_user.privileges.include?(Privilege.find_by_name("TermManager")) or @current_user.admin?
        @batches = Batch.active
        if request.post?
          @student_ids = []
          params[:student].each do |key,value|
            @student_ids << key.to_i if value.to_i == 1
          end
          if params[:batch_id].empty?
            redirect_to :action => "elective_group_comment"
            flash[:notice] = "Select a batch to continue"
          elsif params[:term_id].empty?
            redirect_to :action => "elective_group_comment"
            flash[:notice] = "Select a term to continue"
          elsif params[:elective_group_id].empty?
            redirect_to :action => "elective_group_comment"
            flash[:notice] = "Select a elective_group to continue"
          elsif @student_ids.empty?
            redirect_to :action => "elective_group_comment"
            flash[:notice] = "Select a student to continue"
          end
          @student_ids.each do |student_id|
            @comment = EletiveGroupComment.find_by_student_id_and_elective_group_id(student_id,params[:elective_group_id])
            if @comment.nil?
              EletiveGroupComment.create(:batch_id => params[:batch_id], :term_id => params[:term_id], :elective_group_id => params[:elective_group_id], :comment => params[:comment], :student_id => student_id)
            else
              @comment.update_attributes(:batch_id => params[:batch_id], :term_id => params[:term_id], :elective_group_id => params[:elective_group_id], :comment => params[:comment], :student_id => student_id)
            end
          end
          redirect_to :action => "index"
          flash[:notice] = "Comment is Added Sucussfully"
        end
      else
        flash[:notice] = "#{t('deny_permission')}"
        redirect_to :controller => :exam, :action => :index
      end
    rescue Exception => e
      Rails.logger.info "Exception in report_builders controller, elective_group_comment action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :index
    end
  end

  def batch_elective_groups
    @batch = Batch.find(params[:batch_id])
    @batch_terms = @batch.terms
    @batch_elctive_groups = @batch.elective_groups
    @batch_students = @batch.students
    render(:update) do |page|
      page.replace_html   'batch_elective_groups', :partial=>"batch_elective_groups"
    end
  end

  def house_parent_comment
    begin
      if  @current_user.admin?
        @batches = Batch.active
        if request.post?
                  params[:comment].each do |key,value|
                     @comment = HouseParentComment.find_by_term_id_and_batch_id_and_student_id(params[:term_id],params[:batch_id],key)
                     if @comment.nil?
                        @comment = HouseParentComment.new(:description => value, :user_id => params[:user_id], :batch_id => params[:batch_id],:term_id => params[:term_id], :student_id => key)
                        @comment.save
                        flash[:notice] = "Comment is Added Sucussfully"
                    else
                      @comment.update_attribute(:description => value)
                      flash[:notice] = "Comment is Updated Sucussfully"
                   end
            end
          redirect_to :action =>:index
        end
      else
        flash[:notice] = "#{t('deny_permission')}"
        redirect_to :controller => :exam, :action => :index
      end
    rescue Exception => e
      Rails.logger.info "Exception in terms controller, house_parent_comment action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :index
    end
  end

  def term_boarding_students
  @students = []
  @term = Term.find(params[:term_id])
  @batch = @term.batch
      unless @batch.students.blank?
    @batch.students.each do |student|
      category = student.student_category
      unless category.nil?
        if category.name == "BOARDER"
          @students << student
        end
      end
    end
    end
    render(:update) do |page|
      page.replace_html   'term_boarding_students', :partial=>"term_boarding_students"
    end
  end

  def boarding_students
    @students = []
    @batch = Batch.find(params[:batch_id])
    @terms = @batch.terms
    render(:update) do |page|
      page.replace_html   'boarding_students', :partial=>"boarding_students"
    end
  end

  def update_batch
    begin
      @batch = Batch.find_all_by_course_id(params[:course_name], :conditions => { :is_deleted => false, :is_active => true })
      render(:update) do |page|
        page.replace_html 'update_batch', :partial=>'update_batch'
      end
    rescue Exception => e
      Rails.logger.info "Exception in report_builders controller, update_batch action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def view_comments

  end

  def view_house_parent_comments
    begin
      if @current_user.admin?
        @course= Course.find(:all,:conditions => { :is_deleted => false }, :order => 'code asc')
      end
    end
  end

  def view_tutor_comments
    begin
      if @current_user.admin? or @current_user.employee?
        @course= Course.find(:all,:conditions => { :is_deleted => false }, :order => 'code asc')
      end
    end
  end

  def tutor_comment_list
    begin
      @tutor_comments = TutorComment.find_all_by_batch_id(params[:batch_id])
      render(:update) do |page|
        page.replace_html 'tutor_comments', :partial=>'tutor_comment_list'
      end
    rescue Exception => e
      Rails.logger.info "Exception in report_builders controller, update_batch action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def house_parent_comment_list
    begin
      @house_parent_comments = HouseParentComment.find_all_by_batch_id(params[:batch_id])
      render(:update) do |page|
        page.replace_html 'house_parent_comment', :partial=>'house_parent_comment_list'
      end
    rescue Exception => e
      Rails.logger.info "Exception in report_builders controller, update_batch action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :home
    end
  end

  def update_batches
    @batches = []
    templates = AssignTemplate.find_all_by_report_builder_id(params[:template_id])
    templates.each do |template|
      @batches << Batch.find(template.batch_id) rescue ""
    end

    render(:update) do |page|
      page.replace_html 'template_batch', :partial=>'update_batches'
    end
  end

  def reload_server
    system("svn up #{RAILS_ROOT}/vendor/plugins/pinnacle_term_exam")
    system("cd #{RAILS_ROOT} && rake fedena:install_multischool RAILS_ENV=production")
    system("cd #{RAILS_ROOT} && rake fedena:seed_schools RAILS_ENV=production")
    system("cd #{RAILS_ROOT} && touch tmp/restart.txt")
  end

  def authenticate_report_builder
    begin
      @config_key = Configuration.find_by_config_key('TermwiseExamManagement')
      if @config_key.nil?
        Configuration.create(:config_key => 'TermwiseExamManagement',:config_value => false)
      end
      #      @config_key = Configuration.find_by_config_key('TermwiseExamManagement')
      #      if @config_key.config_value == "0"
      #        redirect_to :controller => "exam", :action => "index"
      #        flash[:notice] = "#{t('enable_term_wise_management')}"
      #      end
    rescue Exception => e
      Rails.logger.info "Exception in report builders controller, authenticate_report_builder action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :index
    end
  end


  def delete_img
    begin
      File.delete(params[:path]) if File.exist?(params[:path])
      redirect_to :action => "image_list"
    rescue Exception => e
      Rails.logger.info "Exception in report builders controller, delete_img action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :index
    end
  end

  def upload_image
    begin
      if params[:upload].present?
        file = params[:upload][:datafile]
        File.open(Rails.root.join('public', 'report_builder_images', file.original_filename), 'wb') do |f|
          f.write(file.read)
        end
      end
      redirect_to :action => "image_list"
    rescue Exception => e
      Rails.logger.info "Exception in report builders controller, upload_image action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :index
    end
  end

  def check_privileges
    @term_privileges = @current_user.privileges.collect(&:name)
  end

  def student_wise_display_report
    begin
      #system("rsync -ruv /srv/fedena_app/rank_fedena/uploads/ /srv/fedena_app/rank_fedena/public/images/uploads/")
      if @current_user.privileges.include?(Privilege.find_by_name("TermManager")) or @current_user.admin? or @current_user.student? or @current_user.parent?
        @templates = ReportBuilder.all
        @batches = Batch.active
        if params[:student_id]
          @student_batches =[]
          student = Student.find(params[:student_id])
          bs = student.batch_students
          unless bs.empty?
            bs.each do |s|
              @student_batches << Batch.find(s.batch_id)
            end
          end
          @student_batches << student.batch
        end
      else
        flash[:notice] = "#{t('deny_permission')}"
        redirect_to :controller => :exam, :action => :index
      end
    rescue Exception => e
      Rails.logger.info "Exception in report builders controller, student_wise_report action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :index
    end
  end

  def student_display
    #begin
      # pdf report is generated for selected student params
      @student_ids = []
      @term_exam_groups =[]
      @weighted_exam_groups = []
      @term_ids =[]
      @weighted_total = 0
      query = []
      @sorted_description =[]
      if params[:student]
        params[:student].each do |key,value|
          @student_ids << key if value.to_i == 1
        end
      elsif params[:student_id]
        @student_ids = params[:student_id].to_a
        @current_student = Student.find(params[:student_id])
        @current_batch = @current_student.batch.id
        if !params[:batch_id].empty?
          if @current_student.batch.id.to_i != params[:batch_id].to_i
            bs = @current_student.batch_students
            batch_student = BatchStudent.find_by_student_id_and_batch_id(@current_student.id,params[:batch_id]) unless bs.empty?
            unless batch_student.nil?
              @current_student.update_attribute :batch_id, params[:batch_id].to_i
            end
          end
        end
      end
      if params[:term]
        params[:term].each do |key,value|
          @term_ids << key if value.to_i == 1
        end
        unless @term_ids.empty?
          @term_ids.each do |term_id|
            @term_exam_groups << Term.find(term_id).exam_groups
          end
          @term_exam_groups = @term_exam_groups.compact.flatten
        end
        unless @term_exam_groups.empty?
          @term_exam_groups.each do |term_exam_group|
            term_group = TermExamGroup.find_by_exam_group_id(term_exam_group.id)
            if term_group.weightage > 5
              if term_group.weightage == 60
                @comment_exam_group = ExamGroup.find(term_group.exam_group_id)
              end
              @weighted_exam_groups <<  ExamGroup.find(term_group.exam_group_id)
              @maximum_marks = Exam.find_by_exam_group_id(term_group.exam_group_id).maximum_marks.to_f
              if @maximum_marks > 5
                Rails.logger.info @maximum_marks
              @weighted_total = @weighted_total + @maximum_marks
              end
            end
          end
        end
      end
      if  params[:batch_id].empty?
        flash[:notice] = "#{t('select_a_batch_to_continue')}"
        redirect_to :action=> 'student_wise_report' and return
      elsif params[:template_id].nil?
      elsif params[:template_id].empty?
        flash[:notice] = "#{t('select_a_template_to_continue')}"
        redirect_to :action=> 'student_wise_report' and return
      elsif @student_ids.empty?
        flash[:notice] = "#{t('select_a_student_to_continue')}"
        redirect_to :action=> 'student_wise_report' and return
      elsif @term_ids.empty?
        flash[:notice] = "Select a term to continue"
        redirect_to :action=> 'student_wise_report' and return
      end
      if params[:template_id].nil?
        assign_template =  AssignTemplate.find_all_by_batch_id(params[:batch_id])
        assign_template.each do |template|
          if @term_ids.first == template.term_id.first
            assign_template = AssignTemplate.find(template.id)
            @report = ReportBuilder.find(assign_template.report_builder_id)
          end
        end
      else
        @report = ReportBuilder.find(params[:template_id])
      end
      @student_ids = ReportBuilder.sort_by_first_name(@student_ids)
      @student_ids.each do |student_id|
        # the content of tinymce description part whole thing is splitted then it is made loop one by one as word
        description = @report.description.split
        description.each_with_index do |word, i|
          @value = ReportBuilder.match_string(query, word, student_id, i, params[:template_id],@term_ids).to_s
        end
        @sorted_description << @value
        query = []
      end
#     PDFKit.configure do |config|
#        config.wkhtmltopdf = '/opt/wkhtmltopdf'
#        config.default_options = {
#          :page_size     => 'Letter',
#          :margin_top    => '0.3in',
#          :margin_right  => @report.page_orientation == 'Landscape' ? '0.3in' : '0.5in',
#          :margin_bottom => '0.1in',
#          :margin_left   => @report.page_orientation == 'Landscape' ? '0.3in' : '0.5in',
#          :print_media_type => true,
#          #:header_right =>  "Page [page]",
#          :orientation => @report.page_orientation == 'Landscape' ?  'Landscape' : 'Portrait'
#        }
#      end
        #render :layout => false
#      rescue Exception => e
#      Rails.logger.info "Exception in report builders controller, student_display action"
#      Rails.logger.info e
#      flash[:notice] = "Sorry, something went wrong. Please inform administration"
#      redirect_to :action => :index
#    end
  end

  def batch_wise_report_terms
    begin
      @batch_terms=[]
      @batch = Batch.find(params[:batch_id])
      #      @batch_terms = @batch.terms
      if params[:profile]
         @batch_terms = @batch.terms
#        assign_template = AssignTemplate.find_all_by_batch_id(params[:batch_id])
#        unless assign_template.empty?
#        assign_template.each do |template|
#          @batch_terms << Term.find(template.term_id.first)
#        end
#      end
      else
        assign_template = AssignTemplate.find_by_batch_id_and_report_builder_id(params[:batch_id],params[:template_id]).to_a
      unless assign_template.empty?
        assign_template.each do |template|
          @batch_terms = Term.find_all_by_id(template.term_id)
        end
      end
      end
      render(:update) do |page|
        page.replace_html 'batch_wise_report_terms',:partial=>'batch_wise_report_terms'
      end
    rescue Exception => e
      Rails.logger.info "Exception in report builders controller, batch_wise_report_terms action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :index
    end
  end
  def batch_wise_report_batches
    @batches = []
    templates = AssignTemplate.find_all_by_report_builder_id(params[:template_id])
    templates.each do |template|
      @batches << Batch.find(template.batch_id) rescue ""
    end
    render(:update) do |page|
        page.replace_html 'batch_wise_report_batches',:partial=>'batch_wise_report_batches'
      end
  end
end
