class AlphaNumericBatchesController < ApplicationController
	def index
	end

	def create_alpha_numeric_batch
		@batch= Batch.active
		if request.post?
			if params[:alpha_numeric_batches].present?
				@alpha_numeric_batch=AlphaNumericBatch.find_by_batch_id_and_term_id_and_exam_group_id(params[:alpha_numeric_batches][:batch_id],params[:alpha_numeric_batches][:term_id],params[:alpha_numeric_batches][:exam_group_id])
				if @alpha_numeric_batch.nil?
					AlphaNumericBatch.create(:exam_type=>params[:alpha_numeric_batches][:exam_type],:batch_id=>params[:alpha_numeric_batches][:batch_id],:term_id=>params[:alpha_numeric_batches][:term_id],:exam_group_id=>params[:alpha_numeric_batches][:exam_group_id])
				else
					@alpha_numeric_batch.update_attributes(:exam_type=>params[:alpha_numeric_batches][:exam_type],:exam_group_id=>params[:alpha_numeric_batches][:exam_group_id])
				end
			end
			flash[:notice] = "#{t('alpha_numeric_batch_assigned_sucessfully')}"
			redirect_to :action => "create_alpha_numeric_batch"
		end
	end

	def list_terms
		if params[:batch_id].present? 
			@terms=Term.find_all_by_batch_id(params[:batch_id])
			render(:update) do |page|
        page.replace_html 'list_terms', :partial=>"list_terms"
      end
    else
      render(:update) do |page|
        page.replace_html 'list_terms', :text=>""
      end
    end
	end

	def list_exam_groups
		if params[:term_id].present? 
			@term=Term.find(params[:term_id])
			@exam_groups=@term.exam_groups
			render(:update) do |page|
        page.replace_html 'list_exam_groups', :partial=>"list_exam_groups"
      end
    else
      render(:update) do |page|
        page.replace_html 'list_exam_groups', :text=>""
      end
    end
	end

	def list_batches
		if @current_user.admin?
			@batch= Batch.active.uniq
		elsif @current_user.employee?
			employee = Employee.find_by_user_id(@current_user.id)
			emp_subjects = employee.employees_subjects
			batches = employee.batches.is_active
			batch1 = []
			emp_subjects.each do |emp_sub|
				subject = Subject.find_by_id(emp_sub.subject_id)
				batch = Batch.find_by_id(subject.batch_id)
				if batch.is_active
					batch1 << batch
				end
			end
			@batch = (batch1 + batches).uniq
		end

	end

	def list_alpha_terms
		if params[:batch_id].present?
			@terms=[]
			@batch_terms=[] 
			@alpha_numeric_batches=AlphaNumericBatch.find_all_by_batch_id(params[:batch_id])
			@alpha_numeric_batches.each do |alpha|
				# @terms<<alpha.term
				@batch_terms<<alpha.term
			end			
			render(:update) do |page|
        page.replace_html 'list_alpha_terms', :partial=>"list_alpha_terms"
      end
    else
      render(:update) do |page|
        page.replace_html 'list_alpha_terms', :text=>""
      end
    end
	end

	def alpha_numeric_exams
		if params[:term_id].present?
			@exam_groups = []
			@alpha_batches=AlphaNumericBatch.find_all_by_term_id(params[:term_id])
			@term = Term.find(params[:term_id])
			@alpha_batches.each do |alpha_batch|
      	@exam_groups << alpha_batch.exam_group
      end
		else
		end
	end
	def manage_alpha_numeric_exam
		@term=Term.find params[:term_id]
		@exam_group=ExamGroup.find(params[:exam_group_id])
		@batch=Batch.find params[:batch_id]
		@alpha_batch=AlphaNumericBatch.find_by_batch_id_and_term_id_and_exam_group_id(params[:batch_id],params[:term_id],params[:exam_group_id])
		@alpha_numeric_exams=AlphaNumericExam.find_all_by_alpha_numeric_batch_id(@alpha_batch.id)
	end

	def alpha_numeric_exam_details
		@sub_ids=[]
		@alpha_numeric_id=AlphaNumericBatch.find_by_batch_id_and_term_id_and_exam_group_id(params[:batch_id],params[:term_id],params[:exam_group_id])
		@exam_group=ExamGroup.find(params[:exam_group_id])
		@subject_ids=Exam.find_all_by_exam_group_id(params[:exam_group_id]).collect(&:subject_id)
		@alpha_exam=AlphaNumericExam.find_all_by_alpha_numeric_batch_id(@alpha_numeric_id.id).collect(&:subject_id)
		@alpha_exam.each do |a|
			@sub_ids<<@subject_ids.reject! {|s| s==a}
		end
		unless @sub_ids.empty?
			@subjects=Subject.find(@sub_ids)
		else
			@subjects=Subject.find(@subject_ids)
		end
	end

	def alpha_numeric_exam_save
		if request.post? and params[:alpha_numeric_exam].present? and params[:alpha_numeric].present?
			params[:alpha_numeric].each do |f, details|
				if details[:no_exam] == "1"

				else
					unless details[:start_date].blank? and details[:end_date].blank? and details[:no_exam] == "0"
							@alpha_numeric_exam = AlphaNumericExam.new(:alpha_numeric_batch_id=>params[:alpha_numeric_exam][:alpha_numeric_id], :max_grade_score=>details[:max_grade_score], :min_grade_score=>details[:min_grade_score], :start_time=>details[:start_date], :end_time=>details[:end_date], :subject_id=>f, :no_exam=>details[:no_exam])
						  @alpha_numeric_exam.save
					else
						# flash[:warn_notice] = "#{t('start time and end time cant be blank')}"
						return redirect_to :action => "alpha_numeric_exam_details", :exam_group_id=>params[:alpha_numeric_exam][:exam_group_id], :term_id=> params[:alpha_numeric_exam][:term_id], :batch_id=>params[:alpha_numeric_exam][:batch_id]
				  end
			  end
			end
			flash[:notice]="#{t('exam_added_successfully')}"
			redirect_to  :action => "manage_alpha_numeric_exam", :exam_group_id=>params[:alpha_numeric_exam][:exam_group_id], :term_id=> params[:alpha_numeric_exam][:term_id], :batch_id=>params[:alpha_numeric_exam][:batch_id]
		end
	end

	def exam_mark
		@alpha_exam=AlphaNumericExam.find params[:id]
		@alpha_batch=AlphaNumericBatch.find @alpha_exam.alpha_numeric_batch_id
		@batch = Batch.find(@alpha_batch.batch_id)
		@subject=Subject.find params[:subject_id]
	  @students = @batch.students.all
	end

	def alpha_numeric_exam_scores
		@alpha_exam = AlphaNumericExam.find params[:alpha_numeric_exam_id]
	  @alpha_batch=AlphaNumericBatch.find @alpha_exam.alpha_numeric_batch_id
		@batch = Batch.find(params[:batch_id])
		@subject=Subject.find params[:subject_id]			
    absent_students = params[:absent_students].present? ? params[:absent_students] : []
    @error= false
    params[:exam].each_pair do |student_id, details|	     

      @exam_score = AlphaNumericScore.find(:first, :conditions => { :student_id => student_id,:batch_id=>@batch.id,:alpha_numeric_exam_id=>@alpha_exam.id, :subject_id => params[:subject_id]})

      if @exam_score.nil?
        if absent_students.include?(student_id.to_s)
          AlphaNumericScore.create(:alpha_numeric_exam_id=>@alpha_exam.id,:batch_id=>@batch.id,:student_id=>student_id,:term_group_id=>@term_group.id, :is_absent => true, :subject_id => params[:subject_id])
        else
          if details[:marks_grade].present?
            # if details[:marks] <= @alpha_exam.maximum_marks
              AlphaNumericScore.create do |score|
                score.batch_id          			= @batch.id
                score.alpha_numeric_exam_id   = @alpha_exam.id
                score.student_id       				= student_id
                score.is_absent								= false
                score.marks_grade            	= details[:marks_grade]
                score.subject_id       				= params[:subject_id]
              end
            # else
            #   @error = true
            # end
          end
        end
      else
        save_flag=0
        absent_flag = false
        if absent_students.include?(student_id.to_s)
          details[:marks_grade] = nil   
          save_flag = 1
          absent_flag = true
        else
          save_flag = 1 if (details[:marks_grade].present?)
          absent_flag = false
        end
        # if details[:marks] <= @alpha_exam.maximum_marks ||  details[:grade] <= @alpha_exam.maximum_grade
          if save_flag == 1
            if @exam_score.update_attributes(details) && @exam_score.update_attributes(:is_absent => absent_flag)
            else
              flash[:warn_notice] = "#{t('score_not_updated_successfully')}"
              @error = nil
            end
          else
            @exam_score.destroy
          end
        # else
        #   @error = true
        # end
      end
    end
    # flash[:warn_notice] = "#{t('marks_should_not_be_greater_than_max_marks')}" 
    # if @error == true
    flash[:notice] = "#{t('score_added_successfully')}" 
    # if @error == false
    redirect_to :action => "exam_mark", :id => @alpha_exam.id, :subject_id => params[:subject_id]
	end

	def edit_alpha_numeric_exam
		@alpha_exam = AlphaNumericExam.find params[:id]
		# @batch = Batch.find(params[:batch_id])
		# puts"-----#{@batch.id.inspect}-------"
		@alpha_batch=AlphaNumericBatch.find(@alpha_exam.alpha_numeric_batch_id)
		@exam_group=ExamGroup.find(@alpha_batch.exam_group_id)
		@subject_ids=Exam.find_all_by_exam_group_id(@alpha_batch.exam_group_id).collect(&:subject_id)
		@subjects=Subject.find(@subject_ids)
    @subjects.reject!{|s| (@alpha_batch.alpha_numeric_exams.all(:conditions=>"id NOT IN(#{@alpha_exam.id})").map{|e| e.subject_id}.include?(s.id))}
    if request.post?
			@alpha_exam.update_attributes(:max_grade_score=>params[:alpha_exam][:max_grade_score], :min_grade_score=> params[:alpha_exam][:min_grade_score], :start_time=>params[:alpha_exam][:start_time], :end_time=>params[:alpha_exam][:end_time], :subject_id =>params[:alpha_exam][:subject_id] )

			flash[:notice]="#{t('alpha_numeric_exam_updated_successfully')}"
		  redirect_to :action => "manage_alpha_numeric_exam", :term_id=>@alpha_batch.term_id, 
		  :batch_id=> @alpha_batch.batch_id, :exam_group_id=> @alpha_batch.exam_group_id
		end
  end
end
