class RankIngresSmsModulesController < ApplicationController
 before_filter :login_required
  filter_access_to :all
  def assign_students
    @batches= Batch.active
    if request.post?
      sms_module = RankIngresSmsModule.find_by_batch_id(params[:students][:batch_id])
      unless  sms_module.nil?
        unless params[:students][:student_ids].blank?
          sms_module.update_attributes(params[:students])
        else
          sms_module.destroy
        end
      else
        RankIngresSmsModule.create(params[:students])
      end
    end
  end

  def student_list
    @dnd_students = []
    batch = Batch.find(params[:batch_id])
    dnd_students = RankIngresSmsModule.find_by_batch_id(params[:batch_id])
    unless dnd_students.nil?
      dnd_students.student_ids.each do |ds|
        @dnd_students << Student.find_by_id(ds)
      end
    end
    @students = batch.students
    render :update do |page|
      page.replace_html 'students', :partial => 'students'
    end
  end

  def  send_sms
    @batches= Batch.active
    if request.post?
      if (params[:students][:attendance_date]).to_date <= Date.today
        remove_students = []
      batch = Batch.find(params[:students][:batch_id])
      dnds = RankIngresSmsModule.find_by_batch_id(params[:students][:batch_id])
      dnd_ids = dnds.student_ids unless dnds.nil?
      absentees = Attendance.find_all_by_batch_id_and_month_date(params[:students][:batch_id],params[:students][:attendance_date])
      as_ids = absentees.map {|s| s.student_id} unless absentees.nil?
      rs_ids = ((dnd_ids.nil? ? dnd_ids = [] :  dnd_ids)  + (as_ids.nil? ? as_ids = [] :  as_ids))
      unless rs_ids.nil? or rs_ids.blank?
      rs_students = rs_ids.uniq
        rs_students.each do |s|
      remove_students << Student.find_by_id(s)
      end
      end
      students = batch.students - remove_students
      unless students.nil? or students.blank?
        students.each do |s|
          sms_setting = SmsSetting.new()
          if sms_setting.application_sms_active and s.is_sms_enabled and sms_setting.attendance_sms_active
            recipients = []
            unless Configuration.find_by_config_key('StudentAttendanceType').config_value=="SubjectWise"
              guardian_message = "#{t('dear_parent')}, #{s.first_and_last_name} #{t('student_present')} #{format_date(params[:students][:attendance_date])}. #{t('thanks')}"
            end
            recipients = []
            if sms_setting.parent_sms_active
              if s.immediate_contact.present?
                guardian = Guardian.find(s.immediate_contact_id)
                recipients.push guardian.mobile_phone.split(',') if guardian.mobile_phone.present?
                if recipients.present?
                  recipients.flatten!
                  recipients.uniq!
                  Delayed::Job.enqueue(SmsManager.new(guardian_message,recipients))
                end
              end
            end
          end
        end
      end
       flash[:notice] = "#{t('message_sent')}"
      else
        flash[:notice] = "#{t('select_a_valid_date')}"
      end
    end
  end
  
end

