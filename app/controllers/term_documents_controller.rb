require 'rank_ingres_deadlines_helper'
class TermDocumentsController < ApplicationController
   include RankIngresDeadlinesHelper
   #protect_from_forgery :only => :download_document
   protect_from_forgery :only => :show_docs

	def show_docs
		@api = "3ab6e857fd4073f1901a1109eb1c12df"
		@documents = TermDocument.find(:all)
		@http = "http://"
		@ip = request.request_uri
		@school = MultiSchool.current_school
		@domain = SchoolDomain.find_by_linkable_id(@school.id).domain
	end
   def save_document
     name =params[:filename].read
     directory = "#{Rails.root}/public/system/term_documents/"
     path = File.join(directory, name)
     Rails.logger.info path
     File.open(path, "wb") { |f| f.write(params[:content].read) }
    
  end
	def index
		# @api = "3ab6e857fd4073f1901a1109eb1c12df"
		# @documents = TermDocument.find(:all)
		# @http = "http://"
		# @ip = request.request_uri
		# @school = MultiSchool.current_school
		# @domain = SchoolDomain.find_by_linkable_id(@school.id).domain
	end
  def downloa_all
     term  = Term.find(params[:term_id])
     students   = term.batch.students
     t = Tempfile.new('tmp-zip-' + request.remote_ip)
     Zip::ZipOutputStream.open(t.path) do |zos|
     students.each do |s|
        path = "#{Rails.root}/public/system/term_documents/#{term.id}-#{s.id}.xls"
        zos.put_next_entry("#{s.full_name}.xls")
        zos.print IO.read(path)
    end
  end
  send_file t.path, :type => "application/zip", :filename => "#{term.batch.name}-#{term.name}.zip"
  t.close
  end
	def new
		@document = TermDocument.new(params[:term_document])
    if request.post?
      if @document.save
        flash[:notice] = "File Uploaded Successfully"
        redirect_to :action => :index
      else
        flash[:notice] = "File can only be XLS"
        redirect_to :action => :new
      end
    end
	end

  def document_list
    @document = TermDocument.find(params[:term_document_id])
    @assigned_batches = TermDocumentBatch.find_all_by_term_document_id(params[:term_document_id])
       render(:update) do |page|
        page.replace_html 'assigned_batch',:partial=>'assigned_batches'
      end
  end

	def destroy
		@document = TermDocument.find(params[:id])
    if @document.delete
      flash[:notice] = "File Deleted Successfully"
      redirect_to :action => :show_docs
    end
	end

  def destroy_assigned_docs
    @assigned_term = TermDocumentBatch.find_by_term_id(params[:id])
    if @assigned_term.delete
      students = Term.find(params[:id]).batch.students
      unless students.blank?
       students.each do |student|
         path = "#{Rails.root}/public/system/term_documents/#{params[:id]}-#{student.id}.xls"
         File.delete(path) if File.exist?(path)
       end
      end
      flash[:notice] = "File Deleted Successfully"
    else
      flash[:notice] = "Error While Deleting File"
    end
    redirect_to  :action => :assign_doc
  end

	def download_doc
		@document = TermDocument.find params[:id]
    unless @document.nil?
      @document.document.path
    end
	end

	def assign_doc
		@documents = TermDocument.find(:all)
		@batches = Batch.active
		if request.post?
			@term_document_batch = TermDocumentBatch.find_by_term_id(params[:term_id])
			if @term_document_batch.nil?
        @term_batch = TermDocumentBatch.new(:batch_id => params[:doc_terms][:batch_id],:term_id => params[:term_id],:term_document_id => params[:doc_terms][:term_document_id])
        if @term_batch.save
          @document = TermDocument.find(@term_batch.term_document_id)
          @students= Batch.find(@term_batch.batch_id).students
          @term= Term.find(@term_batch.term_id)
          @students.each do |student|
          student_report_to_xls(student,@document.document.path,@term)
          end
          flash[:notice] = "Successfully Assigned"
          redirect_to :action => :assign_doc
        end
			else
				@term_document_batch.update_attributes(:term_document_id => params[:doc_terms][:term_document_id])
        @document = TermDocument.find(@term_document_batch.term_document_id)
        @students= Batch.find(@term_document_batch.batch_id).students
        @term= Term.find(@term_document_batch.term_id)
        @students.each do |student|
         student_report_to_xls(student,@document.document.path,@term)
        end
				flash[:notice] = "Term Assignment has changed with Selected Document"
				redirect_to :action => :assign_doc
			end
		end
	end

	def list_terms
		@batch = Batch.find(params[:batch_id])
		@terms = @batch.terms
		render :update do |page|
      page.replace_html 'list-terms', :partial => 'list_terms'
    end
	end

	def download_document
    batch_ids = TermDocumentBatch.all.map {|t| t.batch_id}
     #TermDocumentBatch.delete_all
		@batches = Batch.active.find(batch_ids)
	end

	def list_batch_terms
		@batch = Batch.find(params[:batch_id])
		@terms = @batch.terms
		render :update do |page|
      page.replace_html 'list-terms', :partial => 'list_batch_terms'
    end
	end

  def batch_terms
    @student_id = params[:student_id]
     @batch = Batch.find(params[:batch_id])
		@terms = @batch.terms
    render :update do |page|
      page.replace_html 'update_batch_terms', :partial => 'update_batch_terms'
    end
   end

  def student_files
    @term= Term.find(params[:term_id])
    @assigned = TermDocumentBatch.find_by_term_id(@term)
    @student = Student.find(params[:student_id])
     @http = "http://"
		@api = "3ab6e857fd4073f1901a1109eb1c12df"
		@school = MultiSchool.current_school
		@domain = SchoolDomain.find_by_linkable_id(@school.id).domain
      render :update do |page|
      page.replace_html 'student_file', :partial => 'student_file'
    end
  end

	def list_students
		@term = Term.find(params[:term_id])
		@students = @term.batch.students
		@batch = Batch.find(params[:batch_id])
		@http = "http://"
		@api = "3ab6e857fd4073f1901a1109eb1c12df"
		@term_document_batch = TermDocumentBatch.find_by_term_id(@term.id)
    @school = MultiSchool.current_school
		@domain = SchoolDomain.find_by_linkable_id(@school.id).domain
		unless @term_document_batch.nil?
			@term_document = TermDocument.find(@term_document_batch.term_document_id)
			unless @term_document.nil?
        # send_file  @term_document.document.path , :type=>@term_document.document_content_type
        @file = File.read(@term_document.document.path)
      end
    else
      flash[:notice] = "No Document for #{@term.name}"
      return redirect_to :action => :download_document
    end
		render :update do |page|
      page.replace_html 'list-students', :partial => 'list_students'
    end
	end

	def download_studentwise_document
		@student = Student.find(params[:id])
		@term_document_batch = TermDocumentBatch.find_by_term_id(params[:term_id])
		@term = Term.find(params[:term_id])
		unless @term_document_batch.nil?
			@term_document = TermDocument.find(@term_document_batch.term_document_id)
			unless @term_document.nil?
        send_file  @term_document.document.path , :type=>@term_document.document_content_type
      end
    else
      flash[:notice] = "No Document for #{@term.name}"
      return redirect_to :action => :download_document
    end
	end

  def student_doc
    unless params[:term_student_document][:document].nil?
      if (StudentTermDocument.find_by_student_id_and_term_id(params[:term_student_document][:student_id],params[:term_student_document][:term_id]).present?)
        @doc_present = StudentTermDocument.find_by_student_id_and_term_id(params[:term_student_document][:student_id],params[:term_student_document][:term_id])
        @doc_present.update_attributes(params[:term_student_document])
        redirect_to :action => :download_document
      else
        @document = StudentTermDocument.new(params[:term_student_document])
        if @document.save
          redirect_to :action => :download_document
        end
      end
    end

  end

  def download_attachment
    @document = StudentTermDocument.find params[:id]
    unless @document.nil?
      send_file  @document.document.path , :type=>@document.document_content_type
    end	
  end

  def excel_report
    @student = Student.find(params[:student_id])
    @all_batches = @student.all_batches
  end
end