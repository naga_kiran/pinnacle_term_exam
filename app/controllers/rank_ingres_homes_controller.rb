class RankIngresHomesController < ApplicationController
  require 'fastercsv'

  
def broadsheet_csv_generation
    flash[:clear]
    begin
     term_exam_groups = []
     subjects2 = []
      term = Term.find(params[:term_id])
      batches = term.batch

      #Calculating template to check only for evaluation group and evaluation type
      assigned_template =AssignTemplate.find_by_batch_id(batches.id)
      report_builder=ReportBuilder.find(assigned_template.report_builder_id)
      @groups = term.batch.exam_groups
      term.evaluation_groups.each do |ev_group|
        ev_group.evaluation_types.each do |ev_tp|
          subjects2 << ev_tp.name
        end
      end      

      start_date = batches.start_date.to_date
      end_date = batches.end_date.to_date > Date.today ? batches.end_date.to_date : Date.today
      if start_date.year == end_date.year
        session = "#{start_date.year}"
      else
        session = "#{start_date.year}/#{end_date.strftime("%y")}"
      end
      students = batches.students
      subjects =  batches.subjects.reject {|s| s.no_exams == true}
      exam_groups = term.term_exam_groups
      exam_groups.each do |examgroup|
        exam =  Exam.find_by_exam_group_id(examgroup.exam_group_id)
        unless exam.nil?
          max_marks = exam.maximum_marks.to_f
          if max_marks > 5
            term_exam_groups << examgroup
          end
        end
      end
      exams = Exam.find_all_by_exam_group_id(exam_groups.collect(&:exam_group_id))
      subject_ids = exams.collect(&:subject_id)
      subjects = subjects.reject{|sub| !(subject_ids.include?(sub.id))}
      merged_subjects = RankIngresSubjectMerger.find_all_by_batch_id(batches.id ) 
      unless merged_subjects.nil?  
       merged_subjects.each do |ms|   
         subjects = subjects.reject {|sub| ms.subject_id.include? sub.id.to_s}  
       end
     end
      payroll_csv = FasterCSV.generate({:col_sep => "\t"}) do |csv|
        #Displaying heading for different kind templates
        if report_builder.name == "Junior end of term report(Ikoyi)"
          csv << ["Name","Admission","Gender"] + subjects2 +["Total","Average"]
        elsif report_builder.name == "Junior End of Term Report"
          csv << ["Name","Admission","Gender"] + subjects2 +["Total","Average"]
        else
          csv << ["Name","Admission","Gender"] + subjects.map {|s| s.name} + merged_subjects.map {|s| s.name} +["Total","Average"]
        end


    if report_builder.name == "Junior end of term report(Ikoyi)" 
          
       h1={}
      overall = 0
      students.each do |student|
      row_avg = 0
      abc = 0
      row_avg_count = 0
      total_bc =[]
        term.evaluation_groups.each do |ev_group|
        total1 = 0
        count = 0
        ev_group.evaluation_types.each do |ev_type|
          score = EvaluationScore.find_by_evaluation_type_id_and_student_id(ev_type.id,student.id)
          total = 0
          @groups.each do |exam_group|            
            total +=  (score.nil? ? 0 : score.exam_group_scores["#{exam_group.id}"].blank? ? 0 : score.exam_group_scores["#{exam_group.id}"]).to_f
          end
          unless h1["#{ev_type.id}-#{ev_type.name}#{student.gender}-#{student.id}"].nil?
                h1["#{ev_type.id}-#{ev_type.name}#{student.gender}-#{student.id}"] = h["#{ev_type.id}-#{ev_type.name}#{student.gender}-#{student.id}"]  + total
          else
            h1["#{ev_type.id}-#{ev_type.name}#{student.gender}-#{student.id}"] = total
          end
          total1 = total1 + total
          total_bc << total
          if total > 0 
            count += 1
          end
        end
      total1==0 ? "NA" : total1.to_i
      row_avg = row_avg + total1
      row_avg_count = row_avg_count + count
      if row_avg_count > 0
        abc = (row_avg/(row_avg_count)).to_f.round(2)
      else
        abc==0
      end
      end
      overall = overall + abc
      csv << [student.full_name,student.admission_no,student.gender] + total_bc  + [row_avg] + [abc]
    end 
        subject_total = []
        subject_average = []
        subject_student_count = []
        term.evaluation_groups.each do |ev_group|
         ev_group.evaluation_types.each do |ev_type| 
          string_numeric = []
          sub_name = "#{ev_type.id}-#{ev_type.name}"
          subject_overall = h1.select{ |k,v| k[sub_name]}
          subject_hash = Hash[*subject_overall.flatten]
          subject_value=(subject_hash.values).sum
          (subject_hash.values).each do |val|
            if val.to_i > 0    
              string_numeric << val
            end
          end   
          subject_student_count << string_numeric.count
          subject_total << subject_value
            if string_numeric.count > 0  
              if subject_value > 0
              subject_average << (subject_value/string_numeric.count).round(2)
              else
                subject_average << 0
              end
            else
              subject_average << 0
            end
          end
        end  
        csv << ["","",""] + subject_total + [subject_total.sum,overall] 
        csv << ["","","No of Students"] + subject_student_count +[students.count,students.count]
        if subject_average.all? {|x| x == 0}
          csv << ["","","Class Average"] + subject_average + [0 , 0]
        else 
          csv << ["","","Class Average"] + subject_average + [((subject_total.sum)/students.count).round(2),(overall/students.count).round(2)]
        end
  elsif report_builder.name == "Junior End of Term Report"
      h1={}
      overall = 0
      students.each do |student|
      row_avg = 0
      abc = 0
      row_avg_count = 0
      total_bc =[]
        term.evaluation_groups.each do |ev_group|
        total1 = 0
        count = 0
        ev_group.evaluation_types.each do |ev_type|
          score = EvaluationScore.find_by_evaluation_type_id_and_student_id(ev_type.id,student.id)
          total = 0
          @groups.each do |exam_group|            
            total +=  (score.nil? ? 0 : score.exam_group_scores["#{exam_group.id}"].blank? ? 0 : score.exam_group_scores["#{exam_group.id}"]).to_f
          end
          unless h1["#{ev_type.id}-#{ev_type.name}#{student.gender}-#{student.id}"].nil?
                h1["#{ev_type.id}-#{ev_type.name}#{student.gender}-#{student.id}"] = h["#{ev_type.id}-#{ev_type.name}#{student.gender}-#{student.id}"]  + total
          else
            h1["#{ev_type.id}-#{ev_type.name}#{student.gender}-#{student.id}"] = total
          end
          total1 = total1 + total
          total_bc << total
          if total > 0 
            count += 1
          end
        end
      total1==0 ? "NA" : total1.to_i
      row_avg = row_avg + total1
      row_avg_count = row_avg_count + count
      if row_avg_count > 0
        abc = (row_avg/(row_avg_count)).to_f.round(2)
      else
        abc==0
      end
      end
      overall = overall + abc
      csv << [student.full_name,student.admission_no,student.gender] + total_bc  + [row_avg] + [abc]
      puts "========#{total_bc.inspect}========="
    end 
        subject_total = []
        subject_average = []
        subject_student_count = []
        term.evaluation_groups.each do |ev_group|
         ev_group.evaluation_types.each do |ev_type| 
          string_numeric = []
          sub_name = "#{ev_type.id}-#{ev_type.name}"
          subject_overall = h1.select{ |k,v| k[sub_name]}
          subject_hash = Hash[*subject_overall.flatten]
          subject_value=(subject_hash.values).sum
          (subject_hash.values).each do |val|
            if val.to_i > 0    
              string_numeric << val
            end
          end   
          subject_student_count << string_numeric.count
          subject_total << subject_value
            if string_numeric.count > 0  
              if subject_value > 0
              subject_average << (subject_value/string_numeric.count).round(2)
              else
                subject_average << 0
              end
            else
              subject_average << 0
            end
          end
        end  
        csv << ["","",""] + subject_total + [subject_total.sum,overall] 
        csv << ["","","No of Students"] + subject_student_count +[students.count,students.count]
        if subject_average.all? {|x| x == 0}
          csv << ["","","Class Average"] + subject_average + [0 , 0]
        else 
          csv << ["","","Class Average"] + subject_average + [((subject_total.sum)/students.count).round(2),(overall/students.count).round(2)]
        end
      else

        h={}
        hash_weight = {}
        merged_hash_weight = {}         
        term_exam_groups.each do |exam_group|
          subjects.each do |subject|
            subject_exam = Exam.find_by_exam_group_id_and_subject_id(exam_group.exam_group_id,subject.id)
            if subject_exam.nil? then weightage = 0 else weightage = subject_exam.maximum_marks.to_f end
            unless hash_weight["#{subject.id}"].nil?
              hash_weight["#{subject.id}"] = hash_weight["#{subject.id}"]  + weightage
            else
              hash_weight["#{subject.id}"] = weightage
            end
          end
           
          merged_subjects.each do |merged_subject|
            merged_hash_weight_2 = {}
            
            merged_subject.subject_id.each do |subject_id|
              subject_exam = Exam.find_by_exam_group_id_and_subject_id(exam_group.exam_group_id,subject_id)
              if subject_exam.nil? then weightage = 0 else weightage = subject_exam.maximum_marks.to_f end
              unless merged_hash_weight_2["#{merged_subject.id}"].nil?
                merged_hash_weight_2["#{merged_subject.id}"] = merged_hash_weight_2["#{merged_subject.id}"]  + weightage
              else
                merged_hash_weight_2["#{merged_subject.id}"] = weightage
              end
            
            end
            merged_hash_weight_2["#{merged_subject.id}"] = (merged_hash_weight_2["#{merged_subject.id}"].to_f/merged_subject.subject_id.count).round(2)
            if merged_hash_weight["#{merged_subject.id}"].nil?
              merged_hash_weight["#{merged_subject.id}"] = 0
            else
              merged_hash_weight["#{merged_subject.id}"] = merged_hash_weight["#{merged_subject.id}"] + merged_hash_weight_2["#{merged_subject.id}"]
            end
            # merged_hash_weight_2["#{merged_subject.id}"] = 0
          end
        end
        overall_val = 0
        students.each do |student|
          term_exam_groups.each do |exam_group|
            subjects.each do |subject|
              cas_t = 0
              subject_exam = Exam.find_by_exam_group_id_and_subject_id(exam_group.exam_group_id,subject.id)
              subject_score = ExamScore.find_by_student_id_and_exam_id(student.id,subject_exam.id) unless subject_exam.nil?
              # Rails.logger.info ">>>>>>>>>#{subject_score.marks.to_f.inspect}>>>>#{subject.name}>>>"

              val_weight =  hash_weight["#{subject.id}"].to_s
              if subject_score.nil? 
              #CaTerm Calculation when no marks in current term exam              
              @ca_terms = Term.find(term.ca_term_id) unless term.ca_term_id.nil?
              cas_total =  (subject_score.nil? ? @ca_terms.nil? ? 0 :  (ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@ca_terms)/7.5).round(2).to_f :  subject_score.marks.nil? ? @ca_terms.nil? ? 0 : (ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@ca_terms)/7.5).round(2).to_f:  subject_score.marks).to_f
              
                @score = ((((cas_total*7.5).round(2))/val_weight.to_f)*100).round(2)
                Rails.logger.info "===#{@score.inspect}======#{student.full_name}=====#{subject.name}===="
              else percent = ((subject_score.marks.to_f/val_weight.to_f)*100)
                @score  = percent.nan? ? 0 : percent.to_f.round(2)
                Rails.logger.info "++#{@score.inspect}++++#{student.full_name}++++#{subject.name}++++"
              end
              cas_t += @score
              unless h["#{subject.id}-#{subject.name}#{student.gender}-#{student.id}"].nil?
                h["#{subject.id}-#{subject.name}#{student.gender}-#{student.id}"] = h["#{subject.id}-#{subject.name}#{student.gender}-#{student.id}"]  + cas_t
              else
                h["#{subject.id}-#{subject.name}#{student.gender}-#{student.id}"] = cas_t
              end
            end
            merged_subjects.each do |merged_subject|
              total_score = 0
              val_weight = 0
              term_exam_groups.each do |exam1|              
                merged_subject.subject_id.each do |subject_id|
                  val_weight += calculate_weitage_for_broadsheet(exam1.exam_group_id,subject_id)
                end
              end
              val_weight = (val_weight.to_f/merged_subject.subject_id.count).round(2)
              

              merged_subject.subject_id.each do |subject_id|
                subject_exam = Exam.find_by_exam_group_id_and_subject_id(exam_group.exam_group_id,subject_id)
                subject_score = ExamScore.find_by_student_id_and_exam_id(student.id,subject_exam.id) unless subject_exam.nil?
                # val_weight =  merged_hash_weight["#{merged_subject.id}"].to_s
                if subject_score.nil?

                  @ca_terms = Term.find(term.ca_term_id) unless term.ca_term_id.nil?
                  cas_total =  (subject_score.nil? ? @ca_terms.nil? ? 0 :  ReportBuilder.midterm_to_end_of_term(subject_id,student.id,@ca_terms).to_f :  subject_score.marks.nil? ? @ca_terms.nil? ? 0 : ReportBuilder.midterm_to_end_of_term(subject_id,student.id,@ca_terms).to_f:  subject_score.marks).to_f

                  @score = cas_total.to_f
                else percent = ((subject_score.marks.to_f/val_weight.to_f)*100)
                  @score  = percent.nan? ? 0 : percent.to_f.round(2)
                end
                total_score += @score
              end
              unless h["#{merged_subject.id}-#{merged_subject.name}#{student.gender}-#{student.id}"].nil?
                h["#{merged_subject.id}-#{merged_subject.name}#{student.gender}-#{student.id}"] = h["#{merged_subject.id}-#{merged_subject.name}#{student.gender}-#{student.id}"]  + (total_score/merged_subject.subject_id.count).round(2)
              else
                h["#{merged_subject.id}-#{merged_subject.name}#{student.gender}-#{student.id}"] = (total_score/merged_subject.subject_id.count).round(2)
              end
            end
          end
          arr_values = []
          total = 0
          subjects.each do |subject|
            val = h["#{subject.id}-#{subject.name}#{student.gender}-#{student.id}"].to_s
            Rails.logger.info "-----#{val.inspect}-----#{subject.name}------"
            arr_values <<  val.to_f
            total = total + (val.to_f)
          end
          merged_subjects.each do |merged_subject|
            val = h["#{merged_subject.id}-#{merged_subject.name}#{student.gender}-#{student.id}"].to_s
            arr_values <<  val.to_f
            total = total + (val.to_f)            
          end
          string_to_numeric = []
          arr_values.each do |val|
            if val.to_f > 0
              string_to_numeric << val
            end
          end
          size = string_to_numeric.count
         
        total == 0 ?  overall_val += 0  : overall_val += ((total/size).to_f).round(2)
          arr_values << total
            total == 0 ?  arr_values << total :  arr_values << ((total/size).to_f).round(2)
          csv << [student.full_name,student.admission_no,student.gender] + arr_values 
          puts ">>>>>>>>>#{arr_values.inspect}>>>>>>>>>>>>"
        end
        subject_total = []
        subject_average = []
        subject_student_count = []
        subjects.each do |subject|
           string_numeric = []
          sub_name = "#{subject.id}-#{subject.name}"
          subject_overall = h.select{ |k,v| k[sub_name]}
          subject_hash = Hash[*subject_overall.flatten]
          subject_value=(subject_hash.values).sum
          (subject_hash.values).each do |val|
            if val.to_i > 0    
              string_numeric << val
            end
          end
          subject_student_count << string_numeric.count
          subject_total << subject_value
          if string_numeric.count > 0
          subject_average << (subject_value/string_numeric.count).round(2)
          else
            subject_average << 0
          end
        end
        merged_subjects.each do |merged_subject|
           string_numeric = []
          sub_name = "#{merged_subject.id}-#{merged_subject.name}"
          subject_overall = h.select{ |k,v| k[sub_name]}
          subject_hash = Hash[*subject_overall.flatten]
          subject_value=(subject_hash.values).sum
          (subject_hash.values).each do |val|
            if val.to_i > 0    
              string_numeric << val
            end
          end
          subject_student_count << string_numeric.count
          subject_total << subject_value
          if string_numeric.count > 0
          subject_average << (subject_value/string_numeric.count).round(2)
          else
            subject_average << 0
          end
        end
        csv << ["","",""] +subject_total + [subject_total.sum,overall_val]
        csv << ["","","No of Students"] + subject_student_count +[students.count,students.count]
        if subject_average.all? {|x| x == 0}
        csv << ["","","Class Average"] + subject_average + [0 , 0]
      else
        csv << ["","","Class Average"] + subject_average + [((subject_total.sum)/students.count).round(2),(overall_val/students.count).round(2)]
      end

    #Evaluation Calculation for CSV
    # else
    #   h1={}
    #   overall = 0
    #   students.each do |student|
    #   row_avg = 0
    #   abc = 0
    #   row_avg_count = 0
    #   total_bc =[]
    #     term.evaluation_groups.each do |ev_group|
    #     total1 = 0
    #     count = 0
    #     ev_group.evaluation_types.each do |ev_type|
    #       score = EvaluationScore.find_by_evaluation_type_id_and_student_id(ev_type.id,student.id)
    #       total = 0
    #       @groups.each do |exam_group|
    #         total +=  (score.nil? ? 0 : score.exam_group_scores["#{exam_group.id}"].blank? ? 0 : score.exam_group_scores["#{exam_group.id}"]).to_f
    #         count = count + (score.nil? ? 0 : score.exam_group_scores["#{exam_group.id}"].blank? ? 0 : 1)
    #       end
    #       unless h1["#{ev_type.id}-#{ev_type.name}#{student.gender}-#{student.id}"].nil?
    #             h1["#{ev_type.id}-#{ev_type.name}#{student.gender}-#{student.id}"] = h["#{ev_type.id}-#{ev_type.name}#{student.gender}-#{student.id}"]  + total
    #       else
    #         h1["#{ev_type.id}-#{ev_type.name}#{student.gender}-#{student.id}"] = total
    #       end
    #       total1 = total1 + total
    #       total_bc << total
    #     end
    #   total1==0 ? "NA" : total1.to_i
    #   row_avg = row_avg + total1
    #   row_avg_count = row_avg_count + count
    #   abc = (row_avg/(row_avg_count/2)).to_f.round(2)
    #   end
    #   overall = overall + abc
    #   csv << [student.full_name,student.admission_no,student.gender] + total_bc  + [row_avg] + [abc]
    # end 

    #     subject_total = []
    #     subject_average = []
    #     subject_student_count = []
    #     term.evaluation_groups.each do |ev_group|
    #      ev_group.evaluation_types.each do |ev_type| 
    #       string_numeric = []
    #       sub_name = "#{ev_type.id}-#{ev_type.name}"
    #       subject_overall = h1.select{ |k,v| k[sub_name]}
    #       subject_hash = Hash[*subject_overall.flatten]
    #       subject_value=(subject_hash.values).sum
    #       (subject_hash.values).each do |val|
    #         if val.to_i > 0    
    #           string_numeric << val
    #         end
    #       end   
    #       subject_student_count << string_numeric.count
    #       subject_total << subject_value
    #         if string_numeric.count > 0  
    #           if subject_value > 0
    #           subject_average << (subject_value/string_numeric.count).round(2)
    #           else
    #             subject_average << 0
    #           end
    #         else
    #           subject_average << 0
    #         end
    #       end
    #     end  
    #     csv << ["","",""] + subject_total + [subject_total.sum,overall] 
    #     csv << ["","","No of Students"] + subject_student_count +[students.count,students.count]
    #     if subject_average.all? {|x| x == 0}
    #       csv << ["","","Class Average"] + subject_average + [0 , 0]
    #     else          
    #       csv << ["","","Class Average"] + subject_average + [((subject_total.sum)/students.count).round(2),(overall/students.count).round(2)]
    #     end
#Condition end
    end

  end
      send_data(payroll_csv, :type => 'text/csv; charset=utf-8; header=present', :filename => "#{batches.full_name}-#{term.name}-#{session}.csv")
    rescue Exception => e
      Rails.logger.info e
        if exam_groups.empty?
          flash[:notice] = "No exam created for the Batch"
            else
          flash[:notice] = "Something went wrong please check the data"
        end
      redirect_to :controller => :rank_ingres_homes, :action => :broadsheets
   end
  end
  
  def broadsheets
    begin
      if @current_user.admin?
          @batch = Batch.active
      elsif @current_user.is_a_batch_tutor?
            @employee = @current_user.employee_entry
            @batch = @employee.batches.uniq
      else
        flash[:notice] = "#{t('deny_permission')}"
          redirect_to :controller => :report_builders, :action => :report_and_term
      end
    rescue Exception => e
      Rails.logger.info "Exception in RankIngres controller, update_batch action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :controller => :report_builders,:action => :report_and_term
    end
  end

  def update_batch
    begin
      @batch = Batch.find_all_by_course_id(params[:course_name])
      render(:update) do |page|
        page.replace_html 'update_batch', :partial=>'update_batch'
      end
    rescue Exception => e
      Rails.logger.info "Exception in RankIngres controller, update_batch action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :index
    end
  end

  def batch_term
    begin
      @terms = Term.find_all_by_batch_id(params[:batch_id])
      render(:update) do |page|
        page.replace_html 'batch_terms', :partial=>'batch_terms'
      end
    rescue Exception => e
      Rails.logger.info "Exception in RankIngres controller, batch_term"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :index
    end
  end

  def term_exam_groups
    render(:update) do |page|
      page.replace_html 'terms_buttons', :partial=>'term_buttons'
    end
  end

  private

  def calculate_weitage_for_broadsheet(exam_group_id,subject_id)
    subject_exam = Exam.find_by_exam_group_id_and_subject_id(exam_group_id,subject_id)
    if subject_exam.nil? then weightage = 0 else weightage = subject_exam.maximum_marks.to_f end                
    return weightage
  end

end
