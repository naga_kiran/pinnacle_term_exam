class TermLinkGroupsController < ApplicationController
  def index
    @batches = Batch.active
  end
  def link_terms
    ids = []
    @term = Term.find(params[:term_id])   
    ids << params[:term_id]
    @link_terms = TermLinkGroup.find_all_by_term_id(params[:term_id])
    ids += @link_terms.map {|p| p.link_term_id}
    ids = ids.flatten
    @other_terms = @term.batch.terms -  Term.find_all_by_id(ids)
    render(:update) do |page|
            page.replace_html 'link_terms', :partial=>'link_terms'
            page.replace_html 'linked_terms', :partial=>'linked_terms'
          end
  end

  def update_terms
    @terms = Batch.find(params[:batch_id]).terms
     render(:update) do |page|
            page.replace_html 'terms', :partial=>'batch_terms'
            page.replace_html 'link_terms', :partial=>'link_terms'
          end
  end

  def edit
    ids = []
    @link_term = TermLinkGroup.find(params[:id])
    ids << @link_term.term_id
    @batches= Batch.active
    @batch = @link_term.term.batch
    @terms = @batch.terms
    @link_terms = TermLinkGroup.find_all_by_term_id(@link_term.term_id) - @link_term.to_a
    ids += @link_terms.map {|p| p.link_term_id}
    ids = ids.flatten
    @other_terms = @terms - Term.find_all_by_id(ids)
  end

  def save
    @term_link_group = TermLinkGroup.new(params[:term_links])
    if @term_link_group.save
      flash[:notice] = "Assigned Successfully"
    else
      flash[:notice] = "Error While Assigning"
    end
     redirect_to :action => 'index'
  end

  def update
    @term_link_group = TermLinkGroup.find(params[:id])
      if @term_link_group.update_attributes(params[:term_links])
       flash[:notice] = "Updated Successfully"
    else
      flash[:notice] = "Error While Updateing"
    end
    redirect_to :action => 'index'
    end

  def delete
     @term_link_group = TermLinkGroup.find(params[:id])
     if @term_link_group.destroy
       flash[:notice] = "Deleted Successfully"
       else
         flash[:notice] = "Error While Deleting"
     end
     redirect_to :action => 'index'
  end

end
