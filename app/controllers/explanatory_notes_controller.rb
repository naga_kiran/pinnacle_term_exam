class ExplanatoryNotesController < ApplicationController
	def new_explanatory_notes
		@batch=Batch.active
		if request.post?
			if params[:explanatory_notes].present?
				if params[:explanatory_notes1] == "" 
				else
					@explanatory_notes= ExplanatoryNote.find_by_batch_id_and_term_id(params[:explanatory_notes][:batch_id],params[:explanatory_notes][:term_id])
					if @explanatory_notes.present?							
						@explanatory_notes.update_attributes(:explanatory_note1 => params[:explanatory_notes1], :explanatory_note2 => params[:explanatory_notes2], :explanatory_note3 => params[:explanatory_notes3], :explanatory_note4 => params[:explanatory_notes4], :explanatory_note5 => params[:explanatory_notes5])
					else
						@explanatory_notes = ExplanatoryNote.new(:batch_id => params[:explanatory_notes][:batch_id],:term_id => params[:explanatory_notes][:term_id], :explanatory_note1 => params[:explanatory_notes1], :explanatory_note2 => params[:explanatory_notes2], :explanatory_note3 => params[:explanatory_notes3], :explanatory_note4 => params[:explanatory_notes4], :explanatory_note5 => params[:explanatory_notes5])
						@explanatory_notes.save
					end
				end
			end
			flash[:notice] = "#{t('explanatory_notes_created_sucessfully')}"
			redirect_to :action => "new_explanatory_notes"
	  end
	end

	def list_terms
		if params[:batch_id].present?
			@terms=Term.find_all_by_batch_id(params[:batch_id])
			render(:update) do |page|
		    page.replace_html 'list_terms', :partial=>"list_terms"
		  end
		else
	    render(:update) do |page|
        page.replace_html 'list_terms', :text=>""
      end
    end
	end

	def list_explanatory_notes
		if params[:term_id].present?
			@explanatory_notes= ExplanatoryNote.find_by_batch_id_and_term_id(params[:batch_id],params[:term_id])
			render(:update) do |page|
			  page.replace_html 'list_notes', :partial=>"list_explanatory_notes"
		  end
		else
	    render(:update) do |page|
        page.replace_html 'list_notes', :text=>""
      end
    end
	end
end
