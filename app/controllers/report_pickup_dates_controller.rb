class ReportPickupDatesController < ApplicationController
	def create_report_pickup_date
		@report_builders =  ReportBuilder.all		
		if request.post?
			if params[:report_pickup_dates].present? and params[:report_pickup_date].present?
				params[:report_pickup_date].each do |report, details|
					@template=AssignTemplate.find_by_batch_id_and_report_builder_id(params[:report_pickup_dates][:batch_id],params[:report_pickup_dates][:report_builder_id])
					@report_pickup_dates=ReportPickupDate.find_by_student_id_and_batch_id_and_term_id(report,params[:report_pickup_dates][:batch_id],params[:report_pickup_dates][:term_id])
					if @report_pickup_dates.nil?
						ReportPickupDate.create(:student_id => report, :course_id => params[:report_pickup_dates][:course_id], :term_id => params[:report_pickup_dates][:term_id], :batch_id => params[:report_pickup_dates][:batch_id], :report_pickup => params[:report_pickup], :student_select => details[:s_name],:template_id=>@template.id)
					else
						@report_pickup_dates.update_attributes(:student_select => details[:s_name], :report_pickup => params[:report_pickup])
					end
				end
			end
			flash[:notice] = "#{t('report_pickup_date_added_sucessfully')}"
			redirect_to :action => "create_report_pickup_date"
		end
	end

	def list_batches
		if params[:course_id].present? 
			@batches=Batch.find_all_by_course_id(params[:course_id])
			render(:update) do |page|
        page.replace_html 'batch_list', :partial=>"list_batches"
      end
    else
      render(:update) do |page|
        page.replace_html 'batch_list', :text=>""
      end
    end
	end

	def list_terms
		if params[:batch_id].present? 
			@terms=Term.find_all_by_batch_id(params[:batch_id])
			render(:update) do |page|
        page.replace_html 'current_terms', :partial=>"list_terms"
      end
    else
      render(:update) do |page|
        page.replace_html 'current_terms', :text=>""
      end
		end
	end

	def list_courses
		if params[:report_builder_id].present?
			@courses=Course.find(:all,:conditions => { :is_deleted => false }, :order => 'code asc')
			render(:update) do |page|
		        page.replace_html 'list_courses', :partial=>"list_courses"
		    end
    else
      render(:update) do |page|
        page.replace_html 'list_courses', :text=>""
      end
		end
	end
	def list_templates
		if params[:report_builder_id].present?
			# @template=AssignTemplate.find_by_batch_id_and_report_builder_id(params[:batch_id],params[:report_builder_id])
			@report_pickup_date=ReportPickupDate.find_by_batch_id_and_term_id(params[:batch_id],params[:term_id])
			unless @report_pickup_date.nil?
	    	@batch_students = Batch.find(@report_pickup_date.batch_id).students
			else
	    	@batch_students = Batch.find(params[:batch_id]).students
	    end
			render(:update) do |page|
		        page.replace_html 'templates', :partial=>"list_templates"
		    end
    else
      render(:update) do |page|
        page.replace_html 'templates', :text=>""
      end
		end
	end
end
