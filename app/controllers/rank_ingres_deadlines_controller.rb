require 'rank_ingres_deadlines_helper'
	
class RankIngresDeadlinesController < ApplicationController
  include RankIngresDeadlinesHelper

  before_filter :login_required
  filter_access_to :all
  def index
    @batches = Batch.active
    @normal_subjects = []
    @employees = []
    @terms = []
  end

  def update_terms
    @batch = Batch.find(params[:batch_id])
    @terms = @batch.terms
    render :update do |page|
      page.replace_html 'terms', :partial => 'batch_terms'
    end
  end

  def  update_subjects
    @term = params[:term_id]
    @batch = Batch.find(params[:batch])
    subjects = @batch.subjects.active
    elective_subjects = subjects.select {|s| s.elective_group_id != nil && s.is_deleted == false}
    @normal_subjects = subjects - elective_subjects
    #@students = @batch.students
    render :update do |page|
      page.replace_html 'subjects', :partial => 'subjects'
    end
  end

  def update_subject_employees
    @term = params[:term]
    @batch = params[:batch]
    @subject = params[:subject_id]
    employe_ids = EmployeesSubject.find_all_by_subject_id(params[:subject_id]).map {|p| p.employee_id}
    @employees = Employee.find_all_by_id(employe_ids)
    render :update do |page|
      page.replace_html 'employee', :partial => 'employee'
    end
  end
  
  def show_date
    @deadline = RankIngresDeadline.find_by_batch_id_and_subject_id_and_employee_id_and_term_id(params[:batch],params[:subject],params[:employee_id],params[:term])
    render :update do |page|
      page.replace_html 'date', :partial => 'date'
    end
  end

  def add_deadline
    @deadline = RankIngresDeadline.find_by_batch_id_and_subject_id_and_employee_id_and_term_id(params[:teacher][:batch_id],params[:teacher][:subject_id],params[:teacher][:employee_id],params[:teacher][:term_id])
    unless @deadline.nil?
      @deadline.update_attributes(params[:teacher])
      flash[:notice] = "#{t('updated_sucessfully')}"
    else
      RankIngresDeadline.create(params[:teacher])
      flash[:notice] = "#{t('created_sucessfully')}"
    end 
    redirect_to :action => "index"
  end

  def teacher_progress
    @batches = Batch.active
    @normal_subjects = []
    @employees = []
    @terms = []
  end

  def graph_progress
    term = Term.find params[:teacher][:term_id]
    batch = Batch.find params[:teacher][:batch_id]
    subjects = batch.subjects.reject {|s| s.no_exams == true}
    @precentage = []
     @subject_percentage = {}
    unless  subjects.nil?
      subjects.each do |subject|
        unless !subject.elective_group_id.nil?
           students_counter = batch.students.count
        else
         student_ids = StudentsSubject.find_all_by_subject_id_and_batch_id(subject.id,batch.id).map {|p| p.student_id}
          students_counter = Student.find_all_by_id( student_ids).count unless student_ids.blank?
         end
          exam_groups = term.term_exam_groups
          unless  exam_groups.blank?
            exam_group_count = exam_groups.count
            @subject_percentage["#{subject.name}"] = 0
            exam_groups.each do |exam_group|
              examscore = 0
              exam = Exam.find_by_exam_group_id_and_subject_id(exam_group.exam_group_id,subject.id)
              unless exam.nil?
              if  exam.exam_scores.blank?
                   unless term.ca_term_id.nil?
                       ca_term = Term.find(term.ca_term_id)
                       ca_term_exam_groups = ca_term.exam_groups
                      ca_count =  ca_term_exam_groups.count
                       ca_total_scores  = 0
                       ca_term_exam_groups.each do |ca_exam_group|
                        ca_exam = Exam.find_by_exam_group_id_and_subject_id(ca_exam_group.id,subject.id)
                         ca_count -= 1 if ca_exam.nil?
                         ca_scores =ca_exam.nil? ? 0 : ca_exam.exam_scores.blank?  ?  0 : ca_exam.exam_scores.count
                         ca_total_scores += ca_scores 
                       end
                       examscore = (ca_total_scores/ca_count) unless ca_total_scores == 0
                   end
                 else
                  examscore = exam.exam_scores.count
                end
              else
                  exam_group_count = exam_group_count - 1
              end
              @subject_percentage["#{subject.name}"] = @subject_percentage["#{subject.name}"] + ( examscore == 0 ? 0 : (((examscore.to_f).round(2)/students_counter)*100).round(2) )
            end
            @subject_percentage["#{subject.name}"] = ((@subject_percentage["#{subject.name}"])/exam_group_count) unless @subject_percentage["#{subject.name}"] == 0
          end
       
      end
    end
    @subject_percentage.each do |key , value|
      hash = {}
      #if value > 100 then value = 100 end
      hash["y"] = (value.to_f).round(2)
      hash["label"] = key
      @precentage << hash
    end
    render :layout => false
  end

end
