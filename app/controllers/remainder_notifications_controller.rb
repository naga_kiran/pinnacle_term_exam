class RemainderNotificationsController < ApplicationController
  before_filter :assignment_notification_settings , :only => [:set_assignment_notification]
  before_filter :task_notification_settings , :only => [ :set_task_notification, :create_task, :update_task]

  def new
    # data from tasks controller, new action
    begin
      @user = current_user
      @task = @user.tasks.build
      load_data
      @users = User.active.all
    rescue Exception => e
      Rails.logger.info "Exception in remainder notification controller, new action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :set_notification
    end
  end

  def create_task
    begin
      config_value = Configuration.find_by_config_key('TermwiseExamManagement').config_value
      if config_value == "1"
        @user = current_user
        params[:task][:assignee_ids] = params[:recipients].split(",").reject{|a| a.strip.blank?}.collect{ |s| s.to_i }
        @task = @user.tasks.build(params[:task])
        @task.status = "Assigned"
        if @task.save
          # if task is saved then it checks and select only the task notification configuration key which is having config value true status based on the particular config value true status the mode of notification ie (fedena internal message remainders, sms to mobile and email notification) is done
          config_key =  ['TaskRemainder','TaskRemainderAndSms','TaskRemainderAndEmail','TaskRemainderAndSmsAndEmail']
          notification_mode = config_key.reject { |key| Configuration.find_by_config_key(key).config_value == "0"}.to_s
          unless notification_mode == ""
            if notification_mode == "TaskRemainder"
              RemainderNotification.send_remaider_sms(params[:task],@user)
            elsif notification_mode == 'TaskRemainderAndSms'
              RemainderNotification.send_remaider_sms(params[:task],@user)
              RemainderNotification.send_mobile_sms(params[:task],@user)
            elsif notification_mode == 'TaskRemainderAndEmail'
              RemainderNotification.send_remaider_sms(params[:task],@user)
              RemainderNotification.send_email_notification(params[:task],@user)
            elsif notification_mode == 'TaskRemainderAndSmsAndEmail'
              RemainderNotification.send_remaider_sms(params[:task],@user)
              RemainderNotification.send_mobile_sms(params[:task,@user])
              RemainderNotification.send_email_notification(params[:task],@user)
            end
          end
          flash[:notice] = "#{t('task_creation_successful')}"
          redirect_to :action => "index", :controller => "tasks"
        else
          load_data
          @recipients = params[:recipients]
          render :new
        end
      else
        redirect_to :controller => "tasks", :action => "create"
      end
    rescue Exception => e
      Rails.logger.info "Exception in remainder notification controller, create_task action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :set_notification
    end
  end

  def update_task
    begin
      config_value = Configuration.find_by_config_key('TermwiseExamManagement').config_value
      if config_value == "1"
        @user = current_user
        @task = @user.tasks.find(params[:id])
        @users = User.active.all
        @task.status = params[:status]
        params[:task][:assignee_ids] = params[:recipients].split(",").reject{|a| a.strip.blank?}.collect{ |s| s.to_i }
        if @task.update_attributes(params[:task])
          # if task is updated then it checks and select only the task notification configuration key which is having config value true status based on the particular config value true status the mode of notification ie (fedena internal message remainders, sms to mobile and email notification) is done
          config_key =  ['TaskRemainder','TaskRemainderAndSms','TaskRemainderAndEmail','TaskRemainderAndSmsAndEmail']
          notification_mode = config_key.reject { |key| Configuration.find_by_config_key(key).config_value == "0"}.to_s
          unless notification_mode == ""
            if notification_mode == "TaskRemainder"
              RemainderNotification.send_remaider_sms(params[:task],@user)
            elsif notification_mode == 'TaskRemainderAndSms'
              RemainderNotification.send_remaider_sms(params[:task],@user)
              RemainderNotification.send_mobile_sms(params[:task],@user)
            elsif notification_mode == 'TaskRemainderAndEmail'
              RemainderNotification.send_remaider_sms(params[:task],@user)
              RemainderNotification.send_email_notification(params[:task],@user)
            elsif notification_mode == 'TaskRemainderAndSmsAndEmail'
              RemainderNotification.send_remaider_sms(params[:task],@user)
              RemainderNotification.send_mobile_sms(params[:task],@user)
              RemainderNotification.send_email_notification(params[:task],@user)
            end
          end
          flash[:notice] = "#{t('task_updation_successful')}"
          redirect_to :action => "index"
        else
          @recipients = User.active.find(@task.assignees)
          @batches = Batch.active
          @departments = EmployeeDepartment.active(:order=>"name asc")
          @users = User.active.all
          render :edit
        end
      else
        redirect_to :controller => "tasks", :action => "update"
      end
    rescue Exception => e
      Rails.logger.info "Exception in remainder notification controller, update_task action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :set_notification
    end
  end

  def create_assignment
    begin
      config_value = Configuration.find_by_config_key('TermwiseExamManagement').config_value
      if config_value == "1"
        @user = current_user
        student_ids = params[:assignment][:student_ids]
        params[:assignment].delete(:student_ids)
        @subject = Subject.find_by_id(params[:assignment][:subject_id])
        @assignment = Assignment.new(params[:assignment])
        @assignment.student_list = receipients = student_ids.join(",") unless student_ids.nil?
        @students_ids = receipients.split(",").reject{|a| a.strip.blank?}.collect{ |s| s.to_i }
        @assignment.employee = @user.employee_record
        unless @subject.nil?
          if @assignment.save
            # if assignment is saved then it checks and select only the assignment notification configuration key which is having config value true status based on the particular config value true status the mode of notification ie (fedena internal message remainders, sms to mobile and email notification) is done
            config_key = ['AssignmentRemainder','AssignmentRemainderAndSms','AssignmentRemainderAndEmail','AssignmentRemainderAndSmsAndEmail']
            notification_mode = config_key.reject { |key| Configuration.find_by_config_key(key).config_value == "0"}.to_s
            unless notification_mode == ""
              if notification_mode == "AssignmentRemainder"
                RemainderNotification.assignment_remaider_sms(@assignment,@user)
              elsif notification_mode == 'AssignmentRemainderAndSms'
                RemainderNotification.assignment_remaider_sms(@assignment,@user)
                RemainderNotification.assignment_mobile_sms(@assignment,@user)
              elsif notification_mode == 'AssignmentRemainderAndEmail'
                RemainderNotification.assignment_remaider_sms(@assignment,@user)
                RemainderNotification.assignment_email_notification(@assignment,@user)
              elsif notification_mode == 'AssignmentRemainderAndSmsAndEmail'
                RemainderNotification.assignment_remaider_sms(@assignment,@user)
                RemainderNotification.assignment_mobile_sms(@assignment,@user)
                RemainderNotification.assignment_email_notification(@assignment,@user)
              end
            end

            flash[:notice] = "#{t('new_assignment_sucessfuly_created')}"
            redirect_to :action=>:index, :controller => :assignments
          else
            if current_user.employee?
              @subjects = current_user.employee_record.subjects
              @subjects.reject! {|s| !s.batch.is_active?}
              @students = @subject.batch.students
            end
            render :action=>:new_assignment
          end
        else
          unless @assignment.save
            @subjects = current_user.employee_record.subjects
            render :action=>:new_assignment
          end
        end
      else
        redirect_to :controller => "assignments", :action => "create"
      end
    rescue Exception => e
      Rails.logger.info "Exception in remainder notification controller, create_assignment action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :set_notification
    end
  end

  def subjects_students_list
    # data from assignments controller, subjects_students_list action
    begin
      @subject = Subject.find_by_id params[:subject_id]
      unless @subject.nil?
        if @subject.elective_group_id.nil?
          @students = @subject.batch.students
        else
          assigned_students = StudentsSubject.find_all_by_subject_id(@subject.id)
          @students = assigned_students.map{|s| s.student}
          @students=@students.compact
        end
      end
      render(:update) do |page|
        page.replace_html 'subjects_student_list', :partial=>"subjects_student_list"
      end
    rescue Exception => e
      Rails.logger.info "Exception in remainder notification controller, subjects_students_list action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :set_notification
    end
  end

  def update_assignment
    begin
      config_value = Configuration.find_by_config_key('TermwiseExamManagement').config_value
      if config_value == "1"
        @assignment = Assignment.find_by_id(params[:id])
        unless @assignment.nil?
          student_ids = params[:assignment][:student_ids]
          params[:assignment].delete(:student_ids)
          @assignment.student_list = student_ids.join(",") unless student_ids.nil?
          if  @assignment.update_attributes(params[:assignment])
            # if assignment is updated then it checks and select only the assignment notification configuration key which is having config value true status based on the particular config value true status the mode of notification ie (fedena internal message remainders, sms to mobile and email notification) is done
            config_key = ['AssignmentRemainder','AssignmentRemainderAndSms','AssignmentRemainderAndEmail','AssignmentRemainderAndSmsAndEmail']
            notification_mode = config_key.reject { |key| Configuration.find_by_config_key(key).config_value == "0"}.to_s
            unless notification_mode == ""
              if notification_mode == "AssignmentRemainder"
                RemainderNotification.assignment_remaider_sms(@assignment,@user)
              elsif notification_mode == 'AssignmentRemainderAndSms'
                RemainderNotification.assignment_remaider_sms(@assignment,@user)
                RemainderNotification.assignment_mobile_sms(@assignment,@user)
              elsif notification_mode == 'AssignmentRemainderAndEmail'
                RemainderNotification.assignment_remaider_sms(@assignment,@user)
                RemainderNotification.assignment_email_notification(@assignment,@user)
              elsif notification_mode == 'AssignmentRemainderAndSmsAndEmail'
                RemainderNotification.assignment_remaider_sms(@assignment,@user)
                RemainderNotification.assignment_mobile_sms(@assignment,@user)
                RemainderNotification.assignment_email_notification(@assignment,@user)
              end
            end
            flash[:notice]="#{t('assignment_details_updated')}"
            redirect_to @assignment
          else
            load_data
            render :edit ,:id=>params[:id]
          end
        else
          flash[:notice]=t('flash_msg4')
          redirect_to :controller=>:user ,:action=>:dashboard
        end
      else
        redirect_to :controller => "assignments", :action => "update"
      end
    rescue Exception => e
      Rails.logger.info "Exception in remainder notification controller, update_assignment action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :set_notification
    end
  end

  def set_assignment_notification
    begin
      if @current_user.privileges.include?(Privilege.find_by_name("TermManager")) or @current_user.admin?
        if request.post?
          # if request is post then by default all the assignment config key values is done false
          config_key = ['AssignmentRemainder','AssignmentRemainderAndSms','AssignmentRemainderAndEmail','AssignmentRemainderAndSmsAndEmail']
          config_key.each do |k|
            @default_config = Configuration.find_by_config_key(k)
            @default_config.update_attribute(:config_value, "0")
          end
          # then checks for params of config key which is submitted and its config value is made as true then in the form based on the corresponding values of assignment config keys the value is checked true
          if params[:configuration].present?
            key = params[:configuration].to_s.camelize
            @config = Configuration.find_by_config_key(key)
            @config.update_attribute(:config_value, "1")
            flash[:notice]=t('settings_updated')
          end
        end
        @config_key = Configuration.get_multiple_configs_as_hash ['AssignmentRemainder','AssignmentRemainderAndSms','AssignmentRemainderAndEmail','AssignmentRemainderAndSmsAndEmail']
      else
        flash[:notice] = "#{t('deny_permission')}"
        redirect_to :controller => :exam, :action => :index
      end
    rescue Exception => e
      Rails.logger.info "Exception in remainder notification controller, set_assignment_notification action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :set_notification
    end
  end

  def set_task_notification
    begin
      if @current_user.privileges.include?(Privilege.find_by_name("TermManager")) or @current_user.admin?
        @notification_modes =  ['task_remainder_and_sms','task_remainder_and_email','task_remainder_and_sms_and_email']
        if request.post?
          # if request is post then by default all the task config key values is done false
          config_key =  ['TaskRemainder','TaskRemainderAndSms','TaskRemainderAndEmail','TaskRemainderAndSmsAndEmail']
          config_key.each do |k|
            @default_config = Configuration.find_by_config_key(k)
            @default_config.update_attribute(:config_value, "0")
          end
          # then checks for params of config key which is submitted and its config value is made as true then in the form the corresponding values of task config keys the value is checked true
          if params[:configuration].present?
            key = params[:configuration].to_s.camelize
            @config = Configuration.find_by_config_key(key)
            @config.update_attribute(:config_value, "1")
            flash[:notice]=t('settings_updated')
          end
        end
        @config_key = Configuration.get_multiple_configs_as_hash ['TaskRemainder','TaskRemainderAndSms','TaskRemainderAndEmail','TaskRemainderAndSmsAndEmail']
      else
        flash[:notice] = "#{t('deny_permission')}"
        redirect_to :controller => :exam, :action => :index
      end
    rescue Exception => e
      Rails.logger.info "Exception in remainder notification controller, set_task_notification action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :set_notification
    end
  end

  def assignment_notification_settings
    # find and create assignment notification config keys to the configuration table and the default config value is made as false
    begin
      @AssignmentRemainder = Configuration.find_by_config_key('AssignmentRemainder')
      @AssignmentRemainder_and_sms = Configuration.find_by_config_key('AssignmentRemainderAndSms')
      @AssignmentRemainder_and_email = Configuration.find_by_config_key('AssignmentRemainderAndEmail')
      @AssignmentRemainder_sms_and_email = Configuration.find_by_config_key('AssignmentRemainderAndSmsAndEmail')
      Configuration.create(:config_key => 'AssignmentRemainder',:config_value => false) if @AssignmentRemainder.nil?
      Configuration.create(:config_key => 'AssignmentRemainderAndSms',:config_value => false) if @AssignmentRemainder_and_sms.nil?
      Configuration.create(:config_key => 'AssignmentRemainderAndEmail',:config_value => false) if @AssignmentRemainder_and_email.nil?
      Configuration.create(:config_key => 'AssignmentRemainderAndSmsAndEmail',:config_value => false) if @AssignmentRemainder_sms_and_email.nil?
    rescue Exception => e
      Rails.logger.info "Exception in remainder notification controller, assignment_notification_settings action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :set_notification
    end
  end

  def task_notification_settings
    # find and create task notification config keys to the configuration table and the default config value is made as false
    begin
      @AssignmentRemainder = Configuration.find_by_config_key('TaskRemainder')
      @AssignmentRemainder_and_sms = Configuration.find_by_config_key('TaskRemainderAndSms')
      @AssignmentRemainder_and_email = Configuration.find_by_config_key('TaskRemainderAndEmail')
      @AssignmentRemainder_sms_and_email = Configuration.find_by_config_key('TaskRemainderAndSmsAndEmail')
      Configuration.create(:config_key => 'TaskRemainder',:config_value => false) if @AssignmentRemainder.nil?
      Configuration.create(:config_key => 'TaskRemainderAndSms',:config_value => false) if @AssignmentRemainder_and_sms.nil?
      Configuration.create(:config_key => 'TaskRemainderAndEmail',:config_value => false) if @AssignmentRemainder_and_email.nil?
      Configuration.create(:config_key => 'TaskRemainderAndSmsAndEmail',:config_value => false) if @AssignmentRemainder_sms_and_email.nil?
    rescue Exception => e
      Rails.logger.info "Exception in remainder notification controller, task_notification_settings action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :set_notification
    end
  end

  def load_data
    # data from tasks controller load_data action
    begin
      @departments = EmployeeDepartment.active(:order=>"name asc")
      @batches = Batch.active
    rescue Exception => e
      Rails.logger.info "Exception in remainder notification controller, load_data action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :set_notification
    end
  end


  def select_employee_department
    # data from tasks controller select_employee_department action
    begin
      @user = current_user
      @departments = EmployeeDepartment.find(:all, :conditions=>"status = true")
      render :partial=>"select_employee_department"
    rescue Exception => e
      Rails.logger.info "Exception in remainder notification controller, select_employee_department action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :set_notification
    end
  end

  def select_student_course
    # data from tasks controller select_student_course action
    begin
      @user = current_user
      @batches = Batch.active
      render :partial=> "select_student_course"
    rescue Exception => e
      Rails.logger.info "Exception in remainder notification controller, select_student_course action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :set_notification
    end
  end

  def to_schools
    # data from tasks controller to_schools action
    begin
      if params[:dept_id] == ""
        render :update do |page|
          page.replace_html "dept_and_courses", :text => ""
        end
        return
      end
      school = School.find params[:id]
      @departments = school.employee_departments.active(:order=>"name asc")
      @batches = school.batches.active
      render :update do |page|
        page.replace_html 'depts_and_courses', :partial => 'depts_and_courses'
      end
    rescue Exception => e
      Rails.logger.info "Exception in remainder notification controller, to_schools action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :set_notification
    end
  end

  def to_employees
    # data from tasks controller to_employees action
    begin
      if params[:dept_id] == ""
        render :update do |page|
          page.replace_html "to_users", :text => ""
        end
        return
      end
      department = EmployeeDepartment.find(params[:dept_id])
      employees = department.employees(:include=>:user).sort_by{|a| a.full_name.downcase}
      @to_users = employees.map { |s| s.user }.compact||[]
      render :update do |page|
        page.replace_html 'to_users', :partial => 'to_users', :object => @to_users
      end
    rescue Exception => e
      Rails.logger.info "Exception in remainder notification controller, to_employees action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :set_notification
    end
  end

  def to_students
    # data from tasks controller to_students action
    begin
      if params[:batch_id] == ""
        render :update do |page|
          page.replace_html "to_users2", :text => ""
        end
        return
      end
      batch = Batch.find(params[:batch_id])
      students = batch.students(:include=>:user).sort_by{|a| a.full_name.downcase}
      @to_users = students.map { |s| s.user }.compact||[]
      render :update do |page|
        page.replace_html 'to_users2', :partial => 'to_users', :object => @to_users
      end
    rescue Exception => e
      Rails.logger.info "Exception in remainder notification controller, to_students action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :set_notification
    end
  end

  def update_recipient_list
    # data from tasks controller update_recipient_list action
    begin
      recipients_array = params[:recipients].split(",").collect{ |s| s.to_i }
      @recipients = User.active.find_all_by_id(recipients_array).sort_by{|a| a.full_name.downcase}
      render :update do |page|
        page.replace_html 'recipient-list', :partial => 'recipient_list'
      end
    rescue Exception => e
      Rails.logger.info "Exception in remainder notification controller, update_recipient_list action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :set_notification
    end
  end

  def download_attachment
    # data from tasks controller, download_attachment action
    begin
      @task = Task.find(params[:id])
      if @task.can_be_downloaded_by?(current_user)
        send_file (@task.attachment.path)
      else
        flash[:notice] = "#{t('no_permission_to_download_file')}"
        redirect_to :action => "index"
      end
    rescue Exception => e
      Rails.logger.info "Exception in remainder notification controller, download_attachment action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :action => :set_notification
    end
  end

end
