class EvaluationGroup < ActiveRecord::Base
  belongs_to :term
  has_many :evaluation_types
  validates_presence_of :name,:batch_id, :term_id
 
end
