class AlphaNumericBatch < ActiveRecord::Base
	belongs_to :batch
	belongs_to :term
	belongs_to :exam_group
	has_many :alpha_numeric_exams
end
