class Term < ActiveRecord::Base
#  validates :name, :presence => true
#  validates :code, :presence => true
#  validates :name, :uniqueness => true
  belongs_to :batch
  has_many :term_exam_groups
  has_many :exam_groups,:through => :term_exam_groups
  has_many :evaluation_groups
  has_many :term_link_groups
  has_one :alpha_numeric_batch
  has_one :explanatory_note
end

