class AlphaNumericExam < ActiveRecord::Base
	belongs_to :alpha_numeric_batch
  belongs_to :subject, :conditions => { :is_deleted => false }
  has_many :alpha_numeric_score

  def removable?
	  return false if self.alpha_numeric_score.all(:conditions=>["alpha_numeric_scores.marks_grade IS NOT NULL  or alpha_numeric_scores.is_absent = true"]).present?
	  return true
	end
end
