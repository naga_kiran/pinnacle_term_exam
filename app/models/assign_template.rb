class AssignTemplate < ActiveRecord::Base
  serialize :term_id
  belongs_to :report_builder
  belongs_to :batch
end
