class RemainderNotification < ActiveRecord::Base
   has_attached_file :attachment ,
    :path => "uploads/:class/:id_partition/:basename.:extension"

  def self.send_remaider_sms(task,user)
    # sending internal messages for task assignees in fedena
    @user = user
    Delayed::Job.enqueue(DelayedReminderJob.new( :sender_id  => @user.id,
        :recipient_ids => task[:assignee_ids],
        :subject=> task[:title],
        :body=>task[:description] ))
  end

  def self.send_mobile_sms(task,user)
    # sending sms message to mobile phones for task assignees
    @user = user
    @recipients =[]
    sms_setting = SmsSetting.new()
    task[:assignee_ids].each do |user_id|
      if User.find(user_id).employee?
        employee = Employee.find_by_user_id(user_id)
        if sms_setting.employee_sms_active
          @recipients.push employee.mobile_phone unless (employee.mobile_phone.nil? or employee.mobile_phone == "")
        end
      elsif User.find(user_id).student?
        student = Student.find_by_user_id(user_id)
        if student.is_sms_enabled
          if sms_setting.student_sms_active
            @recipients.push student.phone2 unless (student.phone2.nil? or student.phone2 == "")
          end
        end
      end
    end
    unless @recipients.empty?
      message = "Task is assigned on Date: #{Date.today} by #{@user.name}, Subject is #{task[:title]} and message is #{task[:description]}"
      Delayed::Job.enqueue(SmsManager.new(message,@recipients))
    end
  end

  def self.send_email_notification(task,user)
    # sending email to task assignes
    sender = user.email
    @recipient_list = []
    task[:assignee_ids].each do |user_id|
      if User.find(user_id).employee?
        employee = Employee.find_by_user_id(user_id)
        @recipient_list << employee.email unless (employee.email == "" or employee.email.nil?)
      elsif User.find(user_id).student?
        student = Student.find_by_user_id(user_id)
        if student.is_email_enabled
          @recipient_list << student.email unless (student.email == "" or student.email.nil?)
        end
      end
    end
    unless @recipient_list.empty?
      FedenaMailer::deliver_email(sender, @recipient_list, task[:title], task[:description])
    end
  end

  def self.assignment_remaider_sms(reciepients,user)
    # sending internal messages for assignment assignees in fedena
    student_ids = reciepients[:student_list].split(",").reject{|a| a.strip.blank?}.collect{ |s| s.to_i }
    @user = user
    Delayed::Job.enqueue(DelayedReminderJob.new( :sender_id  => @user.id,
        :recipient_ids => student_ids,
        :subject=> reciepients[:title],
        :body=> reciepients[:content]))
  end

  def self.assignment_mobile_sms(reciepients,user)
    # sending sms message to mobile phones for assignment assignees
    @user = user
    student_ids = reciepients[:student_list].split(",").reject{|a| a.strip.blank?}.collect{ |s| s.to_i }
    sms_setting = SmsSetting.new()
    @recipients=[]
    student_ids.each do |s_id|
      student = Student.find(s_id)
      guardian = student.immediate_contact
      if student.is_sms_enabled
        if sms_setting.student_sms_active
          @recipients.push student.phone2 unless (student.phone2.nil? or student.phone2 == "")
        end
        if sms_setting.parent_sms_active
          unless guardian.nil?
            @recipients.push guardian.mobile_phone unless (guardian.mobile_phone.nil? or guardian.mobile_phone == "")
          end
        end
      end
    end
    unless @recipients.empty?
      message = "Assignment is assigned on Date.today by #{@user.name}, Subject is #{reciepients[:title]} and message is #{reciepients[:content]} "
      sms = Delayed::Job.enqueue(SmsManager.new(message,@recipients))
    end
  end

  def self.assignment_email_notification(reciepients,user)
    # sending email to assignment assignees
    @user = user
    student_ids = reciepients[:student_list].split(",").reject{|a| a.strip.blank?}.collect{ |s| s.to_i }
    sender = @user.email
    @recipient_list = []
    student_ids.each do |stud_id|
      student = Student.find(stud_id)
      if student.is_email_enabled
        @recipient_list << student.email unless (student.email == "" or student.email.nil?)
      end
      guardian = student.immediate_contact
      unless guardian.nil?
        @recipient_list << guardian.email unless (guardian.email.nil? or guardian.email == "")
      end
    end
    unless @recipient_list.empty?
      FedenaMailer::deliver_email(sender, @recipient_list, reciepients[:title], "Assignment is assigned on Date: #{Date.today} by #{@user.name} and message is #{reciepients[:content]}")
    end
  end
  
end
