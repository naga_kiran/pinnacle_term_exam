class ReportBuilder < ActiveRecord::Base
   unloadable
   validates_uniqueness_of :name
   validates_presence_of :name, :description
   xss_terminate :except => [:description]
   has_many :assign_templates
  def self.student_basic_data
    ["student-photo","student-admission_date","student-admission_no","student-first_name","student-middle_name","student-last_name",
      "student-date_of_birth","student-course","student-batch","student-nationality","student-category_name","student-gender","student-blood_group",
      "student-language","student-religion","student-city","student-state","student-country_name","student-address_line1","student-pin_code","student-phone1","student-phone2",
      "student-email","student-immediate_contact"]
  end

  def self.student_previous_data
    ["institution","year","course","total_mark"]
  end

  def self.page_orientation
    [ "portrait", "landscape"]
  end

  def self.parent_data
    ["parent-category","parent-full_name","parent-relation","parent-dob","parent-email","parent-office_phone1","parent-mobile_phone",
      "parent-office_address_line1","parent-city","parent-state","parent-country_name","parent-occupation","parent-income"]
  end

  def self.student_attendance_data
    ["total_classes_at_batch","amount_of_classes_to_date","classes_attended_in_percent","leaves_to_date_in_percent","total_attented_classes","total_leaves"]
  end

  def self.student_examination_data
    ["college_examination_data","college_term_report","position_in_subject","kindergarten_report","next_term_begins","class_position","number_of_time_school_open","number_of_day_present","primary_term_report","college_mid_term_report","college_easter_term_report","college_omega_term_report","total_student_in_class","number_of_times_punctual","junior_examination_data","junior_second_term_report","junior_end_of_term_report","junior_end_of_term_ikoyi_report","infant_examination_data","college_mid_term_comment_report","college_omega_term_report_note","moct_report","overall_score","overall_grade","average_percentage", "terms", "session","students_overall_gpa","class_overall_average","class_overall_percentage","average_age_of_class","house", "age", "home_work_ratings","creative_learning","amville_report","amville_nursery_report","overall_score_gpa","overall_score_grade","student_group_average","group_overall_grade","group_class_average","annual_score","class_annual_overall","annual_gpa","half_term_report","first_term_report","second_term_report","third_term_report","amville_nursery_report2","nursery_2_report","primary_mid_term_report","first_senior_term_report","second_senior_term_report","third_senior_term_report", "checkpoint_report"]
  end

  def self.examination_data
    ["tutor_comments", "tutor_signature","subject_teacher_signature","principal_name","director_signature", "house_parent_comment","school_log"]
  end

  def self.tutor_data
    ["tutor_full_name","tutor_admission_no"]
  end

  def self.date_time
    ["current_date","current_time"]
  end



  def self.fields
    ['current_date','student-photo', 'student-course', 'student-batch', 'student-category_name', 'student-nationality', 'student-immediate_contact', 'parent-full_name' , 'student-country_name','parent-country_name']
  end

  def self.student_additional_data
    stud_add_data = StudentAdditionalField.all.collect(&:name)
    stud_add_data.map{|each| each.squish.downcase.tr(" ","_")} #as name fields may come with blank space in b/w.
  end

  #  "(empty_variable)" are displayed in pdf reports for guardians, when guardian category is not selected or if there is a conflict of different categories.
  def self.match_string(query, word, stud_id, i, report_id,term_ids)
    # If the word is matching with '$%' then the word is splitted  with '$%' and then the word is calculated by fetching values from database and the value is pushed into the array called "query" else directly the word is pushed to array.
    if word.match(/\$%/).present?
      if word.match(/\/>/).present?
        word = word.split("/>").to_s
      end
      field_word = word.split("$%")
      field_word = field_word.compact.reject(&:empty?)
      field_result = []
      unless field_word.nil?
        field_word.each do |field_word_list|
          res = ReportBuilder.match_field_names(field_word_list, stud_id, report_id,term_ids)
          field_string=res[0]
          result=res[1]
          field = res[2]
          # if the sql-result is not nil then the respective field-value is and pushed into variable and then the field-value is pushed into pdf report.
          unless result.nil? or result == "(empty_variable)"
            if result.to_s.match(/#<Mysql::Result/).present?
              result.each_hash do |row|
                row.each do |key, value|
                  field_result = value
                end
              end
            else
              field_result = result
            end
          else
            field_result== "(empty_variable)"
          end
          if result.nil?
            unless field_string.nil?
              if ReportBuilder.student_examination_data.include?(field_string)
                field_string = "$%" + field_string + "%"
              elsif ReportBuilder.tutor_data.include?(field_string)
                field_string = "$%" + field_string + "%"
              end
              query << field_string
            end
            if field.present?
              unless field[1].nil?
                query << field[1]
              end
            end
          else
            unless field_result.nil? or field_result == []
              query << field_result.upcase
            else
              query << "(empty_variable)"
            end
            if field[1].present?
              query << field[1]
            end
          end
        end
      end
      query << " "
      return query
    else
      ReportBuilder.image_data(query,word,i)
      return query
    end
  end

  def self.match_field_names(field_word, stud_id,report_id,term_ids)
    # Checking and matching the word with '%' if found then it is splitted then only the value containing field_name is pushed into array.
    if field_word.match(/%/).present?
      field = field_word.split('%')
      field_name = field[0]
      #if field_name starts with 'parent-field_name' then purticular student guardian information is fetched from database else if field_name starts with 'student-field_name' then purticular student information is fetched.
      unless ReportBuilder.fields.include?(field_name)
        if field_name.match(/parent-/).present?
          f = field_name.split("-")
          fld_name = f[1]
          student = Student.find(stud_id)
          #          guardian = student.guardians
          #          unless guardian.first.nil?
          guardian = student.guardians.first
          if guardian.first.present?
            guardian_id = guardian.first.id
            result = Guardian.connection.execute("select #{fld_name} from guardians where id = #{guardian_id}")
          else
            result = "(empty_variable)"
          end
        elsif field_name.match(/student-/).present?
          f = field_name.split("-")
          fld_name = f[1]
          unless field_name.nil?
            if fld_name == "date_of_birth"
              d = ActiveRecord::Base.connection.select_value("select #{fld_name} from students where id = #{stud_id}")
              result = d.to_date.strftime('%d-%b-%Y').upcase
            else
              result = Student.connection.execute("select #{fld_name} from students where id = #{stud_id}")
            end
          end
        end
      end
      field_string = ReportBuilder.field_word(field_name,stud_id,report_id,term_ids)
    else
      field_string = field_word
    end
    return field_string,result,field
  end

  def self.field_word(field_name, student_id,report_id,term_ids)
    #if the caculated word ie field-name is not calculated in Student or Guardian Table then through relations all the fields is made it array and field value is calculated
    student = Student.find(student_id)
    #find guardian using category_id present in guardians -- new feature
    guardian = student.guardians.first
    #    guardian = student.immediate_contact_id.nil? ? student.guardians.first : Guardian.find(student.immediate_contact_id)
    t = Time.now
    if field_name == 'current_date'
      word = t.strftime("%A #{t.day.ordinalize} %B, %Y ")
    elsif field_name == 'current_time'
      word = t.strftime("%I:%M %P")
    elsif field_name == 'student-course'
      word =  student.batch.course.course_name
    elsif field_name == 'student-batch'
      word =  student.batch.full_name
    elsif field_name == 'student-photo'
      word = "$%" + field_name + "%"
      #      photo_url = student.photo.url
      #      photo_path = student.photo.path
      #      Rails.logger.info"AAAAAAAAAAAAAAAA#{photo_path.inspect}"
      #      Rails.logger.info"BBBBBBBBBBBBBBBBBBBB#{student.photo.url(:original, false).inspect}"
      #      Rails.logger.info"CCCCCCCCCCCCCCCCCCCC#{photo_url.inspect}"
      #
      #      name = photo_url.split("/").last.split("?").first.to_a
      #      x = photo_url.split("/")
      #      x.slice!(-1)
      #      url = x + name
      #      photo_url = url.join("/")
      #      #      if  File.exists?("#{photo_path}")
      #      if student.photo.file?
      #        Rails.logger.info "ZZZZZZZZZZZZZZZ#{photo_url.inspect}"
      #        #        word = ActionController::Base.helpers.image_tag photo_url
      #        word = ActionController::Base.helpers.image_tag student.photo.url.to_s
      #        #        word =  ReportBuildersHelper.wicked_pdf_image_tag_for_public photo_url
      #        word = word.split('/')
      #        word = word - ["public","images"]
      #        word = word.join('/')
      #      else
      #        word = ActionController::Base.helpers.image_tag "master_student/profile/default_student.png"
      #        #        word = ReportBuildersHelper.wicked_pdf_image_tag_for_public "master_student/profile/default_student.png"
      #      end
    elsif field_name == 'student-country_name'
      word = student.country.name unless student.country.nil?
    elsif field_name == 'parent-country_name'
      if guardian.present?
        word = guardian.country.name
      else
        word = "(empty_variable)"
      end
    elsif field_name == 'student-category_name'
      word = StudentCategory.find(student.student_category_id).name unless student.student_category_id.nil?
    elsif field_name == 'parent-full_name'
      if guardian.present?
        word = guardian.full_name
      else
        word = "(empty_variable)"
      end
    elsif field_name == 'student-nationality'
      word = Country.find(student.nationality_id).name unless student.nationality_id.nil?
    elsif field_name == 'student-immediate_contact'
      guardian = student.immediate_contact_id.nil? ? nil : Guardian.find(student.immediate_contact_id)
      word = guardian.full_name unless guardian.nil?
    elsif field_name == 'teachers'
      word = ReportBuilder.teachers_data(student)
    elsif ReportBuilder.student_attendance_data.include?(field_name)
      array = ReportBuilder.attendance_data(student,term_ids)
      word = array[0] if field_name == 'total_classes_at_batch'
      word = array[1] if field_name == 'amount_of_classes_to_date'
      word = array[2] if field_name == 'classes_attended_in_percent'
      word = array[3] if field_name == 'leaves_to_date_in_percent'
      word = array[4] if field_name == 'total_attented_classes'
      word = array[5] if field_name == 'total_leaves'

    elsif ReportBuilder.student_examination_data.include?(field_name)
      word = field_name
    elsif ReportBuilder.examination_data.include?(field_name)
      word = "$%" + field_name + "%"
    elsif ReportBuilder.student_additional_data.include?(field_name)
      original_field_name = field_name.tr("_"," ")# student additional name-field information back to original string.
      add_field = StudentAdditionalField.find_by_name(original_field_name)
      add_info=StudentAdditionalDetail.find_by_additional_field_id_and_student_id(add_field.id,student_id)
      word=add_info.additional_info unless add_info.nil?
    elsif ReportBuilder.student_previous_data.include?(field_name)
      if field_name == "institution"
        word = student.student_previous_data.institution unless student.student_previous_data.nil?
      elsif field_name == "year"
        word = student.student_previous_data.year unless student.student_previous_data.nil?
      elsif field_name == "course"
        word = student.student_previous_data.course unless student.student_previous_data.nil?
      else
        word = student.student_previous_data.total_mark unless student.student_previous_data.nil?
      end
    end
    return word
  end

  def self.teachers_data(student)
    batch = student.batch
    sub_teachers = []
    @subjects = Subject.find_all_by_batch_id(batch.id,:conditions=>"is_deleted=false")
    @subjects.each do |subject|
      emp_names = []
      @assigned_employee = EmployeesSubject.find_all_by_subject_id(subject.id)
      @assigned_employee.each_with_index do |ae, j|
        employee = Employee.find_by_id(ae.employee_id)
        emp_names << employee.full_name + "; "
      end
      sub_teachers << subject.name + "=" + emp_names.to_s  unless @assigned_employee.empty?
    end
    return sub_teachers
  end

  def self.attendance_data(student,term_ids)
    #data from attendance reports controller.
    @batch = student.batch
    @leave =0
    term_ids.each do |term_id|
      @term = Term.find(term_id)
      @start_date= @term.ca_term_id.nil? ?  @term.start_date.to_date :  Term.find(@term.ca_term_id.to_i).start_date.to_date
      @end_date= @term.end_date.to_date
      working_days=@batch.working_days(@start_date)
      @academic_days=  working_days.select{|v| v<=@end_date}.count
      leaves_forenoon=Attendance.count(:all,:conditions=>{:student_id => student.id,:batch_id=>@batch.id,:forenoon=>true,:afternoon=>false,:month_date => @start_date..@end_date})
      leaves_afternoon=Attendance.count(:all,:conditions=>{:student_id => student.id,:batch_id=>@batch.id,:forenoon=>false,:afternoon=>true,:month_date => @start_date..@end_date})
      leaves_full=Attendance.count(:all,:conditions=>{:student_id => student.id, :batch_id=>@batch.id,:forenoon=>true,:afternoon=>true,:month_date => @start_date..@end_date})
      @leaves= leaves_full.to_f+(0.5*(leaves_forenoon.to_f+leaves_afternoon.to_f))
      @leave = @leave + @leaves.to_i
      @leaves_to_date_percent = (@leaves.to_f/@academic_days)*100 unless @academic_days == 0
      @leaves_to_date_percent = @leaves_to_date_percent.round(2)
    end


    #    @batch = student.batch
    #    @start_date=@batch.start_date.to_date
    #    @end_date=@batch.end_date.to_date
    @total_classes_at_batch = @batch.subject_hours(@start_date, @end_date, 0).values.flatten.compact.count
    d=Date.today
    @classes_till_date = @batch.subject_hours(@start_date, d, 0).values.flatten.compact.count
    #data from student_attendance controller.
    @student = student
    @student_leaves = SubjectLeave.find(:all,  :conditions =>{:student_id=>@student.id,:month_date => @start_date..@end_date})
    #    @leaves = @student_leaves.count
    #    @leaves||=0
    @end_date = @batch.end_date.to_date > Date.today ? Date.today : @batch.end_date.to_date #to what it comes first current date or batch end date.
    @total_classes = @batch.subject_hours(@start_date, @end_date, 0).values.flatten.compact.count
    @attended_classes = (@total_classes - @leaves)
    @classes_attended_percent = (@attended_classes/(@attended_classes+@leaves).to_f)*100
    @leaves_to_date_percent = 100-(@classes_attended_percent).to_f
    Rails.logger.info @classes_attended_percent
    classes_attended_percent = ReportBuilder.check_for_nan(@classes_attended_percent)
    #    leaves_to_date_percent = ReportBuilder.check_for_nan(@leaves_to_date_percent)
    return @total_classes_at_batch, @classes_till_date, classes_attended_percent, @leaves_to_date_percent, @attended_classes, @leave
  end

  def self.check_for_nan(value)
    if value.infinite?
      @values = ""
    elsif value.nan?
      @values = ""
    else
      @values = value.round(2)
    end
    return @values
  end

  def self.sort_by_first_name(ids)
    students=[]
    ids.each do |stud_id|
      students.push Student.find(stud_id)
    end
    return students.sort_by {|e| e[:first_name].downcase}.collect(&:id)
  end

  def self.image_data(query,word,i)
    # if the word match with /src=/ while looping, then the image path is calculated in 2 ways one is from if the image uploaded to rails folder then uploded image path is calculated and it is displayed in pdf,
    #  another is from tinymce image path ie, only smileys and it is displayed in pdf using wicked pdf tag
    if word.match(/src=/).present?
      @constant = i
      word = word.split('"')
      @image_path = word[1]
      if @image_path.match(/report_builder_images/).present?
        path = @image_path.split('/')
        file_name = path.last
        @image1 = ActionController::Base.helpers.image_tag("../report_builder_images/#{file_name}")
        #        @image1 = ReportBuildersHelper.wicked_pdf_image_tag_for_public "../report_builder_images/#{file_name}"
      elsif @image_path.match(/http/).present?
        @image = ActionController::Base.helpers.image_tag(@image_path)
      else
        #        Emoticons option is not provided
        @constant1 = i
        path = @image_path.split('/')
        file_name = path.last
        @image2 = ActionController::Base.helpers.image_tag("../tinymce/plugins/emoticons/img/#{file_name}")
        #        @image2 = ReportBuildersHelper.wicked_pdf_image_tag_for_public "../tinymce/plugins/emoticons/img/#{file_name}"
      end
    elsif word.match(/alt=""/).present?
      query << ""
    elsif word.match(/<img/).present?
      word = word.split('<img').to_s
      if word.match(/\/>/).present?
        word = word.split('/>').to_s
        unless @constant1.nil?
          if @constant1 + 4 != i
            word = @image2 + word
          end
        end
      end
      query << word
    elsif word.match(/\/>/).present?
      flag = @constant + 2 unless @constant.nil?
      unless @constant.nil?
        if flag == i or flag + 2 == i
          word = word.split('/>').to_s
          unless @constant1.nil?
            if @constant1 + 2 == i
              word = @image2 + word
            end
          end
          query << word
          query << " "
        end
      else
        query << word
      end
    elsif word.match(/width=/).present?
      count1 = @constant +2  unless @constant.nil?
      unless @constant.nil?
        if count1 == i
          @width = word.split('"').last.to_s
        end
      else
        query << word
      end
    elsif word.match(/height=/).present?
      height = word.split('"').last.to_s
      count2 = @constant + 3  unless @constant.nil?
      unless @constant.nil?
        if count2 == i
          if @image_path.match(/report_builder_images/).present?
            uploaded_image = @image1.split('/>').to_s
            image = uploaded_image +" width = #{@width.nil? ? nil : @width}, height =  #{height.nil? ? nil : height}/>"
            query << image
          elsif @image_path.match(/http/).present?
            uploaded_image = @image.split('/>').to_s
            image = uploaded_image +" width = #{@width.nil? ? nil : @width}, height =  #{height.nil? ? nil : height}/>"
            query << image
          else
            smiley_image = @image2.split('/>').to_s
            image = smiley_image +" width = #{@width.nil? ? nil : @width}, height =  #{height.nil? ? nil : height}/>"
            query << image
          end
        end
      else
        query << word
      end
      #    elsif word.match(/<table/).present?
      #      #width 100% causes border size issue.
      #      query << "<table width='115%'; border; cellpadding='3px'; cellspacing='0px'; border-weight='1px'; border-collapse='collapse'; "
    else
      query << word
      query << " "
    end
    return query
  end

  def self.grouped_exam_subject_total(subject,student,exam_groups,ca_terms)
    total_marks = 0
    @term_exam =[]
    term_exam =[]
    unless exam_groups.nil? or exam_groups.empty?
      exam_groups.each do |exam_group|
        unless exam_group.exam_type == 'Grades'
          exam = Exam.find_by_subject_id_and_exam_group_id(subject.id,exam_group.id)
          unless exam.nil?
            exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>exam.id})
              marks = (exam_score.nil? ? exam.maximum_marks==20 ? ca_terms.nil? ? 0 :  ReportBuilder.midterm_to_end_of_term(subject.id,student.id,ca_terms).to_f : 0 :  exam_score.marks.nil? ? exam.maximum_marks==20 ? ca_terms.nil? ? 0 : ReportBuilder.midterm_to_end_of_term(subject.id,student.id,ca_terms).to_f : 0 :  exam_score.marks).to_f
            total_marks = total_marks + marks.to_f 
          end
        end
      end
    end
    total_marks
  end

  def self.assigned_subject_term_exam_groups(subject_id, term_ids)
    @assigned_groups =[]
    @assigned_group = AssignedExamGroup.find_all_by_subject_id(subject_id)
    term_ids.each do |term_id|
      @assigned_group.each do |assigned_group|
        if SubjectExamGroup.find(assigned_group.subject_exam_group_id).term_id.to_i == term_id.to_i
          @assigned_groups << AssignedExamGroup.find(assigned_group.id)
        end
      end
    end
    @assigned_groups = @assigned_groups.compact.flatten
    return @assigned_groups
  end

  def self.add_comment_to_elective_group(student,elective_group,term_ids)
    @elective_comment =[]
    @elective_comments =  EletiveGroupComment.find(:all, :conditions => {:student_id => student.id, :elective_group_id => elective_group.id})
    term_ids.each do |term_id|
      unless @elective_comments.empty?
        @elective_comments.each do |elective_comment|
          if elective_comment.term_id.to_i == term_id.to_i
            @elective_comment << EletiveGroupComment.find(elective_comment.id)
          end
        end
      end
    end
    @elective_comment = @elective_comment.compact.flatten
    return @elective_comment
  end
  
  def self.get_weighted_exam_group(term)
    @weighted_exam_groups=[]
    unless term.nil? or term == false
      exam_groups = term.exam_groups
      unless exam_groups.empty?
        exam_groups.each do |term_exam_group|
          term_group = TermExamGroup.find_by_exam_group_id(term_exam_group.id)
          if term_group.weightage > 5
            @weighted_exam_groups <<  ExamGroup.find(term_group.exam_group_id)
          end
        end
        return @weighted_exam_groups
      end
    end
  end

  def self.get_weighted_easter_exam_group(term)
    @weighted_exam_groups=[]
    unless term.nil? or term == false
      exam_groups = term.exam_groups
      unless exam_groups.empty?
        exam_groups.each do |term_exam_group|
          term_group = TermExamGroup.find_by_exam_group_id(term_exam_group.id)
          if term_group.weightage == 20
            @weighted_exam_groups <<  ExamGroup.find(term_group.exam_group_id)
          end
        end
        return @weighted_exam_groups
      end
    end
    
  end

  def self.class_subject_total(subject_id,term_exm_groups)
    score_values = {}
    total_weight_age = 0
    total_score = 0
    @average = 0
    term_exm_groups.each do |exam_group|
      exam = Exam.find_by_subject_id_and_exam_group_id(subject_id,exam_group.id)
      total_weight_age += exam.maximum_marks.to_f unless exam.nil?
      scores = exam.exam_scores
      scores.each do |score|
        unless  score_values["#{score.student_id}"].nil?
          score_values["#{score.student_id}"]  += score.marks.to_f
        else
          score_values["#{score.student_id}"]  = score.marks.to_f
        end
      end
    end
    score_values.each do |key,value|
      total_score += ((value/total_weight_age)*100).round(2)
    end
    @average =  (total_score/score_values.size).round(2) unless score_values.blank?
    return @average
  end
 
  def  self.midterm_to_end_of_term(subject,student,term)
    value = 0
    exam_groups = term.exam_groups
    exam_groups.each do |exam_group|
      exam = Exam.find_by_subject_id_and_exam_group_id(subject.to_i,exam_group.id)
      unless exam.nil?
        if exam.maximum_marks.to_i > 5
          exam_score = ExamScore.find_by_student_id(student.to_i, :conditions=>{:exam_id=>exam.id})unless exam.nil?
          unless exam_score.nil?
            value +=  exam_score.marks.nil? ? 0 : exam_score.marks.to_f
          end
        end
      end
    end
    if value == 0
      return @ca_marks = 0
    else
      return @ca_marks = ((value)/2).to_f.round(2)
    end
  end

  def self.term_1(subject,student,term)
    ca_id = term.ca_term_id
    ca_term = Term.find(ca_id) unless ca_id.nil?
    exam_group=term.exam_groups
    total_marks = 0
    total_weightage = 0
    unless exam_group.nil? or exam_group.empty?
      exam_group.each do |eg|
         exam = Exam.find_by_subject_id_and_exam_group_id(subject.id,eg.id)
      unless exam.nil?
           if exam.maximum_marks.to_i > 5
            total_weightage +=exam.maximum_marks.to_f
          exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>exam.id})unless exam.nil?
          marks =  (exam_score.nil? ? exam.maximum_marks==20 ? ca_term.nil? ? 0 :  ReportBuilder.midterm_to_end_of_term(subject.id,student.id,ca_term).to_f : 0 :  exam_score.marks.nil? ? exam.maximum_marks==20 ? ca_term.nil? ? 0 : ReportBuilder.midterm_to_end_of_term(subject.id,student.id,ca_term).to_f : 0 :  exam_score.marks).to_f
          total_marks += marks.to_f
         end
        end
      end
      end
       total_marks == 0 ? total_value = 0 : total_value = ((total_marks/total_weightage)*100).round(2)
       return total_value
  end


  def self.term_2(subject,student,term)
    total_marks = 0
    @term_exam = []
    term_exam = []
    total_weightage = 0
    exam_groups = term.exam_groups
    unless exam_groups.nil? or exam_groups.empty?
      exam_groups.each do |exam_group|
        exam = Exam.find_by_subject_id_and_exam_group_id(subject.id,exam_group.id)
        unless exam.nil?
          term_group = TermExamGroup.find_by_exam_group_id(exam_group.id)
          if term_group.weightage == 20
            total_weightage +=exam.maximum_marks.to_f
          exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>exam.id})unless exam.nil?
          marks = exam_score.nil? ? 0 : exam_score.marks.nil? ? 0 : exam_score.marks.to_f
          total_marks += marks.to_f
          end
        end
      end
    end
     total_marks == 0 ? total_value = 0 : total_value = ((total_marks/total_weightage)*100).round(2)
       return total_value
  end
   
  def self.grade(student,sub,term)
    val = "x"
    @group = EvaluationGroup.find_by_term_id(term.id)
    @type =  EvaluationType.find_by_name_and_evaluation_group_id(sub.name,@group.id)
    @score = EvaluationScore.find_by_evaluation_type_id_and_student_id(@type.id,student.id)
    val = @score.performance.downcase unless @score.nil?
    return val
  end
   

  def self.subject_total(subject_id,term_exm_groups)
    score_values = {}
    total_weight_age = 0
    total_score = 0
    term_exm_groups.each do |exam_group|
      exam = Exam.find_by_subject_id_and_exam_group_id(subject_id,exam_group.id)
      total_weight_age += exam.maximum_marks.to_f unless exam.nil?
      scores = exam.exam_scores
      scores.each do |score|
        unless  score_values["#{score.student_id}"].nil?
          score_values["#{score.student_id}"]  += score.marks.to_f
        else
          score_values["#{score.student_id}"]  = score.marks.to_f
        end
      end
    end
    score_values.each do |key,value|
      total_score += value
    end   
    return total_score
  end

  def self.emerging_grade(student,area_id)
    @score = EvaluationScore.find_by_student_id_and_evaluation_area_id(student,area_id)
    return @score.performance.downcase unless @score.nil?
  end
   
  def self.ca_1(subject,student,term)
       ca_id = term.ca_term_id
       ca_term = Term.find(ca_id) unless ca_id.nil?
       exam_group=ca_term.exam_groups
       total_marks = 0
       total_weightage = 0
       unless exam_group.nil? or exam_group.empty?
        exam_group.each do |eg|
         exam = Exam.find_by_subject_id_and_exam_group_id(subject.id,eg.id)
         unless exam.nil?
           if exam.maximum_marks.to_i > 5
            total_weightage +=exam.maximum_marks.to_f
            exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>exam.id})unless exam.nil?
            marks =  (exam_score.nil? ? exam.maximum_marks==100 ? ca_term.nil? ? 0 :  ReportBuilder.midterm_to_end_of_term(subject.id,student.id,ca_term).to_f : 0 :  exam_score.marks.nil? ? exam.maximum_marks==100 ? ca_term.nil? ? 0 : ReportBuilder.midterm_to_end_of_term(subject.id,student.id,ca_term).to_f : 0 :  exam_score.marks).to_f
            total_marks += marks.to_f
           end
         end
        end
       end
           # total_marks == 0 ? total_value = 0 : total_value = ((total_marks/total_weightage)*100).round(2)
           return total_marks
  end

def self.class_avg(subject,term)
    @score = 0
    subject.batch.students.each do |student|
       @mmg = 1;@g = 1 
       @term_exam_groups = term.exam_groups
       @term_exam_groups.each do |exam_group|
           @exam = Exam.find_by_subject_id_and_exam_group_id(subject.id,exam_group.id)
           exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})  unless @exam.nil? 
              unless @exam.nil?
                   if exam_group.exam_type == "MarksAndGrades"
                      exam_score.nil? ? '-' : @score += exam_score.marks.to_f 
                   elsif exam_group.exam_type == "Marks" 
                      exam_score.nil? ? '-' : @score += exam_score.marks.to_f 
                   else
                      exam_score.nil? ? '-' : (exam_score.grading_level || '-') 
                       @g = 0 
                   end 
                else
                  @score = 0
              end 
        end 
    end    
    @class_avg = ((@score.to_f)/subject.batch.students.count) unless @score.nil?
end

def  self.halfterm_to_end_of_term(subject,student,term)
        value = 0
        ca_term = Term.find(term.ca_term_id)
        exam_groups = ca_term.exam_groups
        exam_groups.each do |exam_group|
          exam = Exam.find_by_subject_id_and_exam_group_id(subject.to_i,exam_group.id)
          unless exam.nil?
            if exam.maximum_marks.to_i <= 20
              exam_score = ExamScore.find_by_student_id(student.to_i, :conditions=>{:exam_id=>exam.id})unless exam.nil?
              unless exam_score.nil?
                value +=  exam_score.marks.nil? ? 0 : exam_score.marks.to_f
              end
            end
          end
        end
        if value == 0
          return @ca_marks = 0
        else
          return @ca_marks = ((value)/10).to_f.round(2)
        end
 end

def self.term_first(subject,student,term)
    ca_id = term.ca_term_id
    ca_term = Term.find(ca_id) unless ca_id.nil?
    exam_group=term.exam_groups
    total_marks = 0
    total_weightage = 0
    unless exam_group.nil? or exam_group.empty?
      exam_group.each do |eg|
         exam = Exam.find_by_subject_id_and_exam_group_id(subject.id,eg.id)
      unless exam.nil?
           if exam.maximum_marks.to_i > 5
            total_weightage +=exam.maximum_marks.to_f
          exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>exam.id})unless exam.nil?
          marks =  (exam_score.nil? ? exam.maximum_marks==20 ? ca_term.nil? ? 0 :  (ReportBuilder.midterm_to_end_of_term(subject.id,student.id,ca_term)/7.5).to_f : 0 :  exam_score.marks.nil? ? exam.maximum_marks==20 ? ca_term.nil? ? 0 : (ReportBuilder.midterm_to_end_of_term(subject.id,student.id,ca_term)/7.5).to_f : 0 :  exam_score.marks).to_f
          total_marks += marks.to_f
         end
        end
      end
      end
       total_marks == 0 ? total_value = 0 : total_value = ((total_marks/total_weightage)*100).round(2)
       return total_value
  end

  def self.merge_class_avg(student,subject,term)
    @score = 0
       @mmg = 1;@g = 1 
       @term_exam_groups = term.exam_groups
       @term_exam_groups.each do |exam_group|
           @exam = Exam.find_by_subject_id_and_exam_group_id(subject.id,exam_group.id)
           exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})  unless @exam.nil? 
              unless @exam.nil?
                   if exam_group.exam_type == "MarksAndGrades"
                      exam_score.nil? ? '-' : @score += exam_score.marks.to_f 
                   elsif exam_group.exam_type == "Marks" 
                      exam_score.nil? ? '-' : @score += exam_score.marks.to_f 
                   else
                      exam_score.nil? ? '-' : (exam_score.grading_level || '-') 
                       @g = 0 
                   end 
                else
                  "#{t('n_a')}"
              end 
        end 
    @class_avg = ((@score)/subject.batch.students.count).round(2) unless @score.nil?
  end
   
  def self.grade_eval(student,type,term)
    val = "x"
    @group = EvaluationGroup.find_by_term_id(term.id)
    @type =  EvaluationType.find_by_name_and_evaluation_group_id(type.name,@group.id)
    @score = EvaluationScore.find_by_evaluation_type_id_and_student_id(@type.id,student.id)
    val = @score.performance.downcase unless @score.nil?
    return val
  end

  def self.class_avg_with_elective(subject,term)
    subject.elective_group_id.nil? ? student_count= @batch_students.to_f : student_count = subject.students.count 
    @score = 0
    subject.students.each do |student|
       @mmg = 1;@g = 1 
       @term_exam_groups = term.exam_groups
       @term_exam_groups.each do |exam_group|
           @exam = Exam.find_by_subject_id_and_exam_group_id(subject.id,exam_group.id)
           exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})  unless @exam.nil? 
              unless @exam.nil?
                   if exam_group.exam_type == "MarksAndGrades"
                      exam_score.nil? ? '-' : @score += exam_score.marks.to_f 
                   elsif exam_group.exam_type == "Marks" 
                      exam_score.nil? ? '-' : @score += exam_score.marks.to_f 
                   else
                      exam_score.nil? ? '-' : (exam_score.grading_level || '-') 
                       @g = 0 
                   end 
                else
                  @score = 0
              end 
        end 
    end    
    @class_avg = ((@score.to_f)/student_count) unless @score.nil?
end


end

