class EvaluationArea < ActiveRecord::Base
  belongs_to :evaluation_type
   validates_presence_of :name, :batch_id, :term_id, :evaluation_group_id,:evaluation_type_id
end
