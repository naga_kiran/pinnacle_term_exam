class TermDocument < ActiveRecord::Base

	has_attached_file :document,
	:url => "/system/:class/:attachment/:id/:style/:basename.:extension",
	:path => ":rails_root/public/system/:class/:attachment/:id/:style/:basename.:extension",
  :reject_if => proc { |attributes| attributes.present? },
  :permitted_file_types =>"application/vnd.ms-excel"

  validates_attachment_content_type :document, :content_type =>"application/vnd.ms-excel",
    :message=>'File can only be XLS',:if=> Proc.new { |p| !p.document_file_name.blank? }
  validates_attachment_presence :document
  validates_uniqueness_of :name
	
end
