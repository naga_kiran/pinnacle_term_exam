class StudentTermDocument < ActiveRecord::Base
	has_attached_file :document,
	:url => "/system/:class/:attachment/:id/:style/:basename.:extension",
	:path => ":rails_root/public/system/:class/:attachment/:id/:style/:basename.:extension"
	validates_attachment_presence :document
end
