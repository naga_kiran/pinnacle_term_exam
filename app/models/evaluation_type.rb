class EvaluationType < ActiveRecord::Base
  belongs_to :evaluation_group
  has_many :evaluation_areas
  validates_presence_of :name, :code, :batch_id, :term_id, :evaluation_group_id
end
