module AlphaNumericBatchesHelper

	def score_for(student_id,subject_id,batch_id,alpha_exam_id)
		exam_score = AlphaNumericScore.find(:first, :conditions => { :student_id => student_id,:subject_id=> subject_id,:batch_id=>batch_id,:alpha_numeric_exam_id=>alpha_exam_id})
	    exam_score.nil? ? AlphaNumericScore.new : exam_score
	end

end
