module ReportBuildersHelper
  def show_header_icon
    "<div class='header-icon report-icon'></div>".html_safe
  end

  
  def wicked_pdf_image_tag_for_public(img, options={})
    if img[0] == "/"
      new_image = img.slice(1..-1)
      image_tag "file://#{Rails.root.join('public', new_image)}", options
    else
      image_tag "file://#{Rails.root.join('public', 'images', img)}", options
    end
  end

  def test(student_id)
    $student_id = student_id
  end

  def key_grade(exam_score)
     unless exam_score.nil?
      if exam_score.between?(70,100)
        @grade = "A"
      elsif exam_score.between?(60,69.99)
        @grade = "C"
      elsif exam_score.between?(50,59.99)
        @grade = "P"
      elsif exam_score.between?(0,49.99)
        @grade = "F"
      end
    end
  end

  def key_grade22(exam_score)
     unless exam_score.nil?
      if exam_score.between?(90,100)
        @grade = "A*"
      elsif exam_score.between?(80,89.99)
        @grade = "A"
      elsif exam_score.between?(70,79.99)
        @grade = "B"
      elsif exam_score.between?(60,69.99)
        @grade = "C"
      elsif exam_score.between?(50,59.99)
        @grade = "D"
      elsif exam_score.between?(40,49.99)
        @grade = "E"
      elsif exam_score.between?(0,39.99)
        @grade = "F"
      end
    end
  end

  def get_junior_grade(total)
    if total.between?(90,100)
      @performance = "A"
      @efforts = 5
    elsif total.between?(70,89.99)
      @performance = "B"
      @efforts = 4
    elsif total.between?(60,69.99)
      @performance = "C"
      @efforts = 3
    elsif total.between?(50,59.99)
      @performance = "D"
      @efforts = 2
    elsif total.between?(1,49.99)
      @performance = "NI"
      @efforts = 1
    else
      @performance = "NA"
      @efforts = "NA"
    end
  end
  
  def t_gpa(total)
    unless total.nil?
      if total.between?(90,100)
        @performance = "A*"
        @efforts = 4.0
      elsif total.between?(85,89.99)
        @performance = "A+"
        @efforts = 3.75
      elsif total.between?(80,84.99)
        @performance = "A"
        @efforts = 3.5
      elsif total.between?(75,79.99)
        @performance = "B+"
        @efforts = 3.0
      elsif total.between?(70,74.99)
        @performance = "B"
        @efforts = 2.75
      elsif total.between?(65,69.99)
        @performance = "C+"
        @efforts = 2.5
      elsif total.between?(60,64.99)
        @performance = "C"
        @efforts = 2.0
      elsif total.between?(55,59.99)
        @performance = "D+"
        @efforts = 1.5
      elsif total.between?(50,54.99)
        @performance = "D"
        @efforts = 1.0
      elsif total.between?(40,49.99)
        @performance = "E"
        @efforts = 0.5
      elsif total.between?(0,39.99)
        @performance = "F"
        @efforts = 0
      end
    end
  end

  def t_gpa_nursery(total)
    unless total.nil?
      if total.between?(90,100)
        @performance = "A"
      elsif total.between?(61,89.99)
        @performance = "B"
      elsif total.between?(51,60.99)
        @performance = "C"
      elsif total.between?(0,50.99)
        @performance = "D"
      end
    end
  end

  def report_class_average(batch,term_exam_groups,exam_groups)
    batch_students = batch.students
    @student_average = 0
    @batch_average = 0
    batch_students.each do |student|
      general_subjects = Subject.find_all_by_batch_id(batch.id, :conditions=>"elective_group_id IS NULL and is_deleted=false")
      student_electives = StudentsSubject.find_all_by_student_id(student.id,:conditions=>"batch_id = #{batch.id}")
      elective_subjects = []
      student_electives.each do |elect|
        elective_subjects.push Subject.find_by_id(elect.subject_id,:conditions => {:is_deleted => false})
      end
      subjects = general_subjects + elective_subjects
      subjects = @subjects.compact.flatten
      subjects.reject!{|s| s.no_exams==true}
      exams = Exam.find_all_by_exam_group_id(exam_groups.collect(&:id))
      subject_ids = exams.collect(&:subject_id)
      subjects.reject!{|sub| !(subject_ids.include?(sub.id))}
      unless subjects.empty?
        all_exams = exam_groups.reject{|ex| ex.exam_type == "Grades"}
        average_total = 0
        subjects.each_with_index do |subject,i|
          mmg = 1
          g = 1
          total = 0
          score = 0
          subject_total = 0
          term_exam_groups.each do |exam_group|
            exam = Exam.find_by_subject_id_and_exam_group_id(subject.id,exam_group.id)
            exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>exam.id})unless exam.nil?
            unless exam.nil?
              if exam_group.exam_type == "MarksAndGrades"
                score =  score + (exam_score.nil? ? 0 :  exam_score.marks.to_f)
              elsif exam_group.exam_type == "Marks"
                score =  score + (exam_score.nil? ? 0 :  exam_score.marks.to_f)
              else
                score =  score + (exam_score.nil? ? 0 :  exam_score.marks.to_f)
              end
            else
              score = score + 0
            end
            subject_total = total + score
          end
          average_total += subject_total
        end
      end
      @student_average += ((average_total/subjects.count).to_f).round(2)
    end
    @batch_average = ((@student_average/batch_students.count).to_f).round(2)
  end

  def house
    student_house = StudentAdditionalField.find_by_name("House")
    @student_house_detail = StudentAdditionalDetail.find_by_student_id_and_additional_field_id($student_id,student_house.id) unless student_house.nil?
  end

  def class_overall_average
    student = Student.find($student_id)
    @batch = student.batch
    batch_students = student.batch.students
    overall_score = 0
    batch_students.each do |student|
      score = student_average_score(student)
      overall_score =  overall_score + score unless score.nil?
    end
    @class_overall_score = overall_score/batch_students.count.to_i unless overall_score.nil?
  end

  def average_age_of_class
    current_student = Student.find($student_id)
    batch_students = current_student.batch.students
    overall_year_age = 0
    overall_month_age = 0
    batch_students.each do |student|
      student_year_age = Date.today.year - student.date_of_birth.year
      if student.date_of_birth.month > Date.today.month
        month = Date.today.month+ 12
        student_year_age = student_year_age - 1
        student_month_age = month - student.date_of_birth.month
      else
        student_month_age = Date.today.month - student.date_of_birth.month
      end
      overall_year_age = overall_year_age + student_year_age
      overall_month_age = overall_month_age + student_month_age
    end
    @student_overall_year_age = overall_year_age/batch_students.count.to_i
    @student_overall_month_age = overall_month_age/batch_students.count.to_i
  end

  def student_average_score(student)
    @batch = student.batch
    @term = Term.find(@term_ids.first)
    general_subjects = Subject.find_all_by_batch_id(@batch.id, :conditions=>"elective_group_id IS NULL and is_deleted=false")
    student_electives = StudentsSubject.find_all_by_student_id(student.id,:conditions=>"batch_id = #{@batch.id}")
    elective_subjects = []
    student_electives.each do |elect|
      @en_disable = RankStudentSubjectElective.find_by_students_subject_id_and_term_id(elect.id,@term.id)
      unless @en_disable.nil?
        if  @en_disable.is_deleted == false
          elective_subjects.push Subject.find(elect.subject_id)
        end
      else
        elective_subjects.push Subject.find(elect.subject_id)
      end
    end
    @subjects = general_subjects + elective_subjects
    @subjects.reject!{|s| s.no_exams==true}
    unless @subjects.empty?
      if @term_exam_groups.empty?
        exams = Exam.find_all_by_exam_group_id(@exam_groups.collect(&:id)) unless @exam_group.nil?
      else
        exams = Exam.find_all_by_exam_group_id(@term_exam_groups.collect(&:id))
      end
      subject_ids = exams.collect(&:subject_id) unless exams.nil?
      @subjects.reject!{|sub| !(subject_ids.include?(sub.id))} unless subject_ids.nil?
      @merged_subjects = RankIngresSubjectMerger.find_all_by_batch_id(@batch.id )
      unless @merged_subjects.nil?
        @merged_subjects.each do |ms|
          @subjects = @subjects.reject {|sub| ms.subject_id.include? sub.id.to_s}
        end
      end
      no_of_subjects = @subjects.flatten.count
      if @term_exam_groups.empty?
        all_exams = @exam_groups.reject{|ex| ex.exam_type == "Grades"} unless @exam_group.nil?
      else
        all_exams = @term_exam_groups.reject{|ex| ex.exam_type == "Grades"}
      end
      @grand_total = 0
      #Subjects Total Average
      unless  @subjects.blank?
        @subjects.each do |subject|
          @mmg = 1;@g = 1
          if @term_exam_groups.empty?
            unless @exam_groups.nil?
              @exam_groups.each do |exam_group|
                @exam = Exam.find_by_subject_id_and_exam_group_id(subject.id,exam_group.id)
                unless @exam.nil?
                  if exam_group.exam_type == "Grades"
                    @g = 0
                  end
                end
              end
            end
          else
            @term_exam_groups.each do |exam_group|
              @exam = Exam.find_by_subject_id_and_exam_group_id(subject.id,exam_group.id)
              unless @exam.nil?
                if exam_group.exam_type == "Grades"
                  @g = 0
                end
              end
            end
          end
          unless all_exams.nil?
            unless all_exams.empty?
              if @mmg == @g
                unless @weighted_exam_groups.nil?
                  unless @ca_exam.nil?
                    @tota = ReportBuilder.grouped_exam_subject_total(subject,student,@ca_exam,@ca_terms)
                    @grand_total = @grand_total.to_f +  @tota.to_f
                  else
                    @total = ReportBuilder.grouped_exam_subject_total(subject,student,@weighted_exam_groups,@ca_terms)
                    @grand_total = @grand_total.to_f +  @total.to_f
                  end
                end
              end
            end
          end
          #@overall_score = (@grand_total/no_of_subjects).to_f.round(2)
        end
      end
      #Merged Subject Total
      @merged_subject_total = 0
      @ms_count = 0
      unless @merged_subjects.blank?
        @merged_subjects.each do |ms|
          @subjects = Subject.find(ms.subject_id)
          @subject_total = 0
          @subjects.each do |subject|
            @mmg = 1;@g = 1
            if @term_exam_groups.empty?
              unless @exam_groups.nil?
                @exam_groups.each do |exam_group|
                  @exam = Exam.find_by_subject_id_and_exam_group_id(subject.id,exam_group.id)
                  unless @exam.nil?
                    if exam_group.exam_type == "Grades"
                      @g = 0
                    end
                  end
                end
              end
            else
              @term_exam_groups.each do |exam_group|
                @exam = Exam.find_by_subject_id_and_exam_group_id(subject.id,exam_group.id)
                unless @exam.nil?
                  if exam_group.exam_type == "Grades"
                    @g = 0
                  end
                end
              end
            end
            unless all_exams.nil?
              unless all_exams.empty?
                if @mmg == @g
                  unless @weighted_exam_groups.nil?
                    unless @ca_exam.nil?
                      @tota = ReportBuilder.grouped_exam_subject_total(subject,student,@ca_exam,@ca_terms)
                      @subject_total = @subject_total.to_f +  @tota.to_f
                    else
                      @total = ReportBuilder.grouped_exam_subject_total(subject,student,@weighted_exam_groups,@ca_terms)
                      @subject_total = @subject_total.to_f +  @total.to_f
                    end
                  end
                end
              end
            end
          end
          @merged_subject_total += (@subject_total/ms.subject_id.count).to_f.round(2) unless @subject_total == 0
        end
        @ms_count += @merged_subjects.count
      end
      @overall_score =  ((@grand_total+@merged_subject_total)/(no_of_subjects+@ms_count)).to_f.round(2)
      return @overall_score
    end

  end
  
  def tutor_full_name
    student = Student.find($student_id)
    @tutor = Employee.find(:all,:conditions=>"FIND_IN_SET(id,\"#{student.batch.employee_id}\")") unless student.batch.employee_id.nil?
    @tutors = @tutor.map{|e| e.full_name} unless @tutor.nil?
  end

  def tutor_admission_no
    student = Student.find($student_id)
    @tutor = Employee.find(:all,:conditions=>"FIND_IN_SET(id,\"#{student.batch.employee_id}\")") unless student.batch.employee_id.nil?
    @tutors = @tutor.map{|e| e.employee_number} unless @tutor.nil?
  end

  def college_examination_data
    @students = Student.find($student_id).to_a
    @type= "Grouped"
    @batch = @students.first.batch
    @term = Term.find(@term_ids.first)
    @exam_groups = ExamGroup.find_all_by_batch_id(@batch.id)
  end

  def college_term_report
    @student = Student.find($student_id)
    @batch = @student.batch
    @ca_exam_groups =[]
    @term = Term.find(@term_ids.first)
    @ca_terms = Term.find(@term.ca_term_id) unless @term.ca_term_id.nil?
    @main_exam_group =[]
    @unweighted_exam_groups =[]
    @exam_groups = ExamGroup.find_all_by_batch_id(@batch.id)
    unless @exam_groups.nil?
      @term_exam_groups.each do |term_exam_group|
        term_group = TermExamGroup.find_by_exam_group_id(term_exam_group.id)
        if term_group.weightage == 20
          @ca_exam_groups << ExamGroup.find(term_group.exam_group_id)
        elsif term_group.weightage == 60
          @main_exam_group << ExamGroup.find(term_group.exam_group_id)
        elsif term_group.weightage == 0
          @unweighted_exam_groups << ExamGroup.find(term_group.exam_group_id)
        end
      end
    end
  end

  def college_mid_term_report
    @students = Student.find($student_id).to_a
    @type= "Grouped"
    @batch = @students.first.batch
    @term = Term.find(@term_ids.first)
    @ca_terms = Term.find(@term.ca_term_id) unless @term.ca_term_id.nil?
    @exam_groups = ExamGroup.find_all_by_batch_id(@batch.id)
  end

  def college_mid_term_comment_report
    @students = Student.find($student_id).to_a
    @type= "Grouped"
    @batch = @students.first.batch
    @ca_exam_groups =[]
    @term = Term.find(@term_ids.first)
    @ca_terms = Term.find(@term.ca_term_id) unless @term.ca_term_id.nil?
    @exam_groups = ExamGroup.find_all_by_batch_id(@batch.id)
  end

  def college_easter_term_report
    @student = Student.find($student_id)
    @batch = @student.batch
    @ca_exam =[]
    @main_exam_group =[]
    @unweighted_exam_groups =[]
    @other_weighted_groups=[]
    @term = Term.find(@term_ids.first)
    @ca_terms = Term.find(@term.ca_term_id) unless @term.ca_term_id.nil?
    @link_term_groups = TermLinkGroup.find_all_by_term_id(@term_ids.first)
    @term_exam_groups.each do |term_exam_group|
      term_group = TermExamGroup.find_by_exam_group_id(term_exam_group.id)
      if term_group.weightage == 20
        @ca_exam << ExamGroup.find(term_group.exam_group_id)
      elsif term_group.weightage == 60
        @main_exam_group << ExamGroup.find(term_group.exam_group_id)
      elsif term_group.weightage == 0
        @unweighted_exam_groups << ExamGroup.find(term_group.exam_group_id)
      elsif term_group.weightage > 0
        @other_weighted_groups << ExamGroup.find(term_group.exam_group_id)
      end
    end
  end

  def creative_learning
    @range = 5..1
    @student = Student.find($student_id)
    @batch = @student.batch
    @exam_groups = ExamGroup.find_all_by_batch_id(@batch.id)
    @term = Term.find(@term_ids.first)
    @groups = []
    evaluation_groups = @term.evaluation_groups
    evaluation_groups.each do |evaluation_group|
      evaluation_area = evaluation_group.evaluation_types.first.evaluation_areas
      if evaluation_area.blank?
        @groups << evaluation_group
      end
    end
  end

  def amville_report
    @activity_group = []
    @activity_group2 = []
    @groups = []
    @report_values = ["A","b","C","d"]
    @student = Student.find($student_id)
    @batch = @student.batch
    @tutor = TermSignature.find_by_is_principal_and_batch_id_and_school_id(false,@batch.id,MultiSchool.current_school.id)
    @principal = TermSignature.find_by_is_principal_and_school_id(true,MultiSchool.current_school.id)
    @exam_groups = ExamGroup.find_all_by_batch_id(@batch.id)
    @term = Term.find(@term_ids.first)
    @all_groups = @term.evaluation_groups
    @all_groups.each do |group|
      if group.score_format == "Extracurricular"
        @activity_group << group
      elsif group.score_format == "Extracurricular2"
        @activity_group2 << group
      else
        @groups << group
      end
    end
    @activity_values = ["Key skill learnt","Conduct","Punctuality","Participation"]
    @activity_values2 = ["Grading Rubric","Excellent","Very Good","Improving","Unsatisfactory"]
    # CALCULATE ATTENDANCE
    @sub = Subject.find_all_by_batch_id(@batch.id,:conditions=>'is_deleted = false')
    @electives = @sub.map{|x|x unless x.elective_group_id.nil?}.compact
    @electives.reject! { |z| z.students.include?(@student)  }
    @sub -= @electives
    @config = Configuration.find_by_config_key('StudentAttendanceType')
    @start_date = @term.start_date.to_date
    @end_date = @term.end_date.to_date > Date.today ? Date.today : @term.end_date.to_date
    unless @config.config_value == 'Daily'
      @academic_days=@batch.subject_hours(@start_date, @end_date, 0).values.flatten.compact.count
      @student_leaves = SubjectLeave.find(:all,  :conditions =>{:student_id=>@student.id,:month_date => @start_date..@end_date, :subject_id => @sub.map(&:id)})
      @leaves= @student_leaves.count
      @leaves||=0
      @attendance = (@academic_days - @leaves)
    else
      @student_leaves = Attendance.find(:all,  :conditions =>{:batch_id=>@batch.id,:student_id=>@student.id,:month_date => @start_date..@end_date})
      @academic_days=@batch.academic_days.select{|v| v<=@end_date}.count
      leaves_forenoon=Attendance.count(:all,:conditions=>{:batch_id=>@batch.id,:student_id=>@student.id,:forenoon=>true,:afternoon=>false,:month_date => @start_date..@end_date})
      leaves_afternoon=Attendance.count(:all,:conditions=>{:batch_id=>@batch.id,:student_id=>@student.id,:forenoon=>false,:afternoon=>true,:month_date => @start_date..@end_date})
      leaves_full=Attendance.count(:all,:conditions=>{:batch_id=>@batch.id,:student_id=>@student.id,:forenoon=>true,:afternoon=>true,:month_date => @start_date..@end_date})
      @leaves=leaves_full.to_f+(0.5*(leaves_forenoon.to_f+leaves_afternoon.to_f))
      @attendance = (@academic_days - @leaves)
    end
  end

  def amville_nursery_report
    @activity_group = []
    @activity_group2 = []
    @groups = []
    @report_values = ["A","b","C","d"]
    @student = Student.find($student_id)
    @batch = @student.batch
    @tutor = TermSignature.find_by_is_principal_and_batch_id_and_school_id(false,@batch.id,MultiSchool.current_school.id)
    @principal = TermSignature.find_by_is_principal_and_school_id(true,MultiSchool.current_school.id)
    @exam_groups = ExamGroup.find_all_by_batch_id(@batch.id)
    @term = Term.find(@term_ids.first)
    @all_groups = @term.evaluation_groups
    @all_groups.each do |group|
      if group.score_format == "Extracurricular"
        @activity_group << group
      elsif group.score_format == "Extracurricular2"
        @activity_group2 << group
      else
        @groups << group
      end
    end
    @activity_values = ["Key skill learnt","Conduct","Punctuality","Participation"]
    @activity_values2 = ["Grading Rubric","Excellent","Very Good","Improving","Unsatisfactory"]
    # CALCULATE ATTENDANCE
    @sub = Subject.find_all_by_batch_id(@batch.id,:conditions=>'is_deleted = false')
    @electives = @sub.map{|x|x unless x.elective_group_id.nil?}.compact
    @electives.reject! { |z| z.students.include?(@student)  }
    @sub -= @electives
    @config = Configuration.find_by_config_key('StudentAttendanceType')
    @start_date = @term.start_date.to_date
    @end_date = @term.end_date.to_date > Date.today ? Date.today : @term.end_date.to_date
    unless @config.config_value == 'Daily'
      @academic_days=@batch.subject_hours(@start_date, @end_date, 0).values.flatten.compact.count
      @student_leaves = SubjectLeave.find(:all,  :conditions =>{:student_id=>@student.id,:month_date => @start_date..@end_date, :subject_id => @sub.map(&:id)})
      @leaves= @student_leaves.count
      @leaves||=0
      @attendance = (@academic_days - @leaves)
    else
      @student_leaves = Attendance.find(:all,  :conditions =>{:batch_id=>@batch.id,:student_id=>@student.id,:month_date => @start_date..@end_date})
      @academic_days=@batch.academic_days.select{|v| v<=@end_date}.count
      leaves_forenoon=Attendance.count(:all,:conditions=>{:batch_id=>@batch.id,:student_id=>@student.id,:forenoon=>true,:afternoon=>false,:month_date => @start_date..@end_date})
      leaves_afternoon=Attendance.count(:all,:conditions=>{:batch_id=>@batch.id,:student_id=>@student.id,:forenoon=>false,:afternoon=>true,:month_date => @start_date..@end_date})
      leaves_full=Attendance.count(:all,:conditions=>{:batch_id=>@batch.id,:student_id=>@student.id,:forenoon=>true,:afternoon=>true,:month_date => @start_date..@end_date})
      @leaves=leaves_full.to_f+(0.5*(leaves_forenoon.to_f+leaves_afternoon.to_f))
      @attendance = (@academic_days - @leaves)
    end
  end

  def highest_lowest_score(subject_id)
    h_score = {}
    @students = @batch.students
    @term_exam_groups.each do |term_group|
      exam = Exam.find_by_subject_id_and_exam_group_id(subject_id,term_group.id)
      @students.each do |student|
        score = ExamScore.find_by_student_id_and_exam_id(student.id,exam.id)
        unless h_score["#{student.id}"].nil? 
          h_score["#{student.id}"] =  h_score["#{student.id}"] +  (score.nil? ? 0 : score.marks).to_f
        else 
          h_score["#{student.id}"] =  (score.nil? ? 0 : score.marks).to_f
        end
      end
    end
    score_values = h_score.values.map(&:to_f)
    @highest_score = (score_values.max).to_d
    @lowest_score = (score_values.min).to_d
    @average_score = score_values.sum/h_score.count
  end

  def college_omega_term_report
    @student = Student.find($student_id)
    @batch = @student.batch
    @ca_exam_groups =[]
    @main_exam_group =[]
    @unweighted_exam_groups =[]
    @other_weighted_groups=[]
    @exam_groups = ExamGroup.find_all_by_batch_id(@batch.id)
    @link_term_groups = TermLinkGroup.find_all_by_term_id(@term_ids.first)
    @term = Term.find(@term_ids.first)
    @ca_terms = Term.find(@term.ca_term_id) unless @term.ca_term_id.nil?
    unless @exam_groups.nil?
      @term_exam_groups.each do |term_exam_group|
        term_group = TermExamGroup.find_by_exam_group_id(term_exam_group.id)
        if term_group.weightage == 20
          @ca_exam_groups << ExamGroup.find(term_group.exam_group_id)
        elsif term_group.weightage == 60
          @main_exam_group << ExamGroup.find(term_group.exam_group_id)
        elsif term_group.weightage == 0
          @unweighted_exam_groups << ExamGroup.find(term_group.exam_group_id)
        elsif term_group.weightage > 0
          @other_weighted_groups << ExamGroup.find(term_group.exam_group_id)
        end
      end
    end
  end

  def college_omega_term_report_note
    @student = Student.find($student_id)
    @batch = @student.batch
    @ca_exam_groups =[]
    @main_exam_group =[]
    @unweighted_exam_groups =[]
    @other_weighted_groups=[]
    @link_term_groups = TermLinkGroup.find_all_by_term_id(@term_ids.first)
    @term = Term.find(@term_ids.first)
    @ca_terms = Term.find(@term.ca_term_id) unless @term.ca_term_id.nil?
    @term_exam_groups.each do |term_exam_group|
      term_group = TermExamGroup.find_by_exam_group_id(term_exam_group.id)
      if term_group.weightage == 20
        @ca_exam_groups << ExamGroup.find(term_group.exam_group_id)
      elsif term_group.weightage == 60
        @main_exam_group << ExamGroup.find(term_group.exam_group_id)
      elsif term_group.weightage == 0
        @unweighted_exam_groups << ExamGroup.find(term_group.exam_group_id)
      elsif term_group.weightage > 0
        @other_weighted_groups << ExamGroup.find(term_group.exam_group_id)
      end
    end
    @weighted_exam_groups = @ca_exam_groups
  end

  def junior_examination_data
    @students = Student.find($student_id).to_a
    @batch = @students.first.batch
    @exam_groups = ExamGroup.find_all_by_batch_id(@batch.id)
    @term =  Term.find(@term_ids.first)
  end

  def junior_second_term_report
    @student = Student.find($student_id)
    @batch = @student.batch
    @term =  Term.find(@term_ids.first)
  end
   
  def infant_examination_data
    @student = Student.find($student_id)
    @batch = @student.batch
    @evaluation_groups = []
    @term_ids.each do |term_id|
      @evaluation_groups << Term.find(term_id).evaluation_groups
    end
    @evaluation_groups = @evaluation_groups.compact.flatten
  end

  def moct_report
    @student = Student.find($student_id)
    @batch = @student.batch
    @term = Term.find(@term_ids.first)
    @exam_group = @term.exam_groups.last
    arr_exam = Exam.find_all_by_exam_group_id(@exam_group.id).map {|p| p.id}
    @exam_score = ExamScore.find_all_by_student_id_and_exam_id(@student.id,arr_exam)
  end

  def tutor_comments
    @student = Student.find($student_id)
    @batch = @student.batch
    @comment = TutorComment.find_by_batch_id_and_term_id_and_student_id_and_is_principal(@batch.id,@term_ids.first,$student_id,false)

  end

  def house_parent_comment
    @student = Student.find($student_id)
    @batch = @student.batch
    @term = Term.find(@term_ids.first)
    @comment = HouseParentComment.find_by_batch_id_and_student_id_and_term_id(@batch.id,$student_id,@term.id)
  end

  def term_grade(exam_score)
    if exam_score.between?(90,100)
      @grade = "A+"
    elsif exam_score.between?(80,89.99)
      @grade = "A"
    elsif exam_score.between?(70,79.99)
      @grade = "B"
    elsif (exam_score.between?(60,69.99))
      @grade = "C"
    elsif (exam_score.between?(50,59.99))
      @grade = "D"
    elsif (exam_score.between?(40,49.99))
      @grade = "E"
    elsif (exam_score.between?(30,39.99))
      @grade = "F"
    elsif (exam_score.between?(20,29.99))
      @grade = "G"
    else
      @grade = "U"
    end
  end
  def t_grade(exam_score)
    if exam_score.between?(94,100)
      @grade = "A*"
    elsif exam_score.between?(92,93.99)
      @grade = "A"
    elsif exam_score.between?(83,91.99)
      @grade = "B"
    elsif (exam_score.between?(78,82.99))
      @grade = "C+"
    elsif (exam_score.between?(73,77.99))
      @grade = "C2"
    elsif (exam_score.between?(60,72.99))
      @grade = "C"
    elsif (exam_score.between?(50,59.99))
      @grade = "D"
    elsif (exam_score.between?(0,44.99))
      @grade = "F"
    else
      @grade = "-"
    end
  end
  def get_grade(exam_score)
    unless exam_score.nil?
      if exam_score.between?(90,100)
        @grade = "A+"
      elsif exam_score.between?(85,89.99)
        @grade = "A"
      elsif exam_score.between?(80,84.99)
        @grade = "A-"
      elsif exam_score.between?(75,79.99)
        @grade = "B"
      elsif (exam_score.between?(70,74.99))
        @grade = "B-"
      elsif (exam_score.between?(65,69.99))
        @grade = "C+"
      elsif (exam_score.between?(60,64.99))
        @grade = "C"
      elsif (exam_score.between?(55,59.99))
        @grade = "C-"
      elsif (exam_score.between?(50,54.99))
        @grade = "D"
      elsif (exam_score.between?(45,49.99))
        @grade = "E"
      elsif (exam_score.between?(0,44.99))
        @grade = "Ungraded"
      else
        @grade = "-"
      end
    end
  end

  def students_overall_gpa
    grade = overall_grade
    if grade == "A+"
      @verall_gpa = 5.0
    elsif grade == "A"
      @verall_gpa = 4.5
    elsif grade == "A-"
      @verall_gpa = 4.0
    elsif grade == "B"
      @verall_gpa = 3.5
    elsif grade == "B-"
      @verall_gpa = 3.0
    elsif grade == "C+"
      @verall_gpa = 2.5
    elsif grade == "C"
      @verall_gpa = 2.0
    elsif grade == "C-"
      @verall_gpa = 1.5
    elsif grade == "D"
      @verall_gpa = 1.0
    elsif grade == "E"
      @verall_gpa = 0.5
    elsif grade == "Ungraded"
      @verall_gpa = 0.0
    end
  end

  def students_average_gpa(grade)
    unless grade.nil?
      if grade == "A+"
        @verall_gpa = 5.0
      elsif grade == "A"
        @verall_gpa = 4.5
      elsif grade == "A-"
        @verall_gpa = 4.0
      elsif grade == "B"
        @verall_gpa = 3.5
      elsif grade == "B-"
        @verall_gpa = 3.0
      elsif grade == "C+"
        @verall_gpa = 2.5
      elsif grade == "C"
        @verall_gpa = 2.0
      elsif grade == "C-"
        @verall_gpa = 1.5
      elsif grade == "D"
        @verall_gpa = 1.0
      elsif grade == "E"
        @verall_gpa = 0.5
      elsif grade == "Ungraded"
        @verall_gpa = 0.0
      end
    end
  end

  def overall_score
    student = Student.find($student_id)
    @overall_score = student_average_score(student)
  end

  def average_percentage
    student = Student.find($student_id)
    @batch = student.batch
    @exam_groups = ExamGroup.find_all_by_batch_id(@batch.id)
    @overall_score = student_average_score(student)
    @average_percentage = ((@overall_score*100)/(@weighted_total.to_i)).to_f.round(2)
  end

  def class_overall_percentage
    current_student = Student.find($student_id)
    batch_students = current_student.batch.students
    overall_percentage = 0
    batch_students.each do |student|
      students = Student.find(student.id)
      @batch = students.batch
      @exam_groups = ExamGroup.find_all_by_batch_id(@batch.id)
      @overall_score = student_average_score(students)
      @average_percentage = ((@overall_score*100)/(@weighted_total.to_i)).to_f.round(2)
      overall_percentage =  overall_percentage + @average_percentage
    end
    @class_overall_percentage = overall_percentage/batch_students.count.to_i
  end
 

  def overall_grade
    student = Student.find($student_id)
    @batch = student.batch
    @exam_groups = ExamGroup.find_all_by_batch_id(@batch.id)
    @overall_score = student_average_score(student)
    @verall_grade = "Ungraded"
    unless @overall_score.to_i == 0
      @average_percentage = ((@overall_score*100)/(@weighted_total.to_i)).to_f.round(2)
      @verall_grade = get_grade(@average_percentage) unless @average_percentage.nil?
    end
    return @verall_grade
  end

  def tutor_signature
    @students = Student.find($student_id)
    @batch = @students.batch
    @tutor = TermSignature.find_by_is_principal_and_batch_id_and_school_id(false,@batch.id,MultiSchool.current_school.id)
  end

  def principal_name
    @principal = TermSignature.find_by_is_principal_and_school_id(true,MultiSchool.current_school.id)
  end

  def director_signature
    @director = TermSignature.find_by_is_director_and_school_id(true,MultiSchool.current_school.id)
  end

  def student_photo
    @students = Student.find($student_id)
  end

  def terms
    @term =  Term.find(@term_ids.first)
  end

  #<<<<<<< HEAD
  #  # def terms
  #  ##    @studnt = Student.first
  #  # end
  #  #
  #  #  def session
  #  ##    @studnt = Student.first
  #  #  end
  #=======
  ## def terms
  ###    @studnt = Student.first
  ## end
  ##
  ##  def session
  ###    @studnt = Student.first
  ##  end
  #>>>>>>> d8cc73e6c1d9a5a7baed8ef1d3545512e1de2e41

  def age
    student = Student.find($student_id)
    dob = student.date_of_birth
    @year = Date.today.year - dob.year
    if dob.month > Date.today.month
      month = Date.today.month+ 12
      @year = @year - 1
      @month = month - dob.month
    else
      @month = Date.today.month - dob.month
    end
  end

  def home_work_ratings
    @home_work = HomeWorkRating.find_by_student_id($student_id)
    @rating = @home_work.rating unless @home_work.nil?
  end
  def school_log

  end
  def junior_end_of_term_report
    @student = Student.find($student_id)
    @batch = @student.batch
    @term = Term.find(@term_ids.first)
    @groups = @term.evaluation_groups
    @group_count = (@groups.count/2).to_i
    @first_group = @groups[0..@group_count]
    # @comment = TutorComment.find_by_batch_id_and_term_id_and_student_id_and_is_principal(@batch.id,@term.id,@student.id,false)
    @second_group = @groups[@group_count+1..@groups.count-1]
    # CALCULATE ATTENDANCE
    @sub = Subject.find_all_by_batch_id(@batch.id,:conditions=>'is_deleted = false')
    @electives = @sub.map{|x|x unless x.elective_group_id.nil?}.compact
    @electives.reject! { |z| z.students.include?(@student)  }
    @sub -= @electives
    @config = Configuration.find_by_config_key('StudentAttendanceType')
    @start_date = @term.start_date.to_date
    @end_date = @term.end_date.to_date > Date.today ? Date.today : @term.end_date.to_date
    unless @config.config_value == 'Daily'
      @academic_days=@batch.subject_hours(@start_date, @end_date, 0).values.flatten.compact.count
      @student_leaves = SubjectLeave.find(:all,  :conditions =>{:student_id=>@student.id,:month_date => @start_date..@end_date, :subject_id => @sub.map(&:id)})
      @leaves= @student_leaves.count
      @leaves||=0
      @attendance = (@academic_days - @leaves)
    else
      @student_leaves = Attendance.find(:all,  :conditions =>{:batch_id=>@batch.id,:student_id=>@student.id,:month_date => @start_date..@end_date})
      @academic_days=@batch.academic_days.select{|v| v<=@end_date}.count
      leaves_forenoon=Attendance.count(:all,:conditions=>{:batch_id=>@batch.id,:student_id=>@student.id,:forenoon=>true,:afternoon=>false,:month_date => @start_date..@end_date})
      leaves_afternoon=Attendance.count(:all,:conditions=>{:batch_id=>@batch.id,:student_id=>@student.id,:forenoon=>false,:afternoon=>true,:month_date => @start_date..@end_date})
      leaves_full=Attendance.count(:all,:conditions=>{:batch_id=>@batch.id,:student_id=>@student.id,:forenoon=>true,:afternoon=>true,:month_date => @start_date..@end_date})
      @leaves=leaves_full.to_f+(0.5*(leaves_forenoon.to_f+leaves_afternoon.to_f))
      @attendance = (@academic_days - @leaves)
      ########Head teacher comment's##########
      student_average = student_group_score(@student)
      unless student_average.nil?
        if student_average <=49
          @comment="Needs to work harder in most areas of learning."
        elsif  student_average<=59.9
          @comment="Keep giving your best. you can do it."
        elsif  student_average <=69.9
          @comment="A good end of term's result. Keep giving your best."
        elsif  student_average<=89.9
          @comment="This is a very good end of term's report."
        else  student_average<=100 
          @comment="This is an excellent result. Well done!"
        end
      end
    end
  end

  def overall_score_gpa
  end

  def overall_score_grade
  end

  def subject_teacher_signature
    @subject_teacher = TermSignature.find_by_is_principal_and_is_director_and_school_id(false,false,MultiSchool.current_school.id)
  end
  
  def total_student_in_class
    @term =Term.find(@term_ids.first)
    @count= @term.batch.students.count unless @term_ids.blank? 
  end

  def number_of_day_present
    @student = Student.find($student_id)
    @batch = @student.batch
    @term = Term.find(@term_ids.first)
    @config = Configuration.find_by_config_key('StudentAttendanceType')
    @start_date = @term.start_date.to_date
    @end_date = @term.end_date.to_date > Date.today ? Date.today : @term.end_date.to_date
    unless @config.config_value == 'Daily'
      @academic_days=@batch.subject_hours(@start_date, @end_date, 0).values.flatten.compact.count
      @student_leaves = SubjectLeave.find(:all,  :conditions =>{:student_id=>@student.id,:month_date => @start_date..@end_date, :subject_id => @sub.map(&:id)})
      @leaves= @student_leaves.count
      @leaves||=0
      @attendance = (@academic_days - @leaves)
    else
      @student_leaves = Attendance.find(:all,  :conditions =>{:batch_id=>@batch.id,:student_id=>@student.id,:month_date => @start_date..@end_date})
      @academic_days=@batch.academic_days.select{|v| v<=@end_date}.count
      leaves_forenoon=Attendance.count(:all,:conditions=>{:batch_id=>@batch.id,:student_id=>@student.id,:forenoon=>true,:afternoon=>false,:month_date => @start_date..@end_date})
      leaves_afternoon=Attendance.count(:all,:conditions=>{:batch_id=>@batch.id,:student_id=>@student.id,:forenoon=>false,:afternoon=>true,:month_date => @start_date..@end_date})
      leaves_full=Attendance.count(:all,:conditions=>{:batch_id=>@batch.id,:student_id=>@student.id,:forenoon=>true,:afternoon=>true,:month_date => @start_date..@end_date})
      @leaves=leaves_full.to_f+(0.5*(leaves_forenoon.to_f+leaves_afternoon.to_f))
      @attendance = (@academic_days - @leaves)
    end
  end

  def number_of_time_school_open
    @student = Student.find($student_id)
    @batch = @student.batch
    @term = Term.find(@term_ids.first)
    @config = Configuration.find_by_config_key('StudentAttendanceType')
    @start_date = @term.start_date.to_date
    @end_date = @term.end_date.to_date > Date.today ? Date.today : @term.end_date.to_date
    unless @config.config_value == 'Daily'
      @academic_days=@batch.subject_hours(@start_date, @end_date, 0).values.flatten.compact.count
    end
  end

  def class_position
    all_score = []
    current_student = Student.find($student_id)
    batch_students = current_student.batch.students
    batch_students.each do |student|
      @overall_score = student_average_score(student)
      all_score << @overall_score
    end
    all_scores = all_score.sort.reverse
    index = all_scores.each_with_index.map{|score,i| score == @overall_score ? i : nil}.compact
    @position = index.join(",").to_i + 1
  end

  def position_in_subject

  end
  
  def next_term_begins
    @term =Term.find(@term_ids.first)
    @assumption_date = @term.assumption_date
  end

  def number_of_times_punctual
    student = Student.find($student_id)
    @overall_score = student_average_score(student)
  end

  def primary_term_report
    @principal = TermSignature.find_by_is_principal_and_school_id(true,MultiSchool.current_school.id)
    @student = Student.find($student_id)
    @batch = @student.batch
    @tutor = TermSignature.find_by_is_principal_and_batch_id_and_school_id(false,@batch.id,MultiSchool.current_school.id)
    @batch_students = @batch.students.count
    @exam_groups = ExamGroup.find_all_by_batch_id(@batch.id)
    @term = Term.find(@term_ids.first)
    @evaluation_group= EvaluationGroup.find_by_term_id(@term.id)
    @target_area = EvaluationComment.find_by_term_id_and_student_id_and_evaluation_group_id(@term.id,@student.id,@evaluation_group.id)
    @class_conduct = ["a","b","c","d","e"]
  end
  
  def kindergarten_report
    @student = Student.find($student_id)
    @batch = @student.batch
    @exam_groups = ExamGroup.find_all_by_batch_id(@batch.id)
    @term = Term.find(@term_ids.first)
    @evaluation_groups = EvaluationGroup.find_all_by_term_id(@term.id)
    @emerging = ["Emerging","Expected","Exceeding"]
    @effort = ["Needs improvement ","Satisfactory ","Excellent"]
    @target_area = EvaluationComment.find_by_term_id_and_student_id_and_evaluation_group_id(@term.id,@student.id,@evaluation_groups.last.id)
    @tutor = TermSignature.find_by_is_principal_and_batch_id_and_school_id(false,@batch.id,MultiSchool.current_school.id)
    @principal = TermSignature.find_by_is_principal_and_school_id(true,MultiSchool.current_school.id)
 
  end
  
  def student_group_score(student)
    no_of_groups = 0
    grand_type_total = 0
    @term = Term.find(@term_ids.first)
    @term_exam_groups = @term.exam_groups
    @evaluation_groups = @term.evaluation_groups
    unless @evaluation_groups.blank? and @term_exam_groups.blank?
      @evaluation_groups.each do |group|
        unless  group.evaluation_types.blank?
          group.evaluation_types.each do |type|
            evaluation_score = EvaluationScore.find_by_student_id_and_evaluation_type_id(student.id,type.id)
            unless  evaluation_score.nil?
              type_total = 0
              @term_exam_groups.each do |exam_group|
                type_total += evaluation_score.exam_group_scores["#{exam_group.id}"].blank? ? 0 : evaluation_score.exam_group_scores["#{exam_group.id}"].to_f
              end
              no_of_groups +=1  unless type_total == 0
            end
            grand_type_total += type_total unless  type_total.nil?
          end
        end
      end
    end
    score = (grand_type_total/no_of_groups).to_f.round(2) unless grand_type_total == 0
    return score
  end
  
  def student_group_average
    @student = Student.find($student_id)
    @student_average = student_group_score(@student)
  end
  def group_overall_grade
  end
  def group_class_average
    current_student = Student.find($student_id)
    batch_students = current_student.batch.students
    @batch = current_student.batch
    overall_marks = 0
    batch_students.each do |student|
      students = Student.find(student.id)
      @overall_score = student_group_score(students)
      overall_marks +=  @overall_score unless @overall_score.nil?
    end
    @class_overall_marks = overall_marks/batch_students.count.to_i unless overall_marks==0
  end

  def annual_score
    student = Student.find($student_id)
    @value =  student_annual_score(student)
  end

  def student_annual_score(student)
    @batch = student.batch
     @term = Term.find(@term_ids.first)
    general_subjects = Subject.find_all_by_batch_id(@batch.id, :conditions=>"elective_group_id IS NULL and is_deleted=false")
    student_electives = StudentsSubject.find_all_by_student_id(student.id,:conditions=>"batch_id = #{@batch.id}")
    elective_subjects = []
    student_electives.each do |elect|
      @en_disable = RankStudentSubjectElective.find_by_students_subject_id_and_term_id(elect.id,@term.id)
      unless @en_disable.nil?
        if  @en_disable.is_deleted == false
          elective_subjects.push Subject.find(elect.subject_id)
        end
      else
        elective_subjects.push Subject.find(elect.subject_id)
      end
    end
    @subjects = general_subjects + elective_subjects
    @subjects.reject!{|s| s.no_exams==true}
    unless @subjects.empty?
      if @term_exam_groups.empty?
        exams = Exam.find_all_by_exam_group_id(@exam_groups.collect(&:id)) unless @exam_group.nil?
      else
        exams = Exam.find_all_by_exam_group_id(@term_exam_groups.collect(&:id))
      end
      subject_ids = exams.collect(&:subject_id) unless exams.nil?
      @subjects.reject!{|sub| !(subject_ids.include?(sub.id))} unless subject_ids.nil?
      @merged_subjects = RankIngresSubjectMerger.find_all_by_batch_id(@batch.id )
      unless @merged_subjects.nil?
        @merged_subjects.each do |ms|
          @subjects = @subjects.reject {|sub| ms.subject_id.include? sub.id.to_s}
        end
      end
      @link_term_groups = TermLinkGroup.find_all_by_term_id(@term_ids.first)
      @normal_total = 0
      @overall_grand_total = 0
      @grand_overall_merge = 0
      unless @link_term_groups.blank?
        @subjects.each do |subject|
          cas_total = 0
          total = 0
          main_exam_total = 0
          grand_total = 0
          @exam_group_marks = 0
          @weighted_total = 0
          unless @ca_exam_groups.nil?
            @ca_exam_groups.each do |exam_group|
              @exam = Exam.find_by_subject_id_and_exam_group_id(subject.id,exam_group.id)
              @weighted_total += @exam.maximum_marks.to_f
              exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil?
              unless @exam.nil?
                if  exam_group.exam_type == "MarksAndGrades"
                  cas_total =  (exam_score.nil? ? @ca_terms.nil? ? 0 :  ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@ca_terms).to_f :  exam_score.marks.nil? ? @ca_terms.nil? ? 0 : ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@ca_terms).to_f:  exam_score.marks).to_f
                  total = total + cas_total
                elsif exam_group.exam_type == "Marks"
                  cas_total =   (exam_score.nil? ? @ca_terms.nil? ? 0 :  ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@ca_terms).to_f :  exam_score.marks.nil? ? @ca_terms.nil? ? 0 : ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@ca_terms).to_f:  exam_score.marks).to_f
                  total = total + cas_total
                end
              end
            end
          end
          unless @main_exam_group.nil?
            @main_exam_group.each do |exam_group|
              @exam = Exam.find_by_subject_id_and_exam_group_id(subject.id,exam_group.id)
              @weighted_total += @exam.maximum_marks.to_f
              exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil?
              unless @exam.nil?
                if exam_group.exam_type == "MarksAndGrades"
                  main_exam_total +=   exam_score.nil? ? 0 :  exam_score.marks

                elsif exam_group.exam_type == "Marks"
                  main_exam_total += exam_score.nil? ? 0 : exam_score.marks
                end
              end
            end
          end
          grand_total = main_exam_total.to_f + total.to_f
          @exam_group_marks =   ((grand_total*100)/@weighted_total.to_i).round(2)  unless grand_total == 0
          
          @link_total = 0
          @total_count = 1
          @link_term_groups.each do |link_term|
            @term =  Term.find(link_term.link_term_id)
            term_value =  (ReportBuilder.term_1(subject,student,@term)).to_f
            @total_count += 1 unless term_value==0 
            @link_total += term_value  unless term_value==0 
          end
          @normal_total += ((@link_total+@exam_group_marks)/@total_count) 
        end
                    
        unless @merged_subjects.blank?
          
          @merged_subjects.each do |ms|
                     @overall_merge_total = 0
            @subject = Subject.find(ms.subject_id)
            @subject.each do |subject|
              @merge_total = 0
              cas_total = 0
              total = 0
              main_exam_total = 0
              grand_total = 0
              @exam_group_marks = 0
              @weighted_total = 0
              unless @ca_exam_groups.nil?
                @ca_exam_groups.each do |exam_group|
                  @exam = Exam.find_by_subject_id_and_exam_group_id(subject.id,exam_group.id)
                  @weighted_total += @exam.maximum_marks.to_f
                  exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil?
                  unless @exam.nil?
                    if  exam_group.exam_type == "MarksAndGrades"
                      cas_total =  (exam_score.nil? ? @ca_terms.nil? ? 0 :  ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@ca_terms).to_f :  exam_score.marks.nil? ? @ca_terms.nil? ? 0 : ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@ca_terms).to_f:  exam_score.marks).to_f
                      total = total + cas_total
                    elsif exam_group.exam_type == "Marks"
                      cas_total =   (exam_score.nil? ? @ca_terms.nil? ? 0 :  ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@ca_terms).to_f :  exam_score.marks.nil? ? @ca_terms.nil? ? 0 : ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@ca_terms).to_f:  exam_score.marks).to_f
                      total = total + cas_total
                    end
                  end
                end
              end
              unless @main_exam_group.nil?
                @main_exam_group.each do |exam_group|
                  @exam = Exam.find_by_subject_id_and_exam_group_id(subject,exam_group.id)
                  @weighted_total += @exam.maximum_marks.to_f
                  exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil?
                  unless @exam.nil?
                    if exam_group.exam_type == "MarksAndGrades"
                      main_exam_total +=   exam_score.nil? ? 0 :  exam_score.marks

                    elsif exam_group.exam_type == "Marks"
                      main_exam_total += exam_score.nil? ? 0 : exam_score.marks
                    end
                  end
                end
              end
              grand_total = main_exam_total.to_f + total.to_f
              @exam_group_marks =   ((grand_total*100)/@weighted_total.to_i).round(2)  unless grand_total == 0
              Rails.logger.info "normal#{@exam_group_marks}"
              @link_total = 0
              @total_count = 1
              @link_term_groups.each do |link_term|
                @term =  Term.find(link_term.link_term_id)
                term_value =  (ReportBuilder.term_1(subject,student,@term)).to_f
                Rails.logger.info "#{term_value}"
                @total_count += 1 unless term_value==0 
                @link_total += term_value  unless term_value==0 
              end
              Rails.logger.info "count#{@total_count}"
              @merge_total += ((@link_total+@exam_group_marks)/@total_count)
              @overall_merge_total += @merge_total == 0 ? 0 : (@merge_total)
            end
            @overall_merge_total = (@overall_merge_total/ms.subject_id.count)
            @overall_grand_total += @overall_merge_total
          end
        end
      end
      merge_count = @merged_subjects.blank? ? 0 : @merged_subjects.count
      normal_count = @subjects.blank? ? 0 : @subjects.count
       no_of_subjects = merge_count.to_i + normal_count.to_i
      @annual_score = @combined_total =  ((@normal_total + @overall_grand_total)/no_of_subjects ).to_f.round(2) unless no_of_subjects==0
    end 
    return @annual_score
  end

def class_annual_overall
    current_student = Student.find($student_id)
    batch_students = current_student.batch.students
    overall_percentage = 0
    batch_students.each do |student|
      @batch = student.batch
      @exam_groups = ExamGroup.find_all_by_batch_id(@batch.id)
      @overall_score = student_annual_score(student)
      overall_percentage =  overall_percentage + @overall_score
    end
    @class_annual_percentage = overall_percentage/batch_students.count.to_i
  end

def annual_gpa
end

def junior_end_of_term_ikoyi_report
   @student = Student.find($student_id)
    @batch = @student.batch
    @term = Term.find(@term_ids.first)
    @groups = @term.evaluation_groups
    @group_count = (@groups.count/2).to_i
    @first_group = @groups[0..@group_count]
    # @comment = TutorComment.find_by_batch_id_and_term_id_and_student_id_and_is_principal(@batch.id,@term.id,@student.id,false)
    @second_group = @groups[@group_count+1..@groups.count-1]
    # CALCULATE ATTENDANCE
    @sub = Subject.find_all_by_batch_id(@batch.id,:conditions=>'is_deleted = false')
    @electives = @sub.map{|x|x unless x.elective_group_id.nil?}.compact
    @electives.reject! { |z| z.students.include?(@student)  }
    @sub -= @electives
    @config = Configuration.find_by_config_key('StudentAttendanceType')
    @start_date = @term.start_date.to_date
    @end_date = @term.end_date.to_date > Date.today ? Date.today : @term.end_date.to_date
    unless @config.config_value == 'Daily'
      @academic_days=@batch.subject_hours(@start_date, @end_date, 0).values.flatten.compact.count
      @student_leaves = SubjectLeave.find(:all,  :conditions =>{:student_id=>@student.id,:month_date => @start_date..@end_date, :subject_id => @sub.map(&:id)})
      @leaves= @student_leaves.count
      @leaves||=0
      @attendance = (@academic_days - @leaves)
    else
      @student_leaves = Attendance.find(:all,  :conditions =>{:batch_id=>@batch.id,:student_id=>@student.id,:month_date => @start_date..@end_date})
      @academic_days=@batch.academic_days.select{|v| v<=@end_date}.count
      leaves_forenoon=Attendance.count(:all,:conditions=>{:batch_id=>@batch.id,:student_id=>@student.id,:forenoon=>true,:afternoon=>false,:month_date => @start_date..@end_date})
      leaves_afternoon=Attendance.count(:all,:conditions=>{:batch_id=>@batch.id,:student_id=>@student.id,:forenoon=>false,:afternoon=>true,:month_date => @start_date..@end_date})
      leaves_full=Attendance.count(:all,:conditions=>{:batch_id=>@batch.id,:student_id=>@student.id,:forenoon=>true,:afternoon=>true,:month_date => @start_date..@end_date})
      @leaves=leaves_full.to_f+(0.5*(leaves_forenoon.to_f+leaves_afternoon.to_f))
      @attendance = (@academic_days - @leaves)

      ###########head teacher comments#############
      student_average = student_group_score(@student)
      if student_average <=49
        @comment="Needs to work harder in most areas of learning."
      elsif  student_average<=59.9
        @comment="Keep giving your best. you can do it."
      elsif  student_average <=69.9
        @comment="A good end of term's result. Keep giving your best."
      elsif  student_average<=89.9
        @comment="This is a very good end of term's report."
      else  student_average<=100 
        @comment="This is an excellent result. Well done!"
      end
    end
  end

  def term_time_school_open
    # @student = Student.find($student_id)
    # @batch = @student.batch
    # @term = Term.find(@term_ids.first)
    # @config = Configuration.find_by_config_key('StudentAttendanceType')
    # @start_date = @term.start_date.to_date
    # @end_date = @term.end_date.to_date > Date.today ? Date.today : @term.end_date.to_date
    #   # unless @config.config_value == 'Daily'
    #    # @academic_day=@batch.subject_hours(@start_date, @end_date, 0).values.flatten.compact.count
    #   # end
    #   @academic_day = (@end_date - @start_date).to_i
    @student = Student.find($student_id)
    @batch = @student.batch
    @term = Term.find(@term_ids.first)
    @config = Configuration.find_by_config_key('StudentAttendanceType')
    @start_date = @term.start_date.to_date
    @end_date = @term.end_date.to_date > Date.today ? Date.today : @term.end_date.to_date
    unless @config.config_value == 'Daily'
      @ing_academic_days=@batch.subject_hours(@start_date, @end_date, 0).values.flatten.compact.count
      @student_leaves = SubjectLeave.find(:all,  :conditions =>{:student_id=>@student.id,:month_date => @start_date..@end_date, :subject_id => @sub.map(&:id)})
      @leaves= @student_leaves.count
      @leaves||=0
      @attendance = (@ing_academic_days - @leaves)
    else
      @student_leaves = Attendance.find(:all,  :conditions =>{:batch_id=>@batch.id,:student_id=>@student.id,:month_date => @start_date..@end_date})
      batch_holidays = @batch.holiday_event_dates    
      range=[]
      date=Configuration.default_time_zone_present_time.to_date
      end_date_take = @end_date
      @holidays = batch_holidays.select { |e| e.between?(@start_date.to_date,end_date_take) } 
      total_weekday_sets=@batch.attendance_weekday_sets.all(:conditions=>["start_date <= ? and end_date >=? ",end_date_take,@start_date.to_date])
      total_weekday_sets.each do |weekdayset|
        week_day_start=weekdayset.start_date.to_date
        week_day_end= (weekdayset.end_date < end_date_take) ? weekdayset.end_date.to_date : end_date_take.to_date
        weekdayset_date_range=week_day_start..week_day_end
        weekday_ids=weekdayset.weekday_set.weekday_ids
        non_holidays=weekdayset_date_range.to_a-@holidays
        range << non_holidays.select{|d| weekday_ids.include? d.wday}
      end
      @range=range.flatten
      @range = @range.select { |e| e.between?(@start_date.to_date,end_date_take) } 
      @ing_academic_days=(@range.count)*2
      leaves_forenoon=Attendance.count(:all,:conditions=>{:batch_id=>@batch.id,:student_id=>@student.id,:forenoon=>true,:afternoon=>false,:month_date => @start_date..@end_date})
      leaves_afternoon=Attendance.count(:all,:conditions=>{:batch_id=>@batch.id,:student_id=>@student.id,:forenoon=>false,:afternoon=>true,:month_date => @start_date..@end_date})
      leaves_full=Attendance.count(:all,:conditions=>{:batch_id=>@batch.id,:student_id=>@student.id,:forenoon=>true,:afternoon=>true,:month_date => @start_date..@end_date})
      @leaves=(leaves_full*2).to_f+(leaves_forenoon.to_f+leaves_afternoon.to_f)
      @attendance = (@ing_academic_days - @leaves)
    end
  end

  def half_term_report
      @students = Student.find($student_id)
      @batch = @students.batch
      @tutor = TermSignature.find_by_is_principal_and_batch_id_and_school_id(false,@batch.id,MultiSchool.current_school.id)
      @principal = TermSignature.find_by_is_principal_and_school_id(true,MultiSchool.current_school.id)
          @students = Student.find($student_id).to_a
          @type= "Grouped"
          @batch = @students.first.batch
          @term = Term.find(@term_ids.first)
          @ca_terms = Term.find(@term.ca_term_id) unless @term.ca_term_id.nil?
          @exam_groups = ExamGroup.find_all_by_batch_id(@batch.id)
          @students.each_with_index do |student,i| 
           general_subjects = Subject.find_all_by_batch_id(@batch.id, :conditions=>"elective_group_id IS NULL and is_deleted=false")
           student_electives = StudentsSubject.find_all_by_student_id(student.id,:conditions=>"batch_id = #{@batch.id}")
           elective_subjects = [] 
           student_electives.each do |elect| 
               @en_disable = RankStudentSubjectElective.find_by_students_subject_id_and_term_id(elect.id,@term.id)
               unless @en_disable.nil?  
                 if  @en_disable.is_deleted == false 
                    elective_subjects.push Subject.find(elect.subject_id) 
                 end 
                 else 
                    elective_subjects.push Subject.find(elect.subject_id)
               end 
           end 
           @subjects = general_subjects + elective_subjects 
           @subjects.reject!{|s| s.no_exams==true} 
           exams = Exam.find_all_by_exam_group_id(@term_exam_groups.collect(&:id))  
           subject_ids = exams.collect(&:subject_id) 
           @subjects.reject!{|sub| !(subject_ids.include?(sub.id))} 
           merged_subjects = RankIngresSubjectMerger.find_all_by_batch_id(@batch.id ) 
              unless merged_subjects.nil?  
                 merged_subjects.each do |ms|   
                 @subjects = @subjects.reject {|sub| ms.subject_id.include? sub.id.to_s}
                 end
              end 
              m_total = 0
              merged_total = []
              unless @subjects.empty? 
                 @subjects = @subjects.sort_by {|u| u.name}  
                 all_exams = @term_exam_groups.reject{|ex| ex.exam_type == "Grades"} 
                 total = []
                 class_average = [] 
                 all_scores = []
                 @subjects.each do |subject| 
                    @mmg = 1;@g = 1 
                    @term_exam_groups.each do |exam_group| 
                    @exam = Exam.find_by_subject_id_and_exam_group_id(subject.id,exam_group.id) 
                    exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                      unless @exam.nil?
                        if exam_group.exam_type == "MarksAndGrades" 
                           exam_score.nil? ? '-' :  "#{exam_score.marks}" 
                        elsif exam_group.exam_type == "Marks"
                           exam_score.nil? ? '-' : "#{exam_score.marks}" 
                        else
                           exam_score.nil? ? '-' : (exam_score.grading_level || '-')  
                           @g = 0
                        end
                       else 
                        "#{t('n_a')}" 
                      end
                    end 
                    total_score = ExamScore.new()
                    unless all_exams.empty? 
                      if @mmg == @g 
                         @totals = ReportBuilder.grouped_exam_subject_total(subject,student,@weighted_exam_groups,@ca_terms) 
                          @total = (@totals/15).to_f.round(2) 
                      else 
                         "-"
                      end
                    end
                    if @total.nil? 
                       "-"
                    else       
                       @subject_percentace = ((@total)*100)/20 
                       @subject_percentace.to_f.round(2)
                       total << @subject_percentace 
                       @total = total.sum
                    end                        
                 end 
              unless merged_subjects.nil?
                  merged_subjects.each do |merged_subject|      
                     h = {} 
                        class_avg = [] 
                        score = 0 
                       merged_subject.subject_id.each do |subject| 
                        @mmg = 1;@g = 1 
                            @term_exam_groups.each do |exam_group| 
                               @exam = Exam.find_by_subject_id_and_exam_group_id(subject,exam_group.id) 
                                 exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})  unless @exam.nil? 
                                unless @exam.nil? 
                                 if exam_group.exam_type == "MarksAndGrades" 
                                       score = exam_score.nil? ? 0 :  exam_score.marks  
                                  elsif exam_group.exam_type == "Marks" 
                                      score = exam_score.nil? ? 0 : exam_score.marks 
                                  else 
                                      score = exam_score.nil? ? 0 : (exam_score.grading_level || 0) 
                                       @g = 0 
                                   end 
                                else 
                                   score = 0
                                end 
                                unless h["#{exam_group.name}"].nil?
                                  h["#{exam_group.name}"] =  h["#{exam_group.name}"] + score 
                                   else 
                                   h["#{exam_group.name}"] = score 
                                end
                             end 
                           subjects = Subject.find subject 
                           m_total =  m_total + ReportBuilder.grouped_exam_subject_total(subjects,student,@weighted_exam_groups,@ca_terms) 
                           merged_subjects.each do |merged_subject|
                             @m_sub = merged_subject.subject_id
                           end
                           @merged_total = (((m_total/merged_subject.subject_id.count)/15)*100/20).to_f.round(2)
                       end
                     end
                   end
                      @over_total = @total.to_f + @merged_total.to_f
              end
            end 
     end

  def first_term_report
      @student = Student.find($student_id)
      @tutor = TermSignature.find_by_is_principal_and_batch_id_and_school_id(false,@batch.id,MultiSchool.current_school.id)
      @principal = TermSignature.find_by_is_principal_and_school_id(true,MultiSchool.current_school.id)
      student_house = StudentAdditionalField.find_by_name("House")
      @student_house_detail = StudentAdditionalDetail.find_by_student_id_and_additional_field_id($student_id,student_house.id) unless student_house.nil?
      @batch = @student.batch
      @ca_exam =[]
      @main_exam_group =[]
      @unweighted_exam_groups =[]
      @other_weighted_groups=[]
      @term = Term.find(@term_ids.first)
      @ca_terms = Term.find(@term.ca_term_id) unless @term.ca_term_id.nil?
      @link_term_groups = TermLinkGroup.find_all_by_term_id(@term_ids.first)
      @term_exam_groups.each do |term_exam_group|
        term_group = TermExamGroup.find_by_exam_group_id(term_exam_group.id)
        if term_group.weightage == 20
          @ca_exam << ExamGroup.find(term_group.exam_group_id)
        elsif term_group.weightage == 60
          @main_exam_group << ExamGroup.find(term_group.exam_group_id)
        elsif term_group.weightage == 0
          @unweighted_exam_groups << ExamGroup.find(term_group.exam_group_id)
        elsif term_group.weightage > 0
          @other_weighted_groups << ExamGroup.find(term_group.exam_group_id)
        end
      end
          @evaluation_groups = EvaluationGroup.find_all_by_term_id(@term.id)
          @scales = ["A","b","C","d","E"]
student = @student 
student_overall = [] 
class_overall = [] 
general_subjects = Subject.find_all_by_batch_id(@batch.id, :conditions=>"elective_group_id IS NULL and is_deleted=false") 
student_electives = StudentsSubject.find_all_by_student_id(student.id,:conditions=>"batch_id = #{@batch.id}") 
elective_subjects = [] 
student_electives.each do |elect| 
   @en_disable = RankStudentSubjectElective.find_by_students_subject_id_and_term_id(elect.id,@term.id) 
        unless @en_disable.nil?  
         if  @en_disable.is_deleted == false
          elective_subjects.push Subject.find(elect.subject_id) 
          end 
         else 
         elective_subjects.push Subject.find(elect.subject_id) 
           end 
 end 
 @subjects = general_subjects + elective_subjects
 @subjects.reject!{|s| s.no_exams==true} 
unless @term_exam_groups.empty? 
   exams = Exam.find_all_by_exam_group_id(@term_exam_groups.collect(&:id))  
  subject_ids = exams.collect(&:subject_id) 
   @subjects.reject!{|sub| !(subject_ids.include?(sub.id))} 
   merged_subjects = RankIngresSubjectMerger.find_all_by_batch_id(@batch.id ) 
  unless merged_subjects.nil?  
     merged_subjects.each do |ms|   
       @subjects = @subjects.reject {|sub| ms.subject_id.include? sub.id.to_s} 
    end
  end 
 unless @subjects.empty? 
     @subjects = @subjects.sort_by {|u| u.name}
    all_exams = @term_exam_groups.reject{|ex| ex.exam_type == "Grades"}
    
       @subjects.each_with_index do |subject,i| 
        
         @count = 0 
       subject.name 
             unless @ca_exam.nil?
               @ca_exam.each do |exam_group| 
                 @count = @count + 1 
            exam_group.name 
               end 
            end 
            @count = @count + 1 
            unless @main_exam_group.nil? 
              @main_exam_group.each do |exam_group| 
                @count = @count + 1 
                 exam_group.name 
               end 
            end 
            @count = @count + 3 
            unless @link_term_groups.blank? 
                 @link_term_groups.each do |link_term| 
                     link_term.link_name
              end 
               end
             unless @other_weighted_groups.nil? 
               @other_weighted_groups.each do |exam_group|
                 @count = @count + 1 
                 exam_group.name
               end 
             end 
             unless @unweighted_exam_groups.nil? 
               @unweighted_exam_groups.each do |exam_group| 
                 @count = @count + 1 
                 exam_group.name
               end 
             end 
          @count = @count + 1 
             @assigned_groups = ReportBuilder.assigned_subject_term_exam_groups(subject.id, @term_ids)  
             unless @assigned_groups.empty? 
               @assigned_groups.each do |assigned_group| 
                 @count = @count + 1 
                 SubjectExamGroup.find(assigned_group.subject_exam_group_id).name 
               end 
             end 
          i = 0 
             cas_total = 0 
            total = 0 
            @mmg = 1;@g = 1 
            unless @ca_exam.nil?
               @ca_exam.each_with_index do |exam_group,i| 
                @exam = Exam.find_by_subject_id_and_exam_group_id(subject.id,exam_group.id)
                 exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                 unless @exam.nil? 
                  if exam_group.exam_type == "MarksAndGrades" 
                      if i == 0 
                     cas_total =  (exam_score.nil? ? @ca_terms.nil? ? 0 :  (ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@ca_terms)/7.5).to_f :  exam_score.marks.nil? ? @ca_terms.nil? ? 0 : ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@ca_terms).to_f:  exam_score.marks).to_f  
                       total = total + cas_total   
                      total = (total).to_f.round(2) 
                       cas_total==0 ? "-" : (cas_total).to_f.round(2) 
                    
                    elsif i == 1 
                         cas_total =  (exam_score.nil? ? @term.nil? ? 0 :  ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@term).to_f :  exam_score.marks.nil? ? @term.nil? ? 0 : ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@term).to_f:  exam_score.marks).to_f
                       total = total + cas_total   
                       cas_total==0 ? "-" : cas_total                    
                   end 
                  elsif exam_group.exam_type == "Marks"                 
                    if i == 0 
                     cas_total =  (exam_score.nil? ? @ca_terms.nil? ? 0 :  (ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@ca_terms/7.5)).to_f :  exam_score.marks.nil? ? @ca_terms.nil? ? 0 : (ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@ca_terms)/7.5).to_f:  exam_score.marks).to_f  
                       total = total + cas_total   
                      total = (total).to_f.round(2) 
                       cas_total==0 ? "-" : (cas_total).to_f.round(2) 
                    
                    elsif i == 1 
                         cas_total =  (exam_score.nil? ? @term.nil? ? 0 :  ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@term).to_f :  exam_score.marks.nil? ? @term.nil? ? 0 : ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@term).to_f:  exam_score.marks).to_f
                       total = total + cas_total   
                       cas_total==0 ? "-" : cas_total                    
                    end 
                   else 
                     exam_score.nil? ? '-' : (exam_score.grading_level || '-')  
                     @g = 0
                  end 
                 else 
                   "#{t('n_a')}"
                 end 
              end 
               unless all_exams.empty? 
                 if @mmg == @g 
                  total.to_f==0 ? "-" : total 
                
                end 
              end 
             end 
              exam_total = 0
              main_total = 0 
            unless @main_exam_group.nil?
               @main_exam_group.each do |exam_group|
                 @exam = Exam.find_by_subject_id_and_exam_group_id(subject.id,exam_group.id) 
                 exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                 unless @exam.nil? 
                   if exam_group.exam_type == "MarksAndGrades" 
                     exam_total = (exam_score.nil? ? '-' : exam_score.marks)  
                      main_total += exam_total.to_f 
                  elsif exam_group.exam_type == "Marks" 
                    exam_total = (exam_score.nil? ? '-' : exam_score.marks) 
                        main_total += exam_total.to_f 
                    else 
                      exam_total = (exam_score.nil? ? '-' : (exam_score.grading_level || '-') )
                     @g = 0 
                   end 
                 else 
                   "#{t('n_a')}" 
                 end
              end
             end

             unless all_exams.empty? 
               if @mmg == @g 
                 @grand_total = (total + main_total) 
                 student_overall << @grand_total
               end
             end 
            @subject_percentace = (@grand_total*100)/(@weighted_total.to_i) 
             key_grade(@subject_percentace) unless @subject_percentace.nil? 
             @grade 
            unless @link_term_groups.blank?
                @link_term_groups.each do |link_term| 
                   @term = Term.find(link_term.link_term_id) 
                     @total =  (ReportBuilder.term_1(subject,student,@term)).to_f
                  @total==0 ? "-" : @total
              end 
               end 
             unless @other_weighted_groups.nil? 
              @other_weighted_groups.each do |exam_group| 
                 @exam = Exam.find_by_subject_id_and_exam_group_id(subject.id,exam_group.id) 
                 exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                unless @exam.nil?
                  if exam_group.exam_type == "MarksAndGrades" 
                     exam_score.nil? ? '-' :  "#{exam_score.marks}"  
                   elsif exam_group.exam_type == "Marks" 
                    exam_score.nil? ? '-' : "#{exam_score.marks}" 
                   else
                     exam_score.nil? ? '-' : (exam_score.grading_level || '-') 
                     @g = 0 
                   end 
                 else 
                   "#{t('n_a')}" 
                 end 
               end 
             end

            unless @unweighted_exam_groups.nil? 
               @unweighted_exam_groups.each do |exam_group| 
                 @exam = Exam.find_by_subject_id_and_exam_group_id(subject.id,exam_group.id) 
                 exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                 unless @exam.nil?
                  if exam_group.exam_type == "MarksAndGrades" 
                     exam_score.nil? ? '-' :  "#{exam_score.marks}" 
                   elsif exam_group.exam_type == "Marks"
                     exam_score.nil? ? '-' : "#{exam_score.marks}"
                   else                    
                    exam_score.nil? ? '-' : (exam_score.grading_level || '-') 
                     @g = 0
                   end 
                 else 
                   "#{t('n_a')}" 
                 end 
               end 
              end 
                ca_id = @term.ca_term_id 
              ca_term = Term.find(ca_id) unless ca_id.nil? 
             @ca = ReportBuilder.class_avg(subject,ca_term) 


               student_electives = StudentsSubject.find_all_by_student_id(student.id,:conditions=>"batch_id = #{@batch.id}") 
               @student_electives =StudentsSubject.find_by_subject_id(subject.id) 
                @subject1 = Subject.find(subject.id) 
                 @subject_counts = @subject1.students.count 
               unless @student_electives.nil?
                   @class_avg = ReportBuilder.class_avg(subject,@term) 
                     @cl_cvg = ((@ca*subject.batch.students.count)/15).to_f.round(2) 
                     @cl_avg1 = (((@cl_cvg*5).to_f.round(2))/@subject_counts).to_f.round(2) 
                     @cl_avg = (@cl_avg1/5).to_f.round(2) + ((@class_avg*subject.batch.students.count).to_f.round(2)/@subject_counts).to_f.round(2) 
                   class_overall << @cl_avg 
               else
                   @class_avg = ReportBuilder.class_avg(subject,@term) 
                   @cl_avg = (@class_avg + ((@ca/3)/5).to_f.round(2)).to_f.round(2)
                  class_overall << @cl_avg 
              end 
             unless @assigned_groups.empty? 
               @assigned_groups.each do |assigned_group| 
                 @score = SubjectExamScore.find_by_student_id_and_assigned_exam_group_id(student.id,assigned_group.id) 
                 unless @score.nil? 
                   @score.value 
                 
                 end
               end 
             end
      end 
     unless merged_subjects.nil? 
            @merge_ca = 0 
         merged_subjects.each_with_index do |merged_subject,i|
            @merge_count = 0 
            unless @ca_exam.nil? 
                 @ca_exam.each do |exam_group| 
                     @merge_count += 1 
                    exam_group.name 
                 end 
               end 
                 @merge_count += 1 
               unless @main_exam_group.nil?
                 @main_exam_group.each do |exam_group| 
                     @merge_count += 1 
                 end 
               end
                 @merge_count += 2
               unless @link_term_groups.blank?
                 @link_term_groups.each do |link_term| 
                   @merge_count += 1                     
                    link_term.link_name
               end 
               end
               unless @other_weighted_groups.nil?
                 @other_weighted_groups.each do |exam_group|
                    @merge_count += 1 
                   exam_group.name 
                 end 
              end 
               unless @unweighted_exam_groups.nil? 
                 @unweighted_exam_groups.each do |exam_group| 
                    @merge_count += 1 
                   exam_group.name 
                 end 
              end
                @merge_count += 1 
               @assigned_groups = ReportBuilder.assigned_subject_term_exam_groups(merged_subject.subject_id.first, @term_ids)
               unless @assigned_groups.empty? 
                 @assigned_groups.each do |assigned_group| 
                    @merge_count += 1 
                   SubjectExamGroup.find(assigned_group.subject_exam_group_id).name 
                end 
               end 
            h = {} 
            score = 0 
            class_avg = [] 
              main_score = 0 
             un_score = 0 
             oh_score = 0
             as_ascore = 0 
            previous_total = 0 
              merged_subject.subject_id.each do |subject|
               @mmg = 1;@g = 1 
                 unless @ca_exam.nil? 
                @ca_exam.each_with_index do |exam_group,i| 
                   @exam = Exam.find_by_subject_id_and_exam_group_id(subject,exam_group.id) 
                   exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                   unless @exam.nil? 
                     if exam_group.exam_type == "MarksAndGrades" 
                        if i == 0 
                             score = (exam_score.nil? ? @ca_terms.nil? ? 0 :  (ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@ca_terms)/7.5).to_f :  exam_score.marks.nil? ? @ca_terms.nil? ? 0 :  R(eportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@ca_terms).to_f/7.5)  : exam_score.marks).to_f 
                           score = (score).to_f.round(2) 
                         elsif i == 1
                             score = (exam_score.nil? ? @term.nil? ? 0 :  (ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@term)).to_f :  exam_score.marks.nil? ? @term.nil? ? 0 :  ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@term).to_f  : exam_score.marks).to_f 
                         end 
                     elsif exam_group.exam_type == "Marks" 
                        if i == 0 
                             score = (exam_score.nil? ? @ca_terms.nil? ? 0 :  (ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@ca_terms)/7.5).to_f :  exam_score.marks.nil? ? @ca_terms.nil? ? 0 :  (ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@ca_terms)/7.5).to_f  : exam_score.marks).to_f 
                           score = (score).to_f.round(2) 
                         elsif i == 1
                             score = (exam_score.nil? ? @term.nil? ? 0 :  (ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@term)).to_f :  exam_score.marks.nil? ? @term.nil? ? 0 :  ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@term).to_f  : exam_score.marks).to_f 
                         end 
                    else 
                       score = exam_score.nil? ? 0 : (exam_score.grading_level || 0)  
                       @g = 0 
                     end 
                  else
                     score = 0 
                  end
                    unless h["#{exam_group.name}"].nil? 
                     h["#{exam_group.name}"] =  h["#{exam_group.name}"] + score  
                   else 
                     h["#{exam_group.name}"] = score 
                   end 
                 end
               end
              unless @main_exam_group.nil?
                @main_exam_group.each do |exam_group| 
                  @exam = Exam.find_by_subject_id_and_exam_group_id(subject.to_i,exam_group.id) 
                   exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                   unless @exam.nil? 
                     if exam_group.exam_type == "MarksAndGrades" 
                        main_score = exam_score.nil? ? 0 :  exam_score.marks 
                     elsif exam_group.exam_type == "Marks" 
                       main_score = exam_score.nil? ? 0 : exam_score.marks 
                     else 
                       main_score = exam_score.nil? ? 0 : (exam_score.grading_level || 0)  
                       @g = 0 
                     end 
                   else 
                       main_score = 0
                   end 
                    unless h["#{exam_group.name}"].nil? 
                     h["#{exam_group.name}"] =  h["#{exam_group.name}"] +main_score
                   else 
                     h["#{exam_group.name}"] = main_score 
                   end
                 end 
               end 
               subjects = Subject.find subject.to_i
              unless @link_term_groups.blank? 
                @link_term_groups.each do |link_term|
                   @term = Term.find(link_term.link_term_id) 
                  unless h["#{link_term.link_name}"].nil?
                     h["#{link_term.link_name}"] =  h["#{link_term.link_name}"] +(ReportBuilder.term_1(subjects,student,@term)).to_f  
                   else
                    h["#{link_term.link_name}"] = (ReportBuilder.term_1(subjects,student,@term)).to_f 
                  end
               end
              end

                @term = Term.find(@term_ids.first) 
                ca_id = @term.ca_term_id 
                ca_term = Term.find(ca_id) unless ca_id.nil?
                   @batch.students.each do |student|                  
                    @ca_avg = ReportBuilder.merge_class_avg(student,subjects,ca_term) 
                   @merge_ca += @ca_avg 
                     end 

               @class_avg = ReportBuilder.class_avg(subjects,@term)
                class_avg << @class_avg 
                  @m_class_avg = ((class_avg.sum.to_f.round(2))/merged_subject.subject_id.count).to_f.round(2) 


                unless @other_weighted_groups.nil? 
                @other_weighted_groups.each do |exam_group| 
                  @exam = Exam.find_by_subject_id_and_exam_group_id(subject.to_i,exam_group.id) 
                  exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                  unless @exam.nil? 
                     if exam_group.exam_type == "MarksAndGrades" 
                      oh_score = exam_score.nil? ? 0 :  exam_score.marks 
                    elsif exam_group.exam_type == "Marks" 
                       oh_score = exam_score.nil? ? 0 :  exam_score.marks
                     else 
                      oh_score = exam_score.nil? ? 0 : (exam_score.grading_level || 0)
                       @g = 0 
                     end 
                        else 
                       oh_score = 0 
                   end 
                    unless h["#{exam_group.name}"].nil? 
                    h["#{exam_group.name}"] =  h["#{exam_group.name}"] +oh_score  
                  else 
                     h["#{exam_group.name}"] = oh_score 
                   end
                end 
               end 
               unless @unweighted_exam_groups.nil? 
                 @unweighted_exam_groups.each do |exam_group| 
                   @exam = Exam.find_by_subject_id_and_exam_group_id(subject.to_i,exam_group.id) 
                   exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                   unless @exam.nil? 
                    if exam_group.exam_type == "MarksAndGrades" 
                       un_score  = exam_score.nil? ? 0 :  exam_score.marks  
                     elsif exam_group.exam_type == "Marks" 
                      un_score  = exam_score.nil? ? 0 :  exam_score.marks 
                        else 
                        un_score  = exam_score.nil? ? 0 : (exam_score.grading_level || 0)  
                       @g = 0 
                     end 
                  else 
                      un_score  =0 
                   end 
                    unless h["#{exam_group.name}"].nil? 
                     h["#{exam_group.name}"] =  h["#{exam_group.name}"] +un_score  
                   else 
                     h["#{exam_group.name}"] = un_score 
                   end 
                 end 
               end 
               unless @assigned_groups.empty? 
                 @assigned_groups.each do |assigned_group| 
                   @score = SubjectExamScore.find_by_student_id_and_assigned_exam_group_id(student,assigned_group.id)
                   unless @score.nil?
                      as_ascore = @score.value 
                  else 
                      as_ascore = 0 
                   end 
                    unless h["a-#{assigned_group.id}"].nil? 
                     h["a-#{assigned_group.id}"] =  h["a-#{assigned_group.id}"] +as_ascore
                  else
                     h["a-#{assigned_group.id}"] = as_ascore  
                   end 
                 end
               end
             end
            
               ca = [] 
               unless @ca_exam.nil?
                 ca_total = 0 
               @ca_exam.each do |exam_group| 
                   ca_total = ca_total + h["#{exam_group.name}"] 
                  @ca = (h["#{exam_group.name}"]/merged_subject.subject_id.count).round(2) unless h["#{exam_group.name}"]==0  
                  ca << @ca 
                 end 
               total_grade =  (ca_total/merged_subject.subject_id.count).round(2) unless ca_total==0 
             end 
               unless @main_exam_group.nil? 
                  main_score = 0 
                  @main_exam_group.each do |exam_group| 
                   main_score = main_score + h["#{exam_group.name}"] 
                    @main = (main_score/merged_subject.subject_id.count).to_f.round(2) 
                  end
              end 
               @merg_total = (ca.sum + @main).to_f.round(2) 
              student_overall << @merg_total 
              key_grade(@subject_percentace) unless @subject_percentace.nil? 
              @grade 
               unless @link_term_groups.blank? 
                @link_term_groups.each do |link_term| 
               (h["#{link_term.link_name}"].to_f/merged_subject.subject_id.count).round(2) unless h["#{link_term.link_name}"].nil? 
              end 
               end 
               unless @other_weighted_groups.nil? 
                 @other_weighted_groups.each do |exam_group| 
                  (h["#{exam_group.name}"]/merged_subject.subject_id.count).round(2) unless h["#{exam_group.name}"]==0 
                end
              end 
               unless @unweighted_exam_groups.nil? 
               @unweighted_exam_groups.each do |exam_group| 
                   (h["#{exam_group.name}"]/merged_subject.subject_id.count).round(2) unless h["#{exam_group.name}"]==0  
                end 
              end
               unless @assigned_groups.empty? 
                @assigned_groups.each do |assigned_group| 
                  (h["a-#{assigned_group.id}"]).chomp.split(//).uniq.join("") unless h["a-#{assigned_group.id}"].nil? 
                 end 
               end 
                              
         end 
      end 
     end
     @overall_student = ((student_overall.sum)/(@subjects.count + merged_subjects.count)).to_f.round(2)
    end             

  end

  def second_term_report
      @student = Student.find($student_id)
      @batch = @student.batch
      student_house = StudentAdditionalField.find_by_name("House")
      @student_house_detail = StudentAdditionalDetail.find_by_student_id_and_additional_field_id($student_id,student_house.id) unless student_house.nil?
      @tutor = TermSignature.find_by_is_principal_and_batch_id_and_school_id(false,@batch.id,MultiSchool.current_school.id)
      @principal = TermSignature.find_by_is_principal_and_school_id(true,MultiSchool.current_school.id)
      @ca_exam =[]
      @main_exam_group =[]
      @unweighted_exam_groups =[]
      @other_weighted_groups=[]
      @term = Term.find(@term_ids.first)
      @ca_terms = Term.find(@term.ca_term_id) unless @term.ca_term_id.nil?
      @link_term_groups = TermLinkGroup.find_all_by_term_id(@term_ids.first)
      @term_exam_groups.each do |term_exam_group|
        term_group = TermExamGroup.find_by_exam_group_id(term_exam_group.id)
        if term_group.weightage == 20
          @ca_exam << ExamGroup.find(term_group.exam_group_id)
        elsif term_group.weightage == 60
          @main_exam_group << ExamGroup.find(term_group.exam_group_id)
        elsif term_group.weightage == 0
          @unweighted_exam_groups << ExamGroup.find(term_group.exam_group_id)
        elsif term_group.weightage > 0
          @other_weighted_groups << ExamGroup.find(term_group.exam_group_id)
        end
      end
           @evaluation_groups = EvaluationGroup.find_all_by_term_id(@term.id)
           @scales = ["A","b","C","d","E"]

student = @student 
student_overall = [] 
class_overall = [] 
general_subjects = Subject.find_all_by_batch_id(@batch.id, :conditions=>"elective_group_id IS NULL and is_deleted=false") 
student_electives = StudentsSubject.find_all_by_student_id(student.id,:conditions=>"batch_id = #{@batch.id}") 
elective_subjects = [] 
student_electives.each do |elect| 
   @en_disable = RankStudentSubjectElective.find_by_students_subject_id_and_term_id(elect.id,@term.id) 
        unless @en_disable.nil?  
         if  @en_disable.is_deleted == false
          elective_subjects.push Subject.find(elect.subject_id) 
          end 
         else 
         elective_subjects.push Subject.find(elect.subject_id) 
           end 
 end 
 @subjects = general_subjects + elective_subjects
 @subjects.reject!{|s| s.no_exams==true} 
unless @term_exam_groups.empty? 
   exams = Exam.find_all_by_exam_group_id(@term_exam_groups.collect(&:id))  
  subject_ids = exams.collect(&:subject_id) 
   @subjects.reject!{|sub| !(subject_ids.include?(sub.id))} 
   merged_subjects = RankIngresSubjectMerger.find_all_by_batch_id(@batch.id ) 
  unless merged_subjects.nil?  
     merged_subjects.each do |ms|   
       @subjects = @subjects.reject {|sub| ms.subject_id.include? sub.id.to_s} 
    end
  end 
 unless @subjects.empty? 
     @subjects = @subjects.sort_by {|u| u.name}
    all_exams = @term_exam_groups.reject{|ex| ex.exam_type == "Grades"}
    
       @subjects.each_with_index do |subject,i| 
        
         @count = 0 
       subject.name 
             unless @ca_exam.nil?
               @ca_exam.each do |exam_group| 
                 @count = @count + 1 
            exam_group.name 
               end 
            end 
            @count = @count + 1 
            unless @main_exam_group.nil? 
              @main_exam_group.each do |exam_group| 
                @count = @count + 1 
                 exam_group.name 
               end 
            end 
            @count = @count + 3 
            unless @link_term_groups.blank? 
                 @link_term_groups.each do |link_term| 
                     link_term.link_name
              end 
               end
             unless @other_weighted_groups.nil? 
               @other_weighted_groups.each do |exam_group|
                 @count = @count + 1 
                 exam_group.name
               end 
             end 
             unless @unweighted_exam_groups.nil? 
               @unweighted_exam_groups.each do |exam_group| 
                 @count = @count + 1 
                 exam_group.name
               end 
             end 
          @count = @count + 1 
             @assigned_groups = ReportBuilder.assigned_subject_term_exam_groups(subject.id, @term_ids)  
             unless @assigned_groups.empty? 
               @assigned_groups.each do |assigned_group| 
                 @count = @count + 1 
                 SubjectExamGroup.find(assigned_group.subject_exam_group_id).name 
               end 
             end 
          i = 0 
             cas_total = 0 
            total = 0 
            @mmg = 1;@g = 1 
            unless @ca_exam.nil?
               @ca_exam.each_with_index do |exam_group,i| 
                @exam = Exam.find_by_subject_id_and_exam_group_id(subject.id,exam_group.id)
                 exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                 unless @exam.nil? 
                  if exam_group.exam_type == "MarksAndGrades" 
                      if i == 0 
                     cas_total =  (exam_score.nil? ? @ca_terms.nil? ? 0 :  (ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@ca_terms)/7.5).to_f :  exam_score.marks.nil? ? @ca_terms.nil? ? 0 :( ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@ca_terms)/7.5).to_f:  exam_score.marks).to_f  
                       total = total + cas_total   
                      # total = (total/7.5).to_f.round(2) 
                       cas_total==0 ? "-" : (cas_total/7.5).to_f.round(2) 
                    
                    elsif i == 1 
                         cas_total =  (exam_score.nil? ? @term.nil? ? 0 :  ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@term).to_f :  exam_score.marks.nil? ? @term.nil? ? 0 : ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@term).to_f:  exam_score.marks).to_f
                       total = total + cas_total   
                       cas_total==0 ? "-" : cas_total                    
                   end 
                  elsif exam_group.exam_type == "Marks"                 
                    if i == 0 
                    # if @ca_terms.name == "Second Mid Term"
                     cas_total =  (exam_score.nil? ? @ca_terms.nil? ? 0 :  (ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@ca_terms)/7.5).to_f :  exam_score.marks.nil? ? @ca_terms.nil? ? 0 : (ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@ca_terms)/7.5).to_f:  exam_score.marks).to_f  
                       total = total + cas_total   
                      total = (total).to_f.round(2) 
                       cas_total==0 ? "-" : (cas_total).to_f.round(2) 
                     # else
                     #  cas_total =  (exam_score.nil? ? @ca_terms.nil? ? 0 :  ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@ca_terms).to_f :  exam_score.marks.nil? ? @ca_terms.nil? ? 0 : ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@ca_terms).to_f:  exam_score.marks).to_f  
                     #   total = total + cas_total   
                     #  total = (total/7.5).to_f.round(2) 
                     #   cas_total==0 ? "-" : (cas_total/7.5).to_f.round(2)
                     # end
                    
                    elsif i == 1 
                         cas_total =  (exam_score.nil? ? @term.nil? ? 0 :  ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@term).to_f :  exam_score.marks.nil? ? @term.nil? ? 0 : ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@term).to_f:  exam_score.marks).to_f
                       total = total + cas_total   
                       cas_total==0 ? "-" : cas_total                    
                   end 
                   else 
                     exam_score.nil? ? '-' : (exam_score.grading_level || '-')  
                     @g = 0
                  end 
                 else 
                   "#{t('n_a')}"
                 end 
              end 
               unless all_exams.empty? 
                 if @mmg == @g 
                  total.to_f==0 ? "-" : total 
                
                end 
              end 
             end 
              exam_total = 0
              main_total = 0 
            unless @main_exam_group.nil?
               @main_exam_group.each do |exam_group|
                 @exam = Exam.find_by_subject_id_and_exam_group_id(subject.id,exam_group.id) 
                 exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                 unless @exam.nil? 
                   if exam_group.exam_type == "MarksAndGrades" 
                     exam_total = (exam_score.nil? ? '-' : exam_score.marks)  
                      main_total += exam_total.to_f 
                  elsif exam_group.exam_type == "Marks" 
                    exam_total = (exam_score.nil? ? '-' : exam_score.marks) 
                        main_total += exam_total.to_f 
                    else 
                      exam_total = (exam_score.nil? ? '-' : (exam_score.grading_level || '-') )
                     @g = 0 
                   end 
                 else 
                   "#{t('n_a')}" 
                 end
              end
             end

             unless all_exams.empty? 
               if @mmg == @g 
                 @grand_total = (total + main_total) 
                 student_overall << @grand_total
               end
             end 
            @subject_percentace = (@grand_total*100)/(@weighted_total.to_i) 
             key_grade(@subject_percentace) unless @subject_percentace.nil? 
             @grade 
            unless @link_term_groups.blank?
                @link_term_groups.each do |link_term| 
                   @term = Term.find(link_term.link_term_id) 
                     @total =  (ReportBuilder.term_1(subject,student,@term)).to_f
                  @total==0 ? "-" : @total
              end 
               end 
             unless @other_weighted_groups.nil? 
              @other_weighted_groups.each do |exam_group| 
                 @exam = Exam.find_by_subject_id_and_exam_group_id(subject.id,exam_group.id) 
                 exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                unless @exam.nil?
                  if exam_group.exam_type == "MarksAndGrades" 
                     exam_score.nil? ? '-' :  "#{exam_score.marks}"  
                   elsif exam_group.exam_type == "Marks" 
                    exam_score.nil? ? '-' : "#{exam_score.marks}" 
                   else
                     exam_score.nil? ? '-' : (exam_score.grading_level || '-') 
                     @g = 0 
                   end 
                 else 
                   "#{t('n_a')}" 
                 end 
               end 
             end

            unless @unweighted_exam_groups.nil? 
               @unweighted_exam_groups.each do |exam_group| 
                 @exam = Exam.find_by_subject_id_and_exam_group_id(subject.id,exam_group.id) 
                 exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                 unless @exam.nil?
                  if exam_group.exam_type == "MarksAndGrades" 
                     exam_score.nil? ? '-' :  "#{exam_score.marks}" 
                   elsif exam_group.exam_type == "Marks"
                     exam_score.nil? ? '-' : "#{exam_score.marks}"
                   else                    
                    exam_score.nil? ? '-' : (exam_score.grading_level || '-') 
                     @g = 0
                   end 
                 else 
                   "#{t('n_a')}" 
                 end 
               end 
              end 
                ca_id = @term.ca_term_id 
              ca_term = Term.find(ca_id) unless ca_id.nil? 
             @ca = ReportBuilder.class_avg(subject,ca_term) 


               student_electives = StudentsSubject.find_all_by_student_id(student.id,:conditions=>"batch_id = #{@batch.id}") 
               @student_electives =StudentsSubject.find_by_subject_id(subject.id) 
                @subject1 = Subject.find(subject.id) 
                 @subject_counts = @subject1.students.count 
               unless @student_electives.nil?
                   @class_avg = ReportBuilder.class_avg(subject,@term) 
                     @cl_cvg = ((@ca*subject.batch.students.count)/15).to_f.round(2) 
                     @cl_avg1 = (((@cl_cvg*5).to_f.round(2))/@subject_counts).to_f.round(2) 
                     @cl_avg = (@cl_avg1/5).to_f.round(2) + ((@class_avg*subject.batch.students.count).to_f.round(2)/@subject_counts).to_f.round(2) 
                   class_overall << @cl_avg 
               else
                   @class_avg = ReportBuilder.class_avg(subject,@term) 
                   @cl_avg = (@class_avg + ((@ca/3)/5).to_f.round(2)).to_f.round(2)
                  class_overall << @cl_avg 
              end 
             unless @assigned_groups.empty? 
               @assigned_groups.each do |assigned_group| 
                 @score = SubjectExamScore.find_by_student_id_and_assigned_exam_group_id(student.id,assigned_group.id) 
                 unless @score.nil? 
                   @score.value 
                 
                 end
               end 
             end
      end 
     unless merged_subjects.nil? 
            @merge_ca = 0 
         merged_subjects.each_with_index do |merged_subject,i|
            @merge_count = 0 
            unless @ca_exam.nil? 
                 @ca_exam.each do |exam_group| 
                     @merge_count += 1 
                    exam_group.name 
                 end 
               end 
                 @merge_count += 1 
               unless @main_exam_group.nil?
                 @main_exam_group.each do |exam_group| 
                     @merge_count += 1 
                 end 
               end
                 @merge_count += 2
               unless @link_term_groups.blank?
                 @link_term_groups.each do |link_term| 
                   @merge_count += 1                     
                    link_term.link_name
               end 
               end
               unless @other_weighted_groups.nil?
                 @other_weighted_groups.each do |exam_group|
                    @merge_count += 1 
                   exam_group.name 
                 end 
              end 
               unless @unweighted_exam_groups.nil? 
                 @unweighted_exam_groups.each do |exam_group| 
                    @merge_count += 1 
                   exam_group.name 
                 end 
              end
                @merge_count += 1 
               @assigned_groups = ReportBuilder.assigned_subject_term_exam_groups(merged_subject.subject_id.first, @term_ids)
               unless @assigned_groups.empty? 
                 @assigned_groups.each do |assigned_group| 
                    @merge_count += 1 
                   SubjectExamGroup.find(assigned_group.subject_exam_group_id).name 
                end 
               end 
            h = {} 
            score = 0 
            class_avg = [] 
              main_score = 0 
             un_score = 0 
             oh_score = 0
             as_ascore = 0 
            previous_total = 0 
              merged_subject.subject_id.each do |subject|
               @mmg = 1;@g = 1 
                 unless @ca_exam.nil? 
                @ca_exam.each_with_index do |exam_group,i| 
                   @exam = Exam.find_by_subject_id_and_exam_group_id(subject,exam_group.id) 
                   exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                   unless @exam.nil? 
                     if exam_group.exam_type == "MarksAndGrades" 
                        if i == 0 
                             score = (exam_score.nil? ? @ca_terms.nil? ? 0 :  (ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@ca_terms)/7.5).to_f :  exam_score.marks.nil? ? @ca_terms.nil? ? 0 :  (ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@ca_terms)/7.5).to_f  : exam_score.marks).to_f 
                           score = (score/7.5).to_f.round(2) 
                         elsif i == 1
                             score = (exam_score.nil? ? @term.nil? ? 0 :  (ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@term)).to_f :  exam_score.marks.nil? ? @term.nil? ? 0 :  ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@term).to_f  : exam_score.marks).to_f 
                         end 
                     elsif exam_group.exam_type == "Marks" 
                         if i == 0
                         # if @ca_terms.name == "Second Mid Term"
                          score = (exam_score.nil? ? @ca_terms.nil? ? 0 :  (ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@ca_terms)/7.5).to_f :  exam_score.marks.nil? ? @ca_terms.nil? ? 0 :  (ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@ca_terms)/7.5).to_f  : exam_score.marks).to_f 
                           score = (score).to_f.round(2) 
                         # else  
                         #     score = (exam_score.nil? ? @ca_terms.nil? ? 0 :  (ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@ca_terms)).to_f :  exam_score.marks.nil? ? @ca_terms.nil? ? 0 :  ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@ca_terms).to_f  : exam_score.marks).to_f 
                         #   score = (score/7.5).to_f.round(2) 
                         # end
                         elsif i == 1
                             score = (exam_score.nil? ? @term.nil? ? 0 :  (ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@term)).to_f :  exam_score.marks.nil? ? @term.nil? ? 0 :  ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@term).to_f  : exam_score.marks).to_f 
                         end
                      else 
                       score = exam_score.nil? ? 0 : (exam_score.grading_level || 0)  
                       @g = 0 
                     end 
                  else
                     score = 0 
                  end
                    unless h["#{exam_group.name}"].nil? 
                     h["#{exam_group.name}"] =  h["#{exam_group.name}"] + score  
                   else 
                     h["#{exam_group.name}"] = score 
                   end 
                 end
               end
              unless @main_exam_group.nil?
                @main_exam_group.each do |exam_group| 
                  @exam = Exam.find_by_subject_id_and_exam_group_id(subject.to_i,exam_group.id) 
                   exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                   unless @exam.nil? 
                     if exam_group.exam_type == "MarksAndGrades" 
                        main_score = exam_score.nil? ? 0 :  exam_score.marks 
                     elsif exam_group.exam_type == "Marks" 
                       main_score = exam_score.nil? ? 0 : exam_score.marks 
                     else 
                       main_score = exam_score.nil? ? 0 : (exam_score.grading_level || 0)  
                       @g = 0 
                     end 
                   else 
                       main_score = 0
                   end 
                    unless h["#{exam_group.name}"].nil? 
                     h["#{exam_group.name}"] =  h["#{exam_group.name}"] +main_score
                   else 
                     h["#{exam_group.name}"] = main_score 
                   end
                 end 
               end 
               subjects = Subject.find subject.to_i
              unless @link_term_groups.blank? 
                @link_term_groups.each do |link_term|
                   @term = Term.find(link_term.link_term_id) 
                  unless h["#{link_term.link_name}"].nil?
                     h["#{link_term.link_name}"] =  h["#{link_term.link_name}"] +(ReportBuilder.term_1(subjects,student,@term)).to_f  
                   else
                    h["#{link_term.link_name}"] = (ReportBuilder.term_1(subjects,student,@term)).to_f 
                  end
               end
              end

                @term = Term.find(@term_ids.first) 
                ca_id = @term.ca_term_id 
                ca_term = Term.find(ca_id) unless ca_id.nil?
                   @batch.students.each do |student|                  
                    @ca_avg = ReportBuilder.merge_class_avg(student,subjects,ca_term) 
                   @merge_ca += @ca_avg 
                     end 

               @class_avg = ReportBuilder.class_avg(subjects,@term)
                class_avg << @class_avg 
                  @m_class_avg = ((class_avg.sum.to_f.round(2))/merged_subject.subject_id.count).to_f.round(2) 


                unless @other_weighted_groups.nil? 
                @other_weighted_groups.each do |exam_group| 
                  @exam = Exam.find_by_subject_id_and_exam_group_id(subject.to_i,exam_group.id) 
                  exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                  unless @exam.nil? 
                     if exam_group.exam_type == "MarksAndGrades" 
                      oh_score = exam_score.nil? ? 0 :  exam_score.marks 
                    elsif exam_group.exam_type == "Marks" 
                       oh_score = exam_score.nil? ? 0 :  exam_score.marks
                     else 
                      oh_score = exam_score.nil? ? 0 : (exam_score.grading_level || 0)
                       @g = 0 
                     end 
                        else 
                       oh_score = 0 
                   end 
                    unless h["#{exam_group.name}"].nil? 
                    h["#{exam_group.name}"] =  h["#{exam_group.name}"] +oh_score  
                  else 
                     h["#{exam_group.name}"] = oh_score 
                   end
                end 
               end 
               unless @unweighted_exam_groups.nil? 
                 @unweighted_exam_groups.each do |exam_group| 
                   @exam = Exam.find_by_subject_id_and_exam_group_id(subject.to_i,exam_group.id) 
                   exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                   unless @exam.nil? 
                    if exam_group.exam_type == "MarksAndGrades" 
                       un_score  = exam_score.nil? ? 0 :  exam_score.marks  
                     elsif exam_group.exam_type == "Marks" 
                      un_score  = exam_score.nil? ? 0 :  exam_score.marks 
                     else 
                        un_score  = exam_score.nil? ? 0 : (exam_score.grading_level || 0)  
                       @g = 0 
                     end 
                  else 
                      un_score  =0 
                   end 
                    unless h["#{exam_group.name}"].nil? 
                     h["#{exam_group.name}"] =  h["#{exam_group.name}"] +un_score  
                   else 
                     h["#{exam_group.name}"] = un_score 
                   end 
                 end 
               end 
               unless @assigned_groups.empty? 
                 @assigned_groups.each do |assigned_group| 
                   @score = SubjectExamScore.find_by_student_id_and_assigned_exam_group_id(student,assigned_group.id)
                   unless @score.nil?
                      as_ascore = @score.value 
                  else 
                      as_ascore = 0 
                   end 
                    unless h["a-#{assigned_group.id}"].nil? 
                     h["a-#{assigned_group.id}"] =  h["a-#{assigned_group.id}"] +as_ascore
                  else
                     h["a-#{assigned_group.id}"] = as_ascore  
                   end 
                 end
               end
             end
            
               ca = [] 
               unless @ca_exam.nil?
                 ca_total = 0 
               @ca_exam.each do |exam_group| 
                   ca_total = ca_total + h["#{exam_group.name}"] 
                  @ca = (h["#{exam_group.name}"]/merged_subject.subject_id.count).round(2) unless h["#{exam_group.name}"]==0  
                  ca << @ca 
                 end 
               total_grade =  (ca_total/merged_subject.subject_id.count).round(2) unless ca_total==0 
             end 
               unless @main_exam_group.nil? 
                  main_score = 0 
                  @main_exam_group.each do |exam_group| 
                   main_score = main_score + h["#{exam_group.name}"] 
                    @main = (main_score/merged_subject.subject_id.count).to_f.round(2) 
                  end
              end 
               @merg_total = (ca.sum + @main).to_f.round(2) 
              student_overall << @merg_total 
              key_grade(@subject_percentace) unless @subject_percentace.nil? 
              @grade 
               unless @link_term_groups.blank? 
                @link_term_groups.each do |link_term| 
               (h["#{link_term.link_name}"].to_f/merged_subject.subject_id.count).round(2) unless h["#{link_term.link_name}"].nil? 
              end 
               end 
               unless @other_weighted_groups.nil? 
                 @other_weighted_groups.each do |exam_group| 
                  (h["#{exam_group.name}"]/merged_subject.subject_id.count).round(2) unless h["#{exam_group.name}"]==0 
                end
              end 
               unless @unweighted_exam_groups.nil? 
               @unweighted_exam_groups.each do |exam_group| 
                   (h["#{exam_group.name}"]/merged_subject.subject_id.count).round(2) unless h["#{exam_group.name}"]==0  
                end 
              end
               unless @assigned_groups.empty? 
                @assigned_groups.each do |assigned_group| 
                  (h["a-#{assigned_group.id}"]).chomp.split(//).uniq.join("") unless h["a-#{assigned_group.id}"].nil? 
                 end 
               end 
                              
         end 
      end 
     end
     @overall_student = ((student_overall.sum)/(@subjects.count + merged_subjects.count)).to_f.round(2)
    end     
  end

  def third_term_report
      @student = Student.find($student_id)
      @batch = @student.batch
      student_house = StudentAdditionalField.find_by_name("House")
      @student_house_detail = StudentAdditionalDetail.find_by_student_id_and_additional_field_id($student_id,student_house.id) unless student_house.nil?
      @tutor = TermSignature.find_by_is_principal_and_batch_id_and_school_id(false,@batch.id,MultiSchool.current_school.id)
      @principal = TermSignature.find_by_is_principal_and_school_id(true,MultiSchool.current_school.id)
      @ca_exam =[]
      @main_exam_group =[]
      @unweighted_exam_groups =[]
      @other_weighted_groups=[]
      @term = Term.find(@term_ids.first)
      @ca_terms = Term.find(@term.ca_term_id) unless @term.ca_term_id.nil?
      @link_term_groups = TermLinkGroup.find_all_by_term_id(@term_ids.first)
      @term_exam_groups.each do |term_exam_group|
        term_group = TermExamGroup.find_by_exam_group_id(term_exam_group.id)
        if term_group.weightage == 20
          @ca_exam << ExamGroup.find(term_group.exam_group_id)
        elsif term_group.weightage == 60
          @main_exam_group << ExamGroup.find(term_group.exam_group_id)
        elsif term_group.weightage == 0
          @unweighted_exam_groups << ExamGroup.find(term_group.exam_group_id)
        elsif term_group.weightage > 0
          @other_weighted_groups << ExamGroup.find(term_group.exam_group_id)
        end
      end
           @evaluation_groups = EvaluationGroup.find_all_by_term_id(@term.id)
           @scales = ["A","b","C","d","E"]
           student = @student 
student_overall = [] 
class_overall = [] 
general_subjects = Subject.find_all_by_batch_id(@batch.id, :conditions=>"elective_group_id IS NULL and is_deleted=false") 
student_electives = StudentsSubject.find_all_by_student_id(student.id,:conditions=>"batch_id = #{@batch.id}") 
elective_subjects = [] 
student_electives.each do |elect| 
   @en_disable = RankStudentSubjectElective.find_by_students_subject_id_and_term_id(elect.id,@term.id) 
        unless @en_disable.nil?  
         if  @en_disable.is_deleted == false
          elective_subjects.push Subject.find(elect.subject_id) 
          end 
         else 
         elective_subjects.push Subject.find(elect.subject_id) 
           end 
 end 
 @subjects = general_subjects + elective_subjects
 @subjects.reject!{|s| s.no_exams==true} 
unless @term_exam_groups.empty? 
   exams = Exam.find_all_by_exam_group_id(@term_exam_groups.collect(&:id))  
  subject_ids = exams.collect(&:subject_id) 
   @subjects.reject!{|sub| !(subject_ids.include?(sub.id))} 
   merged_subjects = RankIngresSubjectMerger.find_all_by_batch_id(@batch.id ) 
  unless merged_subjects.nil?  
     merged_subjects.each do |ms|   
       @subjects = @subjects.reject {|sub| ms.subject_id.include? sub.id.to_s} 
    end
  end 
 unless @subjects.empty? 
     @subjects = @subjects.sort_by {|u| u.name}
    all_exams = @term_exam_groups.reject{|ex| ex.exam_type == "Grades"}
    
       @subjects.each_with_index do |subject,i| 
        
         @count = 0 
       subject.name 
             unless @ca_exam.nil?
               @ca_exam.each do |exam_group| 
                 @count = @count + 1 
            exam_group.name 
               end 
            end 
            @count = @count + 1 
            unless @main_exam_group.nil? 
              @main_exam_group.each do |exam_group| 
                @count = @count + 1 
                 exam_group.name 
               end 
            end 
            @count = @count + 3 
            unless @link_term_groups.blank? 
                 @link_term_groups.each do |link_term| 
                     link_term.link_name
              end 
               end
             unless @other_weighted_groups.nil? 
               @other_weighted_groups.each do |exam_group|
                 @count = @count + 1 
                 exam_group.name
               end 
             end 
             unless @unweighted_exam_groups.nil? 
               @unweighted_exam_groups.each do |exam_group| 
                 @count = @count + 1 
                 exam_group.name
               end 
             end 
          @count = @count + 1 
             @assigned_groups = ReportBuilder.assigned_subject_term_exam_groups(subject.id, @term_ids)  
             unless @assigned_groups.empty? 
               @assigned_groups.each do |assigned_group| 
                 @count = @count + 1 
                 SubjectExamGroup.find(assigned_group.subject_exam_group_id).name 
               end 
             end 
          i = 0 
             cas_total = 0 
            total = 0 
            @mmg = 1;@g = 1 
            unless @ca_exam.nil?
               @ca_exam.each_with_index do |exam_group,i| 
                @exam = Exam.find_by_subject_id_and_exam_group_id(subject.id,exam_group.id)
                 exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                 unless @exam.nil? 
                  if exam_group.exam_type == "MarksAndGrades" 
                      if i == 0 
                     cas_total =  (exam_score.nil? ? @ca_terms.nil? ? 0 :  (ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@ca_terms)/7.5).to_f :  exam_score.marks.nil? ? @ca_terms.nil? ? 0 : ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@ca_terms).to_f:  exam_score.marks).to_f  
                       total = total + cas_total   
                      total = (total).to_f.round(2) 
                       cas_total==0 ? "-" : (cas_total).to_f.round(2) 
                    
                    elsif i == 1 
                         cas_total =  (exam_score.nil? ? @term.nil? ? 0 :  ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@term).to_f :  exam_score.marks.nil? ? @term.nil? ? 0 : ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@term).to_f:  exam_score.marks).to_f
                       total = total + cas_total   
                       cas_total==0 ? "-" : cas_total                    
                   end 
                  elsif exam_group.exam_type == "Marks"                 
                     if i == 0 
                     cas_total =  (exam_score.nil? ? @ca_terms.nil? ? 0 :  (ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@ca_terms)/7.5).to_f :  exam_score.marks.nil? ? @ca_terms.nil? ? 0 : ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@ca_terms).to_f:  exam_score.marks).to_f  
                       total = total + cas_total   
                      total = (total).to_f.round(2) 
                       cas_total==0 ? "-" : (cas_total).to_f.round(2) 
                    
                    elsif i == 1 
                         cas_total =  (exam_score.nil? ? @term.nil? ? 0 :  ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@term).to_f :  exam_score.marks.nil? ? @term.nil? ? 0 : ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@term).to_f:  exam_score.marks).to_f
                       total = total + cas_total   
                       cas_total==0 ? "-" : cas_total                    
                   end 
                   else 
                     exam_score.nil? ? '-' : (exam_score.grading_level || '-')  
                     @g = 0
                  end 
                 else 
                   "#{t('n_a')}"
                 end 
              end 
               unless all_exams.empty? 
                 if @mmg == @g 
                  total.to_f==0 ? "-" : total 
                
                end 
              end 
             end 
              exam_total = 0
              main_total = 0 
            unless @main_exam_group.nil?
               @main_exam_group.each do |exam_group|
                 @exam = Exam.find_by_subject_id_and_exam_group_id(subject.id,exam_group.id) 
                 exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                 unless @exam.nil? 
                   if exam_group.exam_type == "MarksAndGrades" 
                     exam_total = (exam_score.nil? ? '-' : exam_score.marks)  
                      main_total += exam_total.to_f 
                  elsif exam_group.exam_type == "Marks" 
                    exam_total = (exam_score.nil? ? '-' : exam_score.marks) 
                        main_total += exam_total.to_f  
                    else 
                      exam_total = (exam_score.nil? ? '-' : (exam_score.grading_level || '-') )
                     @g = 0 
                   end 
                 else 
                   "#{t('n_a')}" 
                 end
              end
             end

             unless all_exams.empty? 
               if @mmg == @g 
                 @grand_total = (total + main_total) 
                 student_overall << @grand_total
               end
             end 
            @subject_percentace = (@grand_total*100)/(@weighted_total.to_i) 
             key_grade(@subject_percentace) unless @subject_percentace.nil? 
             @grade 
            unless @link_term_groups.blank?
                @link_term_groups.each do |link_term| 
                   @term = Term.find(link_term.link_term_id) 
                     @total =  (ReportBuilder.term_1(subject,student,@term)).to_f
                  @total==0 ? "-" : @total
              end 
               end 
             unless @other_weighted_groups.nil? 
              @other_weighted_groups.each do |exam_group| 
                 @exam = Exam.find_by_subject_id_and_exam_group_id(subject.id,exam_group.id) 
                 exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                unless @exam.nil?
                  if exam_group.exam_type == "MarksAndGrades" 
                     exam_score.nil? ? '-' :  "#{exam_score.marks}"  
                   elsif exam_group.exam_type == "Marks" 
                    exam_score.nil? ? '-' : "#{exam_score.marks}" 
                   else
                     exam_score.nil? ? '-' : (exam_score.grading_level || '-') 
                     @g = 0 
                   end 
                 else 
                   "#{t('n_a')}" 
                 end 
               end 
             end

            unless @unweighted_exam_groups.nil? 
               @unweighted_exam_groups.each do |exam_group| 
                 @exam = Exam.find_by_subject_id_and_exam_group_id(subject.id,exam_group.id) 
                 exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                 unless @exam.nil?
                  if exam_group.exam_type == "MarksAndGrades" 
                     exam_score.nil? ? '-' :  "#{exam_score.marks}" 
                   elsif exam_group.exam_type == "Marks"
                     exam_score.nil? ? '-' : "#{exam_score.marks}"
                   else                    
                    exam_score.nil? ? '-' : (exam_score.grading_level || '-') 
                     @g = 0
                   end 
                 else 
                   "#{t('n_a')}" 
                 end 
               end 
              end 
                ca_id = @term.ca_term_id 
              ca_term = Term.find(ca_id) unless ca_id.nil? 
             @ca = ReportBuilder.class_avg(subject,ca_term) 


               student_electives = StudentsSubject.find_all_by_student_id(student.id,:conditions=>"batch_id = #{@batch.id}") 
               @student_electives =StudentsSubject.find_by_subject_id(subject.id) 
                @subject1 = Subject.find(subject.id) 
                 @subject_counts = @subject1.students.count 
               unless @student_electives.nil?
                   @class_avg = ReportBuilder.class_avg(subject,@term) 
                     @cl_cvg = ((@ca*subject.batch.students.count)/15).to_f.round(2) 
                     @cl_avg1 = (((@cl_cvg*5).to_f.round(2))/@subject_counts).to_f.round(2) 
                     @cl_avg = (@cl_avg1/5).to_f.round(2) + ((@class_avg*subject.batch.students.count).to_f.round(2)/@subject_counts).to_f.round(2) 
                   class_overall << @cl_avg 
               else
                   @class_avg = ReportBuilder.class_avg(subject,@term) 
                   @cl_avg = (@class_avg + ((@ca/3)/5).to_f.round(2)).to_f.round(2)
                  class_overall << @cl_avg 
              end 
             unless @assigned_groups.empty? 
               @assigned_groups.each do |assigned_group| 
                 @score = SubjectExamScore.find_by_student_id_and_assigned_exam_group_id(student.id,assigned_group.id) 
                 unless @score.nil? 
                   @score.value 
                 
                 end
               end 
             end
      end 
     unless merged_subjects.nil? 
            @merge_ca = 0 
         merged_subjects.each_with_index do |merged_subject,i|
            @merge_count = 0 
            unless @ca_exam.nil? 
                 @ca_exam.each do |exam_group| 
                     @merge_count += 1 
                    exam_group.name 
                 end 
               end 
                 @merge_count += 1 
               unless @main_exam_group.nil?
                 @main_exam_group.each do |exam_group| 
                     @merge_count += 1 
                 end 
               end
                 @merge_count += 2
               unless @link_term_groups.blank?
                 @link_term_groups.each do |link_term| 
                   @merge_count += 1                     
                    link_term.link_name
               end 
               end
               unless @other_weighted_groups.nil?
                 @other_weighted_groups.each do |exam_group|
                    @merge_count += 1 
                   exam_group.name 
                 end 
              end 
               unless @unweighted_exam_groups.nil? 
                 @unweighted_exam_groups.each do |exam_group| 
                    @merge_count += 1 
                   exam_group.name 
                 end 
              end
                @merge_count += 1 
               @assigned_groups = ReportBuilder.assigned_subject_term_exam_groups(merged_subject.subject_id.first, @term_ids)
               unless @assigned_groups.empty? 
                 @assigned_groups.each do |assigned_group| 
                    @merge_count += 1 
                   SubjectExamGroup.find(assigned_group.subject_exam_group_id).name 
                end 
               end 
            h = {} 
            score = 0 
            class_avg = [] 
              main_score = 0 
             un_score = 0 
             oh_score = 0
             as_ascore = 0 
            previous_total = 0 
              merged_subject.subject_id.each do |subject|
               @mmg = 1;@g = 1 
                 unless @ca_exam.nil? 
                @ca_exam.each_with_index do |exam_group,i| 
                   @exam = Exam.find_by_subject_id_and_exam_group_id(subject,exam_group.id) 
                   exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                   unless @exam.nil? 
                     if exam_group.exam_type == "MarksAndGrades" 
                        if i == 0 
                             score = (exam_score.nil? ? @ca_terms.nil? ? 0 :  ((ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@ca_terms)/7.5)).to_f :  exam_score.marks.nil? ? @ca_terms.nil? ? 0 :  ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@ca_terms).to_f  : exam_score.marks).to_f 
                           score = (score).to_f.round(2) 
                         elsif i == 1
                             score = (exam_score.nil? ? @term.nil? ? 0 :  (ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@term)).to_f :  exam_score.marks.nil? ? @term.nil? ? 0 :  ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@term).to_f  : exam_score.marks).to_f 
                         end 
                     elsif exam_group.exam_type == "Marks" 
                        if i == 0 
                             score = (exam_score.nil? ? @ca_terms.nil? ? 0 :  ((ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@ca_terms)/7.5)).to_f :  exam_score.marks.nil? ? @ca_terms.nil? ? 0 :  ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@ca_terms).to_f  : exam_score.marks).to_f 
                           score = (score).to_f.round(2) 
                         elsif i == 1
                             score = (exam_score.nil? ? @term.nil? ? 0 :  (ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@term)).to_f :  exam_score.marks.nil? ? @term.nil? ? 0 :  ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@term).to_f  : exam_score.marks).to_f 
                         end   
                    else 
                       score = exam_score.nil? ? 0 : (exam_score.grading_level || 0)  
                       @g = 0 
                     end 
                  else
                     score = 0 
                  end
                    unless h["#{exam_group.name}"].nil? 
                     h["#{exam_group.name}"] =  h["#{exam_group.name}"] + score  
                   else 
                     h["#{exam_group.name}"] = score 
                   end 
                 end
               end
              unless @main_exam_group.nil?
                @main_exam_group.each do |exam_group| 
                  @exam = Exam.find_by_subject_id_and_exam_group_id(subject.to_i,exam_group.id) 
                   exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                   unless @exam.nil? 
                     if exam_group.exam_type == "MarksAndGrades" 
                        main_score = exam_score.nil? ? 0 :  exam_score.marks 
                     elsif exam_group.exam_type == "Marks" 
                       main_score = exam_score.nil? ? 0 : exam_score.marks 
                     else 
                       main_score = exam_score.nil? ? 0 : (exam_score.grading_level || 0)  
                       @g = 0 
                     end 
                   else 
                       main_score = 0
                   end 
                    unless h["#{exam_group.name}"].nil? 
                     h["#{exam_group.name}"] =  h["#{exam_group.name}"] +main_score
                   else 
                     h["#{exam_group.name}"] = main_score 
                   end
                 end 
               end 
               subjects = Subject.find subject.to_i
              unless @link_term_groups.blank? 
                @link_term_groups.each do |link_term|
                   @term = Term.find(link_term.link_term_id) 
                  unless h["#{link_term.link_name}"].nil?
                     h["#{link_term.link_name}"] =  h["#{link_term.link_name}"] +(ReportBuilder.term_1(subjects,student,@term)).to_f  
                   else
                    h["#{link_term.link_name}"] = (ReportBuilder.term_1(subjects,student,@term)).to_f 
                  end
               end
              end

                @term = Term.find(@term_ids.first) 
                ca_id = @term.ca_term_id 
                ca_term = Term.find(ca_id) unless ca_id.nil?
                   @batch.students.each do |student|                  
                    @ca_avg = ReportBuilder.merge_class_avg(student,subjects,ca_term) 
                   @merge_ca += @ca_avg 
                     end 

               @class_avg = ReportBuilder.class_avg(subjects,@term)
                class_avg << @class_avg 
                  @m_class_avg = ((class_avg.sum.to_f.round(2))/merged_subject.subject_id.count).to_f.round(2) 


                unless @other_weighted_groups.nil? 
                @other_weighted_groups.each do |exam_group| 
                  @exam = Exam.find_by_subject_id_and_exam_group_id(subject.to_i,exam_group.id) 
                  exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                  unless @exam.nil? 
                     if exam_group.exam_type == "MarksAndGrades" 
                      oh_score = exam_score.nil? ? 0 :  exam_score.marks 
                    elsif exam_group.exam_type == "Marks" 
                       oh_score = exam_score.nil? ? 0 :  exam_score.marks
                     else 
                      oh_score = exam_score.nil? ? 0 : (exam_score.grading_level || 0)
                       @g = 0 
                     end 
                        else 
                       oh_score = 0 
                   end 
                    unless h["#{exam_group.name}"].nil? 
                    h["#{exam_group.name}"] =  h["#{exam_group.name}"] +oh_score  
                  else 
                     h["#{exam_group.name}"] = oh_score 
                   end
                end 
               end 
               unless @unweighted_exam_groups.nil? 
                 @unweighted_exam_groups.each do |exam_group| 
                   @exam = Exam.find_by_subject_id_and_exam_group_id(subject.to_i,exam_group.id) 
                   exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                   unless @exam.nil? 
                    if exam_group.exam_type == "MarksAndGrades" 
                       un_score  = exam_score.nil? ? 0 :  exam_score.marks  
                     elsif exam_group.exam_type == "Marks" 
                      un_score  = exam_score.nil? ? 0 :  exam_score.marks 
                        else 
                        un_score  = exam_score.nil? ? 0 : (exam_score.grading_level || 0)  
                       @g = 0 
                     end 
                  else 
                      un_score  =0 
                   end 
                    unless h["#{exam_group.name}"].nil? 
                     h["#{exam_group.name}"] =  h["#{exam_group.name}"] +un_score  
                   else 
                     h["#{exam_group.name}"] = un_score 
                   end 
                 end 
               end 
               unless @assigned_groups.empty? 
                 @assigned_groups.each do |assigned_group| 
                   @score = SubjectExamScore.find_by_student_id_and_assigned_exam_group_id(student,assigned_group.id)
                   unless @score.nil?
                      as_ascore = @score.value 
                  else 
                      as_ascore = 0 
                   end 
                    unless h["a-#{assigned_group.id}"].nil? 
                     h["a-#{assigned_group.id}"] =  h["a-#{assigned_group.id}"] +as_ascore
                  else
                     h["a-#{assigned_group.id}"] = as_ascore  
                   end 
                 end
               end
             end
            
               ca = [] 
               unless @ca_exam.nil?
                 ca_total = 0 
               @ca_exam.each do |exam_group| 
                   ca_total = ca_total + h["#{exam_group.name}"] 
                  @ca = (h["#{exam_group.name}"]/merged_subject.subject_id.count).round(2) unless h["#{exam_group.name}"]==0  
                  ca << @ca 
                 end 
               total_grade =  (ca_total/merged_subject.subject_id.count).round(2) unless ca_total==0 
             end 
               unless @main_exam_group.nil? 
                  main_score = 0 
                  @main_exam_group.each do |exam_group| 
                   main_score = main_score + h["#{exam_group.name}"] 
                    @main = (main_score/merged_subject.subject_id.count).to_f.round(2) 
                  end
              end 
               @merg_total = (ca.sum + @main).to_f.round(2) 
              student_overall << @merg_total 
              key_grade(@subject_percentace) unless @subject_percentace.nil? 
              @grade 
               unless @link_term_groups.blank? 
                @link_term_groups.each do |link_term| 
               (h["#{link_term.link_name}"].to_f/merged_subject.subject_id.count).round(2) unless h["#{link_term.link_name}"].nil? 
              end 
               end 
               unless @other_weighted_groups.nil? 
                 @other_weighted_groups.each do |exam_group| 
                  (h["#{exam_group.name}"]/merged_subject.subject_id.count).round(2) unless h["#{exam_group.name}"]==0 
                end
              end 
               unless @unweighted_exam_groups.nil? 
               @unweighted_exam_groups.each do |exam_group| 
                   (h["#{exam_group.name}"]/merged_subject.subject_id.count).round(2) unless h["#{exam_group.name}"]==0  
                end 
              end
               unless @assigned_groups.empty? 
                @assigned_groups.each do |assigned_group| 
                  (h["a-#{assigned_group.id}"]).chomp.split(//).uniq.join("") unless h["a-#{assigned_group.id}"].nil? 
                 end 
               end 
                              
         end 
      end 
     end
     @overall_student = ((student_overall.sum)/(@subjects.count + merged_subjects.count)).to_f.round(2)
    end      
  end

  def amville_nursery_report2
    @student = Student.find($student_id)
    @batch = @student.batch
    @exam_groups = ExamGroup.find_all_by_batch_id(@batch.id)
    @term = Term.find(@term_ids.first)
    @evaluation_groups = EvaluationGroup.find_all_by_term_id(@term.id)
    @emerging = ["Excellent","Very Good","Good","Needs improvement"]
    @target_area = EvaluationComment.find_by_term_id_and_student_id_and_evaluation_group_id(@term.id,@student.id,@evaluation_groups.last.id)
    @tutor = TermSignature.find_by_is_principal_and_batch_id_and_school_id(false,@batch.id,MultiSchool.current_school.id)
    @principal = TermSignature.find_by_is_principal_and_school_id(true,MultiSchool.current_school.id)
  end

  def nursery_2_report
    @groups = []
    @report_values = ["A","B","C","D"]
    @student = Student.find($student_id)
    @students = Student.find($student_id)
    @batch = @students.batch
    @tutor = TermSignature.find_by_is_principal_and_batch_id_and_school_id(false,@batch.id,MultiSchool.current_school.id)
    @principal = TermSignature.find_by_is_principal_and_school_id(true,MultiSchool.current_school.id)
    @students = Student.find($student_id).to_a
    @type= "Grouped"
    @batch = @students.first.batch
    @term = Term.find(@term_ids.first)
    @ca_terms = Term.find(@term.ca_term_id) unless @term.ca_term_id.nil?
    @all_groups = @term.evaluation_groups
    @all_groups.each do |group|
      if group.score_format == "Alphabets"
        @groups << group
      end
    end
    @start_date = @batch.start_date.to_date
    @end_date = @batch.end_date.to_date > Date.today ? @batch.end_date.to_date : Date.today
    @activity_values = ["Excellent","Very Good","Good","Developing"]
    @attendence_values = ["No of times open","No of times presene","No of times absent","Punctuality"]
  end

  def primary_mid_term_report
    @principal = TermSignature.find_by_is_principal_and_school_id(true,MultiSchool.current_school.id)
    @student = Student.find($student_id)
    @batch = @student.batch
    @tutor = TermSignature.find_by_is_principal_and_batch_id_and_school_id(false,@batch.id,MultiSchool.current_school.id)
    @batch_students = @batch.students.count
    @exam_groups = ExamGroup.find_all_by_batch_id(@batch.id)
    @term = Term.find(@term_ids.first)
    @evaluation_group= EvaluationGroup.find_all_by_term_id(@term.id).last    
    @target_area = EvaluationComment.find_by_term_id_and_student_id_and_evaluation_group_id(@term.id,@student.id,@evaluation_group.id)
    @class_conduct = ["a","b","c","d","e"]
    @evaluation_types = @evaluation_group.evaluation_types unless @evaluation_group.nil?
  end

  def term_fedena_grade(exam_score,student_id)
    batch_id=Student.find(student_id).batch_id
    @grading_levels=GradingLevel.find_all_by_batch_id(batch_id)
    @grading=GradingLevel.all if @grading_levels.blank?
    unless @grading_levels.blank?
      @grading_levels.each do |grading|
        if grading.min_score == 90
          if exam_score.between?(grading.min_score,grading.min_score+10)
            @grade = grading.name
          end
        else
          if exam_score.between?(grading.min_score,grading.min_score+9.99)
            @grade = grading.name
          end
        end
      end
    else
      @grading.each do |grading|
        if grading.min_score == 90
          if exam_score.between?(grading.min_score,grading.min_score+10)
            @grade = grading.name
          end
        else
          if exam_score.between?(grading.min_score,grading.min_score+9.99)
            @grade = grading.name
          end
        end
      end
    end
    return @grade
  end

  def first_senior_term_report
    @student = Student.find($student_id)
      @tutor = TermSignature.find_by_is_principal_and_batch_id_and_school_id(false,@batch.id,MultiSchool.current_school.id)
      @principal = TermSignature.find_by_is_principal_and_school_id(true,MultiSchool.current_school.id)
      student_house = StudentAdditionalField.find_by_name("House")
      @student_house_detail = StudentAdditionalDetail.find_by_student_id_and_additional_field_id($student_id,student_house.id) unless student_house.nil?
      @batch = @student.batch
      @ca_exam =[]
      @main_exam_group =[]
      @unweighted_exam_groups =[]
      @other_weighted_groups=[]
      @term = Term.find(@term_ids.first)
      @ca_terms = Term.find(@term.ca_term_id) unless @term.ca_term_id.nil?
      @link_term_groups = TermLinkGroup.find_all_by_term_id(@term_ids.first)
      @term_exam_groups.each do |term_exam_group|
        term_group = TermExamGroup.find_by_exam_group_id(term_exam_group.id)
        if term_group.weightage == 20
          @ca_exam << ExamGroup.find(term_group.exam_group_id)
        elsif term_group.weightage == 60
          @main_exam_group << ExamGroup.find(term_group.exam_group_id)
        elsif term_group.weightage == 0
          @unweighted_exam_groups << ExamGroup.find(term_group.exam_group_id)
        elsif term_group.weightage > 0
          @other_weighted_groups << ExamGroup.find(term_group.exam_group_id)
        end
      end
          @evaluation_groups = EvaluationGroup.find_all_by_term_id(@term.id)
          @scales = ["A","b","C","d","E"]
student = @student 
student_overall = [] 
class_overall = [] 
general_subjects = Subject.find_all_by_batch_id(@batch.id, :conditions=>"elective_group_id IS NULL and is_deleted=false") 
student_electives = StudentsSubject.find_all_by_student_id(student.id,:conditions=>"batch_id = #{@batch.id}") 
elective_subjects = [] 
student_electives.each do |elect| 
   @en_disable = RankStudentSubjectElective.find_by_students_subject_id_and_term_id(elect.id,@term.id) 
        unless @en_disable.nil?  
         if  @en_disable.is_deleted == false
          elective_subjects.push Subject.find(elect.subject_id) 
          end 
         else 
         elective_subjects.push Subject.find(elect.subject_id) 
           end 
 end 
 @subjects = general_subjects + elective_subjects
 @subjects.reject!{|s| s.no_exams==true} 
unless @term_exam_groups.empty? 
   exams = Exam.find_all_by_exam_group_id(@term_exam_groups.collect(&:id))  
  subject_ids = exams.collect(&:subject_id) 
   @subjects.reject!{|sub| !(subject_ids.include?(sub.id))} 
   merged_subjects = RankIngresSubjectMerger.find_all_by_batch_id(@batch.id ) 
  unless merged_subjects.nil?  
     merged_subjects.each do |ms|   
       @subjects = @subjects.reject {|sub| ms.subject_id.include? sub.id.to_s} 
    end
  end 
 unless @subjects.empty? 
     @subjects = @subjects.sort_by {|u| u.name}
    all_exams = @term_exam_groups.reject{|ex| ex.exam_type == "Grades"}
    
       @subjects.each_with_index do |subject,i| 
        
         @count = 0 
       subject.name 
             unless @ca_exam.nil?
               @ca_exam.each do |exam_group| 
                 @count = @count + 1 
            exam_group.name 
               end 
            end 
            @count = @count + 1 
            unless @main_exam_group.nil? 
              @main_exam_group.each do |exam_group| 
                @count = @count + 1 
                 exam_group.name 
               end 
            end 
            @count = @count + 3 
            unless @link_term_groups.blank? 
                 @link_term_groups.each do |link_term| 
                     link_term.link_name
              end 
               end
             unless @other_weighted_groups.nil? 
               @other_weighted_groups.each do |exam_group|
                 @count = @count + 1 
                 exam_group.name
               end 
             end 
             unless @unweighted_exam_groups.nil? 
               @unweighted_exam_groups.each do |exam_group| 
                 @count = @count + 1 
                 exam_group.name
               end 
             end 
          @count = @count + 1 
             @assigned_groups = ReportBuilder.assigned_subject_term_exam_groups(subject.id, @term_ids)  
             unless @assigned_groups.empty? 
               @assigned_groups.each do |assigned_group| 
                 @count = @count + 1 
                 SubjectExamGroup.find(assigned_group.subject_exam_group_id).name 
               end 
             end 
          i = 0 
             cas_total = 0 
            total = 0 
            @mmg = 1;@g = 1 
            unless @ca_exam.nil?
               @ca_exam.each_with_index do |exam_group,i| 
                @exam = Exam.find_by_subject_id_and_exam_group_id(subject.id,exam_group.id)
                 exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                 unless @exam.nil? 
                  if exam_group.exam_type == "MarksAndGrades" 
                      if i == 0 
                     cas_total =  (exam_score.nil? ? @ca_terms.nil? ? 0 :  (ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@ca_terms)/7.5).to_f :  exam_score.marks.nil? ? @ca_terms.nil? ? 0 : (ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@ca_terms)/7.5).to_f:  exam_score.marks).to_f  
                       total = total + cas_total   
                      total = (total).to_f.round(2) 
                       cas_total==0 ? "-" : (cas_total).to_f.round(2) 
                    
                    elsif i == 1 
                         cas_total =  (exam_score.nil? ? @term.nil? ? 0 :  ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@term).to_f :  exam_score.marks.nil? ? @term.nil? ? 0 : ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@term).to_f:  exam_score.marks).to_f
                       total = total + cas_total   
                       cas_total==0 ? "-" : cas_total                    
                   end 
                  elsif exam_group.exam_type == "Marks"                 
                    if i == 0 
                     cas_total =  (exam_score.nil? ? @ca_terms.nil? ? 0 :  (ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@ca_terms)/7.5).to_f :  exam_score.marks.nil? ? @ca_terms.nil? ? 0 : (ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@ca_terms)/7.5).to_f:  exam_score.marks).to_f  
                       total = total + cas_total   
                      total = (total).to_f.round(2) 
                       cas_total==0 ? "-" : (cas_total).to_f.round(2) 
                    
                    elsif i == 1 
                         cas_total =  (exam_score.nil? ? @term.nil? ? 0 :  ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@term).to_f :  exam_score.marks.nil? ? @term.nil? ? 0 : ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@term).to_f:  exam_score.marks).to_f
                       total = total + cas_total   
                       cas_total==0 ? "-" : cas_total                    
                   end
                   else 
                     exam_score.nil? ? '-' : (exam_score.grading_level || '-')  
                     @g = 0
                  end 
                 else 
                   "#{t('n_a')}"
                 end 
              end 
               unless all_exams.empty? 
                 if @mmg == @g 
                  total.to_f==0 ? "-" : total 
                
                end 
              end 
             end 
              exam_total = 0
              main_total = 0 
            unless @main_exam_group.nil?
               @main_exam_group.each do |exam_group|
                 @exam = Exam.find_by_subject_id_and_exam_group_id(subject.id,exam_group.id) 
                 exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                 unless @exam.nil? 
                   if exam_group.exam_type == "MarksAndGrades" 
                     exam_total = (exam_score.nil? ? '-' : exam_score.marks)  
                      main_total += exam_total.to_f 
                  elsif exam_group.exam_type == "Marks" 
                    exam_total = (exam_score.nil? ? '-' : exam_score.marks) 
                        main_total += exam_total.to_f 
                    else 
                      exam_total = (exam_score.nil? ? '-' : (exam_score.grading_level || '-') )
                     @g = 0 
                   end 
                 else 
                   "#{t('n_a')}" 
                 end
              end
             end

             unless all_exams.empty? 
               if @mmg == @g 
                 @grand_total = (total + main_total) 
                 student_overall << @grand_total
               end
             end 
            @subject_percentace = (@grand_total*100)/(@weighted_total.to_i) 
             key_grade(@subject_percentace) unless @subject_percentace.nil? 
             @grade 
            unless @link_term_groups.blank?
                @link_term_groups.each do |link_term| 
                   @term = Term.find(link_term.link_term_id) 
                     @total =  (ReportBuilder.term_1(subject,student,@term)).to_f
                  @total==0 ? "-" : @total
              end 
               end 
             unless @other_weighted_groups.nil? 
              @other_weighted_groups.each do |exam_group| 
                 @exam = Exam.find_by_subject_id_and_exam_group_id(subject.id,exam_group.id) 
                 exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                unless @exam.nil?
                  if exam_group.exam_type == "MarksAndGrades" 
                     exam_score.nil? ? '-' :  "#{exam_score.marks}"  
                   elsif exam_group.exam_type == "Marks" 
                    exam_score.nil? ? '-' : "#{exam_score.marks}" 
                   else
                     exam_score.nil? ? '-' : (exam_score.grading_level || '-') 
                     @g = 0 
                   end 
                 else 
                   "#{t('n_a')}" 
                 end 
               end 
             end

            unless @unweighted_exam_groups.nil? 
               @unweighted_exam_groups.each do |exam_group| 
                 @exam = Exam.find_by_subject_id_and_exam_group_id(subject.id,exam_group.id) 
                 exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                 unless @exam.nil?
                  if exam_group.exam_type == "MarksAndGrades" 
                     exam_score.nil? ? '-' :  "#{exam_score.marks}" 
                   elsif exam_group.exam_type == "Marks"
                     exam_score.nil? ? '-' : "#{exam_score.marks}"
                   else                    
                    exam_score.nil? ? '-' : (exam_score.grading_level || '-') 
                     @g = 0
                   end 
                 else 
                   "#{t('n_a')}" 
                 end 
               end 
              end 
                ca_id = @term.ca_term_id 
              ca_term = Term.find(ca_id) unless ca_id.nil? 
             @ca = ReportBuilder.class_avg(subject,ca_term) 


               student_electives = StudentsSubject.find_all_by_student_id(student.id,:conditions=>"batch_id = #{@batch.id}") 
               @student_electives =StudentsSubject.find_by_subject_id(subject.id) 
                @subject1 = Subject.find(subject.id) 
                 @subject_counts = @subject1.students.count 
               unless @student_electives.nil?
                   @class_avg = ReportBuilder.class_avg(subject,@term) 
                     @cl_cvg = ((@ca*subject.batch.students.count)/15).to_f.round(2) 
                     @cl_avg1 = (((@cl_cvg*5).to_f.round(2))/@subject_counts).to_f.round(2) 
                     @cl_avg = (@cl_avg1/5).to_f.round(2) + ((@class_avg*subject.batch.students.count).to_f.round(2)/@subject_counts).to_f.round(2) 
                   class_overall << @cl_avg 
               else
                   @class_avg = ReportBuilder.class_avg(subject,@term) 
                   @cl_avg = (@class_avg + ((@ca/3)/5).to_f.round(2)).to_f.round(2)
                  class_overall << @cl_avg 
              end 
             unless @assigned_groups.empty? 
               @assigned_groups.each do |assigned_group| 
                 @score = SubjectExamScore.find_by_student_id_and_assigned_exam_group_id(student.id,assigned_group.id) 
                 unless @score.nil? 
                   @score.value 
                 
                 end
               end 
             end
      end 
     unless merged_subjects.nil? 
            @merge_ca = 0 
         merged_subjects.each_with_index do |merged_subject,i|
            @merge_count = 0 
            unless @ca_exam.nil? 
                 @ca_exam.each do |exam_group| 
                     @merge_count += 1 
                    exam_group.name 
                 end 
               end 
                 @merge_count += 1 
               unless @main_exam_group.nil?
                 @main_exam_group.each do |exam_group| 
                     @merge_count += 1 
                 end 
               end
                 @merge_count += 2
               unless @link_term_groups.blank?
                 @link_term_groups.each do |link_term| 
                   @merge_count += 1                     
                    link_term.link_name
               end 
               end
               unless @other_weighted_groups.nil?
                 @other_weighted_groups.each do |exam_group|
                    @merge_count += 1 
                   exam_group.name 
                 end 
              end 
               unless @unweighted_exam_groups.nil? 
                 @unweighted_exam_groups.each do |exam_group| 
                    @merge_count += 1 
                   exam_group.name 
                 end 
              end
                @merge_count += 1 
               @assigned_groups = ReportBuilder.assigned_subject_term_exam_groups(merged_subject.subject_id.first, @term_ids)
               unless @assigned_groups.empty? 
                 @assigned_groups.each do |assigned_group| 
                    @merge_count += 1 
                   SubjectExamGroup.find(assigned_group.subject_exam_group_id).name 
                end 
               end 
            h = {} 
            score = 0 
            class_avg = [] 
              main_score = 0 
             un_score = 0 
             oh_score = 0
             as_ascore = 0 
            previous_total = 0 
              merged_subject.subject_id.each do |subject|
               @mmg = 1;@g = 1 
                 unless @ca_exam.nil? 
                @ca_exam.each_with_index do |exam_group,i| 
                   @exam = Exam.find_by_subject_id_and_exam_group_id(subject,exam_group.id) 
                   exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                   unless @exam.nil? 
                     if exam_group.exam_type == "MarksAndGrades" 
                        if i == 0 
                             score = (exam_score.nil? ? @ca_terms.nil? ? 0 :  (ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@ca_terms)/7.5).to_f :  exam_score.marks.nil? ? @ca_terms.nil? ? 0 :  (ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@ca_terms)/7.5).to_f  : exam_score.marks).to_f 
                           score = (score).to_f.round(2) 
                         elsif i == 1
                             score = (exam_score.nil? ? @term.nil? ? 0 :  (ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@term)).to_f :  exam_score.marks.nil? ? @term.nil? ? 0 :  ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@term).to_f  : exam_score.marks).to_f 
                         end 
                     elsif exam_group.exam_type == "Marks" 
                        if i == 0 
                             score = (exam_score.nil? ? @ca_terms.nil? ? 0 :  (ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@ca_terms)/7.5).to_f :  exam_score.marks.nil? ? @ca_terms.nil? ? 0 :  (ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@ca_terms)/7.5).to_f  : exam_score.marks).to_f 
                           score = (score).to_f.round(2) 
                         elsif i == 1
                             score = (exam_score.nil? ? @term.nil? ? 0 :  (ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@term)).to_f :  exam_score.marks.nil? ? @term.nil? ? 0 :  ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@term).to_f  : exam_score.marks).to_f 
                         end 
                    else 
                       score = exam_score.nil? ? 0 : (exam_score.grading_level || 0)  
                       @g = 0 
                     end 
                  else
                     score = 0 
                  end
                    unless h["#{exam_group.name}"].nil? 
                     h["#{exam_group.name}"] =  h["#{exam_group.name}"] + score  
                   else 
                     h["#{exam_group.name}"] = score 
                   end 
                 end
               end
              unless @main_exam_group.nil?
                @main_exam_group.each do |exam_group| 
                  @exam = Exam.find_by_subject_id_and_exam_group_id(subject.to_i,exam_group.id) 
                   exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                   unless @exam.nil? 
                     if exam_group.exam_type == "MarksAndGrades" 
                        main_score = exam_score.nil? ? 0 :  exam_score.marks 
                     elsif exam_group.exam_type == "Marks" 
                       main_score = exam_score.nil? ? 0 : exam_score.marks 
                     else 
                       main_score = exam_score.nil? ? 0 : (exam_score.grading_level || 0)  
                       @g = 0 
                     end 
                   else 
                       main_score = 0
                   end 
                    unless h["#{exam_group.name}"].nil? 
                     h["#{exam_group.name}"] =  h["#{exam_group.name}"] +main_score
                   else 
                     h["#{exam_group.name}"] = main_score 
                   end
                 end 
               end 
               subjects = Subject.find subject.to_i
              unless @link_term_groups.blank? 
                @link_term_groups.each do |link_term|
                   @term = Term.find(link_term.link_term_id) 
                  unless h["#{link_term.link_name}"].nil?
                     h["#{link_term.link_name}"] =  h["#{link_term.link_name}"] +(ReportBuilder.term_1(subjects,student,@term)).to_f  
                   else
                    h["#{link_term.link_name}"] = (ReportBuilder.term_1(subjects,student,@term)).to_f 
                  end
               end
              end

                @term = Term.find(@term_ids.first) 
                ca_id = @term.ca_term_id 
                ca_term = Term.find(ca_id) unless ca_id.nil?
                   @batch.students.each do |student|                  
                    @ca_avg = ReportBuilder.merge_class_avg(student,subjects,ca_term) 
                   @merge_ca += @ca_avg 
                     end 

               @class_avg = ReportBuilder.class_avg(subjects,@term)
                class_avg << @class_avg 
                  @m_class_avg = ((class_avg.sum.to_f.round(2))/merged_subject.subject_id.count).to_f.round(2) 


                unless @other_weighted_groups.nil? 
                @other_weighted_groups.each do |exam_group| 
                  @exam = Exam.find_by_subject_id_and_exam_group_id(subject.to_i,exam_group.id) 
                  exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                  unless @exam.nil? 
                     if exam_group.exam_type == "MarksAndGrades" 
                      oh_score = exam_score.nil? ? 0 :  exam_score.marks 
                    elsif exam_group.exam_type == "Marks" 
                       oh_score = exam_score.nil? ? 0 :  exam_score.marks
                     else 
                      oh_score = exam_score.nil? ? 0 : (exam_score.grading_level || 0)
                       @g = 0 
                     end 
                        else 
                       oh_score = 0 
                   end 
                    unless h["#{exam_group.name}"].nil? 
                    h["#{exam_group.name}"] =  h["#{exam_group.name}"] +oh_score  
                  else 
                     h["#{exam_group.name}"] = oh_score 
                   end
                end 
               end 
               unless @unweighted_exam_groups.nil? 
                 @unweighted_exam_groups.each do |exam_group| 
                   @exam = Exam.find_by_subject_id_and_exam_group_id(subject.to_i,exam_group.id) 
                   exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                   unless @exam.nil? 
                    if exam_group.exam_type == "MarksAndGrades" 
                       un_score  = exam_score.nil? ? 0 :  exam_score.marks  
                     elsif exam_group.exam_type == "Marks" 
                      un_score  = exam_score.nil? ? 0 :  exam_score.marks 
                        else 
                        un_score  = exam_score.nil? ? 0 : (exam_score.grading_level || 0)  
                       @g = 0 
                     end 
                  else 
                      un_score  =0 
                   end 
                    unless h["#{exam_group.name}"].nil? 
                     h["#{exam_group.name}"] =  h["#{exam_group.name}"] +un_score  
                   else 
                     h["#{exam_group.name}"] = un_score 
                   end 
                 end 
               end 
               unless @assigned_groups.empty? 
                 @assigned_groups.each do |assigned_group| 
                   @score = SubjectExamScore.find_by_student_id_and_assigned_exam_group_id(student,assigned_group.id)
                   unless @score.nil?
                      as_ascore = @score.value 
                  else 
                      as_ascore = 0 
                   end 
                    unless h["a-#{assigned_group.id}"].nil? 
                     h["a-#{assigned_group.id}"] =  h["a-#{assigned_group.id}"] +as_ascore
                  else
                     h["a-#{assigned_group.id}"] = as_ascore  
                   end 
                 end
               end
             end
            
               ca = [] 
               unless @ca_exam.nil?
                 ca_total = 0 
               @ca_exam.each do |exam_group| 
                   ca_total = ca_total + h["#{exam_group.name}"] 
                  @ca = (h["#{exam_group.name}"]/merged_subject.subject_id.count).round(2) unless h["#{exam_group.name}"]==0  
                  ca << @ca 
                 end 
               total_grade =  (ca_total/merged_subject.subject_id.count).round(2) unless ca_total==0 
             end 
               unless @main_exam_group.nil? 
                  main_score = 0 
                  @main_exam_group.each do |exam_group| 
                   main_score = main_score + h["#{exam_group.name}"] 
                    @main = (main_score/merged_subject.subject_id.count).to_f.round(2) 
                  end
              end 
               @merg_total = (ca.sum + @main).to_f.round(2) 
              student_overall << @merg_total 
              key_grade(@subject_percentace) unless @subject_percentace.nil? 
              @grade 
               unless @link_term_groups.blank? 
                @link_term_groups.each do |link_term| 
               (h["#{link_term.link_name}"].to_f/merged_subject.subject_id.count).round(2) unless h["#{link_term.link_name}"].nil? 
              end 
               end 
               unless @other_weighted_groups.nil? 
                 @other_weighted_groups.each do |exam_group| 
                  (h["#{exam_group.name}"]/merged_subject.subject_id.count).round(2) unless h["#{exam_group.name}"]==0 
                end
              end 
               unless @unweighted_exam_groups.nil? 
               @unweighted_exam_groups.each do |exam_group| 
                   (h["#{exam_group.name}"]/merged_subject.subject_id.count).round(2) unless h["#{exam_group.name}"]==0  
                end 
              end
               unless @assigned_groups.empty? 
                @assigned_groups.each do |assigned_group| 
                  (h["a-#{assigned_group.id}"]).chomp.split(//).uniq.join("") unless h["a-#{assigned_group.id}"].nil? 
                 end 
               end 
                              
         end 
      end 
     end
     @overall_student = ((student_overall.sum)/(@subjects.count + merged_subjects.count)).to_f.round(2)
    end
  end

  def second_senior_term_report
      @student = Student.find($student_id)
      @batch = @student.batch
      student_house = StudentAdditionalField.find_by_name("House")
      @student_house_detail = StudentAdditionalDetail.find_by_student_id_and_additional_field_id($student_id,student_house.id) unless student_house.nil?
      @tutor = TermSignature.find_by_is_principal_and_batch_id_and_school_id(false,@batch.id,MultiSchool.current_school.id)
      @principal = TermSignature.find_by_is_principal_and_school_id(true,MultiSchool.current_school.id)
      @ca_exam =[]
      @main_exam_group =[]
      @unweighted_exam_groups =[]
      @other_weighted_groups=[]
      @term = Term.find(@term_ids.first)
      @ca_terms = Term.find(@term.ca_term_id) unless @term.ca_term_id.nil?
      @link_term_groups = TermLinkGroup.find_all_by_term_id(@term_ids.first)
      @term_exam_groups.each do |term_exam_group|
        term_group = TermExamGroup.find_by_exam_group_id(term_exam_group.id)
        if term_group.weightage == 20
          @ca_exam << ExamGroup.find(term_group.exam_group_id)
        elsif term_group.weightage == 60
          @main_exam_group << ExamGroup.find(term_group.exam_group_id)
        elsif term_group.weightage == 0
          @unweighted_exam_groups << ExamGroup.find(term_group.exam_group_id)
        elsif term_group.weightage > 0
          @other_weighted_groups << ExamGroup.find(term_group.exam_group_id)
        end
      end
           @evaluation_groups = EvaluationGroup.find_all_by_term_id(@term.id)
           @scales = ["A","b","C","d","E"]

student = @student 
student_overall = [] 
class_overall = [] 
general_subjects = Subject.find_all_by_batch_id(@batch.id, :conditions=>"elective_group_id IS NULL and is_deleted=false") 
student_electives = StudentsSubject.find_all_by_student_id(student.id,:conditions=>"batch_id = #{@batch.id}") 
elective_subjects = [] 
student_electives.each do |elect| 
   @en_disable = RankStudentSubjectElective.find_by_students_subject_id_and_term_id(elect.id,@term.id) 
        unless @en_disable.nil?  
         if  @en_disable.is_deleted == false
          elective_subjects.push Subject.find(elect.subject_id) 
          end 
         else 
         elective_subjects.push Subject.find(elect.subject_id) 
           end 
 end 
 @subjects = general_subjects + elective_subjects
 @subjects.reject!{|s| s.no_exams==true} 
unless @term_exam_groups.empty? 
   exams = Exam.find_all_by_exam_group_id(@term_exam_groups.collect(&:id))  
  subject_ids = exams.collect(&:subject_id) 
   @subjects.reject!{|sub| !(subject_ids.include?(sub.id))} 
   merged_subjects = RankIngresSubjectMerger.find_all_by_batch_id(@batch.id ) 
  unless merged_subjects.nil?  
     merged_subjects.each do |ms|   
       @subjects = @subjects.reject {|sub| ms.subject_id.include? sub.id.to_s} 
    end
  end 
 unless @subjects.empty? 
     @subjects = @subjects.sort_by {|u| u.name}
    all_exams = @term_exam_groups.reject{|ex| ex.exam_type == "Grades"}
    
       @subjects.each_with_index do |subject,i| 
        
         @count = 0 
       subject.name 
             unless @ca_exam.nil?
               @ca_exam.each do |exam_group| 
                 @count = @count + 1 
            exam_group.name 
               end 
            end 
            @count = @count + 1 
            unless @main_exam_group.nil? 
              @main_exam_group.each do |exam_group| 
                @count = @count + 1 
                 exam_group.name 
               end 
            end 
            @count = @count + 3 
            unless @link_term_groups.blank? 
                 @link_term_groups.each do |link_term| 
                     link_term.link_name
              end 
               end
             unless @other_weighted_groups.nil? 
               @other_weighted_groups.each do |exam_group|
                 @count = @count + 1 
                 exam_group.name
               end 
             end 
             unless @unweighted_exam_groups.nil? 
               @unweighted_exam_groups.each do |exam_group| 
                 @count = @count + 1 
                 exam_group.name
               end 
             end 
          @count = @count + 1 
             @assigned_groups = ReportBuilder.assigned_subject_term_exam_groups(subject.id, @term_ids)  
             unless @assigned_groups.empty? 
               @assigned_groups.each do |assigned_group| 
                 @count = @count + 1 
                 SubjectExamGroup.find(assigned_group.subject_exam_group_id).name 
               end 
             end 
          i = 0 
             cas_total = 0 
            total = 0 
            @mmg = 1;@g = 1 
            unless @ca_exam.nil?
               @ca_exam.each_with_index do |exam_group,i| 
                @exam = Exam.find_by_subject_id_and_exam_group_id(subject.id,exam_group.id)
                 exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                 unless @exam.nil? 
                  if exam_group.exam_type == "MarksAndGrades" 
                      if i == 0 
                     cas_total =  (exam_score.nil? ? @ca_terms.nil? ? 0 :  (ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@ca_terms)/7.5).to_f :  exam_score.marks.nil? ? @ca_terms.nil? ? 0 : (ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@ca_terms)/7.5).to_f:  exam_score.marks).to_f  
                       total = total + cas_total   
                      total = (total/7.5).to_f.round(2) 
                       cas_total==0 ? "-" : (cas_total/7.5).to_f.round(2) 
                    
                    elsif i == 1 
                         cas_total =  (exam_score.nil? ? @term.nil? ? 0 :  ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@term).to_f :  exam_score.marks.nil? ? @term.nil? ? 0 : ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@term).to_f:  exam_score.marks).to_f
                       total = total + cas_total   
                       cas_total==0 ? "-" : cas_total                    
                   end 
                  elsif exam_group.exam_type == "Marks"                 
                    if i == 0 
                      # if @ca_terms.name == "Second Mid Term"  and student.batch.name == "11G"
                     cas_total =  (exam_score.nil? ? @ca_terms.nil? ? 0 :  (ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@ca_terms)/7.5).to_f :  exam_score.marks.nil? ? @ca_terms.nil? ? 0 :( ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@ca_terms)/7.5).to_f:  exam_score.marks).to_f  
                       total = total + cas_total   
                      total = (total).to_f.round(2) 
                       cas_total==0 ? "-" : (cas_total).to_f.round(2) 
                     # else
                     #   cas_total =  (exam_score.nil? ? @ca_terms.nil? ? 0 :  ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@ca_terms).to_f :  exam_score.marks.nil? ? @ca_terms.nil? ? 0 : ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@ca_terms).to_f:  exam_score.marks).to_f  
                     #   total = total + cas_total   
                     #  total = (total/7.5).to_f.round(2) 
                     #   cas_total==0 ? "-" : (cas_total/7.5).to_f.round(2)
                     # end
                    
                    elsif i == 1 
                         cas_total =  (exam_score.nil? ? @term.nil? ? 0 :  ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@term).to_f :  exam_score.marks.nil? ? @term.nil? ? 0 : ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@term).to_f:  exam_score.marks).to_f
                       total = total + cas_total   
                       cas_total==0 ? "-" : cas_total                    
                   end
                   else 
                     exam_score.nil? ? '-' : (exam_score.grading_level || '-')  
                     @g = 0
                  end 
                 else 
                   "#{t('n_a')}"
                 end 
              end 
               unless all_exams.empty? 
                 if @mmg == @g 
                  total.to_f==0 ? "-" : total 
                
                end 
              end 
             end 
              exam_total = 0
              main_total = 0 
            unless @main_exam_group.nil?
               @main_exam_group.each do |exam_group|
                 @exam = Exam.find_by_subject_id_and_exam_group_id(subject.id,exam_group.id) 
                 exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                 unless @exam.nil? 
                   if exam_group.exam_type == "MarksAndGrades" 
                     exam_total = (exam_score.nil? ? '-' : exam_score.marks)  
                      main_total += exam_total.to_f 
                  elsif exam_group.exam_type == "Marks" 
                    exam_total = (exam_score.nil? ? '-' : exam_score.marks) 
                        main_total += exam_total.to_f 
                    else 
                      exam_total = (exam_score.nil? ? '-' : (exam_score.grading_level || '-') )
                     @g = 0 
                   end 
                 else 
                   "#{t('n_a')}" 
                 end
              end
             end

             unless all_exams.empty? 
               if @mmg == @g 
                 @grand_total = (total + main_total) 
                 student_overall << @grand_total
               end
             end 
            @subject_percentace = (@grand_total*100)/(@weighted_total.to_i) 
             key_grade(@subject_percentace) unless @subject_percentace.nil? 
             @grade 
            unless @link_term_groups.blank?
                @link_term_groups.each do |link_term| 
                   @term = Term.find(link_term.link_term_id) 
                     @total =  (ReportBuilder.term_1(subject,student,@term)).to_f
                  @total==0 ? "-" : @total
              end 
               end 
             unless @other_weighted_groups.nil? 
              @other_weighted_groups.each do |exam_group| 
                 @exam = Exam.find_by_subject_id_and_exam_group_id(subject.id,exam_group.id) 
                 exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                unless @exam.nil?
                  if exam_group.exam_type == "MarksAndGrades" 
                     exam_score.nil? ? '-' :  "#{exam_score.marks}"  
                   elsif exam_group.exam_type == "Marks" 
                    exam_score.nil? ? '-' : "#{exam_score.marks}" 
                   else
                     exam_score.nil? ? '-' : (exam_score.grading_level || '-') 
                     @g = 0 
                   end 
                 else 
                   "#{t('n_a')}" 
                 end 
               end 
             end

            unless @unweighted_exam_groups.nil? 
               @unweighted_exam_groups.each do |exam_group| 
                 @exam = Exam.find_by_subject_id_and_exam_group_id(subject.id,exam_group.id) 
                 exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                 unless @exam.nil?
                  if exam_group.exam_type == "MarksAndGrades" 
                     exam_score.nil? ? '-' :  "#{exam_score.marks}" 
                   elsif exam_group.exam_type == "Marks"
                     exam_score.nil? ? '-' : "#{exam_score.marks}"
                   else                    
                    exam_score.nil? ? '-' : (exam_score.grading_level || '-') 
                     @g = 0
                   end 
                 else 
                   "#{t('n_a')}" 
                 end 
               end 
              end 
                ca_id = @term.ca_term_id 
              ca_term = Term.find(ca_id) unless ca_id.nil? 
             @ca = ReportBuilder.class_avg(subject,ca_term) 


               student_electives = StudentsSubject.find_all_by_student_id(student.id,:conditions=>"batch_id = #{@batch.id}") 
               @student_electives =StudentsSubject.find_by_subject_id(subject.id) 
                @subject1 = Subject.find(subject.id) 
                 @subject_counts = @subject1.students.count 
               unless @student_electives.nil?
                   @class_avg = ReportBuilder.class_avg(subject,@term) 
                     @cl_cvg = ((@ca*subject.batch.students.count)/15).to_f.round(2) 
                     @cl_avg1 = (((@cl_cvg*5).to_f.round(2))/@subject_counts).to_f.round(2) 
                     @cl_avg = (@cl_avg1/5).to_f.round(2) + ((@class_avg*subject.batch.students.count).to_f.round(2)/@subject_counts).to_f.round(2) 
                   class_overall << @cl_avg 
               else
                   @class_avg = ReportBuilder.class_avg(subject,@term) 
                   @cl_avg = (@class_avg + ((@ca/3)/5).to_f.round(2)).to_f.round(2)
                  class_overall << @cl_avg 
              end 
             unless @assigned_groups.empty? 
               @assigned_groups.each do |assigned_group| 
                 @score = SubjectExamScore.find_by_student_id_and_assigned_exam_group_id(student.id,assigned_group.id) 
                 unless @score.nil? 
                   @score.value 
                 
                 end
               end 
             end
      end 
     unless merged_subjects.nil? 
            @merge_ca = 0 
         merged_subjects.each_with_index do |merged_subject,i|
            @merge_count = 0 
            unless @ca_exam.nil? 
                 @ca_exam.each do |exam_group| 
                     @merge_count += 1 
                    exam_group.name 
                 end 
               end 
                 @merge_count += 1 
               unless @main_exam_group.nil?
                 @main_exam_group.each do |exam_group| 
                     @merge_count += 1 
                 end 
               end
                 @merge_count += 2
               unless @link_term_groups.blank?
                 @link_term_groups.each do |link_term| 
                   @merge_count += 1                     
                    link_term.link_name
               end 
               end
               unless @other_weighted_groups.nil?
                 @other_weighted_groups.each do |exam_group|
                    @merge_count += 1 
                   exam_group.name 
                 end 
              end 
               unless @unweighted_exam_groups.nil? 
                 @unweighted_exam_groups.each do |exam_group| 
                    @merge_count += 1 
                   exam_group.name 
                 end 
              end
                @merge_count += 1 
               @assigned_groups = ReportBuilder.assigned_subject_term_exam_groups(merged_subject.subject_id.first, @term_ids)
               unless @assigned_groups.empty? 
                 @assigned_groups.each do |assigned_group| 
                    @merge_count += 1 
                   SubjectExamGroup.find(assigned_group.subject_exam_group_id).name 
                end 
               end 
            h = {} 
            score = 0 
            class_avg = [] 
              main_score = 0 
             un_score = 0 
             oh_score = 0
             as_ascore = 0 
            previous_total = 0 
              merged_subject.subject_id.each do |subject|
               @mmg = 1;@g = 1 
                 unless @ca_exam.nil? 
                @ca_exam.each_with_index do |exam_group,i| 
                   @exam = Exam.find_by_subject_id_and_exam_group_id(subject,exam_group.id) 
                   exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                   unless @exam.nil? 
                     if exam_group.exam_type == "MarksAndGrades" 
                        if i == 0 
                             score = (exam_score.nil? ? @ca_terms.nil? ? 0 :  (ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@ca_terms)/7.5).to_f :  exam_score.marks.nil? ? @ca_terms.nil? ? 0 : ( ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@ca_terms)/7.5).to_f  : exam_score.marks).to_f 
                           score = (score/7.5).to_f.round(2) 
                         elsif i == 1
                             score = (exam_score.nil? ? @term.nil? ? 0 :  (ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@term)).to_f :  exam_score.marks.nil? ? @term.nil? ? 0 :  ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@term).to_f  : exam_score.marks).to_f 
                         end 
                     elsif exam_group.exam_type == "Marks" 
                        if i == 0 
                          # if @ca_terms.name == "Second Mid Term"  and student.batch.name == "11G"
                             score = (exam_score.nil? ? @ca_terms.nil? ? 0 :  (ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@ca_terms)/7.5).to_f :  exam_score.marks.nil? ? @ca_terms.nil? ? 0 :  (ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@ca_terms)/7.5).to_f  : exam_score.marks).to_f 
                           score = (score).to_f.round(2) 
                         # else
                         #   score = (exam_score.nil? ? @ca_terms.nil? ? 0 :  (ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@ca_terms)).to_f :  exam_score.marks.nil? ? @ca_terms.nil? ? 0 :  ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@ca_terms).to_f  : exam_score.marks).to_f 
                         #   score = (score/7.5).to_f.round(2) 
                         # end
                         elsif i == 1
                             score = (exam_score.nil? ? @term.nil? ? 0 :  (ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@term)).to_f :  exam_score.marks.nil? ? @term.nil? ? 0 :  ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@term).to_f  : exam_score.marks).to_f 
                         end
                    else 
                       score = exam_score.nil? ? 0 : (exam_score.grading_level || 0)  
                       @g = 0 
                     end 
                  else
                     score = 0 
                  end
                    unless h["#{exam_group.name}"].nil? 
                     h["#{exam_group.name}"] =  h["#{exam_group.name}"] + score  
                   else 
                     h["#{exam_group.name}"] = score 
                   end 
                 end
               end
              unless @main_exam_group.nil?
                @main_exam_group.each do |exam_group| 
                  @exam = Exam.find_by_subject_id_and_exam_group_id(subject.to_i,exam_group.id) 
                   exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                   unless @exam.nil? 
                     if exam_group.exam_type == "MarksAndGrades" 
                        main_score = exam_score.nil? ? 0 :  exam_score.marks 
                     elsif exam_group.exam_type == "Marks" 
                       main_score = exam_score.nil? ? 0 : exam_score.marks 
                     else 
                       main_score = exam_score.nil? ? 0 : (exam_score.grading_level || 0)  
                       @g = 0 
                     end 
                   else 
                       main_score = 0
                   end 
                    unless h["#{exam_group.name}"].nil? 
                     h["#{exam_group.name}"] =  h["#{exam_group.name}"] +main_score
                   else 
                     h["#{exam_group.name}"] = main_score 
                   end
                 end 
               end 
               subjects = Subject.find subject.to_i
              unless @link_term_groups.blank? 
                @link_term_groups.each do |link_term|
                   @term = Term.find(link_term.link_term_id) 
                  unless h["#{link_term.link_name}"].nil?
                     h["#{link_term.link_name}"] =  h["#{link_term.link_name}"] +(ReportBuilder.term_1(subjects,student,@term)).to_f  
                   else
                    h["#{link_term.link_name}"] = (ReportBuilder.term_1(subjects,student,@term)).to_f 
                  end
               end
              end

                @term = Term.find(@term_ids.first) 
                ca_id = @term.ca_term_id 
                ca_term = Term.find(ca_id) unless ca_id.nil?
                   @batch.students.each do |student|                  
                    @ca_avg = ReportBuilder.merge_class_avg(student,subjects,ca_term) 
                   @merge_ca += @ca_avg 
                     end 

               @class_avg = ReportBuilder.class_avg(subjects,@term)
                class_avg << @class_avg 
                  @m_class_avg = ((class_avg.sum.to_f.round(2))/merged_subject.subject_id.count).to_f.round(2) 


                unless @other_weighted_groups.nil? 
                @other_weighted_groups.each do |exam_group| 
                  @exam = Exam.find_by_subject_id_and_exam_group_id(subject.to_i,exam_group.id) 
                  exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                  unless @exam.nil? 
                     if exam_group.exam_type == "MarksAndGrades" 
                      oh_score = exam_score.nil? ? 0 :  exam_score.marks 
                    elsif exam_group.exam_type == "Marks" 
                       oh_score = exam_score.nil? ? 0 :  exam_score.marks
                     else 
                      oh_score = exam_score.nil? ? 0 : (exam_score.grading_level || 0)
                       @g = 0 
                     end 
                        else 
                       oh_score = 0 
                   end 
                    unless h["#{exam_group.name}"].nil? 
                    h["#{exam_group.name}"] =  h["#{exam_group.name}"] +oh_score  
                  else 
                     h["#{exam_group.name}"] = oh_score 
                   end
                end 
               end 
               unless @unweighted_exam_groups.nil? 
                 @unweighted_exam_groups.each do |exam_group| 
                   @exam = Exam.find_by_subject_id_and_exam_group_id(subject.to_i,exam_group.id) 
                   exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                   unless @exam.nil? 
                    if exam_group.exam_type == "MarksAndGrades" 
                       un_score  = exam_score.nil? ? 0 :  exam_score.marks  
                    elsif exam_group.exam_type == "Marks" 
                      un_score  = exam_score.nil? ? 0 :  exam_score.marks 
                    else 
                        un_score  = exam_score.nil? ? 0 : (exam_score.grading_level || 0)  
                       @g = 0 
                    end 
                  else 
                      un_score  =0 
                   end 
                    unless h["#{exam_group.name}"].nil? 
                     h["#{exam_group.name}"] =  h["#{exam_group.name}"] +un_score  
                   else 
                     h["#{exam_group.name}"] = un_score 
                   end 
                 end 
               end 
               unless @assigned_groups.empty? 
                 @assigned_groups.each do |assigned_group| 
                   @score = SubjectExamScore.find_by_student_id_and_assigned_exam_group_id(student,assigned_group.id)
                   unless @score.nil?
                      as_ascore = @score.value 
                  else 
                      as_ascore = 0 
                   end 
                    unless h["a-#{assigned_group.id}"].nil? 
                     h["a-#{assigned_group.id}"] =  h["a-#{assigned_group.id}"] +as_ascore
                  else
                     h["a-#{assigned_group.id}"] = as_ascore  
                   end 
                 end
               end
             end
            
               ca = [] 
               unless @ca_exam.nil?
                 ca_total = 0 
               @ca_exam.each do |exam_group| 
                   ca_total = ca_total + h["#{exam_group.name}"] 
                  @ca = (h["#{exam_group.name}"]/merged_subject.subject_id.count).round(2) unless h["#{exam_group.name}"]==0  
                  ca << @ca 
                 end 
               total_grade =  (ca_total/merged_subject.subject_id.count).round(2) unless ca_total==0 
             end 
               unless @main_exam_group.nil? 
                  main_score = 0 
                  @main_exam_group.each do |exam_group| 
                   main_score = main_score + h["#{exam_group.name}"] 
                    @main = (main_score/merged_subject.subject_id.count).to_f.round(2) 
                  end
              end 
               @merg_total = (ca.sum + @main).to_f.round(2) 
              student_overall << @merg_total 
              key_grade(@subject_percentace) unless @subject_percentace.nil? 
              @grade 
               unless @link_term_groups.blank? 
                @link_term_groups.each do |link_term| 
               (h["#{link_term.link_name}"].to_f/merged_subject.subject_id.count).round(2) unless h["#{link_term.link_name}"].nil? 
              end 
               end 
               unless @other_weighted_groups.nil? 
                 @other_weighted_groups.each do |exam_group| 
                  (h["#{exam_group.name}"]/merged_subject.subject_id.count).round(2) unless h["#{exam_group.name}"]==0 
                end
              end 
               unless @unweighted_exam_groups.nil? 
               @unweighted_exam_groups.each do |exam_group| 
                   (h["#{exam_group.name}"]/merged_subject.subject_id.count).round(2) unless h["#{exam_group.name}"]==0  
                end 
              end
               unless @assigned_groups.empty? 
                @assigned_groups.each do |assigned_group| 
                  (h["a-#{assigned_group.id}"]).chomp.split(//).uniq.join("") unless h["a-#{assigned_group.id}"].nil? 
                 end 
               end 
                              
         end 
      end 
     end
     @overall_student = ((student_overall.sum)/(@subjects.count + merged_subjects.count)).to_f.round(2)
    end     
  end

  def third_senior_term_report
      @student = Student.find($student_id)
      @batch = @student.batch
      student_house = StudentAdditionalField.find_by_name("House")
      @student_house_detail = StudentAdditionalDetail.find_by_student_id_and_additional_field_id($student_id,student_house.id) unless student_house.nil?
      @tutor = TermSignature.find_by_is_principal_and_batch_id_and_school_id(false,@batch.id,MultiSchool.current_school.id)
      @principal = TermSignature.find_by_is_principal_and_school_id(true,MultiSchool.current_school.id)
      @ca_exam =[]
      @main_exam_group =[]
      @unweighted_exam_groups =[]
      @other_weighted_groups=[]
      @term = Term.find(@term_ids.first)
      @ca_terms = Term.find(@term.ca_term_id) unless @term.ca_term_id.nil?
      @link_term_groups = TermLinkGroup.find_all_by_term_id(@term_ids.first)
      @term_exam_groups.each do |term_exam_group|
        term_group = TermExamGroup.find_by_exam_group_id(term_exam_group.id)
        if term_group.weightage == 20
          @ca_exam << ExamGroup.find(term_group.exam_group_id)
        elsif term_group.weightage == 60
          @main_exam_group << ExamGroup.find(term_group.exam_group_id)
        elsif term_group.weightage == 0
          @unweighted_exam_groups << ExamGroup.find(term_group.exam_group_id)
        elsif term_group.weightage > 0
          @other_weighted_groups << ExamGroup.find(term_group.exam_group_id)
        end
      end
           @evaluation_groups = EvaluationGroup.find_all_by_term_id(@term.id)
           @scales = ["A","b","C","d","E"]
           student = @student 
student_overall = [] 
class_overall = [] 
general_subjects = Subject.find_all_by_batch_id(@batch.id, :conditions=>"elective_group_id IS NULL and is_deleted=false") 
student_electives = StudentsSubject.find_all_by_student_id(student.id,:conditions=>"batch_id = #{@batch.id}") 
elective_subjects = [] 
student_electives.each do |elect| 
   @en_disable = RankStudentSubjectElective.find_by_students_subject_id_and_term_id(elect.id,@term.id) 
        unless @en_disable.nil?  
         if  @en_disable.is_deleted == false
          elective_subjects.push Subject.find(elect.subject_id) 
          end 
         else 
         elective_subjects.push Subject.find(elect.subject_id) 
           end 
 end 
 @subjects = general_subjects + elective_subjects
 @subjects.reject!{|s| s.no_exams==true} 
unless @term_exam_groups.empty? 
   exams = Exam.find_all_by_exam_group_id(@term_exam_groups.collect(&:id))  
  subject_ids = exams.collect(&:subject_id) 
   @subjects.reject!{|sub| !(subject_ids.include?(sub.id))} 
   merged_subjects = RankIngresSubjectMerger.find_all_by_batch_id(@batch.id ) 
  unless merged_subjects.nil?  
     merged_subjects.each do |ms|   
       @subjects = @subjects.reject {|sub| ms.subject_id.include? sub.id.to_s} 
    end
  end 
 unless @subjects.empty? 
     @subjects = @subjects.sort_by {|u| u.name}
    all_exams = @term_exam_groups.reject{|ex| ex.exam_type == "Grades"}
    
       @subjects.each_with_index do |subject,i| 
        
         @count = 0 
       subject.name 
             unless @ca_exam.nil?
               @ca_exam.each do |exam_group| 
                 @count = @count + 1 
            exam_group.name 
               end 
            end 
            @count = @count + 1 
            unless @main_exam_group.nil? 
              @main_exam_group.each do |exam_group| 
                @count = @count + 1 
                 exam_group.name 
               end 
            end 
            @count = @count + 3 
            unless @link_term_groups.blank? 
                 @link_term_groups.each do |link_term| 
                     link_term.link_name
              end 
               end
             unless @other_weighted_groups.nil? 
               @other_weighted_groups.each do |exam_group|
                 @count = @count + 1 
                 exam_group.name
               end 
             end 
             unless @unweighted_exam_groups.nil? 
               @unweighted_exam_groups.each do |exam_group| 
                 @count = @count + 1 
                 exam_group.name
               end 
             end 
          @count = @count + 1 
             @assigned_groups = ReportBuilder.assigned_subject_term_exam_groups(subject.id, @term_ids)  
             unless @assigned_groups.empty? 
               @assigned_groups.each do |assigned_group| 
                 @count = @count + 1 
                 SubjectExamGroup.find(assigned_group.subject_exam_group_id).name 
               end 
             end 
          i = 0 
             cas_total = 0 
            total = 0 
            @mmg = 1;@g = 1 
            unless @ca_exam.nil?
               @ca_exam.each_with_index do |exam_group,i| 
                @exam = Exam.find_by_subject_id_and_exam_group_id(subject.id,exam_group.id)
                 exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                 unless @exam.nil? 
                  if exam_group.exam_type == "MarksAndGrades" 
                      if i == 0 
                     cas_total =  (exam_score.nil? ? @ca_terms.nil? ? 0 :  ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@ca_terms).to_f :  exam_score.marks.nil? ? @ca_terms.nil? ? 0 : ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@ca_terms).to_f:  exam_score.marks).to_f  
                       total = total + cas_total   
                      total = (total/7.5).to_f.round(2) 
                       cas_total==0 ? "-" : (cas_total/7.5).to_f.round(2) 
                    
                    elsif i == 1 
                         cas_total =  (exam_score.nil? ? @term.nil? ? 0 :  ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@term).to_f :  exam_score.marks.nil? ? @term.nil? ? 0 : ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@term).to_f:  exam_score.marks).to_f
                       total = total + cas_total   
                       cas_total==0 ? "-" : cas_total                    
                   end 
                  elsif exam_group.exam_type == "Marks"                 
                    if i == 0 
                     cas_total =  (exam_score.nil? ? @ca_terms.nil? ? 0 :  ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@ca_terms).to_f :  exam_score.marks.nil? ? @ca_terms.nil? ? 0 : ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@ca_terms).to_f:  exam_score.marks).to_f  
                       total = total + cas_total   
                      total = (total/7.5).to_f.round(2) 
                       cas_total==0 ? "-" : (cas_total/7.5).to_f.round(2) 
                    
                    elsif i == 1 
                         cas_total =  (exam_score.nil? ? @term.nil? ? 0 :  ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@term).to_f :  exam_score.marks.nil? ? @term.nil? ? 0 : ReportBuilder.midterm_to_end_of_term(subject.id,student.id,@term).to_f:  exam_score.marks).to_f
                       total = total + cas_total   
                       cas_total==0 ? "-" : cas_total                    
                   end  
                   else 
                     exam_score.nil? ? '-' : (exam_score.grading_level || '-')  
                     @g = 0
                  end 
                 else 
                   "#{t('n_a')}"
                 end 
              end 
               unless all_exams.empty? 
                 if @mmg == @g 
                  total.to_f==0 ? "-" : total 
                
                end 
              end 
             end 
              exam_total = 0
              main_total = 0 
            unless @main_exam_group.nil?
               @main_exam_group.each do |exam_group|
                 @exam = Exam.find_by_subject_id_and_exam_group_id(subject.id,exam_group.id) 
                 exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                 unless @exam.nil? 
                   if exam_group.exam_type == "MarksAndGrades" 
                     exam_total = (exam_score.nil? ? '-' : exam_score.marks)  
                      main_total += exam_total.to_f 
                  elsif exam_group.exam_type == "Marks" 
                    exam_total = (exam_score.nil? ? '-' : exam_score.marks) 
                        main_total += exam_total.to_f 
                    else 
                      exam_total = (exam_score.nil? ? '-' : (exam_score.grading_level || '-') )
                     @g = 0 
                   end 
                 else 
                   "#{t('n_a')}" 
                 end
              end
             end

             unless all_exams.empty? 
               if @mmg == @g 
                 @grand_total = (total + main_total) 
                 student_overall << @grand_total
               end
             end 
            @subject_percentace = (@grand_total*100)/(@weighted_total.to_i) 
             key_grade(@subject_percentace) unless @subject_percentace.nil? 
             @grade 
            unless @link_term_groups.blank?
                @link_term_groups.each do |link_term| 
                   @term = Term.find(link_term.link_term_id) 
                     @total =  (ReportBuilder.term_1(subject,student,@term)).to_f
                  @total==0 ? "-" : @total
              end 
               end 
             unless @other_weighted_groups.nil? 
              @other_weighted_groups.each do |exam_group| 
                 @exam = Exam.find_by_subject_id_and_exam_group_id(subject.id,exam_group.id) 
                 exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                unless @exam.nil?
                  if exam_group.exam_type == "MarksAndGrades" 
                     exam_score.nil? ? '-' :  "#{exam_score.marks}"  
                   elsif exam_group.exam_type == "Marks" 
                    exam_score.nil? ? '-' : "#{exam_score.marks}" 
                   else
                     exam_score.nil? ? '-' : (exam_score.grading_level || '-') 
                     @g = 0 
                   end 
                 else 
                   "#{t('n_a')}" 
                 end 
               end 
             end

            unless @unweighted_exam_groups.nil? 
               @unweighted_exam_groups.each do |exam_group| 
                 @exam = Exam.find_by_subject_id_and_exam_group_id(subject.id,exam_group.id) 
                 exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                 unless @exam.nil?
                  if exam_group.exam_type == "MarksAndGrades" 
                     exam_score.nil? ? '-' :  "#{exam_score.marks}" 
                   elsif exam_group.exam_type == "Marks"
                     exam_score.nil? ? '-' : "#{exam_score.marks}"
                   else                    
                    exam_score.nil? ? '-' : (exam_score.grading_level || '-') 
                     @g = 0
                   end 
                 else 
                   "#{t('n_a')}" 
                 end 
               end 
              end 
                ca_id = @term.ca_term_id 
              ca_term = Term.find(ca_id) unless ca_id.nil? 
             @ca = ReportBuilder.class_avg(subject,ca_term) 


               student_electives = StudentsSubject.find_all_by_student_id(student.id,:conditions=>"batch_id = #{@batch.id}") 
               @student_electives =StudentsSubject.find_by_subject_id(subject.id) 
                @subject1 = Subject.find(subject.id) 
                 @subject_counts = @subject1.students.count 
               unless @student_electives.nil?
                   @class_avg = ReportBuilder.class_avg(subject,@term) 
                     @cl_cvg = ((@ca*subject.batch.students.count)/15).to_f.round(2) 
                     @cl_avg1 = (((@cl_cvg*5).to_f.round(2))/@subject_counts).to_f.round(2) 
                     @cl_avg = (@cl_avg1/5).to_f.round(2) + ((@class_avg*subject.batch.students.count).to_f.round(2)/@subject_counts).to_f.round(2) 
                   class_overall << @cl_avg 
               else
                   @class_avg = ReportBuilder.class_avg(subject,@term) 
                   @cl_avg = (@class_avg + ((@ca/3)/5).to_f.round(2)).to_f.round(2)
                  class_overall << @cl_avg 
              end 
             unless @assigned_groups.empty? 
               @assigned_groups.each do |assigned_group| 
                 @score = SubjectExamScore.find_by_student_id_and_assigned_exam_group_id(student.id,assigned_group.id) 
                 unless @score.nil? 
                   @score.value 
                 
                 end
               end 
             end
      end 
     unless merged_subjects.nil? 
            @merge_ca = 0 
         merged_subjects.each_with_index do |merged_subject,i|
            @merge_count = 0 
            unless @ca_exam.nil? 
                 @ca_exam.each do |exam_group| 
                     @merge_count += 1 
                    exam_group.name 
                 end 
               end 
                 @merge_count += 1 
               unless @main_exam_group.nil?
                 @main_exam_group.each do |exam_group| 
                     @merge_count += 1 
                 end 
               end
                 @merge_count += 2
               unless @link_term_groups.blank?
                 @link_term_groups.each do |link_term| 
                   @merge_count += 1                     
                    link_term.link_name
               end 
               end
               unless @other_weighted_groups.nil?
                 @other_weighted_groups.each do |exam_group|
                    @merge_count += 1 
                   exam_group.name 
                 end 
              end 
               unless @unweighted_exam_groups.nil? 
                 @unweighted_exam_groups.each do |exam_group| 
                    @merge_count += 1 
                   exam_group.name 
                 end 
              end
                @merge_count += 1 
               @assigned_groups = ReportBuilder.assigned_subject_term_exam_groups(merged_subject.subject_id.first, @term_ids)
               unless @assigned_groups.empty? 
                 @assigned_groups.each do |assigned_group| 
                    @merge_count += 1 
                   SubjectExamGroup.find(assigned_group.subject_exam_group_id).name 
                end 
               end 
            h = {} 
            score = 0 
            class_avg = [] 
              main_score = 0 
             un_score = 0 
             oh_score = 0
             as_ascore = 0 
            previous_total = 0 
              merged_subject.subject_id.each do |subject|
               @mmg = 1;@g = 1 
                 unless @ca_exam.nil? 
                @ca_exam.each_with_index do |exam_group,i| 
                   @exam = Exam.find_by_subject_id_and_exam_group_id(subject,exam_group.id) 
                   exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                   unless @exam.nil? 
                     if exam_group.exam_type == "MarksAndGrades" 
                        if i == 0 
                             score = (exam_score.nil? ? @ca_terms.nil? ? 0 :  (ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@ca_terms)).to_f :  exam_score.marks.nil? ? @ca_terms.nil? ? 0 :  ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@ca_terms).to_f  : exam_score.marks).to_f 
                           score = (score/7.5).to_f.round(2) 
                         elsif i == 1
                             score = (exam_score.nil? ? @term.nil? ? 0 :  (ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@term)).to_f :  exam_score.marks.nil? ? @term.nil? ? 0 :  ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@term).to_f  : exam_score.marks).to_f 
                         end 
                     elsif exam_group.exam_type == "Marks" 
                        if i == 0 
                             score = (exam_score.nil? ? @ca_terms.nil? ? 0 :  (ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@ca_terms)).to_f :  exam_score.marks.nil? ? @ca_terms.nil? ? 0 :  ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@ca_terms).to_f  : exam_score.marks).to_f 
                           score = (score/7.5).to_f.round(2) 
                         elsif i == 1
                             score = (exam_score.nil? ? @term.nil? ? 0 :  (ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@term)).to_f :  exam_score.marks.nil? ? @term.nil? ? 0 :  ReportBuilder.midterm_to_end_of_term(subject.to_i,student.id,@term).to_f  : exam_score.marks).to_f 
                         end
                    else 
                       score = exam_score.nil? ? 0 : (exam_score.grading_level || 0)  
                       @g = 0 
                     end 
                  else
                     score = 0 
                  end
                    unless h["#{exam_group.name}"].nil? 
                     h["#{exam_group.name}"] =  h["#{exam_group.name}"] + score  
                   else 
                     h["#{exam_group.name}"] = score 
                   end 
                 end
               end
              unless @main_exam_group.nil?
                @main_exam_group.each do |exam_group| 
                  @exam = Exam.find_by_subject_id_and_exam_group_id(subject.to_i,exam_group.id) 
                   exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                   unless @exam.nil? 
                     if exam_group.exam_type == "MarksAndGrades" 
                        main_score = exam_score.nil? ? 0 :  exam_score.marks 
                     elsif exam_group.exam_type == "Marks" 
                       main_score = exam_score.nil? ? 0 : exam_score.marks 
                     else 
                       main_score = exam_score.nil? ? 0 : (exam_score.grading_level || 0)  
                       @g = 0 
                     end 
                   else 
                       main_score = 0
                   end 
                    unless h["#{exam_group.name}"].nil? 
                     h["#{exam_group.name}"] =  h["#{exam_group.name}"] +main_score
                   else 
                     h["#{exam_group.name}"] = main_score 
                   end
                 end 
               end 
               subjects = Subject.find subject.to_i
              unless @link_term_groups.blank? 
                @link_term_groups.each do |link_term|
                   @term = Term.find(link_term.link_term_id) 
                  unless h["#{link_term.link_name}"].nil?
                     h["#{link_term.link_name}"] =  h["#{link_term.link_name}"] +(ReportBuilder.term_1(subjects,student,@term)).to_f  
                   else
                    h["#{link_term.link_name}"] = (ReportBuilder.term_1(subjects,student,@term)).to_f 
                  end
               end
              end

                @term = Term.find(@term_ids.first) 
                ca_id = @term.ca_term_id 
                ca_term = Term.find(ca_id) unless ca_id.nil?
                   @batch.students.each do |student|                  
                    @ca_avg = ReportBuilder.merge_class_avg(student,subjects,ca_term) 
                   @merge_ca += @ca_avg 
                     end 

               @class_avg = ReportBuilder.class_avg(subjects,@term)
                class_avg << @class_avg 
                  @m_class_avg = ((class_avg.sum.to_f.round(2))/merged_subject.subject_id.count).to_f.round(2) 


                unless @other_weighted_groups.nil? 
                @other_weighted_groups.each do |exam_group| 
                  @exam = Exam.find_by_subject_id_and_exam_group_id(subject.to_i,exam_group.id) 
                  exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                  unless @exam.nil? 
                     if exam_group.exam_type == "MarksAndGrades" 
                      oh_score = exam_score.nil? ? 0 :  exam_score.marks 
                    elsif exam_group.exam_type == "Marks" 
                       oh_score = exam_score.nil? ? 0 :  exam_score.marks
                     else 
                      oh_score = exam_score.nil? ? 0 : (exam_score.grading_level || 0)
                       @g = 0 
                     end 
                        else 
                       oh_score = 0 
                   end 
                    unless h["#{exam_group.name}"].nil? 
                    h["#{exam_group.name}"] =  h["#{exam_group.name}"] +oh_score  
                  else 
                     h["#{exam_group.name}"] = oh_score 
                   end
                end 
               end 
               unless @unweighted_exam_groups.nil? 
                 @unweighted_exam_groups.each do |exam_group| 
                   @exam = Exam.find_by_subject_id_and_exam_group_id(subject.to_i,exam_group.id) 
                   exam_score = ExamScore.find_by_student_id(student.id, :conditions=>{:exam_id=>@exam.id})unless @exam.nil? 
                   unless @exam.nil? 
                    if exam_group.exam_type == "MarksAndGrades" 
                       un_score  = exam_score.nil? ? 0 :  exam_score.marks  
                     elsif exam_group.exam_type == "Marks" 
                      un_score  = exam_score.nil? ? 0 :  exam_score.marks 
                        else 
                        un_score  = exam_score.nil? ? 0 : (exam_score.grading_level || 0)  
                       @g = 0 
                     end 
                  else 
                      un_score  =0 
                   end 
                    unless h["#{exam_group.name}"].nil? 
                     h["#{exam_group.name}"] =  h["#{exam_group.name}"] +un_score  
                   else 
                     h["#{exam_group.name}"] = un_score 
                   end 
                 end 
               end 
               unless @assigned_groups.empty? 
                 @assigned_groups.each do |assigned_group| 
                   @score = SubjectExamScore.find_by_student_id_and_assigned_exam_group_id(student,assigned_group.id)
                   unless @score.nil?
                      as_ascore = @score.value 
                  else 
                      as_ascore = 0 
                   end 
                    unless h["a-#{assigned_group.id}"].nil? 
                     h["a-#{assigned_group.id}"] =  h["a-#{assigned_group.id}"] +as_ascore
                  else
                     h["a-#{assigned_group.id}"] = as_ascore  
                   end 
                 end
               end
             end
            
               ca = [] 
               unless @ca_exam.nil?
                 ca_total = 0 
               @ca_exam.each do |exam_group| 
                   ca_total = ca_total + h["#{exam_group.name}"] 
                  @ca = (h["#{exam_group.name}"]/merged_subject.subject_id.count).round(2) unless h["#{exam_group.name}"]==0  
                  ca << @ca 
                 end 
               total_grade =  (ca_total/merged_subject.subject_id.count).round(2) unless ca_total==0 
             end 
               unless @main_exam_group.nil? 
                  main_score = 0 
                  @main_exam_group.each do |exam_group| 
                   main_score = main_score + h["#{exam_group.name}"] 
                    @main = (main_score/merged_subject.subject_id.count).to_f.round(2) 
                  end
              end 
               @merg_total = (ca.sum + @main).to_f.round(2) 
              student_overall << @merg_total 
              key_grade(@subject_percentace) unless @subject_percentace.nil? 
              @grade 
               unless @link_term_groups.blank? 
                @link_term_groups.each do |link_term| 
               (h["#{link_term.link_name}"].to_f/merged_subject.subject_id.count).round(2) unless h["#{link_term.link_name}"].nil? 
              end 
               end 
               unless @other_weighted_groups.nil? 
                 @other_weighted_groups.each do |exam_group| 
                  (h["#{exam_group.name}"]/merged_subject.subject_id.count).round(2) unless h["#{exam_group.name}"]==0 
                end
              end 
               unless @unweighted_exam_groups.nil? 
               @unweighted_exam_groups.each do |exam_group| 
                   (h["#{exam_group.name}"]/merged_subject.subject_id.count).round(2) unless h["#{exam_group.name}"]==0  
                end 
              end
               unless @assigned_groups.empty? 
                @assigned_groups.each do |assigned_group| 
                  (h["a-#{assigned_group.id}"]).chomp.split(//).uniq.join("") unless h["a-#{assigned_group.id}"].nil? 
                 end 
               end 
                              
         end 
      end 
     end
     @overall_student = ((student_overall.sum)/(@subjects.count + merged_subjects.count)).to_f.round(2)
    end      
  end

  def checkpoint_report
    @alpha_exam=[]
    @alpha_numeric_batch=[]
    @subjects=[]
    @exams=[]
    @student = Student.find($student_id)
    @batch=@student.batch
    @tutor = TermSignature.find_by_is_principal_and_batch_id_and_school_id(false,@batch.id,MultiSchool.current_school.id)
    @principal = TermSignature.find_by_is_principal_and_school_id(true,MultiSchool.current_school.id)
    @students = Student.find($student_id).to_a
    @batch = @students.first.batch
    @term = Term.find(@term_ids.first)
    @exam_groups=@term.exam_groups
    @exam_groups.each do |exam_group|
      @alpha_numeric_batch<<AlphaNumericBatch.find_by_exam_group_id(exam_group.id,:conditions=> {:batch_id=>@batch.id, :term_id => @term.id})
    end
    @alpha_numeric_batch.each do |alpha_batch|
      @exams << alpha_batch.exam_group unless alpha_batch.nil?
      @alpha_exam<<AlphaNumericExam.find_all_by_alpha_numeric_batch_id(alpha_batch.id)
    end
    @subject_ids=@alpha_exam.flatten.collect(&:subject_id)
    @subjects<<Subject.find(@subject_ids) unless @subject_ids.empty?
  end

  def marks(alpha_batch_id,batch_id,student_id,subject_id)
    @alpha_exam = AlphaNumericExam.find_by_subject_id(subject_id,:conditions=>{:alpha_numeric_batch_id=>alpha_batch_id})
    @score=AlphaNumericScore.find_by_alpha_numeric_exam_id(@alpha_exam.id, :conditions => {:batch_id => batch_id, :student_id => student_id, :subject_id => subject_id})
    unless @score.nil?
      unless @score.is_absent
        return @score.marks_grade unless @score.nil?        
      else        
        return "AB"
      end
    else
      return "NA" 
    end
  end

end



