authorization do

  role :term_manager do
    has_permission_on [:terms],
      :to => [
      :index,
      :home,
      :update_term,
      :term_exams,
      :assign_exam,
      :new_term,
      :settings,
      :create_exam,
      :update_batch,
      :term_exam_group,
      :new_exam_group,
      :update_exam_form,
      :create_exam_group,
      :batch_term,
      :generated_report4,
      :combined_grouped_exam_report_pdf,
      :grouped_exam_report,
      :final_report_type,
      :upload_signature,
      :tutor_signature,
      :principal_signature,
      :tutor_comments,
      :save_comments,
      :tutor_batch_students,
      :add_grading_scale,
      :list_terms,
      :edit,
      :update,
      :destroy,
      :existing_terms,
      :assign_exam_to_subject,
      :term_subjects,
      :exam_subject,
      :subject_students,
      :save_subject_score,
      :add_exam_to_term,
      :delete_subject_exam_groups,
      :director_signature,
      :upload_director_signature,
      :infant_data,
      :add_evaluation_group,
      :add_evaluation_type,
      :add_evaluation_area,
      :term_evaluation_group,
      :term_evaluation_type,
      :get_evaluation_type,
      :get_evaluation_area,
      :evaluation_score,
      :save_score,
      :assign_template,
      :get_terms,
      :reports,
      :assigned_batches,
      :delete_assigned_batch,
      :evaluation_comment,
      :manage_evaluation,
      :manage_evaluation_groups,
      :manage_evaluation_type,
      :manage_evaluation_area,
      :update_evaluation_term,
      :update_evaluation_group,
      :edit_groups,
      :update_groups,
      :group_destroy,
      :list_eval_types,
      :edit_types,
      :update_type,
      :type_destroy,
      :junior_batch_ratings,
      :rating_batch_students,
      :term_exam_groups,
      :save_ratings_comments,
      :infant_students,
      :save_infant_comments,
      :list_eval_areas,
      :edit_areas,
      :update_areas,
      :area_destroy,
      :subject_destroy,
      :subject_elective_destroy,
      :update_term_subjects,
      :evaluation_performance,
      :term_evaluation,
      :view,
      :update_batch_subjects,
      :batch_terms_comments,
      :update_employees
    ]

    has_permission_on [:term_link_groups],
      :to => [
      :index
      ]
  end

   role :view_student_report do
     has_permission_on [:terms],
      :to => [
      :home,
      :reports
    ]
  end
  
  role :rt_report_builder_manager do
    has_permission_on [:report_builders],
      :to => [
      :index,
      :edit,
      :update,
      :destroy,
      :report_pdf,
      :header,
      :footer,
      :generate_template,
      :create_template,
      :generate_report,
      :student_wise_report,
      :batch_wise_report,
      :view_template,
      :student_wise_pdf_report,
      :batch_wise_pdf_report,
      :batch_students,
      :image_list,
      :delete_img,
      :upload_image,
      :load_sub_menu_values,
      :page_orientation,
      :reload_server,
      :home_work_ratings,
      :elective_group_comment,
      :batch_elective_groups,
      :house_parent_comment,
      :boarding_students,
      :view_tutor_comments,
      :view_house_parent_comments,
      :view_comments,
      :update_batch,
      :tutor_comment_list,
      :house_parent_comment_list,
      :update_batches,
      :student_wise_display_report,
      :student_display,
      :report_and_term,
      :term_boarding_students,
      :batch_wise_report_terms,
      :batch_wise_report_batches
    ]
  end


  role :rt_remainder_notifications do
    has_permission_on [:remainder_notifications],
      :to => [
      :set_notification,
      :set_assignment_notification,
      :set_task_notification,
      :assignment_notification_settings,
      :task_notification_settings,
      :create_task,
      :update_task,
      :create_assignment,
      :update_assignment,
      :load_data,
      :new,
      :to_schools,
      :select_student_course,
      :select_employee_department,
      :to_employees,
      :to_students,
      :update_recipient_list,
      :download_attachment,
      :new_assignment,
      :subjects_students_list
    ]
  end

  role :rt_report_builder_assistant do
    has_permission_on [:report_builders],
      :to => [:index]
  end

 role :rank_subject_management do
    has_permission_on [:rank_student_subject_electives],
      :to => [
        :index,
        :search_ajax,
        :rank_elective_subjects,
        :elective_disable
    ]
    has_permission_on [:rank_ingres_homes],
      :to => [
        :index,
        :broadsheets,
        :broadsheet_csv_generation,
        :batch_term,
        :term_exam_groups
        ]
    has_permission_on [:rank_ingres_subject_mergers],
      :to => [
      :index,
      :new,
      :edit,
      :update,
      :destroy,
      :batch_subjects      ]
    has_permission_on [:rank_ingres_teacher_students],
      :to => [
      :assign_students_teacher,
      :update_subjects,
      :update_subject_employees,
      :student_list
      ]
      has_permission_on [:rank_ingres_sms_modules],
      :to => [
      :assign_students,
      :student_list,
      :send_sms
      ]
      has_permission_on [:rank_ingres_teacher_screens],
      :to => [
      :index,
      :update_batch,
      :batch_term,
      :term_subjects,
      :add_scores_to_teacher_screen,
      :save_scores,
      :tutor_reports,
      :terms_update
      ]
      has_permission_on [:rank_ingres_deadlines],
      :to => [
      :index,
      :update_terms,
      :update_subjects,
      :update_subject_employees,
      :show_date,
      :add_deadline,
      :teacher_progress,
      :graph_progress
      ]
 end

  role :rank_alpha_numeric_management do
    has_permission_on [:alpha_numeric_batches],
    :to => [
    :index,
    :create_alpha_numeric_batch,
    :list_batches,
    :alpha_numeric_exams,
    :manage_alpha_numeric_exam,
    :alpha_numeric_exam_details,
    :exam_mark,
    :list_terms,
    :list_exam_groups,
    :list_alpha_terms,
    :alpha_numeric_exam_save,
    :alpha_numeric_exam_scores,
    :edit_alpha_numeric_exam
     ]    
  end
  role :rank_explanatory_notes_management do
    has_permission_on [:explanatory_notes],
    :to => [
    :new_explanatory_notes,
    :list_terms
     ]    
  end

  role :admin do
    includes :term_manager
    includes :rt_report_builder_manager
    includes :rt_report_builder_assistant
    includes :rt_remainder_notifications
    includes :view_student_report
    includes :rank_subject_management
    includes :rank_alpha_numeric_management
    includes :rank_explanatory_notes_management
  end


  role :employee do
    includes :term_manager
    includes :rt_report_builder_manager
    includes :rt_report_builder_assistant
    includes :rt_remainder_notifications
    includes :view_student_report
    includes :rank_subject_management
    includes :rank_alpha_numeric_management
    includes :rank_explanatory_notes_management
  end

  role :student do
    has_permission_on [:terms],
      :to => [
      :home,
      :reports
    ]
      has_permission_on [:report_builders],
      :to => [
      :student_wise_report,
      :student_wise_pdf_report,
      :batch_students,
      :update_batches
      ]
  end

  role :parent do
    has_permission_on [:terms],
      :to => [
      :home,
      :reports
    ]

    
    has_permission_on [:report_builders],
      :to => [
      :student_wise_report,
      :student_wise_pdf_report,
      :batch_students,
      :update_batches
      ]
  end

end