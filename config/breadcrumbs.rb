Gretel::Crumbs.layout do

	crumb :rank_ingres_homes_index do
	    link I18n.t('ingres_academics'), {:controller=>"rank_ingres_homes", :action=>"index"}
	end
	crumb :rank_ingres_homes_broadsheets do
	    link I18n.t('generte_csv_files'), {:controller=>"rank_ingres_homes", :action=>"broadsheets"}
		parent :report_builders_report_and_term
	end
  ########################################
  #rank_ingres_subject_mergers
  ########################################
	crumb :rank_ingres_subject_mergers_index do
	    link I18n.t('subject_merger'), {:controller=>"rank_ingres_subject_mergers", :action=>"index"}
		parent :rank_ingres_homes_index
	end
	crumb :rank_ingres_subject_mergers_new do
	    link I18n.t('create_new_subject_merger'), {:controller=>"rank_ingres_subject_mergers", :action=>"new"}
		parent :rank_ingres_subject_mergers_index
	end
	crumb :rank_ingres_subject_mergers_edit do |@subject_merger|
	    link I18n.t('update_subject_merger'), {:controller=>"rank_ingres_subject_mergers", :action=>"update", :id =>@subject_merger.id }
		parent :rank_ingres_subject_mergers_index
	end
  ########################################
  #rank_student_subject_electives
  ########################################
	crumb :rank_student_subject_electives_index do
	    link I18n.t('elective_enable_disable'), {:controller=>"rank_student_subject_electives", :action=>"index"}
		parent :rank_ingres_homes_index
	end
	crumb :rank_student_subject_electives_rank_elective_subjects do |@student|
	    link I18n.t('elective_enable_disable'), {:controller=>"rank_student_subject_electives", :action=>"rank_elective_subjects", :id =>@student.id}
		parent :rank_student_subject_electives_index
	end
  ########################################
  #rank_ingres_sms_modules
  ########################################
  crumb :rank_ingres_sms_modules_send_sms do
	    link I18n.t('tsend_sms_to_parents'), {:controller=>"rank_ingres_sms_modules", :action=>"send_sms"}
		parent :rank_ingres_homes_index
  end
  crumb :rank_ingres_sms_modules_assign_students do
	    link I18n.t('tsend_sms_to_parents'), {:controller=>"rank_ingres_sms_modules", :action=>"assign_students"}
		parent :rank_ingres_sms_modules_send_sms
  end
   ########################################
  #rank_ingres_teacher_students
  ########################################
  crumb :rank_ingres_teacher_students_assign_students_teacher do
	    link I18n.t('subject_teacher_groups'), {:controller=>"rank_ingres_teacher_students", :action=>"assign_students_teacher"}
		parent :rank_ingres_homes_index
  end
  ########################################
  #rank_ingres_teacher_screens
  ########################################
  crumb :rank_ingres_teacher_screens_index do
	    link I18n.t('adding_exam_scores'), {:controller=>"rank_ingres_teacher_screens", :action=>"index"}
		parent :rank_ingres_homes_index
  end
  crumb :rank_ingres_teacher_screens_term_subjects do |@batch|
	    link I18n.t('select_subject_merger'), {:controller=>"rank_ingres_teacher_screens", :action=>"term_subjects", :id =>@batch.id}
		parent :rank_ingres_teacher_screens_index
  end
  crumb :rank_ingres_teacher_screens_add_scores_to_teacher_screen do |@batch|
	    link I18n.t('teacher_screen'), {:controller=>"rank_ingres_teacher_screens", :action=>"add_scores_to_teacher_screen", :id =>@batch.id}
		parent :rank_ingres_teacher_screens_index
  end
  crumb :rank_ingres_teacher_screens_tutor_reports do 
	    link I18n.t('tutor_reports'), {:controller=>"rank_ingres_teacher_screens", :action=>"tutor_reports"}
		parent :rank_ingres_teacher_screens_index
  end
  ########################################
  #rank_ingres_deadlines
  ########################################
  crumb :rank_ingres_deadlines_index do
	    link I18n.t('input_scores'), {:controller=>"rank_ingres_deadlines", :action=>"index"}
		parent :rank_ingres_homes_index
  end
  crumb :rank_ingres_deadlines_teacher_progress do
	    link I18n.t('tutor_reports'), {:controller=>"rank_ingres_deadlines", :action=>"teacher_progress"}
		parent :rank_ingres_homes_index
  end
  ########################################
  #report_builders
  ########################################
  crumb :report_builders_report_and_term do
	    link I18n.t('reports_terms'), {:controller=>"report_builders", :action=>"report_and_term"}
  end 
  crumb :report_builders_index do
	    link I18n.t('report_builder'), {:controller=>"report_builders", :action=>"index"}
	    parent :report_builders_report_and_term
  end
  crumb :report_builders_generate_template do
	    link I18n.t('create_template'), {:controller=>"report_builders", :action=>"generate_template"}
	    parent :report_builders_index
  end
  crumb :report_builders_generate_report do
	    link I18n.t('generate_report_for_student'), {:controller=>"report_builders", :action=>"generate_report"}
	    parent :report_builders_index
  end
  crumb :report_builders_student_wise_report do
	    link I18n.t('student_wise_report'), {:controller=>"report_builders", :action=>"student_wise_report"}
	    parent :report_builders_generate_report
  end 
  crumb :report_builders_student_wise_display_report do
	    link I18n.t('disp_report'), {:controller=>"report_builders", :action=>"student_wise_display_report"}
	    parent :report_builders_student_wise_report
  end 
  crumb :report_builders_batch_wise_report do
	    link I18n.t('view_batchwise_report'), {:controller=>"report_builders", :action=>"batch_wise_report"}
	    parent :report_builders_generate_report
  end 
  crumb :report_builders_view_template do
	    link I18n.t('template'), {:controller=>"report_builders", :action=>"view_template"}
	    parent :report_builders_index
  end
  crumb :report_builders_edit do |@template|
	    link I18n.t('edit'), {:controller=>"report_builders", :action=>"edit", id=> @template.id}
	    parent :report_builders_view_template
  end 
  crumb :report_builders_home_work_ratings do
	    link I18n.t('home_work_ratings'), {:controller=>"report_builders", :action=>"home_work_ratings"}
	    parent :report_builders_index
  end
  crumb :report_builders_elective_group_comment do
	    link I18n.t('add_elective_group_desc'), {:controller=>"report_builders", :action=>"elective_group_comment"}
	    parent :report_builders_index
  end
  crumb :report_builders_house_parent_comment do
	    link I18n.t('house_parent_comment_desc'), {:controller=>"report_builders", :action=>"house_parent_comment"}
	    parent :report_builders_index
  end
  crumb :term_documents_index do
	    link I18n.t('create_excel'), {:controller=>"term_documents", :action=>"index"}
	    parent :report_builders_index
  end
  crumb :report_builders_house_parent_comment do
	    link I18n.t('house_parent_comment'), {:controller=>"report_builders", :action=>"house_parent_comment"}
	    parent :report_builders_index
  end
  crumb :report_builders_view_comments do
	    link I18n.t('view_comments'), {:controller=>"report_builders", :action=>"view_comments"}
	    parent :report_builders_index
  end 
  crumb :report_builders_view_tutor_comments do
	    link I18n.t('tutor_comments'), {:controller=>"report_builders", :action=>"view_tutor_comments"}
	    parent :report_builders_view_comments
  end
  crumb :report_builders_view_house_parent_comments do
	    link I18n.t('view_house_parent_comments'), {:controller=>"report_builders", :action=>"view_house_parent_comments"}
	    parent :report_builders_view_comments
  end
  ########################################
  #terms
  ########################################
  crumb :terms_index do
	    link I18n.t('assign_exams_expln'), {:controller=>"terms", :action=>"index"}
	    parent :terms_home
  end
  crumb :terms_home do
	    link I18n.t('term_exam_expln'), {:controller=>"terms", :action=>"home"}
	    parent :report_builders_report_and_term
  end
  crumb :terms_new_term do
	    link I18n.t('create_new_term'), {:controller=>"terms", :action=>"new_term"}
	    parent :terms_home
  end
  crumb :terms_term_exams do |@batch|
	    link I18n.t('update_exam_score'), {:controller=>"terms", :action=>"term_exams", :id =>@batch.id}
	    parent :terms_index
  end 
  crumb :terms_grouped_exam_report do 
	    link I18n.t('select_batch'), {:controller=>"terms", :action=>"grouped_exam_report"}
	    parent :terms_home
  end
    crumb :terms_tutor_comments do
	    link I18n.t('create_template'), {:controller=>"terms", :action=>"tutor_comments"}
	    parent :report_builders_index
  end
  crumb :terms_generated_report4 do 
	    link I18n.t('generate_report'), {:controller=>"terms", :action=>"generated_report4"}
	    parent :terms_grouped_exam_report
  end
  crumb :terms_list_terms do 
	    link I18n.t('terms_list'), {:controller=>"terms", :action=>"list_terms"}
	    parent :terms_home
  end
  crumb :terms_edit do |@term|
	    link I18n.t('update_term_details'), {:controller=>"terms", :action=>"edit", :id =>@term.id}
	    parent :terms_list_terms
  end 
  crumb :terms_add_exam_to_term do 
	    link I18n.t('add_exam_to_term_desc'), {:controller=>"terms", :action=>"add_exam_to_term"}
	    parent :terms_home
  end
  crumb :terms_assign_exam_to_subject do 
	    link I18n.t('assign_exam_to_subject'), {:controller=>"terms", :action=>"assign_exam_to_subject"}
	    parent :terms_home
  end
  crumb :terms_infant_data do 
	    link I18n.t('infant_data'), {:controller=>"terms", :action=>"infant_data"}
	    parent :terms_home
  end 
  crumb :terms_add_evaluation_group do 
	    link I18n.t('create_evaluation_groups_desc'), {:controller=>"terms", :action=>"add_evaluation_group"}
	    parent :terms_infant_data
  end 
  crumb :terms_add_evaluation_type do 
	    link I18n.t('add_evaluation_type'), {:controller=>"terms", :action=>"terms_add_evaluation_type"}
	    parent :terms_infant_data
  end
  crumb :terms_add_evaluation_area do 
	    link I18n.t('add_evaluation_area'), {:controller=>"terms", :action=>"add_evaluation_area"}
	    parent :terms_infant_data
  end 
  crumb :terms_evaluation_comment do 
	    link I18n.t('evaluation_comment'), {:controller=>"terms", :action=>"evaluation_comment"}
	    parent :terms_infant_data
  end
  crumb :terms_manage_evaluation do 
	    link I18n.t('manage_group_type_area'), {:controller=>"terms", :action=>"manage_evaluation"}
	    parent :terms_infant_data
  end
  crumb :terms_manage_evaluation_groups do 
	    link I18n.t('manage_evaluation_groups'), {:controller=>"terms", :action=>"manage_evaluation_groups"}
	    parent :terms_manage_evaluation
  end
  crumb :terms_manage_evaluation_type do 
	    link I18n.t('manage_evaluation_type'), {:controller=>"terms", :action=>"manage_evaluation_type"}
	    parent :terms_manage_evaluation
  end
  crumb :terms_manage_evaluation_area do 
	    link I18n.t('manage_evaluation_area'), {:controller=>"terms", :action=>"manage_evaluation_area"}
	    parent :terms_manage_evaluation
  end 
  crumb :terms_evaluation_performance do 
	    link I18n.t('add_scores_to_evaluations'), {:controller=>"terms", :action=>"evaluation_performance"}
	    parent :terms_infant_data
  end 
  crumb :terms_term_evaluation do |@term|
	    link I18n.t('select_exam_group'), {:controller=>"terms", :action=>"term_evaluation", :id =>@term.id}
	    parent :terms_evaluation_performance
  end
  crumb :terms_get_evaluation_type do |@evaluation_group|
	    link I18n.t('update_scores'), {:controller=>"terms", :action=>"get_evaluation_type", :id =>@evaluation_group.id}
	    parent :terms_evaluation_performance
  end 
  crumb :terms_subject_destroy do 
	    link I18n.t('delete_subject'), {:controller=>"terms", :action=>"subject_destroy"}
	    parent :terms_home
  end
  crumb :terms_assign_template do
	    link I18n.t('assign_template'), {:controller=>"terms", :action=>"assign_template"}
	    parent :report_builders_index
  end 
  crumb :terms_upload_signature do
	    link I18n.t('upload_signature'), {:controller=>"terms", :action=>"upload_signature"}
	    parent :report_builders_index
  end 
  crumb :terms_tutor_signature do
	    link I18n.t('tutor_signature_desc'), {:controller=>"terms", :action=>"tutor_signature"}
	    parent :terms_upload_signature
  end
  crumb :terms_principal_signature do
	    link I18n.t('principal_signature_desc'), {:controller=>"terms", :action=>"principal_signature"}
	    parent :terms_upload_signature
  end
  crumb :terms_director_signature do
	    link I18n.t('director_signature_desc'), {:controller=>"terms", :action=>"director_signature"}
	    parent :terms_upload_signature
  end
  crumb :terms_junior_batch_ratings do
	    link I18n.t('junior_rating_comment'), {:controller=>"terms", :action=>"junior_batch_ratings"}
	    parent :report_builders_index
  end 
  crumb :terms_create_exam do
	    link I18n.t('exam_managemant'), {:controller=>"terms", :action=>"create_exam"}
	    parent :exam_index
  end 
  crumb :terms_term_exam_group do |@term|
	    link I18n.t('update_exam_score_and_publish_exam'), {:controller=>"terms", :action=>"term_exam_group", :id=>@term.id}
	    parent :terms_create_exam
  end

  crumb :term_link_groups_index  do
	    link I18n.t('term_links'), {:controller=>"term_link_groups", :action=>"index"}
	    parent :terms_home
  end
  crumb :term_link_groups_edit  do |@term|
	    link I18n.t('edit'), {:controller=>"term_link_groups", :action=>"edit",:id=>@term.id}
	    parent :term_link_groups_index
  end

  ########################################
  #remainder_notifications
  ########################################
  crumb :remainder_notifications_set_notification do
	    link I18n.t('notification_settings'), {:controller=>"remainder_notifications", :action=>"set_notification"}
	    parent :report_builders_report_and_term
  end
  crumb :remainder_notifications_set_assignment_notification do
	    link I18n.t('assignment_desc'), {:controller=>"remainder_notifications", :action=>"set_assignment_notification"}
	    parent :remainder_notifications_set_notification
  end
  crumb :remainder_notifications_set_task_notification do
	    link I18n.t('task_desc'), {:controller=>"remainder_notifications", :action=>"set_task_notification"}
	    parent :remainder_notifications_set_notification
  end
  
  ###########################
  ##Report pickup date
  ###########################
  crumb :report_pickup_dates_create_report_pickup_date do
    link I18n.t('report_pickup_date'), {:controller=>"report_pickup_dates", :action=>"create_report_pickup_date"}
    parent :report_builders_index
  end

  ###############################
  ##Alpha Numeric batch
  ###############################
  crumb :alpha_numeric_batches_index do
    link I18n.t('alpha_numeric_batch'), {:controller=>"alpha_numeric_batches", :action=>"index"}
    parent :report_builders_index
  end

  crumb :alpha_numeric_batches_create_alpha_numeric_batch do
    link I18n.t('create_alpha_numeric_batch'), {:controller=>"alpha_numeric_batches", :action=>"create_alpha_numeric_batch"}
    parent :alpha_numeric_batches_index
  end

  crumb :alpha_numeric_batches_list_batches do
    link I18n.t('alpha_numeric_score'), {:controller=>"alpha_numeric_batches", :action=>"list_batches"}
    parent :alpha_numeric_batches_index
  end

  crumb :alpha_numeric_batches_alpha_numeric_exams do |term|
    link "#{term.name}", {:controller=>"alpha_numeric_batches", :action=>"alpha_numeric_exams", :term_id => term.id}
    parent :alpha_numeric_batches_index
  end

  crumb :alpha_numeric_batches_manage_alpha_numeric_exam do |term,exam_group|
    link "#{exam_group.name}", {:controller=>"alpha_numeric_batches", :action=>"manage_alpha_numeric_exam", :term_id => term.id, :exam_group_id => exam_group.id, :batch_id=>term.batch_id}
    parent :alpha_numeric_batches_alpha_numeric_exams,term
  end

  crumb :alpha_numeric_batches_alpha_numeric_exam_details do |term|
    link I18n.t('add_alpha_numeric_exam'), {:controller=>"alpha_numeric_batches", :action=>"alpha_numeric_exam_details",:term_id=>term.id}
    parent :alpha_numeric_batches_alpha_numeric_exams,term
  end

  crumb :alpha_numeric_batches_exam_mark do |alpha_exam|
    link "#{alpha_exam.subject.name}", {:controller => "alpha_numeric_batches", :action => "exam_mark", :id=>alpha_exam.id, :subject_id=>alpha_exam.subject_id}
    parent :alpha_numeric_batches_manage_alpha_numeric_exam, [alpha_exam.alpha_numeric_batch.term, alpha_exam.alpha_numeric_batch.exam_group]
  end

  crumb :alpha_numeric_batches_edit_alpha_numeric_exam do |alpha_exam|
    link I18n.t('edit'), {:controller=>"alpha_numeric_batches", :action=>"edit_alpha_numeric_exam", :id=>alpha_exam.id}
    parent :alpha_numeric_batches_exam_mark,alpha_exam
  end

  #############################
  ###########Explanatory notes
  ###############################
  crumb :explanatory_notes_new_explanatory_notes do
    link I18n.t('explanatory_notes'), {:controller=>"explanatory_notes", :action=>"new_explanatory_notes"}
    parent :report_builders_index
  end
end