ActionController::Routing::Routes.draw do |map|
  map.terms_home 'terms/home',:controller => 'terms',:action => 'home'
  map.terms_assign_exam 'terms/assign_exam', :controller => 'terms',:action => 'assign_exam'
  map.feed 'report_builders/batch_wise_pdf_report.pdf', :controller => 'report_builders', :action => 'batch_wise_pdf_report'
  map.feed 'report_builders/student_wise_pdf_report.pdf', :controller => 'report_builders', :action => 'student_wise_pdf_report'
  map.feed 'terms/combined_grouped_exam_report_pdf.pdf', :controller => 'terms', :action => 'combined_grouped_exam_report_pdf'
  map.feed 'report_builders/student_wise_display_report', :controller => 'report_builders', :action => 'student_wise_display_report'
  map.feed 'report_builders/report/generate_template', :controller => 'report_builders', :action => 'generate_template'
  map.feed 'report_builders/:id/edit', :controller => 'report_builders', :action => 'edit'
  map.feed 'report_builders/report/create_template', :controller => 'report_builders', :action => 'create_template'
  map.feed 'report_builders/report/update', :controller => 'report_builders', :action => 'update'
  map.feed 'report_builders/report/image_list', :controller => 'report_builders', :action => 'image_list'
  map.feed 'report_builders/:id/image_list', :controller => 'report_builders', :action => 'image_list'
  map.feed 'report_builders/upload_image', :controller => 'report_builders', :action => 'upload_image'
  map.create_terms_exam_group 'terms/create_exam_group', :controller => "terms", :action => "create_exam_group"
  #map.resources :term_documents,:member=>{:save_document =>:post}
  
end
