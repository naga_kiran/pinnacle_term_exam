require 'dispatcher'
require 'overides/pinnacle_config_controller'
require 'overides/rank_ingres_student'
require 'overides/rank_exam'


module PinnacleTermExam

  def self.attach_overrides
    Dispatcher.to_prepare  do
       ::ConfigurationController.instance_eval { include PinnacleConfigController }
       ::ExamGroupsController.instance_eval {include PinnacleExamGroups}
       ::Exam.instance_eval { include RankExam }
       ::StudentController.instance_eval { include RankIngresStudent}
       ::Student.instance_eval { include RankStudentModel }
       ::ExamsController.instance_eval {include RankIngresExams}
       #::ExamController.instance_eval {include RankIngresExamPrevilege}
    end
  end

  # module TermwiseCategory
  #   def self.included(base)
  #     base.class_eval do
  #       before_filter :add_term_wise_exam, :only => [:settings]
  #     end

  #     def add_term_wise_exam
  #       if request.post?
  #         redirect_to :action => "settings", :controller => "terms",:configuration => params[:configuration],
  #           :school_detail=> {:school_logo => params[:school_detail][:school_logo]},
  #           :language => session[:language]
  #       else
  #         redirect_to :action => "settings", :controller => "terms"
  #       end
  #     end

  #   end
  # end




  module TermwiseExam
    def self.included(base)
      base.class_eval do
        before_filter :term_exam, :only => [:create_exam]
      end
      
      def term_exam
        @config_key = Configuration.find_by_config_key('TermwiseExamManagement')
        Rails.logger.info "AAAAAAAAAAAAAA#{@config_key.inspect}"
       if @config_key.config_value == "1"
         redirect_to :action => "create_exam", :controller => "terms"
       end
      end
    end
  end

  module TermwiseExamNew
    def self.included(base)
      base.class_eval do
        before_filter :term_exam, :only => [:index]
      end

      def term_exam
        @config_key = Configuration.find_by_config_key('TermwiseExamManagement')
        Rails.logger.info "AAAAAAAAAAAAAA#{@config_key.inspect}"
       if @config_key.config_value == "1"
         redirect_to :action => "create_exam", :controller => "terms"
       end
      end
    end
  end
 

  module TermexamGroup
    def self.included(base)
      base.class_eval do
        before_filter :term_exam_group, :only => [:new]
      end
      def term_exam_group
        redirect_to :action => "new_exam_group", :controller => "terms",:batch_id => params[:batch_id],:term_id => params[:term_id]
      end
    end
  end

  module TermexamCreate
    def self.included(base)
      base.class_eval do
        before_filter :create_term_exam_group, :only => [:create]
      end
      def create_term_exam_group
        redirect_to :controller => "terms", :action => "create_exam_group", :exam_group => params[:exam_group], :batch_id => params[:batch_id],:term_id => params[:term_id]
      end
    end
  end

  module TaskNotification
    def self.included(base)
      base.class_eval do
        before_filter :task_notification_for_create, :only => [:create]
        before_filter :task_notification_for_update, :only => [:update]
      end

      def task_notification_for_create
        redirect_to :action => "create_task", :controller => "remainder_notifications",:recipients => params[:recipients], :task => params[:task]
      end

      def task_notification_for_update
        redirect_to :action => "update_task", :controller => "remainder_notifications",:recipients => params[:recipients], :task => params[:task], :id => params[:id]
      end
    end
  end

  module AssignmentNotification
    def self.included(base)
      base.class_eval do
        before_filter :assignment_notification_for_create, :only => [:create]
        before_filter :assignment_notification_for_update, :only => [:update]
      end

      def assignment_notification_for_create
        redirect_to :action => "create_assignment", :controller => "remainder_notifications",:assignment => params[:assignment]
      end

      def assignment_notification_for_update
        redirect_to :action => "update_assignment", :controller => "remainder_notifications",:assignment => params[:assignment], :id => params[:id]
      end
    end
  end

  module CustomReport
    def self.included(base)
      base.class_eval do
        before_filter :custom_reports, :only => [:reports]
      end

      def custom_reports
        redirect_to :action => "reports", :controller => "terms", :id => params[:id]
      end
    end
  end

end

