module PinnacleTermExam
  module PinnacleConfigController


 def self.included (base)
      base.instance_eval do
         alias_method_chain :settings, :tmpl
      end
    end


 def settings_with_tmpl
    @config = Configuration.get_multiple_configs_as_hash ['InstitutionName', 'InstitutionAddress', 'InstitutionPhoneNo', \
        'StudentAttendanceType', 'CurrencyType', 'ExamResultType', 'AdmissionNumberAutoIncrement','EmployeeNumberAutoIncrement', \
        'Locale','FinancialYearStartDate','FinancialYearEndDate','EnableNewsCommentModeration','DefaultCountry',\
        'TimeZone','TermwiseExamManagement','FirstTimeLoginEnable','FeeReceiptNo','EnableSibling','PrecisionCount','DateFormat','DateFormatSeparator','StartDayOfWeek','InstitutionType', 'EnableRollNumber']
    @grading_types = Course::GRADINGTYPES
    @enabled_grading_types = Configuration.get_grading_types
    @time_zones = TimeZone.all
    @school_detail = SchoolDetail.first || SchoolDetail.new
    @countries=Country.all
    if request.post?
      Configuration.set_config_values(params[:configuration])
      session[:language] = nil unless session[:language].nil?
      @school_detail.logo = params[:school_detail][:logo] if params[:school_detail].present? and params[:school_detail][:logo].present?
      unless @school_detail.save
        @config = Configuration.get_multiple_configs_as_hash ['InstitutionName', 'InstitutionAddress', 'InstitutionPhoneNo', \
            'StudentAttendanceType', 'CurrencyType', 'ExamResultType', 'AdmissionNumberAutoIncrement','EmployeeNumberAutoIncrement', \
            'Locale','FinancialYearStartDate','FinancialYearEndDate','EnableNewsCommentModeration','DefaultCountry','TimeZone','FirstTimeLoginEnable','EnableSibling',\
            'PrecisionCount','DateFormat','DateFormatSeparator','StartDayOfWeek','InstitutionType', 'EnableRollNumber']
        return
      end
      @current_user.clear_menu_cache
      @current_user.clear_school_name_cache(request.host_with_port)
      Configuration.clear_school_cache(@current_user)
      News.new.reload_news_bar
       load "#{Rails.root}/vendor/plugins/pinnacle_term_exam/init.rb"
      flash[:notice] = "#{t('flash_msg8')}"
      redirect_to :action => "settings"  and return
    end
    render :template => "configuration/settings_with_tmpl"
  end

end

module PinnacleExamGroups

  def self.included (base)
      base.instance_eval do
         alias_method_chain :index, :tmpl
      end
    end

  def index_with_tmpl



    index_without_tmpl
    render :template => "term_exam_groups/index_with_tmpl"
  end
end
end
