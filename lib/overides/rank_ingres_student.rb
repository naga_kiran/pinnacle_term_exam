module PinnacleTermExam
  module RankIngresStudent
    def self.included (base)
      base.instance_eval do
        alias_method_chain :my_subjects, :tmpl
        alias_method_chain :admission1, :tmpl
        alias_method_chain :edit, :tmpl
      end
    end

    def admission1_with_tmpl   
    @student = Student.new(params[:student])
    @selected_value = Configuration.default_country
    @application_sms_enabled = SmsSetting.find_by_settings_key("ApplicationEnabled")
    @last_admitted_student = User.last(:select=>"username",:conditions=>["student=?",true])
    @next_admission_no=User.next_admission_no("student")
    @config = Configuration.find_by_config_key('AdmissionNumberAutoIncrement')
    @categories = StudentCategory.active
    @batches = []
    if request.post?
      if params[:course].first.present? && params[:student][:batch_id].present?
        @roll_number_prefix = Batch.find(params[:student][:batch_id]).get_roll_number_prefix
        @selected_batch = params[:student][:batch_id]
        @selected_course = params[:course].first
        @batches = Course.find(@selected_course.to_i).batches.active
      end
      if @config.config_value.to_i == 1
        @exist = Student.first(:conditions => ["admission_no LIKE BINARY(?)",params[:student][:admission_no]])
        if @exist.nil?
          @status = @student.save
        else
          @status = @student.save
        end
      else
        @status = @student.save
      end
    
      if @status
        if @student.batch.course.enable_student_elective_selection
          unless @student.batch.elective_groups.active.empty? #&& !@student.batch.subjects.select{|es| es.elective_group_id != nil}.blank?
            @student.batch.elective_groups.active.each do |eg|
              if !eg.end_date.nil? && !eg.subjects.active.empty? && eg.end_date >= Date.today
                end_date = eg.end_date
                recipients_array = [@student.user.id]
                sender = current_user.id
                Reminder.send_message(recipients_array,sender,end_date,eg.name)
              end
            end
          end
        end
      end
      if @status
        #        sms_setting = SmsSetting.new()
        #        if sms_setting.application_sms_active and @student.is_sms_enabled
        #          recipients = []
        #          message = "#{t('student_admission_done_for')} #{@student.first_and_last_name}. #{t('username_is')} #{@student.admission_no}, #{t('guardian_password_is')} #{@student.admission_no}123. #{t('thanks')}"
        #          if sms_setting.student_admission_sms_active
        #            recipients.push @student.phone2 unless @student.phone2.blank?
        #          end
        #          unless recipients.empty?
        #            Delayed::Job.enqueue(SmsManager.new(message,recipients))
        #          end
        #        end
        if Configuration.find_by_config_key('EnableSibling').present? and Configuration.find_by_config_key('EnableSibling').config_value=="1"
          flash[:notice] = "#{t('flash22')}"
          redirect_to :controller => "student", :action => "admission1_2", :id => @student.id
        else
          flash[:notice] = "#{t('flash8')}"
          redirect_to :controller => "student", :action => "admission2", :id => @student.id
        end
      end
    # render :template => 'rank_ingres_student/admission1_with_tmpl'
  else
      render :template => "rank_ingres_student/admission1_with_tmpl"
  end
  end
  def edit_with_tmpl
    @student = Student.find(params[:id])
    @student.gender=@student.gender.downcase
    @student_user = @student.user
    @student_categories = StudentCategory.active
    @student_dependency= @student.student_dependencies_list.empty? ? true:false
    unless @student.student_category.present? and @student_categories.collect(&:name).include?(@student.student_category.name)
      current_student_category=@student.student_category
      @student_categories << current_student_category if current_student_category.present?
    end 
    @batches = Batch.active
    @student.biometric_id = BiometricInformation.find_by_user_id(@student.user_id).try(:biometric_id)
    @application_sms_enabled = SmsSetting.find_by_settings_key("ApplicationEnabled")
    if Configuration.enabled_roll_number?
      @roll_number_prefix = @student.batch.get_roll_number_prefix
      @student.roll_number.to_s.slice!(@roll_number_prefix.to_s)
      @roll_number_suffix = @student.roll_number
    end
    if request.put?
      unless params[:student][:image_file].blank?
        unless params[:student][:image_file].size.to_f > 280000
          if @student.update_attributes(params[:student])
            flash[:notice] = "#{t('flash3')}"
            redirect_to :controller => "student", :action => "profile", :id => @student.id
          end
        else
          flash[:notice] = "#{t('flash_msg11')}"
          redirect_to :controller => "student", :action => "edit", :id => @student.id
        end
      else
        if @student.update_attributes(params[:student])
          flash[:notice] = "#{t('flash3')}"
          redirect_to :controller => "student", :action => "profile", :id => @student.id
        end
      end
    else
      render :template => "rank_ingres_student/edit_with_tmpl"
    end
  end

    def my_subjects_with_tmpl
      @student = Student.find(params[:id])
      subjects = @student.batch.subjects.active
      elective_subjects = subjects.select {|s| s.elective_group_id != nil && s.is_deleted == false}
      @elective_groups = @student.batch.elective_groups.active.all(:joins => :subjects, :select => 'elective_groups.*, count(subjects.id) as count', :conditions =>'subjects.is_deleted is false',:group => 'elective_groups.id')
      @normal_subjects = subjects - elective_subjects
      @can_select_elective = @student.batch.course.enable_student_elective_selection
      student_sub = StudentsSubject.find(:all, :conditions => {:student_id => @student.id, :batch_id => @student.batch.id}, :include => [:subject => :elective_group])
      student_subjects = []
      student_sub.each do |ss|
        elective_disable =  RankStudentSubjectElective.find_by_students_subject_id(ss.id)
        if elective_disable.nil?
          student_subjects << ss
        end
      end
      @student_electives = {}
      student_subjects.each do |s|
        if @student_electives.has_key? s.subject.elective_group.name
          @student_electives[s.subject.elective_group.name] << s.subject.name
        else
          @student_electives[s.subject.elective_group.name] = [s.subject.name]
        end
      end
    end
  end

 module RankIngresExamPrevilege
      def self.included (base)
      base.instance_eval do
        alias_method_chain :index, :tmpl
      end
    end 
    def index_with_tmpl
       render :template => "exam/index_with_tmpl"
    end
    

  end

  module RankIngresExams
    def self.included (base)
      base.instance_eval do
        alias_method_chain :show, :tmpl
      end
    end
    def show_with_tmpl
      @employee_subjects=[]
      @employee_subjects= @current_user.employee_record.subjects.map { |n| n.id} if @current_user.employee?
      @exam = Exam.find params[:id], :include => :exam_group
      unless @employee_subjects.include?(@exam.subject_id) or @current_user.admin? or @current_user.privileges.map{|p| p.name}.include?('ExaminationControl') or @current_user.privileges.map{|p| p.name}.include?('EnterResults')
        flash[:notice] = "#{t('flash_msg6')}"
        redirect_to :controller=>"user", :action=>"dashboard"
      end
      exam_subject = Subject.find(@exam.subject_id)
      is_elective = exam_subject.elective_group_id
      if is_elective == nil
        employee = Employee.find_by_user_id(@current_user.id)
         rank_teachers = RankIngresTeacherStudent.find_by_batch_id_and_subject_id_and_employee_id(@batch.id,@exam.subject_id,employee.id)
        unless rank_teachers.nil?
          @students = []
         rank_teachers.student_ids.each do|ids|
           @students << Student.find_by_id(ids)
         end
        else
        @students = @batch.students.by_first_name
        end
      else
        assigned_stu = StudentsSubject.find_all_by_subject_id(exam_subject.id)
        assigned_students = []
        assigned_stu.each do |ss|
          elective_disable =  RankStudentSubjectElective.find_by_students_subject_id(ss.id)
          if @exam.score_for(ss.student_id).marks.nil?
            if elective_disable.nil?
              assigned_students << ss
            end
          else
            assigned_students << ss
          end
        end
        @students = []
        assigned_students.each do |s|
          student = @batch.students.find_by_id(s.student_id)
          @students.push [student.first_name, student.id, student] unless student.nil?
        end
        @ordered_students = @students.uniq.sort
        @students=[]
        @ordered_students.each do|s|
          @students.push s[2]
        end
      end
      @config = Configuration.get_config_value('ExamResultType') || 'Marks'

      @grades = @batch.grading_level_list
    end
  end

  module RankStudentModel

    def self.included (base)
      base.instance_eval do
    has_attached_file :upload_document,
    :url => "/system/:class/:attachment/:id/:style/:basename.:extension",
    :path => ":rails_root/public/system/:class/:attachment/:id/:style/:basename.:extension"
    validates_attachment_content_type :upload_document, :content_type => ['image/jpeg','image/jpg', 'image/png']   
    
    validates_attachment_size :upload_document, :less_than => 2097152  
      end
    end  
  end

end
