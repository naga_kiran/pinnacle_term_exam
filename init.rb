# Include hook code here
require 'active_record'
require 'translator'
require 'dispatcher'
require File.join(File.dirname(__FILE__), "lib", "pinnacle_term_exam")
require File.join(File.dirname(__FILE__), "config", "breadcrumbs")
require 'pdfkit'
# require 'zip/zip'
# require 'zip/zipfilesystem'
config = Rails::Configuration.new
config.middleware.use "PDFKit::Middleware",  :print_media_type => true
FedenaPlugin.register =
  {
  :name=>"pinnacle_term_exam",
  :description=>"Pinnacle Term Exam",
  :auth_file=>"config/pinnacle_term_exam_auth.rb",
  :more_menu=>{:title=>"Term Exam Plugin",:controller=>"terms",:action=>"index"},
  :generic_hook =>
     
    [{ :title=>"term_wise_management",
      :source=>{:controller=>"exam", :action=>"index" },
      :destination=>{:controller=>"terms",:action=>"home"},
      :description=>"term_exam_expln",

    },{ :title=>"report_builder",
      :source=>{:controller=>"exam", :action=>"index" },
      :destination=>{:controller=>"report_builders",:action=>"index"},
      :description=>"design_report_template",

    },{ :title=>"set_notification",
      :source=>{:controller=>"exam", :action=>"index" },
      :destination=>{:controller=>"remainder_notifications",:action=>"set_notification"},
      :description=>"notification_setting",

    }]

}

Dir[File.join("#{File.dirname(__FILE__)}/config/locales/*.yml")].each do |locale|
  I18n.load_path.unshift(locale)
end

if RAILS_ENV == 'development'
  ActiveSupport::Dependencies.load_once_paths.\
    reject!{|x| x =~ /^#{Regexp.escape(File.dirname(__FILE__))}/}
end

PinnacleTermExam.attach_overrides

Dispatcher.to_prepare  do
#  ConfigurationController.send(:include,PinnacleTermExam::TermwiseCategory)
  Batch.send :has_many, :terms
  Batch.send :has_many, :assign_templates
  Batch.send :has_many, :rank_ingres_subject_mergers
  ExamGroup.send :belongs_to , :term
  ExamGroup.send :has_many, :term_exam_groups
  ExamGroup.send :has_many, :term_exam_groups
  ExamGroup.send :has_many, :terms, :through => :term_exam_groups
#  @config_value =  ActiveRecord::Base.connection.select_value("select config_value from configurations where config_key = 'TermwiseExamManagement'")
#  if @config_value == "1"
    ExamController.send(:include,PinnacleTermExam::TermwiseExam)
    CourseExamGroupsController.send(:include, PinnacleTermExam::TermwiseExamNew)
    ExamGroupsController.send(:include,PinnacleTermExam::TermexamGroup)
    ExamGroupsController.send(:include,PinnacleTermExam::TermexamCreate)
    TasksController.send(:include,PinnacleTermExam::TaskNotification)
    AssignmentsController.send(:include,PinnacleTermExam::AssignmentNotification)
    StudentController.send(:include,PinnacleTermExam::CustomReport)    
    ExamGroup.send :has_many , :alpha_numeric_batch
    Batch.send :has_many, :alpha_numeric_batch
    Subject.send :has_many, :alpha_numeric_exam
    Batch.send :has_one, :explanatory_note
#  end
end
