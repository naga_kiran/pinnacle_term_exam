class AddColumnToEvaluationGroups < ActiveRecord::Migration
  def self.up
    add_column :evaluation_groups, :employee_id, :integer
  end

  def self.down
    remove_column :evaluation_groups, :employee_id
  end
end
