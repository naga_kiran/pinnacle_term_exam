class CreateRankIngresSubjectMergers < ActiveRecord::Migration
  def self.up
    create_table :rank_ingres_subject_mergers do |t|
    t.string :name
    t.text :subject_id
    t.references :batch
      t.timestamps
    end
  end

  def self.down
    drop_table :rank_ingres_subject_mergers
  end
end
