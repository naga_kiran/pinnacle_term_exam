class CreateRankIngresSmsModules < ActiveRecord::Migration
  def self.up
    create_table :rank_ingres_sms_modules do |t|
      t.integer :batch_id
      t.text :student_ids
      t.timestamps
    end
  end

  def self.down
    drop_table :rank_ingres_sms_modules
  end
end
