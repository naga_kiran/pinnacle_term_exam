class CreateTutorComments < ActiveRecord::Migration
  def self.up
    create_table :tutor_comments do |t|
      t.string :description
      t.references :batch
      t.references :user
      t.references :student
      t.timestamps
    end
  end

  def self.down
    drop_table :tutor_comments
  end
end
