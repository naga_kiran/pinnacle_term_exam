class CreateEletiveGroupComments < ActiveRecord::Migration
  def self.up
    create_table :eletive_group_comments do |t|
      t.string :name
      t.references :batch
      t.references :term
      t.references :elective_group
      t.references :student
      t.string  :comment
      t.integer    :school_id
      t.timestamps
    end
  end

  def self.down
    drop_table :eletive_group_comments
  end
end
