class CreateStudentTermDocuments < ActiveRecord::Migration
  def self.up
    create_table :student_term_documents do |t|
     t.integer :student_id
     t.integer :term_id
     t.integer :batch_id
      t.string :document_file_name
      t.string :document_content_type
      t.binary :document_data, :limit => 75.kilobytes
      t.integer :document_file_size

      t.timestamps
    end
  end

  def self.down
    drop_table :student_term_documents
  end
end
