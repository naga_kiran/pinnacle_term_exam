class CreateAlphaNumericScores < ActiveRecord::Migration
  def self.up
    create_table :alpha_numeric_scores do |t|
    	t.integer :batch_id
			t.integer :subject_id
			t.references :alpha_numeric_exam
      t.integer :student_id
      t.string :marks_grade
      t.boolean :is_absent, :default => false
      t.integer :school_id
      t.timestamps
    end
  end

  def self.down
    drop_table :alpha_numeric_scores
  end
end
