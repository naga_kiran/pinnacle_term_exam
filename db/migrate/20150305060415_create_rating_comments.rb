class CreateRatingComments < ActiveRecord::Migration
  def self.up
    create_table :rating_comments do |t|
      t.integer :student_id
      t.integer :user_id
      t.integer :exam_id
      t.text :comment_desc
      t.integer :effort
      t.integer :class_work
      t.integer :attitude
      t.integer :home_work
      t.timestamps
    end
  end

  def self.down
    drop_table :rating_comments
  end
end
