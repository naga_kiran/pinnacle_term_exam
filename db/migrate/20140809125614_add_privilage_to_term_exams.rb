class AddPrivilageToTermExams < ActiveRecord::Migration
  def self.up
    PrivilegeTag.find_or_create_by_name_tag :name_tag => "term_wise_management"
    Privilege.find_or_create_by_name :name => "TermManager", :description => 'term_manager', :privilege_tag_id => PrivilegeTag.find_by_name_tag('term_wise_management').id
  end

  def self.down
    privilege_tag = PrivilegeTag.find_by_name_tag("term_wise_management")
    privilege_tag.destroy unless privilege_tag.nil?
    privilege1 = Privilege.find_by_name("TermManager")
    privilege1.destroy unless privilege1.nil?
  end
end
