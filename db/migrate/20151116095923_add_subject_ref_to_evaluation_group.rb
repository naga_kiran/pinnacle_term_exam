class AddSubjectRefToEvaluationGroup < ActiveRecord::Migration
  def self.up
    add_column :evaluation_groups, :subject_id, :integer
    add_column :evaluation_groups, :score_format, :string
  end

  def self.down
    remove_column :evaluation_groups, :subject_id
    remove_column :evaluation_groups, :score_format
  end
end
