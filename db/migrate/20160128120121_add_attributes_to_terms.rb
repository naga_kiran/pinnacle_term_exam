class AddAttributesToTerms < ActiveRecord::Migration
  def self.up
  	add_column :terms, :assumption_date, :date
  end

  def self.down
  	add_column :terms, :assumption_date, :date
  end
end
