class AddSchoolIdToPinnacleTerm < ActiveRecord::Migration
  def self.up
    add_column :terms, :school_id, :integer
    add_column :term_exam_groups, :school_id, :integer
    add_column :report_builders, :school_id, :integer
    add_column :term_signatures, :school_id, :integer
    add_column :tutor_comments, :school_id, :integer
  end

  def self.down
    remove_column :terms, :school_id, :integer
    remove_column :term_exam_groups, :school_id, :integer
    remove_column :report_builders, :school_id, :integer
    remove_column :term_signatures, :school_id, :integer
    remove_column :tutor_comments, :school_id, :integer
  end
end
