class CreateSubjectExamScores < ActiveRecord::Migration
  def self.up
    create_table :subject_exam_scores do |t|
      t.references :student
      t.references :assigned_exam_group
      t.string     :value
      t.integer    :school_id
      t.timestamps
    end
  end

  def self.down
    drop_table :subject_exam_scores
  end
end
