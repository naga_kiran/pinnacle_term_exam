class CreateTermDocumentBatches < ActiveRecord::Migration
  def self.up
    create_table :term_document_batches do |t|
      t.integer :batch_id
      t.integer :term_id
      t.integer :term_document_id

      t.timestamps
    end
  end

  def self.down
    drop_table :term_document_batches
  end
end
