class AddColumnIsDirectorToTermSignature < ActiveRecord::Migration
  def self.up
    add_column :term_signatures, :is_director, :boolean, :default => false
  end

  def self.down
    remove_column :term_signatures, :is_director, :boolean
  end
end
