class CreateRtReportBuilders < ActiveRecord::Migration
  def self.up
    create_table :report_builders do |t|
      t.string :name
      t.text :description
      t.timestamps
    end
  end

  def self.down
    drop_table :report_builders
  end
end
