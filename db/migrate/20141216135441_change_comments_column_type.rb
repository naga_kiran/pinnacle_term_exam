class ChangeCommentsColumnType < ActiveRecord::Migration
  def self.up
    change_column :tutor_comments, :description, :text
    change_column :house_parent_comments, :description, :text
  end

end
