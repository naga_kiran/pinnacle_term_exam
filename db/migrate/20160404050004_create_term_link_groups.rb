class CreateTermLinkGroups < ActiveRecord::Migration
  def self.up
    create_table :term_link_groups do |t|
      t.string :link_name
      t.integer :link_term_id
      t.references :term

      t.timestamps
    end
  end

  def self.down
    drop_table :term_link_groups
  end
end
