class AddColumnPageOrientaionReport < ActiveRecord::Migration
  def self.up
    add_column :report_builders, :page_orientation, :string
  end

  def self.down
    remove_column :report_builders, :page_orientation, :string
  end
end
