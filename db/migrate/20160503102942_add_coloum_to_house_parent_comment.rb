class AddColoumToHouseParentComment < ActiveRecord::Migration
  def self.up
    add_column :house_parent_comments, :term_id, :integer
  end

  def self.down
     remove_column :house_parent_comments, :term_id
  end
end
