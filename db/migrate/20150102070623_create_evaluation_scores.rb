class CreateEvaluationScores < ActiveRecord::Migration
  def self.up
    create_table :evaluation_scores do |t|
      t.references :student
      t.references :evaluation_type
      t.references :evaluation_area
      t.string :performance
      t.integer :school_id

      t.timestamps
    end
  end

  def self.down
    drop_table :evaluation_scores
  end
end
