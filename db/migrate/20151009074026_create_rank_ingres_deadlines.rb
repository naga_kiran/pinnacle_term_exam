class CreateRankIngresDeadlines < ActiveRecord::Migration
  def self.up
    create_table :rank_ingres_deadlines do |t|
      t.integer :batch_id
      t.integer :subject_id
      t.integer :employee_id
      t.datetime :deadline_date
      t.integer :term_id
      t.timestamps
    end
  end

  def self.down
    drop_table :rank_ingres_deadlines
  end
end
