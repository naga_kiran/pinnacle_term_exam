class CreateEvaluationGroups < ActiveRecord::Migration
  def self.up
    create_table :evaluation_groups do |t|
      t.string :name
      t.string :code
      t.string :area
      t.references :batch
      t.references :term
      t.integer    :school_id
      t.timestamps
    end
  end

  def self.down
    drop_table :evaluation_groups
  end
end
