class AddColumnToArchievedStudent < ActiveRecord::Migration
  def self.up
  	add_column :archived_students, :upload_document_file_name, :string
	add_column :archived_students, :upload_document_content_type, :string
	add_column :archived_students, :upload_document_data, :binary, :limit => 500.kilobytes
	add_column :archived_students, :upload_document_file_size, :integer
  end

  def self.down
  	remove_column :archived_students, :upload_document_file_name
  	remove_column :archived_students, :upload_document_content_type
  	remove_column :archived_students, :upload_document_data
  	remove_column :archived_students, :upload_document_file_size
  end
end
