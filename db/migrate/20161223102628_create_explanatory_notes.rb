class CreateExplanatoryNotes < ActiveRecord::Migration
  def self.up
    create_table :explanatory_notes do |t|
      t.references :batch
      t.references :term
      t.text :explanatory_note1
      t.text :explanatory_note2
      t.text :explanatory_note3
      t.text :explanatory_note4
      t.text :explanatory_note5
      t.integer :school_id
      t.timestamps
    end
  end

  def self.down
    drop_table :explanatory_notes
  end
end
