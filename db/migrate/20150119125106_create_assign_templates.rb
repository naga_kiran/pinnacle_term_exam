class CreateAssignTemplates < ActiveRecord::Migration
  def self.up
    create_table :assign_templates do |t|
      t.references :batch
      t.references :report_builder
      t.references :term
      t.integer :school_id

      t.timestamps
    end
  end

  def self.down
    drop_table :assign_templates
  end
end
