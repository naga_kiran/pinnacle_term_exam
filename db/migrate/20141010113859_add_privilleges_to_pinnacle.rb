class AddPrivillegesToPinnacle < ActiveRecord::Migration
  def self.up
    PrivilegeTag.find_or_create_by_name_tag :name_tag => "pinnacle_term_exam"
    Privilege.find_or_create_by_name :name => "PinnacleTermManager", :description => 'pinnacle_term_manager', :privilege_tag_id => PrivilegeTag.find_by_name_tag('pinnacle_term_exam').id
  end

  def self.down
    privilege_tag = PrivilegeTag.find_by_name_tag("pinnacle_term_exam")
    privilege_tag.destroy unless privilege_tag.nil?
    privilege = Privilege.find_by_name("PinnacleTermManager")
    privilege.destroy unless privilege.nil?
  end
end
