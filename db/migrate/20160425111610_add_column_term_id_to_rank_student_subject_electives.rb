class AddColumnTermIdToRankStudentSubjectElectives < ActiveRecord::Migration
  def self.up
    add_column :rank_student_subject_electives, :term_id, :integer
  end

  def self.down
    add_column :rank_student_subject_electives, :term_id
  end
end
