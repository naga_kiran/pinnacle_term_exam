class ChangeRemarkFieldTypeInExamScores < ActiveRecord::Migration
 def self.up
    change_column :exam_scores, :remarks, :text
  end
end
