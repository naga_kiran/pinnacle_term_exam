class AddColumnsToEvaluationScores < ActiveRecord::Migration
  def self.up
  add_column :evaluation_scores, :exam_group_scores, :text
 
  end

  def self.down
  	remove_column :evaluation_scores, :exam_group_scores
  	
  end
end
