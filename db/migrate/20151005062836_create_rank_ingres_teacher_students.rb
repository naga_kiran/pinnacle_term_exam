class CreateRankIngresTeacherStudents < ActiveRecord::Migration
  def self.up
    create_table :rank_ingres_teacher_students do |t|
      t.integer :batch_id
      t.integer :subject_id
      t.integer :employee_id
      t.text :student_ids
      t.timestamps
    end
  end

  def self.down
    drop_table :rank_ingres_teacher_students
  end
end
