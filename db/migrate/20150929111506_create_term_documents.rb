class CreateTermDocuments < ActiveRecord::Migration
  def self.up
    create_table :term_documents do |t|
      t.string :name
      t.string :document_file_name
      t.string :document_content_type
      t.binary :document_data, :limit => 75.kilobytes
      t.integer :document_file_size
      t.timestamps
    end
  end

  def self.down
    drop_table :term_documents
  end
end
