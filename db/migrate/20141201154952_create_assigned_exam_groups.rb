class CreateAssignedExamGroups < ActiveRecord::Migration
  def self.up
    create_table :assigned_exam_groups do |t|
      t.references :subject
      t.references :subject_exam_group
      t.integer    :school_id
      t.timestamps
    end
  end

  def self.down
    drop_table :assigned_exam_groups
  end
end
