class CreateEvaluationComments < ActiveRecord::Migration
  def self.up
    create_table :evaluation_comments do |t|
      t.text :comment
      t.references :batch
      t.references :term
      t.references :evaluation_group
      t.references :user
      t.integer    :school_id
      t.timestamps
    end
  end

  def self.down
    drop_table :evaluation_comments
  end
end
