class CreateReportPickupDates < ActiveRecord::Migration
  def self.up
    create_table :report_pickup_dates do |t|
      t.integer :batch_id
      t.integer :course_id
      t.integer :term_id
      t.integer :template_id
      t.integer :student_id
      t.date :report_pickup
      t.boolean :student_select, :default => false
      t.integer :school_id
      t.timestamps
    end
  end

  def self.down
    drop_table :report_pickup_dates
  end
end
