class ChangeTermIdTypeToText < ActiveRecord::Migration
  def self.up
     change_column :assign_templates, :term_id, :text
  end
end
