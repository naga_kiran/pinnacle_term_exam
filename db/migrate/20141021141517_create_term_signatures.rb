class CreateTermSignatures < ActiveRecord::Migration
  def self.up
    create_table :term_signatures do |t|
      t.string     :name
      t.references :batch
      t.boolean    :is_principal
      t.column   :signature_attachment_file_name,       :string
      t.column   :signature_attachment_content_type,   :string
      t.column   :signature_attachment_data,           :binary,
        :limit => 50.kilobytes
      t.timestamps
    end
  end

  def self.down
    drop_table :term_signatures
  end
end
