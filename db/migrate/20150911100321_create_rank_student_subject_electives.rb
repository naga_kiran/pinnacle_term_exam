class CreateRankStudentSubjectElectives < ActiveRecord::Migration
  def self.up
    create_table :rank_student_subject_electives do |t|
      t.references :students_subject
      t.boolean    :is_deleted
      t.timestamps
    end
  end

  def self.down
    drop_table :rank_student_subject_electives
  end
end
