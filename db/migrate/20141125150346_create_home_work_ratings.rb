class CreateHomeWorkRatings < ActiveRecord::Migration
  def self.up
    create_table :home_work_ratings do |t|
      t.string :rating
      t.references :batch
      t.references :user
      t.references :student
      t.integer :school_id
      t.timestamps
    end
  end

  def self.down
    drop_table :home_work_ratings
  end
end
