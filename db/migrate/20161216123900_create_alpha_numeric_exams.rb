class CreateAlphaNumericExams < ActiveRecord::Migration
  def self.up
    create_table :alpha_numeric_exams do |t|
      t.references :alpha_numeric_batch
      t.references :subject
      t.datetime :start_time
      t.datetime :end_time
      t.string :max_grade_score
      t.string :min_grade_score
      t.boolean :no_exam, :default => false
      t.integer :school_id
      t.timestamps
    end
  end

  def self.down
    drop_table :alpha_numeric_exams
  end
end
