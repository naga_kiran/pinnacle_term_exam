class CreateTermExamGroups < ActiveRecord::Migration
  def self.up
    create_table :term_exam_groups do |t|
      t.references :term
      t.references :exam_group
      t.integer :weightage
      t.timestamps
    end
  end

  def self.down
    drop_table :term_exam_groups
  end
end
