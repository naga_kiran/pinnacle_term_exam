class CreateAlphaNumericBatches < ActiveRecord::Migration
  def self.up
    create_table :alpha_numeric_batches do |t|
      t.references :batch
      t.references :exam_group
      t.references :term
      t.string :exam_type
      t.boolean :is_published, :default => false
      t.boolean :result_published, :default => false
      t.timestamps
    end
  end

  def self.down
    drop_table :alpha_numeric_batches
  end
end
