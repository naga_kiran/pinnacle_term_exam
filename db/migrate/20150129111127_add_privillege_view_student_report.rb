class AddPrivillegeViewStudentReport < ActiveRecord::Migration
 def self.up
     Privilege.find_or_create_by_name :name => "ViewStudentReport", :description => 'view_student_report', :privilege_tag_id => PrivilegeTag.find_by_name_tag('term_wise_management').id
  end

  def self.down
    privilege1 = Privilege.find_by_name("ViewStudentReport")
    privilege1.destroy unless privilege1.nil?
  end
end
