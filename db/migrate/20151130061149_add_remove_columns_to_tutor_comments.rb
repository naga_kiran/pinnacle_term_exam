class AddRemoveColumnsToTutorComments < ActiveRecord::Migration
  def self.up
    add_column :tutor_comments, :term_id, :integer
    add_column :tutor_comments,:is_principal ,:boolean,:default => false
  end

  def self.down
    remove_column :tutor_comments, :term_id
    remove_column :tutor_comments, :is_principal
  end
end
