class CreateEvaluationTypes < ActiveRecord::Migration
  def self.up
    create_table :evaluation_types do |t|
      t.string :name
      t.string :code
      t.references :batch
      t.references :term
      t.references :evaluation_group
      t.integer    :school_id

      t.timestamps
    end
  end

  def self.down
    drop_table :evaluation_types
  end
end
