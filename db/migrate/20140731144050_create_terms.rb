class CreateTerms < ActiveRecord::Migration
  def self.up
    create_table :terms do |t|
      t.string     :name
      t.string     :code
      t.references :batch
      t.datetime   :start_date
      t.datetime   :end_date
      t.timestamps
    end
  end

  def self.down
    drop_table :terms
  end
end
