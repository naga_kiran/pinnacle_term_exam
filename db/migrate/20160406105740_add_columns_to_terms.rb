class AddColumnsToTerms < ActiveRecord::Migration
  def self.up
    add_column :terms, :ca_term_id, :integer
  end

  def self.down
    remove_column :terms, :ca_term_id
  end
end
