class CreateHouseParentComments < ActiveRecord::Migration
  def self.up
    create_table :house_parent_comments do |t|
      t.string :description
      t.references :batch
      t.references :user
      t.references :student
      t.integer    :school_id
      t.timestamps
    end
  end

  def self.down
    drop_table :house_parent_comments
  end
end
