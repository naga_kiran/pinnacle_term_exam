class AddExtraPhotoColumn < ActiveRecord::Migration
  def self.up
  	add_column :students, :upload_document_file_name, :string
	add_column :students, :upload_document_content_type, :string
	add_column :students, :upload_document_data, :binary, :limit => 500.kilobytes
	add_column :students, :upload_document_file_size, :integer
  end

  def self.down
  end
end
